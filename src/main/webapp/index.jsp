<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<dis:head title="Benvenuto"/>
<body id="page-top">
<div data-bs-parallax-bg="true"
     style="height: 400px;background-image: url('assets/img/iu.jpeg');background-position: center;background-size: cover;">
    <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0 shadow" href="<c:url value="/" />"
       style="font-size: 35px;background-color: #ffffff;position: fixed;width: 100%;z-index: 1;margin: 0 0 10px;padding-left: 23px;">
        <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-pills"></i></div>
        <div class="sidebar-brand-text mx-3"><span>Servizi Sanitari</span></div>
    </a>
</div>
<div></div>
<div class="features-clean">
    <div class="container-fluid">
        <div class="d-sm-flex justify-content-between align-items-center mb-4"></div>
        <div class="row">
            <div class="col">
                <p class="text-center">Per accedere al servizio sanitario è necessario essere registrati.&nbsp;</p>
                <div class="row">
                    <div class="col text-center" style="margin-bottom: 12px;">
                        <a class="btn btn-primary btn-icon-split" role="button" href="login"><span
                                class="text-white-50 icon"><i class="fas fa-unlock"></i></span><span
                                class="text-white text">Accedi
                            <c:if test="${sessionScope.user != null}"> come ${sessionScope.user.getName()}</c:if></span></a>
                        <c:if test="${sessionScope.user != null}">
                            <a class="btn btn-primary btn-icon-split" role="button" href="logout"
                               style="margin-left: 12px;"><span
                                    class="text-white-50 icon"><i class="fas fa-sign-out-alt"></i></span><span
                                    class="text-white text">Logout</span></a>
                        </c:if>
                        <a class="btn btn-primary btn-icon-split" role="button" style="margin-left: 12px;"
                           href="register"><span class="text-white-50 icon"><i class="fas fa-user-plus"></i></span><span
                                class="text-white text">Registrati</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="intro">
            <h2 class="text-center">Funzionalità</h2>
            <p class="text-center">Ecco una lista di motivi per cui ti conviene utilizzare il sito web!</p>
        </div>
        <div class="row features">
            <div class="col-sm-6 col-lg-4 item"><i class="fa fa-map-marker icon"></i>
                <h3 class="name">Funziona Dappertutto!</h3>
                <p class="description">Vuoi vedere i tuoi esami mentre sei sul 5/ ora puoi!</p>
            </div>
            <div class="col-sm-6 col-lg-4 item"><i class="fa fa-clock-o icon"></i>
                <h3 class="name">Sempre Disponibile!</h3>
                <p class="description">Se ti svegli alle 3 di notte perchè non ti ricordi se la tua biopsia era andata a
                    buon fine ora puoi saperlo!</p>
            </div>
            <div class="col-sm-6 col-lg-4 item"><i class="fa fa-file-code-o icon"></i>
                <h3 class="name">Fatto in JAVA</h3>
                <p class="description">Giuro che questa è una feature, non un bug.</p>
            </div>
            <div class="col-sm-6 col-lg-4 item"><i class="fa fa-leaf icon"></i>
                <h3 class="name">Nessuno spreco di carta!</h3>
                <p class="description">Così puoi sentirti con il cuore in pace anche mentre l'amazzonia brucia!</p>
            </div>
            <div class="col-sm-6 col-lg-4 item"><i class="fa fa-plane icon"></i>
                <h3 class="name">Veloce</h3>
                <p class="description">Non devi più aspettare che la commessa al banco si metta a cercare tra i tuoi
                    referti.</p>
            </div>
            <div class="col-sm-6 col-lg-4 item"><i class="fa fa-phone icon"></i>
                <h3 class="name">Mobile-first</h3>
                <p class="description">Perchè in fondo lo sappiamo che accedere dal PC è faticoso!&nbsp;</p>
            </div>
        </div>
    </div>
</div>
<div id="wrapper">
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content"></div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
<dis:scripts/>
</body>

</html>