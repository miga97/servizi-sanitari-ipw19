<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Report"/>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="report"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold">Report delle ricette erogate</p>
                    </div>
                    <div class="card-body" style="overflow: hidden;">
                        <table id="report-table" class="table dataTable my-0 dtr-inline" style="width:100%">
                            <thead>
                            <tr>
                                <th>Farmaco</th>
                                <th>Pagato il</th>
                                <th>Prescritto da</th>
                                <th>Paziente</th>
                                <th>Farmacia</th>
                                <th>Costo</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable/>
    <script>
        const reportTable = {
            ajax: {
                url: '<c:url value="/services/dataTable?dt=drug&completed=true&type=province&search=${requestScope.sspProvinceAcronym}"/>',
                dataSrc: 'results'
            }, columns: [
                {
                    data: 'drugPrescription.description',
                },
                {
                    data: 'drugPrescription.completionTimestamp',
                    render: renderDT.dateHour
                },
                {
                    data: 'planner',
                    render: renderDT.userWithPhoto('plannerProvince','plannerPhoto')
                },
                {
                    data: 'patient',
                    render: renderDT.userWithPhoto('patientProvince','patientPhoto')
                },
                {
                    data: 'performer',
                    render: renderDT.userWithPhoto('performerProvince','performerPhoto')
                },
                {
                    data: 'drugPrescription.cost',
                    render: $.fn.dataTable.render.number( '.', ',', 2, '', '€')
                },
            ]
        };
    </script>
    <dis:make-datatable elementId="report-table" params="reportTable" printReports="true"/>
</dis:scripts>
</body>
</html>