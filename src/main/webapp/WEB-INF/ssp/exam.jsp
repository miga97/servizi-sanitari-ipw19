<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="it_IT"/>
<!DOCTYPE html>
<html>
<dis:head title="Scheda Paziente"/>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="exam"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold">Esame</p>
                    </div>
                    <div class="card-body" style="overflow: hidden;">
                        <h5 class="card-title">
                            ${requestScope.examPrescriptionName} -
                            prescritta il <fmt:formatDate value="${sessionScope.examPrescription.prescriptionTimestamp}"
                                                          pattern="dd MMM yyyy HH:mm"/>
                        </h5>
                        <form method="post" action="">
                            <div class="form-group text-capitalize text-left">
                                <div class="form-label">
                                    <span>Risultati:</span></div>
                                <div class="form-row"><label>
                                        <textarea class="form-control" name="results" rows="5" cols="33" required></textarea>
                                </label></div>
                            </div>
                            <div class="form-group d-lg-flex">
                                <button class="btn btn-primary btn-icon-split m-1" type="submit" name="send" formnovalidate>
                                    <span class="text-white-50 icon"><i class="fas fa-check"></i></span>
                                    <span class="text-white text">Termina esame</span>
                                </button>
                                <button class="btn btn-danger btn-icon-split m-1" type="submit" name="cancel" formnovalidate>
                                    <span class="text-white-50 icon"><i class="fas fa-times"></i></span>
                                    <span class="text-white text">Annulla</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:selectize/>
</dis:scripts>
</body>
</html>