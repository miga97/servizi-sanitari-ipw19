<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Richiamo"/>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="recall"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow mb-3">
                    <div class="card-header">
                        <p class="text-primary mb-2 font-weight-bold">Richiamo</p>
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">
                                <i class="fas fa-file-medical-alt rounded mr-2"></i>Esami</a></li>
                            <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">
                                <i class="fas fa-stethoscope rounded mr-2"></i>Visite</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active show" role="tabpanel" id="tab-1">
                                <form method="post" action="recall">
                                    <div class="form-group text-capitalize text-left">
                                        <div class="form-row">
                                            <select name="exam" class="selectize" required>
                                                <option value="" selected hidden disabled>Seleziona esame</option>
                                                <c:forEach var="exam" items="${applicationScope.exams}">
                                                    <option value="${exam.id}">${exam.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-form-label offset-md-0 col-auto d-md-flex align-items-md-center">
                                                <span>Fascia d'età:</span>
                                            </div>
                                            <div class="col offset-md-0 col-auto richiamodx">
                                                <label style="float: left;display: flex;">
                                                    <input style="width: 5em;" class="form-control" type="number"
                                                           placeholder="min" name="minAge" required>
                                                    <input style="width: 5em;" class="form-control" type="number"
                                                           placeholder="max" name="maxAge" required>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group d-lg-flex">
                                        <button class="btn btn-primary btn-icon-split" type="submit">
                                            <span class="text-white-50 icon"><i class="fas fa-calendar-check"></i></span>
                                            <span class="text-white text">Effettua richiamo</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade in" role="tabpanel" id="tab-2">
                                <form method="post" action="recall">
                                    <div class="form-group text-capitalize text-left">
                                        <div class="form-row">
                                            <select name="visit" class="selectize" required>
                                                <option value="" selected hidden disabled>Seleziona visita</option>
                                                <c:forEach var="visit" items="${applicationScope.visits}">
                                                    <option value="${visit.id}">${visit.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col col-form-label offset-md-0 col-auto d-md-flex align-items-md-center">
                                                <span>Fascia d'età:</span>
                                            </div>
                                            <div class="col offset-md-0 col-auto richiamodx">
                                                <label style="float: left;display: flex;">
                                                    <input style="width: 5em;" class="form-control" type="number"
                                                           placeholder="min" name="minAge" required>
                                                    <input style="width: 5em;" class="form-control" type="number"
                                                           placeholder="max" name="maxAge" required>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group d-lg-flex">
                                        <button class="btn btn-primary btn-icon-split" type="submit">
                                            <span class="text-white-50 icon"><i class="fas fa-calendar-check"></i></span>
                                            <span class="text-white text">Effettua richiamo</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:selectize/>
</dis:scripts>
</body>
</html>