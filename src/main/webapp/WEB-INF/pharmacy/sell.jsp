<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Home">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/DataTables/datatables.min.css"/>"/>
</dis:head>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="sell"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                <p class="text-primary m-0 font-weight-bold">Ricette erogabili selezionate</p>
                            </div>
                            <div class="col-xs">
                                <h5>Costo Totale: ${requestScope.cost} €</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="overflow: hidden;">
                        <div class="row pb-2">
                            <div class="col"></div>
                            <div class="col-xs">
                                <form method="post" action="" class="suppress-enter">
                                    <div class="row flex-lg-row">
                                        <div class="col">
                                            <button class="btn m-1 btn-danger btn-icon-split" type="submit"
                                                    name="delete"
                                                    role="button"
                                                    style="height: 35px;min-width: max-content;">
                                                <span class="text-white-50 icon"><i class="fas fa-undo"></i></span>
                                                <span class="text-white text">Annulla</span>
                                            </button>
                                        </div>
                                        <div class="col">
                                            <button class="btn m-1 btn-success btn-icon-split" type="submit"
                                                    name="forward"
                                                    role="button"
                                                    style="height: 35px;min-width: max-content;">
                                        <span class="text-white-50 icon"><i
                                                class="fas fa-dollar-sign"></i></span>
                                                <span class="text-white text">Eroga Pagamento</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <table id="drug-table" class="table dataTable my-0 dtr-inline" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Codice Fiscale</th>
                                <th>Ricetta</th>
                                <th>Prescritto il</th>
                                <th>Prezzo</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable/>
<script>
    const drugTable = {
        ajax: {
            url: '<c:url value="/services/dataTable?dt=drug&type=filtered&search=1&completed=false"/>',
            dataSrc: 'results'
        },
        columns: [
            {
                data: 'patient',
                render: renderDT.userWithPhoto('patientProvince', 'patientPhoto')
            },
            {data: 'patient.cf'},
            {data: 'drugPrescription.description'},
            {
                data: 'drugPrescription.prescriptionTimestamp',
                render: renderDT.dateHour
            },
            {
                data: 'drugPrescription.cost',
                render: $.fn.dataTable.render.number('.', ',', 2, '', '€')
            },
            {
                data: 'drugPrescription',
                render: renderDT.sellButton
            }
        ]
    };
</script>
    <dis:make-datatable elementId="drug-table" params="drugTable"/>
</dis:scripts>
</html>
