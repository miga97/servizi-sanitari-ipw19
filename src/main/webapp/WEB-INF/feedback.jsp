<%--
  Created by IntelliJ IDEA.
  User: matte
  Date: 08/09/2019
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<dis:head title="Info"/>
<body class="bg-gradient-primary">
<a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="<c:url value="/" />" style="color: rgb(255,255,255);font-size: 35px;margin-top: 20px;padding-left: 25px;">
    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-pills"></i></div>
    <div class="sidebar-brand-text mx-3"><span>Servizi Sanitari</span></div>
</a>
<div class="container">
    <div class="row justify-content-center" style="margin-top: -20px;">
        <div class="col-md-9 col-lg-12 col-xl-10">
            <div class="card shadow-lg o-hidden border-0 my-5">
                <div class="card-body p-0">
                    <div class="p-5">
                        <div class="text-center"><a class="btn ${requestScope.feedback.type} btn-circle ml-1"
                                                    role="button"
                                                    style="padding: 38px;margin: 0;"><i
                                class="fas fa-jedi text-white" style="font-size: 57px;opacity: 1;"></i></a>
                        </div>
                        <div class="text-center">
                            <h4 class="text-dark mb-4" style="padding-top: 10px;">${requestScope.feedback.title}</h4>
                            <p class="text-justify" style="padding: -4px;">${requestScope.feedback.message}</p>
                        </div>
                        <div class="text-center"><a href="<c:url value="${requestScope.feedback.link}"/>"
                                                    class="btn btn-primary btn-icon-split" role="button"><span
                                class="text-white-50 icon"><i class="fas fa-arrow-right"></i></span><span
                                class="text-white text">Continua</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<dis:scripts/>
</body>

</html>
