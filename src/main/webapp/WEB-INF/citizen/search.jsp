<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Ricerca"/>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="search"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow mb-3">
                    <div class="card-header">
                        <p class="text-primary mb-2 font-weight-bold">Ricerca prestazioni</p>
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">
                                <i class="fas fa-file-medical-alt rounded mr-2"></i>Esami</a></li>
                            <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">
                                <i class="fas fa-stethoscope rounded mr-2"></i>Visite</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active show" role="tabpanel" id="tab-1">
                                <table style="width:100%" class="cell-border table dataTable my-0 dtr-inline">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="exam" items="${applicationScope.exams}">
                                        <tr>
                                            <td>${exam.name}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade in" role="tabpanel" id="tab-2">
                                <table style="width:100%" class="cell-border table dataTable my-0 dtr-inline">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="visit" items="${applicationScope.visits}">
                                        <tr>
                                            <td>${visit.name}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable/>
    <dis:make-datatable/>
</dis:scripts>
</body>
</html>