<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>

<!DOCTYPE html>
<html>
<dis:head title="Ticket"/>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="ticket"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card shadow my-3">
                            <div class="card-header pt-3">
                                <p class="text-primary mb-2 font-weight-bold"><i
                                        class="fas fa-receipt rounded mr-2"></i>Ticket</p>
                            </div>
                            <div class="p-3 m-3">
                                <table id="tickets-table" style="width:100%"
                                       class="cell-border table my-0 dtr-inline">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Prescritto il</th>
                                        <th>Pagato il</th>
                                        <th>Erogato da</th>
                                        <th>Costo</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card shadow my-3">
                            <div class="card-header pt-3">
                                <p class="text-primary mb-2 font-weight-bold"><i
                                        class="fas fa-chart-pie rounded mr-2"></i>Statistiche</p>
                            </div>
                            <div class="p-3 m-3">
                                <canvas id="tickets-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable/>
    <script>
        const ticketsTable = {
            ajax: {
                url: '<c:url value="/services/dataTable?dt=ticket&type=citizenId&search=${sessionScope.user.id}"/>',
                dataSrc: 'results'
            }, columns: [
                {'data': 'ticket.description'},
                {
                    data: 'ticket.prescriptionTimestamp',
                    render: renderDT.dateHour
                },
                {
                    data: 'ticket.completionTimestamp',
                    render: renderDT.dateHour
                },
                {
                    data: 'performer',
                    render: renderDT.userWithPhoto('performerProvince','performerPhoto'),
                },
                {
                    data: 'ticket.cost',
                    render: $.fn.dataTable.render.number( '.', ',', 2, '', '€')
                },
            ]
        };

        const ticketsChart = {
            type: 'doughnut',
            data: {
                labels: ['Visite', 'Esami', 'Ricette'],
                datasets: [{
                    label: 'Soldi spesi',
                    data: [${requestScope.visitsExpense}, ${requestScope.examsExpense}, ${requestScope.drugsExpense}],
                    backgroundColor: [
                        'rgba(82, 118, 224, 1)',
                        'rgba(44, 202, 143, 1)',
                        'rgba(82, 192, 208, 1)'
                    ],
                    borderWidth: 1
                }]
            },
        };
    </script>
    <dis:make-datatable elementId="tickets-table" printReports="true" params="ticketsTable"/>
    <dis:chart elementId="tickets-chart" params="ticketsChart"/>
</dis:scripts>
</body>
</html>