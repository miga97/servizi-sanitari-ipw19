<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>

<!DOCTYPE html>
<html>
<dis:head title="Cartella Clinica"/>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="medicalRecords"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header pt-3">
                        <p class="text-primary mb-2 font-weight-bold">Cartella Clinica</p>
                    </div>
                    <div class="p-3 m-3">
                        <dis:medical-records patient="${sessionScope.user}" patientPhoto="${sessionScope.userPhoto}"/>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable medicalRecords="true" patientId="${sessionScope.user.id}"/>
    <dis:make-datatable elementId="visit-datatable" params="visitTable"/>
    <dis:make-datatable elementId="exam-datatable" params="examTable"/>
    <dis:make-datatable elementId="drug-datatable" params="drugTable"/>
    <dis:make-datatable elementId="photo-datatable" params="photoTable"/>
</dis:scripts>
</body>
</html>