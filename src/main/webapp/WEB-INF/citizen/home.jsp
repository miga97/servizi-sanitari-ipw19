<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Home"/>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="home"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header pt-3">
                        <p class="text-primary mb-2 font-weight-bold">Prescrizioni inevase</p>
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab"
                                                    href="#tab-1">
                                <i class="fas fa-stethoscope rounded mr-2"></i>Visite</a></li>
                            <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab"
                                                    href="#tab-2">
                                <i class="fas fa-file-medical-alt rounded mr-2"></i>Esami</a></li>
                            <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab"
                                                    href="#tab-3">
                                <i class="fas fa-prescription-bottle rounded mr-2"></i>Ricette</a></li>
                        </ul>
                    </div>
                    <div class="p-3">
                        <div class="tab-content m-3">
                            <div class="tab-pane show fade in active" role="tabpanel" id="tab-1">
                                <table id="pending-visit" style="width:100%" class="cell-border table my-0 dtr-inline">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Prescritta il</th>
                                        <th>Prescritta da</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="tab-2">
                                <table id="pending-exam" style="width:100%" class="cell-border table my-0 dtr-inline">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Prescritto il</th>
                                        <th>Prescritto da</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="tab-3">
                                <table id="pending-drug" style="width:100%" class="cell-border table my-0 dtr-inline">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Prescritta il</th>
                                        <th>Prescritta da</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable/>
    <script>
        const visitTablePending = {
            order: [[1, "desc"]],
            ajax: {
                url: '<c:url value="/services/dataTable?dt=visit&type=citizenid&completed=false&search=${sessionScope.user.id}"/>',
                dataSrc: 'results'
            }, columns: [
                {data: 'visitName'},
                {
                    data: 'visitPrescription.prescriptionTimestamp',
                    render: renderDT.dateHour
                },
                {
                    data: 'planner',
                    render: renderDT.userWithPhoto('plannerProvince', 'plannerPhoto')
                }
            ]
        };
        const examTablePending = {
            order: [[1, "desc"]],
            ajax: {
                url: '<c:url value="/services/dataTable?dt=exam&type=citizenid&completed=false&search=${sessionScope.user.id}"/>',
                dataSrc: 'results'
            }, columns: [
                {'data': 'examName'},
                {
                    data: 'examPrescription.prescriptionTimestamp',
                    render: renderDT.dateHour
                },
                {
                    data: 'planner',
                    render: renderDT.userWithPhoto('plannerProvince', 'plannerPhoto')
                }
            ]
        };
        const drugTablePending = {
            order: [[1, "desc"]],
            ajax: {
                url: '<c:url value="/services/dataTable?dt=drug&type=citizenid&completed=false&search=${sessionScope.user.id}"/>',
                dataSrc: 'results'
            }, columns: [
                {data: 'drugPrescription.description'},
                {
                    data: 'drugPrescription.prescriptionTimestamp',
                    render: renderDT.dateHour
                },
                {
                    data: 'planner',
                    render: renderDT.userWithPhoto('plannerProvince', 'plannerPhoto')
                },
                {
                    data: 'drugPrescription',
                    render: renderDT.drugQrButton
                }
            ]
        };
    </script>
    <dis:make-datatable elementId="pending-visit" params="visitTablePending"/>
    <dis:make-datatable elementId="pending-exam" params="examTablePending"/>
    <dis:make-datatable elementId="pending-drug" params="drugTablePending"/>
</dis:scripts>
</body>
</html>