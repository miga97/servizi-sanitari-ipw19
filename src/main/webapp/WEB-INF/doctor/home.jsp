<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="it_IT"/>
<!DOCTYPE html>
<html>
<dis:head title="Home">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/DataTables/datatables.min.css"/>"/>
</dis:head>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="home"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold">
                            <i class="fas fa-hand-holding-heart mr-2"></i>I miei Pazienti</p>
                    </div>
                    <div class="card-body" style="overflow: hidden;">
                        <table id="patients-table" class="table dataTable my-0 dtr-inline" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Codice Fiscale</th>
                                <th>Data di Nascita</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card shadow my-3">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold"><i
                                class="fas fa-history rounded mr-2"></i>Attivitá recenti</p>
                    </div>
                    <div class="card-body" style="overflow: hidden;">
                        <table class="table my-0 dtr-inline" style="width:100%">
                            <thead>
                            <tr>
                                <th>Paziente</th>
                                <th>Prescrizione</th>
                                <th>Prescritta il</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <c:choose>
                                    <c:when test="${requestScope.visitPrescriptionView != null}">
                                        <td>
                                            <div class="d-flex">
                                                <div class="rounded-circle align-self-center mr-2" style="background-image:url('<c:url value="${requestScope.visitPrescriptionView.patientPhoto.path}"/>');background-size:cover;background-position:center;height:34px;width:34px;flex:none;">
                                                </div><span class="align-self-center mr-3">${requestScope.visitPrescriptionView.patient.name} ${requestScope.visitPrescriptionView.patient.surname}</span>
                                            </div>
                                        </td>
                                        <td><i class="fas fa-stethoscope mr-2"></i>${requestScope.visitPrescriptionView.visitName}</td>
                                        <td><fmt:formatDate value="${requestScope.visitPrescriptionView.visitPrescription.prescriptionTimestamp}"
                                                            pattern="dd MMM yyyy HH:mm"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td colspan="3" class="text-center">
                                            <i class="fas fa-stethoscope mr-2"></i>Nessuna visita prescritta</td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                            <tr>
                                <c:choose>
                                    <c:when test="${requestScope.examPrescriptionView != null}">
                                        <td>
                                            <div class="d-flex">
                                                <div class="rounded-circle align-self-center mr-2" style="background-image:url('<c:url value="${requestScope.examPrescriptionView.patientPhoto.path}"/>');background-size:cover;background-position:center;height:34px;width:34px;flex:none;">
                                                </div><span class="align-self-center mr-3">${requestScope.examPrescriptionView.patient.name} ${requestScope.examPrescriptionView.patient.surname}</span>
                                            </div>
                                        </td>
                                        <td><i class="fas fa-file-medical-alt mr-2"></i>${requestScope.examPrescriptionView.examName}</td>
                                        <td><fmt:formatDate value="${requestScope.examPrescriptionView.examPrescription.prescriptionTimestamp}"
                                                            pattern="dd MMM yyyy HH:mm"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td colspan="3" class="text-center">
                                            <i class="fas fa-file-medical-alt mr-2"></i>Nessun esame prescritto</td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                            <tr>
                            <c:choose>
                                <c:when test="${requestScope.drugPrescriptionView != null}">
                                    <td>
                                        <div class="d-flex">
                                            <div class="rounded-circle align-self-center mr-2" style="background-image:url('<c:url value="${requestScope.drugPrescriptionView.patientPhoto.path}"/>');background-size:cover;background-position:center;height:34px;width:34px;flex:none;">
                                            </div><span class="align-self-center mr-3">${requestScope.drugPrescriptionView.patient.name} ${requestScope.drugPrescriptionView.patient.surname}</span>
                                        </div>
                                    </td>
                                    <td><i class="fas fa-prescription-bottle mr-2"></i>${requestScope.drugPrescriptionView.drugPrescription.description}</td>
                                    <td><fmt:formatDate value="${requestScope.drugPrescriptionView.drugPrescription.prescriptionTimestamp}"
                                                        pattern="dd MMM yyyy HH:mm"/></td>
                                </c:when>
                                <c:otherwise>
                                    <td colspan="3" class="text-center">
                                        <i class="fas fa-prescription-bottle mr-2"></i>Nessuna ricetta prescritta</td>
                                </c:otherwise>
                            </c:choose>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable/>
    <script>
        const patientsTable = {
            ajax: {
                url: '<c:url value="/services/dataTable?dt=user&search=${sessionScope.user.id}"/>',
                dataSrc: 'results'
            }, columns: [
                {
                    data: 'patient',
                    render: renderDT.userWithPhoto('patientProvince', 'patientPhoto')
                },
                {
                    data: 'patient.cf',
                },
                {
                    data: 'patient.birthDate',
                    render: renderDT.date
                },
                {
                    data: 'patient',
                    render: renderDT.doctorButton
                },
            ]
        };
    </script>
    <dis:make-datatable elementId="patients-table" params="patientsTable"/>
</dis:scripts>
</body>
</html>