<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<%--
  ~ Copyright (c) Alberto Xanea GM 2019.
  --%>

<!DOCTYPE html>
<html>
<dis:head title="Scheda Paziente">
    <link rel="stylesheet" href="<c:url value="/assets/css/bootstrap-tagsinput.css"/>">
</dis:head>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="visit"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header pt-3">
                        <p class="text-primary mb-2 font-weight-bold">Scheda Paziente</p>
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab"
                                                    href="#tab-1">
                                <i class="fas fa-id-card rounded mr-2"></i>Cartella Clinica</a></li>
                            <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab"
                                                    href="#tab-2">
                                <i class="fas fa-stethoscope rounded mr-2"></i>Visita di base</a></li>
                        </ul>
                    </div>
                    <div class="p-3">
                        <div class="tab-content m-3">
                            <div class="tab-pane show fade in active" role="tabpanel" id="tab-1">
                                <dis:medical-records patient="${sessionScope.patient}" patientPhoto="${requestScope.patientPhoto}"/>
                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="tab-2">
                                <form method="post" action="" class="suppress-enter">
                                    <div class="form-group text-capitalize text-left">
                                        <div class="form-label">
                                            <span>Anamnesi:</span></div>
                                        <div class="form-row">
                                            <label>
                                                <textarea class="form-control" name="anamnesi" rows="5"
                                                          cols="33"></textarea>
                                            </label></div>
                                    </div>
                                    <div class="form-group text-capitalize text-left">
                                        <div class="form-label">
                                            <span>Esami prescritti:</span></div>
                                        <div class="form-row">
                                            <select name="exams[]" class="selectize" multiple="multiple">
                                                <c:forEach var="exam" items="${applicationScope.exams}">
                                                    <option value="${exam.id}">${exam.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group text-capitalize text-left">
                                        <div class="form-label">
                                            <span>Visite Specialistiche:</span></div>
                                        <div class="form-row">
                                            <select name="visits[]" class="selectize"
                                                    multiple="multiple">
                                                <c:forEach var="visit" items="${applicationScope.visits}">
                                                    <option value="${visit.id}">${visit.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group text-capitalize text-left">
                                        <div class="form-label">
                                            <span>Farmaci prescritti:</span></div>
                                        <div class="form-row">
                                            <select multiple data-role="tagsinput" class="form-control" name="drugs">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group d-lg-flex">
                                        <button class="btn btn-primary btn-icon-split m-1" type="submit" name="send" formonvalidate>
                                            <span class="text-white-50 icon"><i class="fas fa-check"></i></span>
                                            <span class="text-white text">Effettua Visita</span>
                                        </button>
                                        <button class="btn btn-danger btn-icon-split m-1" type="submit" name="cancel" formnovalidate>
                                            <span class="text-white-50 icon"><i class="fas fa-times"></i></span>
                                            <span class="text-white text">Annulla</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:selectize/>
    <dis:datatable medicalRecords="true" patientId="${sessionScope.patient.id}"/>
    <dis:make-datatable elementId="visit-datatable" params="visitTable"/>
    <dis:make-datatable elementId="exam-datatable" params="examTable"/>
    <dis:make-datatable elementId="drug-datatable" params="drugTable"/>
    <dis:make-datatable elementId="photo-datatable" params="photoTable"/>
    <script src="<c:url value="/assets/js/bootstrap-tagsinput.min.js"/>"></script>
</dis:scripts>
</body>
</html>