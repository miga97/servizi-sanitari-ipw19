<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Password reset"/>
<body class="bg-gradient-primary">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-password-image" style="background-image: url('<c:url value="/assets/img/iu.jpeg"/>');"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-2">Scegli una nuova Password!</h4>
                                        <p class="mb-4">Ci sei quasi... ora non perdere tempo, scegli una nuova password e il problema sarà risolto!</p>
                                        <form class="user" method="post" action="resetPassword">
                                            <div class="form-group"><input class="form-control form-control-user first-password" type="password" id="resetPasswordInput" placeholder="Password" name="password" autocomplete="new-password" required=""></div>
                                            <div class="form-group"><input class="form-control form-control-user repeat-password" type="password" id="resetPasswordInput2" placeholder="Ripeti Password" name="password2" autocomplete="new-password" required=""></div>
                                            <input class="form-control" type="hidden" id="reset-password-token" name="token" value="${requestScope.token}"><button class="btn btn-primary btn-block text-white btn-user" type="submit">Cambia Password</button></form>
                                    </div>
                                    <div class="text-center">
                                        <hr><a class="small" href="<c:url value="register"/>">Registra un nuovo account!</a></div>
                                    <div class="text-center"><a class="small" href="<c:url value="login"/>">Hai già un account? Effettua il login!<br></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <dis:scripts/>
</body>

</html>