<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Home">
    <link rel="stylesheet" type="text/css" href="<c:url value="/assets/DataTables/datatables.min.css"/>"/>
</dis:head>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="home"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="card shadow my-3">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold">Visite erogabili</p>
                    </div>
                    <div class="card-body" style="overflow: hidden;">
                        <table id="visit-table" class="table dataTable my-0 dtr-inline" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Codice Fiscale</th>
                                <th>Visita prescritta</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Nome</th>
                                <th>Codice Fiscale</th>
                                <th>Visita prescritta</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<dis:scripts>
    <dis:datatable/>
    <script>
        const visitTable = {
            ajax: {
                url: '<c:url value="/services/dataTable?dt=visit&type=filtered&search=0&completed=false"/>',
                dataSrc: 'results'
            }, columns: [
                {
                    data: 'patient',
                    render: renderDT.userWithPhoto('patientProvince', 'patientPhoto'),
                },
                {data: 'patient.cf'},
                {data: 'visitName'},
                {
                    data: 'visitPrescription',
                    render: renderDT.specialistButton
                }
            ]
        };
    </script>
    <dis:make-datatable elementId="visit-table" params="visitTable"/>
</dis:scripts>
</body>
</html>