<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Profilo">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/css/fileinput.min.css"
          media="all" type="text/css"/>
</dis:head>
<body id="page-top">
<div id="wrapper">
    <dis:sidebar selected="profile"/>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <dis:top-nav/>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card mb-3">
                            <div class="card-body text-center shadow">
                                <div class="rounded-circle mb-3" style="display:inline-flex;width:120pt;height:120pt;background-image:url('<c:url value="${sessionScope.userPhoto.path}"/>');background-size:cover;background-position:center;"></div>
                                <div class="mb-3">
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal"
                                            data-target="#pictureModal">Aggiungi una nuova foto
                                    </button>
                                </div>
                            </div>
                        </div>
                        <c:if test="${sessionScope.user.isCitizen()}">
                            <div class="card mb-3">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 font-weight-bold"><i
                                            class="fas fa-image rounded mr-2"></i>Le tue pics</p>
                                </div>
                                <div class="card-body text-center shadow">
                                    <table id="photo-datatable" style="width:100%" class="cell-border my-0 table dataTable dtr-inline">
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card shadow mb-3" id="doctorChoice">
                                <div class="card-header py-3">
                                    <p class="text-primary m-0 font-weight-bold"><i
                                            class="fas fa-balance-scale rounded mr-2"></i>Libera scelta</p>
                                </div>
                                <div class="card-body">
                                    <form id="change-medic" method="post">
                                        <input type="hidden" hidden name="type" value="changeDoctor">
                                        <div class="form-group"><label for="newDoctor"><strong>Medico di
                                            Base</strong></label>
                                            <select id="newDoctor" required class="form-control" type="select"
                                                    name="newDoctor">
                                                <c:if test="${sessionScope.user.doctorId == null}">
                                                    <option value="" selected hidden disabled>Scegli il tuo medico!
                                                    </option>
                                                </c:if>
                                                <c:forEach var="doctor" items="${requestScope.doctors}">
                                                    <c:if test="${doctor.id != sessionScope.user.id}">
                                                        <option value="${doctor.id}" ${doctor.id == sessionScope.user.doctorId ? 'selected' : ''}>${doctor.name} ${doctor.surname}</option>
                                                    </c:if>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="form-group d-md-flex justify-content-md-end">
                                            <input value="Annulla" class="btn btn-secondary btn-sm mr-1" type="button"
                                                   onclick="location.reload()">
                                            <button class="btn btn-primary btn-sm" type="submit">
                                                Save Settings
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </c:if>
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col">
                                <div class="card shadow mb-3">
                                    <div class="card-header py-3">
                                        <p class="text-primary m-0 font-weight-bold"><i
                                                class="fas fa-user rounded mr-2"></i>Dettagli Personali</p>
                                    </div>
                                    <div class="card-body">
                                        <form id="user-details" method="post">
                                            <input type="hidden" name="type" value="changeInfo">
                                            <dis:generalities-form user="${sessionScope.user}"/>
                                            <div class="form-row">
                                                <div class="col d-sm-flex d-lg-flex justify-content-sm-end align-items-sm-end justify-content-lg-end align-items-lg-end">
                                                    <div class="form-group">
                                                        <input value="Annulla" class="btn btn-secondary btn-sm"
                                                               type="button"
                                                               onclick="location.reload()">
                                                        <button class="btn btn-primary btn-sm" type="submit">Save
                                                            Settings
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="card shadow mb-3">
                                    <div class="card-header py-3" id="security">
                                        <p class="text-primary m-0 font-weight-bold"><i
                                                class="fas fa-lock rounded mr-2"></i>Sicurezza</p>
                                    </div>
                                    <div class="card-body">
                                        <form method="post">
                                            <input type="hidden" hidden name="type" value="changePassword">
                                            <input type="hidden" hidden name="email" value="${sessionScope.user.email}">
                                            <div class="form-group"><label for="old-password">
                                                <strong>Password attuale</strong></label>
                                                <input id="old-password" class="form-control" type="password"
                                                       placeholder="Password"
                                                       name="oldPassword"
                                                       autocomplete="current-password"
                                                       required=""></div>
                                            <div class="form-group"><label for="passwordInput">
                                                <strong>Nuova password</strong></label>
                                                <input id="passwordInput"
                                                       class="form-control first-password"
                                                       type="password"
                                                       placeholder="Password"
                                                       name="password"
                                                       autocomplete="new-password"
                                                       required=""></div>
                                            <div class="form-group"><label for="passwordInput2">
                                                <strong>Ripeti nuova password</strong></label>
                                                <input id="passwordInput2"
                                                       class="form-control repeat-password"
                                                       type="password"
                                                       placeholder="Password"
                                                       name="new_password_confirm"
                                                       autocomplete="new-password"
                                                       required=""></div>
                                            <div class="form-group d-md-flex justify-content-md-end">
                                                <button class="btn btn-primary btn-sm" type="submit">
                                                    Cambia Password
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="pictureModal" tabindex="-1" role="dialog" aria-labelledby="pictureModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="pictureModalLabel">Carica una nuova foto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form enctype="multipart/form-data" method="post">
                            <input type="hidden" name="type" value="changePhoto"/>
                            <div class="file-loading">
                                <input id="file" class="file" name="file" type="file" data-theme="fas">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
<dis:scripts>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/piexif.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/purify.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/themes/fas/theme.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/locales/it.js"></script>
    <script type="text/javascript">
        let editingDetails = true;
        const editDetails = () => {
            editingDetails = !editingDetails;
            $('#user-details :input:not(:button)').each(function () {
                $(this).prop('disabled', !editingDetails)
            });
            let buttons = $('#user-details :button');
            buttons[0].hidden = !editingDetails;
            buttons[1].textContent = editingDetails ? 'Salva Modifiche' : 'Modifica';
            return !editingDetails;
        };
        editDetails();
        <c:if test="${sessionScope.user.role < 3}">
        let editingMedic = true;
        const editMedic = () => {
            editingMedic = !editingMedic;
            $('#change-medic :input:not(:button)').each(function () {
                $(this).prop('disabled', !editingMedic)
            });
            let buttons = $('#change-medic :button');
            buttons[0].hidden = !editingMedic;
            buttons[1].textContent = editingMedic ? 'Salva Modifiche' : 'Modifica';
            return !editingMedic;
        };
        editMedic();
        </c:if>
        $(document).ready(function () {
            $('#user-details').on('submit', function (e) {
                e.preventDefault();
                if (editDetails()) {
                    editDetails();
                    this.submit();
                }
            });
            <c:if test="${sessionScope.user.role < 3}">
            $('#change-medic').on('submit', function (e) {
                e.preventDefault();
                if (editMedic()) {
                    editMedic();
                    this.submit();
                }
            });
            </c:if>
        });
    </script>
    <dis:datatable/>
    <script>
        const photoTable = {
            order: [[ 1, "desc" ]],
            pageLength: 4,
            dom: "tp",
            ajax: {
                url: '<c:url value="/services/photos?search=${sessionScope.user.id}"/>',
                dataSrc: 'results'
            },
            columns: [
                {
                    data: 'path',
                    render: renderDT.clickablePhoto
                },
                {
                    data: 'creationTimestamp',
                    render: renderDT.dateHour
                },
            ],
            fnDrawCallback: () => $("#photo-datatable thead").remove()
        };
    </script>
    <dis:make-datatable elementId="photo-datatable" params="photoTable"/>
</dis:scripts>
</body>
</html>