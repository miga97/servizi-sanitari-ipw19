<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>

<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="it_IT"/>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<%@ attribute name="patient" type="it.disorganizzazione.progettoweb.persistence.entities.users.Citizen" required="true" %>
<%@ attribute name="patientPhoto" type="it.disorganizzazione.progettoweb.persistence.entities.Photo" required="true" %>
<div class="row">
    <div class="col-xs mb-3">
        <div class="col col-1">
            <div class="rounded-circle mb-3" style="width:120pt;height:120pt;background-image:url('<c:url value="${patientPhoto.path}"/>');background-size:cover;background-position:center;"></div>
        </div>
        <div class="col">
            <h4>${patient.name} ${patient.surname}</h4>
            <p>Nato il <fmt:formatDate value="${patient.birthDate}" pattern="dd MMM yyyy"/>, ${patient.gender}</p>
        </div>
    </div>
    <div class="col">
        <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-general-tab" data-toggle="pill"
                   href="#pills-general" role="tab" aria-controls="pills-general"
                   aria-selected="true">Generalità</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-visit-tab" data-toggle="pill"
                   href="#pills-visit" role="tab" aria-controls="pills-visits"
                   aria-selected="false">Visite</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-exams-tab" data-toggle="pill"
                   href="#pills-exams" role="tab" aria-controls="pills-exams"
                   aria-selected="false">Esami</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-recipes-tab" data-toggle="pill"
                   href="#pills-recipes" role="tab" aria-controls="pills-recipes"
                   aria-selected="false">Ricette</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-photos-tab" data-toggle="pill"
                   href="#pills-photos" role="tab" aria-controls="pills-photos"
                   aria-selected="false">Foto</a>
            </li>
        </ul>
        <div class="tab-content m-3" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-general" role="tabpanel" aria-labelledby="pills-general-tab">
                <dis:generalities-form user="${patient}" status="disabled"/>
            </div>
            <div class="tab-pane fade" id="pills-visit" role="tabpanel"
                 aria-labelledby="pills-visit-tab">
                <table id="visit-datatable" style="width:100%" class="cell-border my-0 table dataTable dtr-inline">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Prescritta il</th>
                        <th>Stato</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane fade" id="pills-exams" role="tabpanel" aria-labelledby="pills-exams-tab">
                <table id="exam-datatable" style="width:100%" class="cell-border my-0 table dataTable dtr-inline">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Prescritto il</th>
                        <th>Stato</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane fade" id="pills-recipes" role="tabpanel" aria-labelledby="pills-recipes-tab">
                <table id="drug-datatable" style="width:100%" class="cell-border my-0 table dataTable dtr-inline">
                    <thead>
                    <tr>
                        <th>Farmaco</th>
                        <th>Prescritto il</th>
                        <th>Stato</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane fade" id="pills-photos" role="tabpanel" aria-labelledby="pills-photos-tab">
                <table id="photo-datatable" style="width:100%" class="cell-border my-0 table dataTable dtr-inline">
                    <thead>
                    <tr>
                        <th>Immagine</th>
                        <th>Aggiunta il</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>