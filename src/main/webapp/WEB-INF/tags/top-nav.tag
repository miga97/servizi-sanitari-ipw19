<!--
~ Copyright (c) Alberto Xamin 2019.
-->
<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.user != null}">
    <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
        <div class="container-fluid">
            <button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i
                    class="fas fa-bars"></i></button>
            <ul class="nav navbar-nav flex-nowrap ml-auto">
                <li class="nav-item dropdown no-arrow">
                    <a class="dropdown-toggle nav-link"
                       data-toggle="dropdown" aria-expanded="false" href="#">
                    <span class="d-none d-lg-inline mr-2 text-gray-600 small">${sessionScope.user.name}
                        <c:if test="${sessionScope.user.isCitizen()}">
                            ${sessionScope.user.surname}
                        </c:if>
                    </span>
                        <div class="rounded-circle align-self-center border img-profile" alt="User picture"
                             style="background-image: url(<c:url value="${sessionScope.userPhoto.path}"/>);background-size:cover;background-position:center;height:34px;width:34px;flex:none;"></div>
                    </a>
                    <div class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu">
                        <c:if test="${sessionScope.user.isDoctor()}">
                            <%-- il Dottore può vedere la sua home da cittadino --%>
                            <c:if test="${requestScope.userView == 'doctor'}">
                                <a class="dropdown-item" role="presentation" href="<c:url value="/citizen/home"/>">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>Naviga come Paziente
                                </a>
                            </c:if>
                            <c:if test="${requestScope.userView == 'citizen'}">
                                <a class="dropdown-item" role="presentation" href="<c:url value="/doctor/home"/>">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>Naviga come Dottore
                                </a>
                            </c:if>
                            <div class="dropdown-divider"></div>
                        </c:if>
                        <c:if test="${sessionScope.user.isSpecialist()}">
                            <%-- il Dottore specialista può vedere la sua home da cittadino --%>
                            <c:if test="${requestScope.userView == 'specialist'}">
                                <a class="dropdown-item" role="presentation" href="<c:url value="/citizen/home"/>">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>Naviga come Paziente</a>
                            </c:if>
                            <c:if test="${requestScope.userView == 'citizen'}">
                                <a class="dropdown-item" role="presentation" href="<c:url value="/specialist/home"/>">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>Naviga come Dottore</a>
                            </c:if>
                            <div class="dropdown-divider"></div>
                        </c:if>
                        <a class="dropdown-item" role="presentation"
                           href="<c:url value="/${requestScope.userView}/profile"/>">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>Profilo</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" role="presentation" href=<c:url value="/logout"/>>
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <c:if test="${requestScope.userView == 'citizen' && sessionScope.user.doctorId == null}">
        <div class="container-fluid"><div class="alert alert-danger">
            <strong>Attenzione!</strong> Non hai ancora definito un medico di base. Definiscilo nel <a href="profile#doctorChoice" class="alert-link">tuo profilo</a>
        </div></div>
    </c:if>
</c:if>
<c:if test="${requestScope.toast != null}">
    <div class="toast mr-3"
         style="position: absolute; top: 60pt; right: 0; z-index: 1; font-size: 0.9rem; max-width: 500px">
        <div class="toast-header" style="color: #fff;background-color: ${requestScope.toast.type};">
            <i class="fas ${requestScope.toast.icon} rounded mr-2"></i>
            <strong class="mr-auto">${requestScope.toast.title}</strong>
            <small>adesso</small>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true" style="color: white;">&times;</span>
            </button>
        </div>
        <div class="toast-body">${requestScope.toast.message}</div>
    </div>
    <script src="<c:url value="/assets/js/jquery.min.js"/>"></script>
    <script>
        $(document).ready(function () {
            $('.toast').toast({
                autohide: true,
                delay: 5000
            }).toast('show')
        });
    </script>
</c:if>