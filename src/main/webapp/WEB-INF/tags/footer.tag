<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>
<%@tag pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<footer class="bg-white sticky-footer mt-2">
    <div class="container flex-shrink-1 flex-fill my-auto">
        <div class="text-center d-sm-flex flex-shrink-1 flex-wrap align-items-sm-center copyright"><span
                class="d-flex flex-grow-1 flex-fill justify-content-center" style="padding: 6px;">Copyright © Disorganizzazione XIII 2019</span><a
                class="flex-grow-1 flex-fill" href="<c:url value="/privacy"/>">Privacy</a></div>
    </div>
</footer>