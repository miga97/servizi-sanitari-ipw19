<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>

<%@ tag pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="user" type="it.disorganizzazione.progettoweb.persistence.entities.users.User" %>
<%@ attribute name="status" %>
<c:if test="${user.isCitizen()}">
    <div class="form-row">
        <div class="col-xl-5 m-auto">
            <div class="form-group"><label for="cod_fis">
                <strong>Codice Fiscale</strong></label>
                <input class="form-control" type="text" required ${status}
                       placeholder="Codice fiscale" name="cf"
                       id="cod_fis" value="${user.cf}">
            </div>
        </div>
        <div class="col-xl-5 m-auto">
            <div class="form-group"><label
                    for="sex"><strong>Sesso</strong></label>
                <select id="sex" class="form-control" type="select" required ${status}
                        name="gender">
                    <option value="A" ${user.gender == "A" ? 'selected' : ''}>Altro</option>
                    <option value="M" ${user.gender == "M" ? 'selected' : ''}>Maschio</option>
                    <option value="F" ${user.gender == "F" ? 'selected' : ''}>Femmina</option>
                </select></div>
        </div>
    </div>
</c:if>
<div class="form-row">
    <div class="col-xl-5 m-auto">
        <div class="form-group"><label
                for="first_name"><strong>Nome</strong></label><input
                class="form-control" type="text" placeholder="Nome" required ${status}
                name="name" id="first_name"
                value="${user.name}">
        </div>
    </div>
    <c:if test="${user.isCitizen()}">
        <div class="col-xl-5 m-auto">
            <div class="form-group"><label
                    for="last_name"><strong>Cognome</strong><br></label><input
                    class="form-control" type="text" placeholder="Cognome"
                    id="last_name" required ${status}
                    name="surname" value="${user.surname}">
            </div>
        </div>
    </c:if>
</div>
<c:if test="${user.isCitizen()}">
    <div class="form-row">
        <div class="col-xl-5 m-auto">
            <div class="form-group"><label for="birthday">
                <strong>Data di Nascita</strong></label>
                <input class="form-control birthDate" type="date"
                       name="birthDate" id="birthday" required ${status}
                       value="${user.birthDate}">
            </div>
        </div>
        <div class="col-xl-5 m-auto">
            <div class="form-group"><label for="birthplace">
                <strong>Luogo di Nascita</strong></label>
                <input class="form-control" type="text"
                       placeholder="Luogo di nascita"
                       name="birthTown" required id="birthplace" ${status}
                       value="${user.birthTown}">
            </div>
        </div>
    </div>
</c:if>
<div class="form-row">
    <div class="col-xl-5 m-auto">
        <div class="form-group"><label
                for="province"><strong>Provincia</strong></label>
            <select id="province" required class="form-control" ${status}
                    type="select" name="province">
                <c:forEach var="province"
                           items="${applicationScope.provinces}">
                    <option value="${province.id}" ${province.id == user.province ? 'selected' : ''}>
                            ${province.name} (${province.acronym})
                    </option>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="col-xl-5 m-auto">
        <div class="form-group"><label
                for="email"><strong>Email</strong></label><input
                class="form-control" type="email" name="email" id="email"
                placeholder="Indirizzo email" required ${status}
                pattern="^[\w!#$%&'*+/=?{|}~^-]+(?:\.[\w!#$%&'*+/=?{|}~^-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$"
                value="${user.email}"></div>
    </div>
</div>