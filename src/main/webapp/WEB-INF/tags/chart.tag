<%--
  ~ Copyright (c) Matteo Zanella 2019.
  --%>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="params" %>
<%@attribute name="elementId" required="true" %>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script>
    $(document).ready(function () {
        new Chart($("${'#'.concat(elementId)}")[0].getContext('2d'), ${params});
    });
</script>