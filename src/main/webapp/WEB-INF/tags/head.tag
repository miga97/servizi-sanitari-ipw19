<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="title" required="true" %>

<head>
    <title>${title} - Servizi Sanitari</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta property="og:type" content="website">
    <meta name="description" content="Servizi Sanitari">
    <link rel="icon" type="image/png" sizes="16x16" href="<c:url value="/assets/img/16.png"/>">
    <link rel="icon" type="image/png" sizes="32x32" href="<c:url value="/assets/img/32.png"/>">
    <link rel="icon" type="image/png" sizes="180x180" href="<c:url value="/assets/img/180.png"/>">
    <link rel="icon" type="image/png" sizes="192x192" href="<c:url value="/assets/img/192.png"/>">
    <link rel="icon" type="image/png" sizes="512x512" href="<c:url value="/assets/img/512.png"/>">
    <%-- Safari iOS --%>
    <link rel="apple-touch-icon" sizes="180x180" href="<c:url value="/assets/img/180.png"/>">
    <link rel="apple-touch-icon" sizes="16x16" href="<c:url value="/assets/img/16.png"/>">
    <link rel="apple-touch-icon" sizes="32x32" href="<c:url value="/assets/img/32.png"/>">
    <link rel="apple-touch-icon" sizes="180x180" href="<c:url value="/assets/img/180.png"/>">
    <link rel="apple-touch-icon" sizes="192x192" href="<c:url value="/assets/img/192.png"/>">
    <link rel="apple-touch-icon" sizes="512x512" href="<c:url value="/assets/img/512.png"/>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Servizi Sanitari">
    <link rel="apple-touch-startup-image" href="<c:url value="/assets/img/512.png"/>">
    <%-- Endof: Safari iOS --%>
    <link rel="stylesheet" href="<c:url value="/assets/bootstrap/css/bootstrap.min.css"/>">
    <link rel="manifest" href="<c:url value="/manifest.json"/>">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="<c:url value="/assets/fonts/fontawesome-all.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/assets/fonts/font-awesome.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/assets/fonts/fontawesome5-overrides.min.css"/>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="<c:url value="/assets/css/styles.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/assets/DataTables/datatables.min.css"/>"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"/>
    <style>
        .selectize-control {
            display: contents;
        }
    </style>
    <jsp:doBody/>
</head>