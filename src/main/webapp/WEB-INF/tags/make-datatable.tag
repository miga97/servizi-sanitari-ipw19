<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>
<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="elementId" %>
<%@ attribute name="printReports" type="java.lang.Boolean" %>
<%@ attribute name="params" %>
<script>
    $(document).ready(function () {
        $.fn.dataTable.moment( 'lll', 'it' );
        $.fn.dataTable.moment( 'll', 'it' );
        $("${elementId != null ? '#'.concat(elementId) : '.dataTable'}").DataTable(Object.assign({
            language: {
                "sEmptyTable": "Nessun dato presente nella tabella",
                "sInfo": "Mostrati da _START_ a _END_ su _TOTAL_ elementi",
                "sInfoEmpty": "Nessun elemento",
                "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Visualizza _MENU_ elementi",
                "sLoadingRecords": "Caricamento...",
                "sProcessing": "Elaborazione...",
                "sSearch": "Cerca:",
                "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                "oPaginate": {
                    "sFirst": "⇤",
                    "sPrevious": "←",
                    "sNext": "→",
                    "sLast": "⇥"
                },
                "oAria": {
                    "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                    "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                }
            },
            responsive: true,
            <c:if test="${printReports eq 'true'}">
            dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'B><'col-sm-12 col-md-7'p>>",
            buttons: {
                buttons: [
                    { extend: 'excel', className: 'btn-primary' },
                    { extend: 'pdf', className: 'btn-primary' },
                ],
                dom: {
                    button: {
                        className: 'btn'
                    }
                }
            },
            </c:if>
        }, ${params}));
        $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
            $.fn.dataTable
                .tables({visible: true, api: true})
                .columns.adjust();
        })
    });
</script>