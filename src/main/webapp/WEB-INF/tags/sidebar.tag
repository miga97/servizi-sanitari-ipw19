<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>
<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="selected" %>
<nav id="dis-sidebar" class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0 ${cookie['sidebar-status'].value}"
     style="transition: all 0.2s ease-out;">
    <div class="container-fluid d-flex flex-column p-0">
        <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="<c:url value="/"/>"
           style="padding-left: 16px;height: 80px;font-size: 14px;">
            <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-pills"></i></div>
            <div class="sidebar-brand-text mx-3"><span>Servizi Sanitari</span></div>
        </a>
        <hr class="sidebar-divider my-0">
        <ul class="nav navbar-nav text-light" id="accordionSidebar">
            <c:choose>
                <c:when test="${requestScope.userView == 'citizen'}">
                    <%-- Cittadino --%>
                    <%-- Presentazione delle prescrizioni non ancora erogate --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'home' ? 'active' : '' }" href="<c:url value="/citizen/home"/>"><i
                            class="fas fa-band-aid"></i><span>Prescrizioni inevase</span></a></li>
                    <%-- Presentazione delle prescrizioni già erogate, aka cartella clinica --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'medicalRecords' ? 'active' : '' }" href="<c:url value="/citizen/medicalRecords"/>"><i
                            class="fas fa-notes-medical"></i><span>Cartella clinica</span></a></li>
                    <%-- Visualizzazione dei ticket completa, con report PDF --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'ticket' ? 'active' : '' }" href="<c:url value="/citizen/ticket"/>"><i
                            class="fas fa-receipt"></i><span>Ticket</span></a></li>
                    <%-- Ricerca tra i nomi degli esami e delle visite --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'search' ? 'active' : '' }" href="<c:url value="/citizen/search"/>"><i
                            class="fas fa-search"></i><span>Ricerca prestazioni</span></a></li>
                </c:when>
                <c:when test="${requestScope.userView == 'doctor'}">
                    <%-- Dottore --%>
                    <%-- Home con tutti i pazienti --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'home' ? 'active' : '' }" href="<c:url value="/doctor/home"/>"><i
                            class="fas fa-hand-holding-heart"></i><span>Pazienti</span></a></li>
                    <%-- Scheda paziente, contenente cartella clinica e form per creare la visita --%>
                    <c:if test="${sessionScope.patient != null}">
                        <li class="nav-item" role="presentation"><a
                                class="nav-link ${selected == 'visit' ? 'active' : '' }" href="<c:url value="/doctor/visit"/>"><i
                                class="fas fa-notes-medical"></i><span>Scheda Paziente</span></a></li>
                    </c:if>
                </c:when>
                <c:when test="${requestScope.userView == 'specialist'}">
                    <%-- Specialista --%>
                    <%-- Home dove sono presenti tutte le visite completabili dallo specialista, scelte tra tutti i cittadini --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'home' ? 'active' : '' }" href="<c:url value="/specialist/home"/>"><i
                            class="fas fa-hand-holding-heart"></i><span>Visite erogabili</span></a></li>
                    <%-- Scheda dove lo specialista completa la visita prescritta selezionata nella home, dove é presente anche la scheda paziente --%>
                    <c:if test="${sessionScope.visitPrescription != null}">
                        <li class="nav-item" role="presentation"><a
                                class="nav-link ${selected == 'visit' ? 'active' : '' }" href="<c:url value="/specialist/visit"/>"><i
                                class="fas fa-notes-medical"></i><span>Scheda paziente</span></a></li>
                    </c:if>
                </c:when>
                <c:when test="${requestScope.userView == 'ssp'}">
                    <%-- SSP --%>
                    <%-- Home con l'elenco di tutti gli esami che il SSP può somministrare --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'home' ? 'active' : '' }" href="<c:url value="/ssp/home"/>"><i
                            class="fas fa-hand-holding-heart"></i><span>Esami erogabili</span></a></li>
                    <%-- Esegui l'esame tra quelli scelti nella home, senza la cartella clinica --%>
                    <c:if test="${sessionScope.examPrescription != null}">
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'exam' ? 'active' : '' }" href="<c:url value="/ssp/exam"/>"><i
                            class="fas fa-file-medical-alt"></i><span>Scheda esame</span></a></li>
                    </c:if>
                    <%-- Pagina per eseguire i richiami --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'recall' ? 'active' : '' }" href="<c:url value="/ssp/recall"/>"><i
                            class="fas fa-redo-alt"></i><span>Prescrivi richiamo</span></a></li>
                    <%-- Pagina contenente i vari report sanitari con grafici --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'report' ? 'active' : '' }" href="<c:url value="/ssp/report"/>"><i
                            class="fas fa-chart-pie"></i><span>Report</span></a></li>
                </c:when>
                <c:when test="${requestScope.userView == 'pharmacy'}">
                    <%-- Farmacia --%>
                    <%-- Home con l'elenco di tutti i cittadini (con prescrizioni in sospeso? Anche no forse) --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'home' ? 'active' : '' }" href="<c:url value="/pharmacy/home"/>"><i
                            class="fas fa-hand-holding-heart"></i><span>Ricerca Ricette</span></a></li>
                    <%-- Elenco dei farmaci di cui il paziente selezionato ha la ricetta --%>
                    <c:if test="${sessionScope.receipt != null}">
                        <li class="nav-item" role="presentation"><a
                                class="nav-link ${selected == 'sell' ? 'active' : '' }" href="<c:url value='/pharmacy/sell'/>"><i
                                class="fas fa-prescription-bottle"></i><span>Eroga Ricette</span></a></li>
                    </c:if>
                </c:when>
            </c:choose>
                    <%-- Profilo dell'utente comune a tutti --%>
                    <li class="nav-item" role="presentation"><a
                            class="nav-link ${selected == 'profile' ? 'active' : '' }" href="<c:url value="/${requestScope.userView}/profile"/>"><i
                            class="fas fa-user"></i><span>Profilo</span></a></li>
                    <li class="nav-item" role="presentation"></li>
        </ul>
        <div class="text-center d-none d-md-inline">
            <button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button>
        </div>
    </div>
</nav>
