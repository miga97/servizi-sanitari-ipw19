<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/assets/js/jquery.min.js"/>"></script>
<script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js"/>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
<script src="<c:url value="/assets/js/script.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/moment-with-locales.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/assets/js/jquery.touchSwipe.min.js"/>"></script>
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="infoModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="infoModalImage" class="mb-1" style="width: 100%"/>
                <p id="infoModalText" style="white-space: pre-wrap;">
            </div>
        </div>
    </div>
</div>
<script>
    const urlBase = "<c:url value="/"/>".slice(0, -1);
    const dis_alert = function (title, message, image_url) {
        $('#infoModalLabel').text((title) ? title : '');
        $('#infoModalImage').attr('src', (image_url) ? image_url : '');
        $('#infoModalText').text(message);
        $('#infoModal').modal();
    };
    const formatDate = function (date) {
        console.log(date);
        let reg = /:[0-9]+\.[0-9]+$/gm;
        let match = reg.exec(date);
        return date.replace(match[0], '');
    };
    moment.locale('it');
</script>
<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('table').DataTable().columns.adjust()
    });
</script>
<script>
    //http://labs.rampinteractive.co.uk/touchSwipe/demos/index.html
    if (screen.width < 500) {
        $.fn.swipe.defaults.preventDefaultEvents = false;
        $("#content").swipe({
            swipeRight: function (event, direction, distance, duration, fingerCount, fingerData) {
                if ($('#dis-sidebar').hasClass('toggled')) {
                    $('#sidebarToggleTop').click();
                }
            },
            swipeLeft: function (event, direction, distance, duration, fingerCount, fingerData) {
                if (!$('#dis-sidebar').hasClass('toggled')) {
                    $('#sidebarToggleTop').click();
                }
            }
        });
    }
</script>
<jsp:doBody/>