<%--
  ~ Copyright (c) Alberto Xamin 2019.
  --%>
<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="medicalRecords" type="java.lang.Boolean" %>
<%@ attribute name="patientId" type="java.lang.Integer" %>
<script type="text/javascript" src="<c:url value='/assets/DataTables/pdfmake.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/assets/DataTables/vfs_fonts.js'/>"></script>
<script type="text/javascript" src="<c:url value='/assets/DataTables/datatables.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/assets/DataTables/datetime-moment.js'/>"></script>
<script>
    const prescriptionType = (row) => {
        if (row['visitPrescription']) {
            return 'visitPrescription';
        } else if (row['examPrescription']) {
            return 'examPrescription';
        } else if (row['drugPrescription']) {
            return 'drugPrescription';
        } else return undefined;
    };
    const prescriptionResult = (row) => {
        if (row['visitPrescription']) {
            return (row.visitPrescription.anamnesi)? 'Anamnesi: ' +row.visitPrescription.anamnesi:'';
        } else if (row['examPrescription']) {
            return (row.examPrescription.results)?'Risultati: ' + row.examPrescription.results:'';
        } else if (row['drugPrescription']) {
            return (row.drugPrescription.description)? 'Descrizione: ' + row.drugPrescription.description:'';
        } else return undefined;
    };
    const prescriptionDescription = (row) => {
        let prescription = prescriptionType(row);
        if (!prescription) {
            console.log(row);
            console.log(prescription);
            return 'Error';
        }
        return 'Prescritto il: ' +
            renderDT.date(row[prescription]['prescriptionTimestamp']) + ' da ' + renderDT.user('plannerProvince')(row.planner) + '\n' +
            (row['performer']['id'] ? 'Erogato il: ' +
                renderDT.date(row[prescription]['completionTimestamp']) + ' da ' + renderDT.user('performerProvince')(row.performer) + '\n' : '') +
            (prescriptionResult(row) ? prescriptionResult(row) + '\n' : '') +
            '';
    };
    const renderDT = {
        dateHour: (data, type, row) => moment(data).format('lll'),
        date: (data, type, row) => moment(data).format('ll'),
        user: (userProvince) => (data, type, row) => {
            switch (data['role']) {
                case 0:
                    return data['name'] + ' ' + data['surname'];
                case 1:
                case 2:
                    return 'Dr. ' + data['surname'] + ' ' + data['name'];
                case 3:
                    return data['name'] + ' - SSP ' + ((row && row[userProvince]) ? row[userProvince]['acronym'] : '');
                default:
                    return data['name'];
            }
        },
        clickablePhoto: (data, type, row) => {
            let photoPath = urlBase + data;
            return `<div class="rounded-circle align-self-center" style="background-image:url('\${photoPath}');background-size:cover;background-position:center;height:34px;width:34px;flex:none;cursor:pointer;"
                         onclick="dis_alert('Foto utente', '\${moment(row['creationTimestamp']).format('lll')}', '\${photoPath}')">`
        },
        userWithPhoto: (userProvince, userPhoto) => (data, type, row) => {
            let photoPath = urlBase + row[userPhoto]['path'];
            return `<div class="d-flex">
                        <div class="rounded-circle align-self-center" style="background-image:url('\${photoPath}');background-size:cover;background-position:center;height:34px;width:34px;flex:none;">
                    </div>
                    <span class="align-self-center ml-3">\${renderDT.user(userProvince)(data,type,row)}</span></div>`;
        },
        prescriptionInfoButton: (data, type, row) => {
            let btnClass = !row['performer']['id'] ? "btn-info" : "btn-success";
            let btnText = !row['performer']['id'] ? "Inevaso" : "Evaso";
            let btnIcon = !row['performer']['id'] ? "fa-hourglass-half" : "fa-check-circle";
            let description = prescriptionDescription(row);
            return `<button class="btn \${btnClass} btn-icon-split" role="button" style="height: 35px;min-width: max-content;"
                            onclick="dis_alert('Prescrizione', \`\${description}\`)">
                        <span class="text-white-50 icon"><i class="fas \${btnIcon}"></i></span>
                        <span class="text-white text">\${btnText}</span>
                    </button>`
        },
        doctorButton: (data, type, row) => {
            return `<form action="#" method="post">
                        <button class="btn btn-info btn-icon-split" role="button" style="height: 35px;min-width: max-content;"
                                name="patientId" value="\${data['id']}">
                            <span class="text-white-50 icon"><i class="fas fa-notes-medical"></i></span>
                            <span class="text-white text">Inizia visita</span>
                        </button>
                    </form>`
        },
        pharmacyButton: (data, type, row) => {
            return `<form action="#" method="post">
                        <input name="plannerId" value="\${data['plannerId']}" hidden/>
                        <input name="patientId" value="\${data['patientId']}" hidden/>
                        <input name="prescriptionTimestamp" value="\${data['prescriptionTimestamp']}" hidden/>
                        <button class="btn btn-info btn-icon-split" role="button" style="height: 35px;min-width: max-content;">
                            <span class="text-white-50 icon"><i class="fas fa-shopping-cart"></i></span>
                            <span class="text-white text">Seleziona Ricetta</span>
                        </button>
                    </form>`
        },
        drugQrButton: (data, type, row) => {
            return `<form action="#" method="post">
                            <input name="plannerId" value="\${data['plannerId']}" hidden/>
                            <input name="patientId" value="\${data['patientId']}" hidden/>
                            <input name="prescriptionTimestamp" value="\${data['prescriptionTimestamp']}" hidden/>
                            <button class="btn btn-info btn-icon-split" name="print" role="button" style="height: 35px;min-width: max-content;">
                                <span class="text-white-50 icon"><i class="fas fa-print"></i></span>
                                <span class="text-white text">Stampa ricetta</span>
                            </button>
                        </form>`
        },
        sellButton: (data, type, row) => {
            return `<form action="#" method="post">
                        <input name="plannerId" value="\${data['plannerId']}" hidden/>
                        <input name="patientId" value="\${data['patientId']}" hidden/>
                        <input name="prescriptionTimestamp" value="\${data['prescriptionTimestamp']}" hidden/>
                        <button class="btn btn-danger btn-icon-split" role="button" style="height: 35px;min-width: max-content;">
                            <span class="text-white-50 icon"><i class="fas fa-undo"></i></span>
                            <span class="text-white text">Annulla</span>
                        </button>
                    </form>`
        },
        specialistButton: (data, type, row) => {
            return `<form action="#" method="post">
                        <input name="plannerId" value="\${data['plannerId']}" hidden/>
                        <input name="patientId" value="\${data['patientId']}" hidden/>
                        <input name="prescriptionTimestamp" value="\${data['prescriptionTimestamp']}" hidden/>
                        <button class="btn btn-info btn-icon-split" role="button" style="height: 35px;min-width: max-content;">
                            <span class="text-white-50 icon"><i class="fas fa-notes-medical"></i></span>
                            <span class="text-white text">Inizia visita</span>
                        </button>
                    </form>`
        },
        sspButton: (data, type, row) => {
            return `<form action="#" method="post">
                        <input name="plannerId" value="\${data['plannerId']}" hidden/>
                        <input name="patientId" value="\${data['patientId']}" hidden/>
                        <input name="prescriptionTimestamp" value="\${data['prescriptionTimestamp']}" hidden/>
                        <button class="btn btn-info btn-icon-split" role="button" style="height: 35px;min-width: max-content;">
                            <span class="text-white-50 icon"><i class="fas fa-file-medical-alt"></i></span>
                            <span class="text-white text">Inizia esame</span>
                        </button>
                    </form>`
        }
    };
    <c:if test="${medicalRecords eq 'true'}">
    const visitTable = {
        order: [[1, "desc"]],
        ajax: {
            url: '<c:url value="/services/dataTable?dt=visit&type=citizenid&search=${patientId}"/>',
            dataSrc: 'results'
        }, columns: [
            {data: 'visitName'},
            {
                data: 'visitPrescription.prescriptionTimestamp',
                render: renderDT.dateHour
            },
            {
                data: null,
                render: renderDT.prescriptionInfoButton
            }
        ]
    };
    const examTable = {
        order: [[1, "desc"]],
        ajax: {
            url: '<c:url value="/services/dataTable?dt=exam&type=citizenid&search=${patientId}"/>',
            dataSrc: 'results'
        }, columns: [
            {data: 'examName'},
            {
                data: 'examPrescription.prescriptionTimestamp',
                render: renderDT.dateHour
            },
            {
                data: null,
                render: renderDT.prescriptionInfoButton
            }
        ]
    };
    const drugTable = {
        order: [[1, "desc"]],
        ajax: {
            url: '<c:url value="/services/dataTable?dt=drug&type=citizenid&search=${patientId}"/>',
            dataSrc: 'results'
        }, columns: [
            {data: 'drugPrescription.description'},
            {
                data: 'drugPrescription.prescriptionTimestamp',
                render: renderDT.dateHour
            },
            {
                data: null,
                render: renderDT.prescriptionInfoButton
            }
        ]
    };
    const photoTable = {
        order: [[1, "desc"]],
        ajax: {
            url: '<c:url value="/services/photos?search=${patientId}"/>',
            dataSrc: 'results'
        }, columns: [
            {
                data: 'path',
                render: renderDT.clickablePhoto
            },
            {
                data: 'creationTimestamp',
                render: renderDT.dateHour
            },
        ]
    };
    </c:if>
</script>