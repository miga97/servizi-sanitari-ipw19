<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Privacy"/>
<body id="page-top">
    <div data-bs-parallax-bg="true" style="height: 65px;background-position: center;background-size: cover;">
        <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0 shadow" href="." style="font-size: 35px;background-color: #ffffff;position: fixed;width: 100%;z-index: 1;margin: 0 0 10px 0;padding-left: 23px;">
            <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-pills"></i></div>
            <div class="sidebar-brand-text mx-3"><span>Servizi Sanitari</span></div>
        </a>
    </div>
    <div></div>
    <div class="features-clean">
        <div class="container">
            <h2 class="text-center">Informativa sulla privacy</h2>
            <p class="text-justify">In ottemperanza degli obblighi derivanti dalla normativa nazionale (D. Lgs 30 giugno 2003 n. 196, Codice in materia di protezione dei dati personali) e comunitaria, (Regolamento europeo per la protezione dei dati personali n. 679/2016, GDPR)
                e successive modifiche, il presente sito rispetta e tutela la riservatezza dei visitatori e degli utenti, ponendo in essere ogni sforzo possibile e proporzionato per non ledere i diritti degli utenti.</p>
            <p class="text-justify">La presente privacy policy si applica esclusivamente alle attività online del presente sito ed è valida per i visitatori/utenti del sito. Non si applica alle informazioni eventualmente raccolte tramite canali diversi dal presente sito web.<br></p>
            <h5>Base giuridica del trattamento</h5>
            <p class="text-justify">Il presente sito tratta i dati in base al consenso. Con l’uso o la consultazione del presente sito i visitatori e gli utenti approvano esplicitamente la presente informativa privacy e acconsentono al trattamento dei loro dati personali in
                relazione alle modalità e alle finalità di seguito descritte, , compreso l’eventuale diffusione a terzi se necessaria per l’erogazione di un servizio. Il conferimento dei dati e quindi il Consenso alla raccolta e al trattamento dei dati
                è facoltativo, l’Utente può negare il consenso, e può revocare in qualsiasi momento un consenso già fornito (tramite email:&nbsp;<a href="mailto:privacy@disorganizzazione.it">privacy@disorganizzazione.it</a>). Tuttavia negare il consenso
                può comportare l’impossibilità di erogare alcuni servizi e l’esperienza di navigazione nel sito potrebbe essere compromessa.<br></p>
            <p class="text-justify">A partire dal 25 maggio 2018 (data di entrata in vigore del GDPR), il presente sito tratterà alcuni dei dati in base ai legittimi interessi del titolare del trattamento.<br></p>
            <h5>Tipi di dati trattati e finalità del trattamento<br></h5>
            <p class="text-justify">I sistemi informatici e le procedure software preposte al funzionamento di questo sito acquisiscono, nel corso del loro normale esercizio, alcuni dati personali la cui trasmissione è implicita nell'uso dei protocolli di comunicazione di Internet.<br></p>
            <p
                class="text-justify">In questa categoria di dati rientrano gli indirizzi IP o i nomi a dominio dei computer e dei terminali utilizzati dagli utenti, gli indirizzi in notazione URI/URL (Uniform Resource Identifier/Locator) delle risorse richieste, l'orario della
                richiesta, il metodo utilizzato nel sottoporre la richiesta al server, la dimensione del file ottenuto in risposta, il codice numerico indicante lo stato della risposta data dal server (buon fine, errore, ecc.) ed altri parametri relativi
                al sistema operativo e all'ambiente informatico dell'utente.<br></p>
                <p class="text-justify">Tali dati, necessari per la fruizione dei servizi web, vengono anche trattati allo scopo di:<br></p>
                <ul style="color: grey;">
                    <li class="text-justify">Ottenere informazioni statistiche sull'uso dei servizi (pagine più visitate, numero di visitatori per fascia oraria o giornaliera, aree geografiche di provenienza, ecc.)<br></li>
                    <li class="text-justify">Controllare il corretto funzionamento dei servizi offerti<br></li>
                </ul>
                <p class="text-justify">L'invio facoltativo, esplicito e volontario di informazioni personali agli indirizzi di contatto del Sistema Sanitario, nonché la compilazione e l'inoltro dei moduli presenti sul sito stesso, comportano l'acquisizione dei dati forniti
                    dall'utente, necessari al fine di fornire i servizi previsti dal sistema.<br></p>
                <p class="text-justify">Non viene fatto uso di cookie per la profilazione degli utenti, né vengono impiegati altri metodi di tracciamento a fini commerciali.<br>Viene invece fatto uso di cookie di sessione (non persistenti) in modo strettamente limitato a quanto
                    necessario per la navigazione sicura ed efficiente dei siti. La memorizzazione dei cookie di sessione nei terminali o nei browser è sotto il controllo dell'utente, laddove sui server, al termine delle sessioni HTTP, informazioni relative
                    ai cookie restano registrate nei log dei servizi.<br></p>
                <h5>Diffusione dei dati</h5>
                <p class="text-justify">I tuoi dati personali non verranno diffusi in alcun modo al di fuori dei necessari utilizzi da parte del sistema, se non negli eventuali casi in cui ciò fosse espressamente richiesto dagli organi di Legge.<br></p>
                <h5>Misure di sicurezza<br></h5>
                <p class="text-justify">Il presente sito tratta i dati degli utenti in maniera lecita e corretta, adottando le opportune misure di sicurezza volte ad impedire accessi non autorizzati, divulgazione, modifica o distruzione non autorizzata dei dati. Il trattamento
                    viene effettuato mediante strumenti informatici e/o telematici, con modalità organizzative e con logiche strettamente correlate alle finalità indicate. In particolare il software di gestione dei sito è costantemente aggiornato, e regolarmente
                    scansionato al fine di verificare la presenza di virus e codici pericolosi.<br></p>
                <p class="text-justify">Oltre al titolare, in alcuni casi, potrebbero avere accesso ai dati categorie di incaricati coinvolti nell’organizzazione del sito (personale amministrativo, commerciale, legali, amministratori di sistema) ovvero soggetti esterni (come
                    fornitori di servizi tecnici terzi, corrieri postali, hosting provider, società informatiche, agenzie di comunicazione).<br></p>
                <h5>Diritti degli interessati<br></h5>
                <p class="text-justify">Gli interessati hanno il diritto di ottenere dal Sistema Sanitario, nei casi previsti, l'accesso ai propri dati personali e la rettifica o la cancellazione degli stessi o la limitazione del trattamento che li riguarda o di opporsi al trattamento
                    (artt. 15 e ss. del Regolamento). L'apposita istanza al Sistema Sanitario è presentata contattando il Responsabile della protezione dei dati presso il Sistema Sanitario all'indirizzo email&nbsp;<a href="mailto:protezione.dati@disorganizzazione.it?subject=Accesso%20ai%20dati%20personali">protezione.dati@disorganizzazione.it</a><br></p>
                <h5>Diritto di reclamo</h5>
                <p class="text-justify">Gli interessati che ritengono che il trattamento dei dati personali a loro riferiti effettuato attraverso questo sito avvenga in violazione di quanto previsto dal Regolamento hanno il diritto di proporre reclamo al Garante, come previsto
                    dall'art. 77 del Regolamento stesso, o di adire le opportune sedi giudiziarie (art. 79 del Regolamento).<br></p>
                <h5>Modifiche al sito</h5>
                <p class="text-justify">Il Titolare del Sito si riserva il diritto di modificare i contenuti del Sito in qualsiasi momento e senza alcun preavviso. L’utente accetta di essere vincolato a tali eventuali e future revisioni e si impegna, pertanto, a visitare periodicamente
                    il Sito per essere informato su eventuali variazioni.<br></p>
                <h5>Modifiche all'informativa sulla privacy</h5>
                <p class="text-justify">Il Titolare del trattamento si riserva la facoltà di apportare delle modifiche alla presente pagina. L’utente accetta di essere vincolato a tali eventuali e future revisioni e si impegna, pertanto, a visitare periodicamente questa pagina
                    per essere informato su eventuali variazioni.<br></p>
        </div>
    </div>
    <div id="wrapper">
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content"></div>
            <footer class="bg-white sticky-footer">
                <div class="container flex-shrink-1 flex-fill my-auto">
                    <div class="text-center d-sm-flex flex-shrink-1 flex-wrap align-items-sm-center copyright"><span class="d-flex flex-grow-1 flex-fill justify-content-center" style="padding: 6px;">Copyright © Disorganizzazione XIII 2019</span><a class="flex-grow-1 flex-fill" href=".">Back to Home</a></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
<dis:scripts/>
</body>

</html>