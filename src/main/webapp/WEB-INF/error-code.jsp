<%@ page isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Pagina non trovata"/>
<body id="page-top">
<div id="wrapper">
    <c:choose>
        <c:when test="${sessionScope.user != null}">
            <dis:sidebar/>
        </c:when>
        <c:otherwise>
            <div class="sidebar" style="width:0!important;"></div>
        </c:otherwise>
    </c:choose>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <c:if test="${sessionScope.user != null}">
                <dis:top-nav/>
            </c:if>
            <div class="container-fluid">
                <div class="text-center mt-5">
                    <div class="error mx-auto" data-text="${pageContext.response.status}">
                        <p class="m-0">${pageContext.response.status}</p>
                    </div>
                    <p class="text-dark mb-5 lead">
                        <c:choose>
                            <c:when test="${pageContext.response.status == 404}">
                                Pagina non trovata
                            </c:when>
                            <c:otherwise>
                                Errore del server
                            </c:otherwise>
                        </c:choose>
                    </p>
                    <p class="text-black-50 mb-0">Sembra che tu abbia trovato un glitch in Matrix...</p>
                    <c:choose>
                        <c:when test="${sessionScope.user != null}">
                            <a href="<c:url value="/login"/>">← Torna alla home</a>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="/"/>">← Torna indietro</a>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <dis:footer/>
    </div>
    <dis:scripts/>
</div>
</body>
</html>