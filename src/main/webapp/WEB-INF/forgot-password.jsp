<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Recupero password"/>
<body class="bg-gradient-primary">
<a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="<c:url value="/" />"
   style="color: rgb(255,255,255);font-size: 35px;margin-top: 20px;padding-left: 25px;">
    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-pills"></i></div>
    <div class="sidebar-brand-text mx-3"><span>Servizi Sanitari</span></div>
</a>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9 col-lg-12 col-xl-10">
            <div class="card shadow-lg o-hidden border-0 my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-flex">
                            <div class="flex-grow-1 bg-password-image"
                                 style="background-image: url('<c:url value="/assets/img/iu.jpeg"/>');"></div>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h4 class="text-dark mb-2">Hai dimenticato la Password?</h4>
                                    <p class="mb-4">Lo sappiamo, queste cose succedono a tutti. Inserisci il tuo
                                        indirizzo e-mail qui sotto e ti invieremo un link per reimpostare la
                                        password!</p>
                                </div>
                                <form class="user" method="post" action="forgotPassword">
                                    <div class="form-group"><input
                                            class="form-control form-control-user" type="email" id="exampleInputEmail"
                                            aria-describedby="emailHelp"
                                            placeholder="Inserisci il tuo indirizzo Email..." name="email" required="">
                                    </div>
                                    <button class="btn btn-primary btn-block text-white btn-user"
                                            type="submit">Invia link di recupero
                                    </button>
                                </form>
                                <div class="text-center">
                                    <hr>
                                    <a class="small" href="register"><span style="text-decoration: underline;">Registra un nuovo account!</span><br></a>
                                </div>
                                <div class="text-center"><a class="small" href="login">Hai già un account? Effettua il login!<br></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<dis:scripts/>
</body>
</html>