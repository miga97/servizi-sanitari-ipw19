<%--
  User: matteo
  Date: 27/08/19
--%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="${pageContext.response.status}"/>
<body id="page-top">
<div id="wrapper">
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <div class="container-fluid">
                <div class="text-center mt-5">
                    <div class="error" data-text="${pageContext.response.status}"
                         style="display: table; margin: 0 auto;">
                        <p class="m-0">${pageContext.response.status}</p>
                    </div>
                    <p class="text-dark mb-5 lead">Page Not Found</p>
                    <p class="text-black-50 mb-0">Sembra che tu abbia trovato un glitch in Matrix...</p>
                    <a href="<c:url value="/"/>">← Back to Dashboard</a></div>
                <c:set var="type" value='${requestScope.type}'/>
                <c:set var="title" value='${requestScope.title}'/>
                <c:set var="message" value='${requestScope.message}'/>
                <c:set var="exception" value="${requestScope['javax.servlet.error.exception']}"/>
                <div class="form-group">
                    <c:if test="${exception !=null || type !=null}">
                    <c:if test="${(title !=null && message !=null)||exception!=null}">
                    <c:choose>

                    <c:when test="${type == 0}">
                    <div class="alert alert-success">

                        </c:when>

                        <c:when test="${type == 1}">
                        <div class="alert alert-info">
                            </c:when>

                            <c:when test="${type == 2}">
                            <div class="alert alert-warning">
                                </c:when>

                                <c:when test="${type == 3}">
                                <div class="alert alert-danger">
                                    </c:when>
                                    <c:otherwise>
                                    <c:set var="type" value='-1'/>
                                    <div class="alert alert-danger">
                                        </c:otherwise>
                                        </c:choose>
                                        </c:if>
                                        <c:choose>
                                            <c:when test="${type!=-1}">
                                                <strong><c:if test="${title!=null}"> <c:out
                                                        value="${title}"/></c:if></strong><c:if test="${message!=null}">:
                                                <c:out value="${message}"/></c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <strong>Exception:</strong> <c:out value="${exception}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${requestScope.desc!=null}">
                                        <textarea class="form-control text-danger" rows="20"
                                                  style="border-color: red; resize: none;background-color: rgba(255,6,0,0.2);font-size: smaller"
                                                  id="description" disabled>
                                        <c:out value="${requestScope.desc}"/>
                                        </textarea>
                                    </c:when>
                                    <c:when test="${type==-1}">
                                            <textarea class="form-control text-danger" rows="20"
                                                      style="border-color: red; resize: none;background-color: rgba(255,6,0,0.2);font-size: smaller"
                                                      id="exception" disabled>
                                                    ${pageContext.errorData.throwable.stackTrace}
                                            </textarea>
                                    </c:when>
                                </c:choose>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
        </div>
    </div>
    <dis:footer/>
</div>
<dis:scripts/>
</body>

</html>