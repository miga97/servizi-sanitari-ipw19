<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Login"/>
<body class="bg-gradient-primary">
<a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="<c:url value="/" />"
   style="color: rgb(255,255,255);font-size: 35px;margin-top: 20px;padding-left: 25px;">
    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-pills"></i></div>
    <div class="sidebar-brand-text mx-3"><span>Servizi Sanitari</span></div>
</a>
<div class="container">
    <div class="row justify-content-center" style="margin-top: -20px;">
        <div class="col-md-9 col-lg-12 col-xl-10">
            <div class="card shadow-lg o-hidden border-0 my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-flex">
                            <div class="flex-grow-1 bg-login-image"
                                 style="background-image: url('<c:url value="/assets/img/medica.jpg"/>');"></div>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h4 class="text-dark mb-4">Bentornato!</h4>
                                </div>
                                <form class="user" action="login" method="post">
                                    <c:if test="${requestScope.loginError != null}">
                                        <div class="alert alert-danger" role="alert">
                                            <i class="fas fa-exclamation-triangle"></i>
                                                ${requestScope.loginError}
                                        </div>
                                        <style>
                                            #login_btn {
                                                animation: shake 0.82s cubic-bezier(.36, .07, .19, .97) both;
                                                transform: translate3d(0, 0, 0);
                                                backface-visibility: hidden;
                                                perspective: 1000px;
                                            }

                                            @keyframes shake {
                                                10%, 90% {
                                                    transform: translate3d(-1px, 0, 0);
                                                }
                                                20%, 80% {
                                                    transform: translate3d(2px, 0, 0);
                                                }
                                                30%, 50%, 70% {
                                                    transform: translate3d(-4px, 0, 0);
                                                }
                                                40%, 60% {
                                                    transform: translate3d(4px, 0, 0);
                                                }
                                            }
                                        </style>
                                    </c:if>
                                    <div class="form-group"><input
                                            class="form-control form-control-user" type="email"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            placeholder="Indirizzo Email" name="username"
                                            autocomplete="email"
                                            value="${requestScope.username}" required
                                            pattern="^[\w!#$%&'*+/=?{|}~^-]+(?:\.[\w!#$%&'*+/=?{|}~^-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$">
                                    </div>
                                    <div class="form-group"><input
                                            class="form-control form-control-user"
                                            type="password" id="exampleInputPassword"
                                            autocomplete="current-password"
                                            placeholder="Password" name="password" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <div class="form-check"><input class="form-check-input custom-control-input"
                                                                           type="checkbox" id="formCheck-1" value="1"
                                                                           name="rememberme"><label
                                                    class="form-check-label custom-control-label" for="formCheck-1">Ricordami
                                                per 2 settimane</label></div>
                                        </div>
                                    </div>
                                    <button id="login_btn" class="btn btn-primary btn-block text-white btn-user"
                                            type="submit">Login
                                    </button>
                                    <hr>
                                </form>
                                <div class="text-center"><a class="small" href="<c:url value="/forgotPassword"/>">
                                    Password dimenticata?</a></div>
                                <div class="text-center"><a class="small" href="<c:url value="/register"/>">
                                    Registra un nuovo account!</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<dis:scripts/>
</body>

</html>