<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dis" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<dis:head title="Registrati"/>
<body class="bg-gradient-primary">
    <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="<c:url value="/" />" style="color: rgb(255,255,255);font-size: 35px;margin-top: 20px;padding-left: 25px;">
        <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-pills"></i></div>
        <div class="sidebar-brand-text mx-3"><span>Servizi Sanitari</span></div>
    </a>
    <div class="container" style="margin-top: -20px;">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <div class="flex-grow-1 bg-register-image" style="background-image: url('<c:url value="/assets/img/iu.jpeg"/>');"></div>
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h4 class="text-dark mb-4">Crea il tuo Account!</h4>
                            </div>
                            <form class="user" method="post" action="register">
                                <div class="form-group" style="height: 50px;"><select class="custom-select form-control-user" id="registerRoleSelector" required="" name="role" style="height: 3rem;padding: 0 1rem;"><option value="" selected="" disabled="" hidden="">Ruolo</option><option value="0">Cittadino</option><option value="1">Medico di Base</option><option value="2">Medico Specialista</option><option value="3">Servizio Sanitario Provinciale</option><option value="4">Farmacia</option></select></div>
                                <div
                                    class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0"><input class="form-control form-control-user" type="text" id="registerFirstName" placeholder="Nome" name="name" required="" autocomplete="given-name"></div>
                                    <div class="col-sm-6"><input class="form-control form-control-user" type="text" id="registerSurname" placeholder="Cognome" name="surname" disabled="" autocomplete="family-name"></div>
                        </div>
                        <div class="form-group" style="height: 50px;"><input class="form-control form-control-user" type="email" id="registerInputEmail" aria-describedby="emailHelp" placeholder="Indirizzo Email" name="email" required="" pattern="^[\w!#$%&amp;'*+/=?{|}~^-]+(?:\.[\w!#$%&amp;'*+/=?{|}~^-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$"
                                                                        autocomplete="email"></div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0"><input class="form-control form-control-user first-password" type="password" id="registerPasswordInput" placeholder="Password" name="password" required="" autocomplete="new-password"></div>
                            <div class="col-sm-6"><input class="form-control form-control-user repeat-password" type="password" id="registerPasswordInput2" placeholder="Ripeti Password" name="password2" required="" autocomplete="new-password"></div>
                        </div>
                        <div class="form-group" style="height: 50px;">
                            <select class="custom-select form-control-user" id="registerProvinceSelector" name="province" style="padding: 0 1rem;height: 3rem;" required="">
                                <option value="" selected hidden disabled>Provincia di residenza</option>
                                <c:forEach var="province" items="${applicationScope.provinces}">
                                    <option value="${province.id}">${province.name} (${province.acronym})</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div
                            class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0" id="registerGroupBirthTown"><input class="form-control form-control-user" type="text" id="registerBirthTown" name="birthTown" placeholder="Comune di Nascita" disabled=""></div>
                            <div class="col-sm-6" id="registerGroupBirthDate"><input class="form-control form-control-user birthDate" id="registerBirthDate" type="date" name="birthDate" placeholder="Data di Nascita" disabled="" step="1 day" autocomplete="bday"></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0"><input class="form-control form-control-user" type="text" id="registerCF" placeholder="Codice Fiscale" name="cf" minlength="16" maxlength="16" disabled=""></div>
                        <div class="col-sm-6"><select class="custom-select form-control-user" id="registerGenderSelector" name="gender" style="padding: 0 1rem;height: 3rem;" autocomplete="sex" disabled=""><option value="" selected="" hidden="" disabled="">Sesso</option><option value="A">Altro</option><option value="M">Maschio</option><option value="F">Femmina</option></select></div>
                    </div><button class="btn btn-primary btn-block text-white btn-user" id="registerSubmit" type="submit">Registrati</button>
                    <hr>
                    </form>
                    <div class="text-center"><a class="small" href="<c:url value="login"/>">Hai già un account? Effettua il login!</a></div>
                    <div class="text-center"><a class="small" href="<c:url value="privacy"/>">Informativa sulla privacy</a></div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <dis:scripts/>
</body>

</html>