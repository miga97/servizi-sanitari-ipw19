package it.disorganizzazione.progettoweb.utils;


import it.disorganizzazione.progettoweb.persistence.entities.users.User;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Properties;

public class Mailer {
    //Singleton
    private static final String host = "smtp.gmail.com";
    private static final String port = "587";
    private static final String username = "disorganizzazioneXIII@gmail.com";
    private static final String password = "owqrznoblokujvak";
    private static Session session = null;
    public static final Mailer Instance = new Mailer(host, port, username, password);

    private Mailer(String host, String port, String username, String password) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        //props.put("mail.debug","true");
        // Get the Session object.
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
    }

    public void Send(@NotNull User recipient, @NotNull String subject, @NotNull String text) throws MessagingException {
        // Create a default MimeMessage object.
        Message message = new MimeMessage(session);

        // Set From: header field of the header.
        message.setFrom(new InternetAddress(username));

        // Set To: header field of the header.
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient.getEmail()));

        // Set Subject: header field
        message.setSubject(subject);

        // Now set the actual message
        message.setText(text);

        // Send message
        Transport.send(message);
    }

    public static String getServerURL(HttpServletRequest request) {
        return request.getScheme() + "://"
                + request.getServerName()
                + (request.getServerPort() == 80 || request.getServerPort() == 443 ? "" : ":" + request.getServerPort());
    }
}
