/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sanitizer {
    /**
     * Use this to prevent XSS
     *
     * @param string that should be sanitized
     * @return sanitized string
     */
    public static String sanitize(String string) {
        if (string != null) {
            return string
                    .replaceAll("(?i)<script.*?>.*?</script.*?>", "")   // case 1
                    .replaceAll("(?i)<.*?javascript:.*?>.*?</.*?>", "") // case 2
                    .replaceAll("(?i)<.*?\\s+on.*?>.*?</.*?>", "");     // case 3
        } else return null;
    }

    public static String getSanitizedParameter(HttpServletRequest request, String parameter) {
        if (request.getParameter(parameter) != null) {
            return sanitize(request.getParameter(parameter));
        } else return null;
    }

    public static String[] getSanitizedValues(HttpServletRequest request, String parameter) {
        if (request.getParameter(parameter) != null) {
            List<String> toReturn = new ArrayList<>();
            String[] lines = request.getParameterValues(parameter);
            for (String line : lines) {
                toReturn.add(sanitize(line));
            }
            Object[] gfg = toReturn.toArray();
            String[] str = Arrays.copyOf(gfg,
                    gfg.length,
                    String[].class);
            return str;
        } else return null;
    }
}
