package it.disorganizzazione.progettoweb.utils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;

public class Feedback {

    public static final String SUCCESS = "btn-success";
    public static final String ERROR = "btn-danger";
    public static final String INFO = "btn-info";
    public static final String WARNING = "btn-warning";

    private String type;
    private String title;
    private String message;
    private String link;

    public Feedback(@NotNull String type, @NotNull String title, @NotNull String message, @NotNull String link) {
        this.type = type;
        this.title = title;
        this.message = message;
        this.link = link;
    }

    public static Feedback error() {
        return new Feedback(ERROR, "", "", "");
    }

    public static Feedback warning() {
        return new Feedback(WARNING, "", "", "");
    }

    public static Feedback info() {
        return new Feedback(INFO, "", "", "");
    }

    public static Feedback success() {
        return new Feedback(SUCCESS, "", "", "");
    }

    public static Feedback invalidRequestError(String link) {
        String title = "Richiesta non valida";
        String message = "Non siamo riusciti a portare a termine la richiesta perchè era un po' strana... Forse abbiamo perso qualche informazione per strada, riprova ancora!";
        return new Feedback(ERROR, title, message, link);
    }

    public static Feedback serverError(String link) {
        String title = "Errore del server";
        String message = "Sembra che il server stia avendo problemi... Mentre noi spegniamo e riaccendiamo tutto, tu riprova ad eseguire le tue operazioni";
        return new Feedback(ERROR, title, message, link);
    }

    public static Feedback mailerError(String link) {
        String title = "Errore del sistema email";
        String message = "Sembra che le nostre scimmie dattilografe abbiano smesso di lavorare al sistema email... Contatta al più presto l'amministratore del sito che provvederà a rifornirle di noccioline";
        return new Feedback(ERROR, title, message, link);
    }

    public static Feedback requestSuccess(String link) {
        String title = "Operazione completata";
        String message = "Fantastico, è andato tutto per il verso giusto! Non dimenticare che anche i più piccoli successi nella vita accadono sempre per merito tuo";
        return new Feedback(SUCCESS, title, message, link);
    }

    public static Feedback registrationSuccess(String link) {
        String title = "Registrazione completata";
        String message = "Fantastico, adesso anche tu sei parte della grande famiglia dei Servizi Sanitari! Mentre noi ricamiamo il tuo nome nel nostro enorme arazzo celebrativo, tu puoi entrare nella tua area riservata facendo login";
        return new Feedback(SUCCESS, title, message, link);
    }

    public static Feedback emailSentSuccess(String link) {
        String title = "Email inviata";
        String message = "Operazione completata con successo! Controlla subito la posta elettronica per recuperare l'email che ti abbiamo inviato. Se non la ricevi, ricordati di controllare se è finita tra nello spam, altrimenti riprova ad effettuare la procedura o contatta l'amministratore del sito";
        return new Feedback(SUCCESS, title, message, link);
    }

    public static Feedback invalidTokenWarning(String link) {
        String title = "Token scaduto";
        String message = "Sembra che hai aspettato troppo per cambiare la password oppure hai richiesto nuovamente di cambiarla, dovresti chiedere un nuovo link!";
        return new Feedback(WARNING, title, message, link);
    }

    public static Feedback unknownUserWarning(String link) {
        String title = "Utente non registrato";
        String message = "Non ci risulta che questo utente sia registrato. Sei sicuro di non aver commesso qualche errore? Riprova ancora per favore oppure resistrati!";
        return new Feedback(WARNING, title, message, link);
    }

    public static Feedback existingUserWarning(String link) {
        String title = "Utente già registrato";
        String message = "Ci risulta che questo utente sia già registrato... Sei sicuro di non aver commesso qualche errore? Riprova ancora per favore oppure recupera la tua password!";
        return new Feedback(WARNING, title, message, link);
    }

    public void forward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("feedback", this);
        request.getRequestDispatcher("/WEB-INF/feedback.jsp").forward(request, response);
    }

    public Feedback setType(@NotNull String type) {
        this.type = type;
        return this;
    }

    public Feedback setTitle(@NotNull String title) {
        this.title = title;
        return this;
    }

    public Feedback setMessage(@NotNull String message) {
        this.message = message;
        return this;
    }

    public Feedback setLink(@NotNull String link) {
        this.link = link;
        return this;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getLink() {
        return link;
    }
}
