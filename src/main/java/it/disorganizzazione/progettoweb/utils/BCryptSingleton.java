/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.utils;

import org.mindrot.jbcrypt.BCrypt;

public class BCryptSingleton {

    public static final BCryptSingleton Instance = new BCryptSingleton(11);

    private final int logRounds;

    private BCryptSingleton(int logRounds) {
        this.logRounds = logRounds;
    }

    public String hash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(logRounds));
    }

    public boolean verifyHash(String password, String hash) {
        return BCrypt.checkpw(password, hash);
    }
}
