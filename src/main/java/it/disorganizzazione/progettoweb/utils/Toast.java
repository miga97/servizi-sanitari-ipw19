package it.disorganizzazione.progettoweb.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;

public class Toast {
    public static final String SUCCESS = "#1cc88a";
    public static final String ERROR = "#e74a3b";
    public static final String INFO = "#36b9cc";
    public static final String WARNING = "#f6c2e3";

    public static final String ENVELOPE = "fa-envelope";
    public static final String WRONG = "fa-times-circle";

    private String type = INFO;
    private String icon = ENVELOPE;
    private String title;
    private String message;
    private String link = "#";

    public Toast(String type, String icon, String title, String message, String link) {
        this.type = type;
        this.icon = icon;
        this.title = title;
        this.message = message;
        this.link = link;
    }

    public Toast(String title, String message, String link) {
        this.title = title;
        this.message = message;
        this.link = link;
    }

    public Toast(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public static Toast successInfoChange(String title) {
        String message = "Informazioni modificate correttamente";
        return new Toast(title, message);
    }

    public static Toast successDrugDelete() {
        String title = "Acquisto";
        String message = "Ricette cancellate correttamente";
        return new Toast(title, message);
    }

    public static Toast successDrugDelete(String message) {
        String title = "Acquisto";
        return new Toast(title, message);
    }

    public static Toast successDrugAdd() {
        String title = "Acquisto";
        String message = "Ricetta aggiunta correttamente";
        return new Toast(title, message);
    }

    public static Toast successPrescription(String title) {
        String message = "Prescrizione inserita correttamente";
        return new Toast(title, message);
    }

    public static Toast successCompletion(String title) {
        String message = "Prescrizione erogata correttamente";
        return new Toast(title, message);
    }

    public static Toast wrongPassword() {
        String title = "Password incorretta";
        String message = "La vecchia password inserita non corrisponde.";
        return new Toast(ERROR, WRONG, title, message, "#");
    }

    public void redirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setAttribute("flash.toast", this);
        response.sendRedirect(response.encodeRedirectURL(link));
    }

    public Toast setType(@NotNull String type) {
        this.type = type;
        return this;
    }

    public Toast setIcon(@NotNull String icon) {
        this.icon = icon;
        return this;
    }

    public Toast setTitle(@NotNull String title) {
        this.title = title;
        return this;
    }

    public Toast setMessage(@NotNull String message) {
        this.message = message;
        return this;
    }

    public Toast setLink(@NotNull String link) {
        this.link = link;
        return this;
    }

    public String getType() {
        return type;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getLink() {
        return link;
    }
}
