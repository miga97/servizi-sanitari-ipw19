package it.disorganizzazione.progettoweb.servlets.pharmacy;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.users.Pharmacy;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Toast;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SellServlet extends HttpServlet {
    private DrugPrescriptionDAO drugPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            drugPrescriptionDAO = daoFactory.getDAO(DrugPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<DrugPrescriptionView> receipt = (List<DrugPrescriptionView>) Objects.requireNonNull(request.getSession().getAttribute("receipt"));
            float totalCost = 0;
            for (DrugPrescriptionView dpv : receipt) {
                totalCost += dpv.getDrugPrescription().getCost();
            }
            request.setAttribute("cost", totalCost);
            request.getRequestDispatcher("/WEB-INF/pharmacy/sell.jsp").forward(request, response);
        } catch (NullPointerException ex) { /* No drugs selected */
            response.sendRedirect(response.encodeRedirectURL("home"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (request.getParameterMap().containsKey("delete")) {
                request.getSession().removeAttribute("receipt");
                Toast.successDrugDelete().setLink("home").redirect(request,response);
                return;
            }
            if (request.getParameterMap().containsKey("forward")) {
                Pharmacy pharmacy = (Pharmacy) Objects.requireNonNull(request.getSession().getAttribute("user"));
                List<DrugPrescriptionView> receipt = (List<DrugPrescriptionView>) Objects.requireNonNull(request.getSession().getAttribute("receipt"));
                for (DrugPrescriptionView dpv : receipt) {
                    DrugPrescription dp = dpv.getDrugPrescription();
                    if (!dp.isComplete()) {
                        dp.setPerformerId(pharmacy.getId());
                        dp.setCompletionTimestamp(new Timestamp(System.currentTimeMillis()));
                        drugPrescriptionDAO.complete(dp);
                    }
                }
                request.getSession().removeAttribute("receipt");
                Toast.successCompletion("Acquisto").setLink("home").redirect(request,response);
                return;
            }
            PrescriptionId pid = PrescriptionId.getFromRequest(request);
            DrugPrescriptionView drugPrescriptionView = Objects.requireNonNull(drugPrescriptionDAO.getViewByPrimaryKey(pid));
            if (!drugPrescriptionView.getDrugPrescription().isComplete()) {
                List<DrugPrescriptionView> receipt;
                try {
                    receipt = (List<DrugPrescriptionView>) Objects.requireNonNull(request.getSession().getAttribute("receipt"));
                } catch (NullPointerException ex) { /* No drugs selected */
                    response.sendRedirect(response.encodeRedirectURL("home"));
                    return;
                }
                List<DrugPrescriptionView> selected = new ArrayList<>();
                selected.addAll(receipt);
                for (DrugPrescriptionView drugPVSelected : selected) {
                    if (drugPVSelected.equals(drugPrescriptionView))
                        receipt.remove(drugPVSelected);
                }
                if (!receipt.isEmpty()) {
                    request.getSession().setAttribute("receipt", receipt);
                    Toast.successDrugDelete("Ricetta cancellata correttamente").setLink("sell").redirect(request,response);
                } else {
                    request.getSession().removeAttribute("receipt");
                    Toast.successDrugDelete("Ricetta cancellata correttamente").setLink("home").redirect(request,response);
                }

            } else throw new IllegalArgumentException("Invalid prescription");
        } catch (NullPointerException | IllegalArgumentException ex) {
            ex.printStackTrace();
            Feedback.invalidRequestError("home").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
