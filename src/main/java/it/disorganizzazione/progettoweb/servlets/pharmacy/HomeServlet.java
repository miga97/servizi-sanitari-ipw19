package it.disorganizzazione.progettoweb.servlets.pharmacy;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Toast;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HomeServlet extends HttpServlet {
    private DrugPrescriptionDAO drugPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            drugPrescriptionDAO = daoFactory.getDAO(DrugPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/pharmacy/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameterMap().containsKey("delete")) {
            request.getSession().removeAttribute("receipt");
            Toast.successDrugDelete().setLink("home").redirect(request,response);
            return;
        }
        if (request.getParameterMap().containsKey("forward")) {
            response.sendRedirect(response.encodeRedirectURL("sell"));
            return;
        }
        try {
            PrescriptionId pid = PrescriptionId.getFromRequest(request);
            DrugPrescriptionView drugPrescriptionView = Objects.requireNonNull(drugPrescriptionDAO.getViewByPrimaryKey(pid));

            if (!drugPrescriptionView.getDrugPrescription().isComplete()) {
                List<DrugPrescriptionView> receipt;
                try {
                    receipt = (List<DrugPrescriptionView>) Objects.requireNonNull(request.getSession().getAttribute("receipt"));
                } catch (NullPointerException ex) { /* No drugs selected */
                    receipt = new ArrayList<>();
                }
                receipt.add(drugPrescriptionView);
                request.getSession().setAttribute("receipt", receipt);
                Toast.successDrugAdd().setLink("home").redirect(request,response);

            } else throw new IllegalArgumentException("Invalid prescription");
        } catch (NullPointerException | IllegalArgumentException ex) {
            ex.printStackTrace();
            Feedback.invalidRequestError("home").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
