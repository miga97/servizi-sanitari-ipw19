package it.disorganizzazione.progettoweb.servlets.doctor;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ExamPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.PhotoDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Mailer;
import it.disorganizzazione.progettoweb.utils.Sanitizer;
import it.disorganizzazione.progettoweb.utils.Toast;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that handles the visit and medical record.
 *
 * @author Matteo Rizzi
 */
@WebServlet(name = "MedicalRecords")
public class VisitServlet extends HttpServlet {
    private PhotoDAO photoDAO;
    private VisitPrescriptionDAO visitPrescriptionDAO;
    private ExamPrescriptionDAO examPrescriptionDAO;
    private DrugPrescriptionDAO drugPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            photoDAO = daoFactory.getDAO(PhotoDAO.class);
            visitPrescriptionDAO = daoFactory.getDAO(VisitPrescriptionDAO.class);
            examPrescriptionDAO = daoFactory.getDAO(ExamPrescriptionDAO.class);
            drugPrescriptionDAO = daoFactory.getDAO(DrugPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            Citizen patient = (Citizen) Objects.requireNonNull(request.getSession().getAttribute("patient"));
            Photo patientPhoto = photoDAO.getLastByUserId(patient.getId());
            request.setAttribute("patientPhoto", patientPhoto != null ? patientPhoto : Photo.defaultPhoto());
            request.getRequestDispatcher("/WEB-INF/doctor/visit.jsp").forward(request, response);
        } catch (NullPointerException ex) { // No patient selected
            response.sendRedirect(response.encodeRedirectURL("home"));
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameterMap().containsKey("cancel")) {
            request.getSession().removeAttribute("patient");
            response.sendRedirect(response.encodeRedirectURL("home"));
        } else if (request.getParameterMap().containsKey("send")) {
            try {
                Doctor doctor = (Doctor) Objects.requireNonNull(request.getSession().getAttribute("user"));
                Citizen patient = (Citizen) Objects.requireNonNull(request.getSession().getAttribute("patient"));
                String[] exams = Sanitizer.getSanitizedValues(request, "exams[]");
                String[] visits = Sanitizer.getSanitizedValues(request, "visits[]");
                String anamnesi = Sanitizer.getSanitizedParameter(request, "anamnesi");
                String[] drugs = Sanitizer.getSanitizedValues(request, "drugs");

                //This visit of the doctor
                if (anamnesi != null && !anamnesi.trim().isEmpty()) {
                    VisitPrescription doctorVisit = new VisitPrescription();
                    doctorVisit.setPlannerId(doctor.getId());
                    doctorVisit.setPatientId(patient.getId());
                    doctorVisit.setCost(0f);
                    doctorVisit.setCompletionTimestamp(doctorVisit.getPrescriptionTimestamp());
                    doctorVisit.setPerformerId(doctor.getId());
                    doctorVisit.setAnamnesi(anamnesi);
                    visitPrescriptionDAO.insert(doctorVisit);
                }
                // Prescription of drugs
                if (drugs != null) {
                    for (String drug : drugs) {
                        DrugPrescription drugPrescription = new DrugPrescription();
                        drugPrescription.setPlannerId(doctor.getId());
                        drugPrescription.setPatientId(patient.getId());
                        drugPrescription.setDescription(drug);
                        drugPrescriptionDAO.insert(drugPrescription);
                    }
                }
                // Prescription of visits
                if (visits != null) {
                    for (String visitId : visits) {
                        VisitPrescription visitPrescription = new VisitPrescription();
                        visitPrescription.setPlannerId(doctor.getId());
                        visitPrescription.setPatientId(patient.getId());
                        visitPrescription.setVisitId(Integer.parseInt(visitId));
                        visitPrescriptionDAO.insert(visitPrescription);
                    }
                }
                // Prescriptions of exams
                if (exams != null) {
                    for (String exam : exams) {
                        ExamPrescription examPrescription = new ExamPrescription();
                        examPrescription.setPlannerId(doctor.getId());
                        examPrescription.setPatientId(patient.getId());
                        examPrescription.setExamId(Integer.parseInt(exam));
                        examPrescriptionDAO.insert(examPrescription);
                    }
                }

                Toast.successCompletion("Visita di base").setLink("home").redirect(request, response);
                String link = Mailer.getServerURL(request) + request.getContextPath() + "/login";
                Mailer.Instance.Send(patient, "Hai delle nuove prescrizioni", "Gentile " + patient.getName() + " " + patient.getSurname() + ",\nil Sistema Sanitario vuole comunicarLe che ci sono nuove prescrizioni da visualizzare.\nAcceda al sito per prenderne visione:\n" + link);

            } catch (NullPointerException | NumberFormatException ex) { // Some fields are null or invalid null
                Feedback.invalidRequestError("visit").forward(request, response);
            } catch (DAOException ex) {
                ex.printStackTrace();
                Feedback.serverError("/").forward(request, response);
            } catch (MessagingException e) {
                e.printStackTrace();
                Feedback.mailerError("/").forward(request, response);
            }
        }
    }
}
