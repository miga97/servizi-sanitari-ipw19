package it.disorganizzazione.progettoweb.servlets.doctor;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ExamPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.PhotoDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.utils.Feedback;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class HomeServlet extends HttpServlet {

    private CitizenDAO citizenDAO;
    private VisitPrescriptionDAO visitPrescriptionDAO;
    private ExamPrescriptionDAO examPrescriptionDAO;
    private DrugPrescriptionDAO drugPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            visitPrescriptionDAO = daoFactory.getDAO(VisitPrescriptionDAO.class);
            examPrescriptionDAO = daoFactory.getDAO(ExamPrescriptionDAO.class);
            drugPrescriptionDAO = daoFactory.getDAO(DrugPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Doctor doctor = (Doctor) request.getSession().getAttribute("user");
        try {
            // Attività recenti del dottore
            List<VisitPrescriptionView> visitView = visitPrescriptionDAO.getViewByPlannerId(doctor.getId(),null);
            List<ExamPrescriptionView> examView = examPrescriptionDAO.getViewByPlannerId(doctor.getId(),null);
            List<DrugPrescriptionView> drugView = drugPrescriptionDAO.getViewByPlannerId(doctor.getId(),null);
            if (visitView != null && !visitView.isEmpty())
                request.setAttribute("visitPrescriptionView", visitView.get(visitView.size()-1));
            if (examView != null && !examView.isEmpty())
                request.setAttribute("examPrescriptionView", examView.get(examView.size()-1));
            if (drugView != null && !drugView.isEmpty())
                request.setAttribute("drugPrescriptionView", drugView.get(drugView.size()-1));
            request.getRequestDispatcher("/WEB-INF/doctor/home.jsp").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Doctor doctor = (Doctor) request.getSession().getAttribute("user");
        try {
            int patientId = Integer.parseInt(request.getParameter("patientId"));
            Citizen patient = citizenDAO.getByPrimaryKey(patientId);
            if (patient.getDoctorId().equals(doctor.getId())) {
                request.getSession().setAttribute("patient", patient);
                response.sendRedirect(response.encodeRedirectURL("visit"));
            } else {
                throw new IllegalArgumentException("Invalid patient");
            }
        } catch (NullPointerException | IllegalArgumentException ex) {
            Feedback.invalidRequestError("home").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
