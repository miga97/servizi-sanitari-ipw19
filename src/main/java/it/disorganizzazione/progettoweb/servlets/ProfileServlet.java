
/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.servlets;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.PhotoDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ProvinceDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.DoctorDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;
import it.disorganizzazione.progettoweb.utils.BCryptSingleton;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Sanitizer;
import it.disorganizzazione.progettoweb.utils.Toast;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@MultipartConfig
public class ProfileServlet extends HttpServlet {
    private UserDAO userDao;
    private CitizenDAO citizenDAO;
    private ProvinceDAO provinceDAO;
    private DoctorDAO doctorDAO;
    private PhotoDAO photoDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            userDao = daoFactory.getDAO(UserDAO.class);
            provinceDAO = daoFactory.getDAO(ProvinceDAO.class);
            doctorDAO = daoFactory.getDAO(DoctorDAO.class);
            photoDAO = daoFactory.getDAO(PhotoDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            User user = (User) request.getSession().getAttribute("user");
            request.setAttribute("doctors", doctorDAO.getByProvince(user.getProvince()));
            request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("user");
        try {
            String type = request.getParameter("type");
            switch (type) {
                case "changeInfo":
                    updateGeneral(request, response);
                    break;
                case "changePassword":
                    updatePassword(request, response);
                    break;
                case "changeDoctor":
                    updateDoctor(request, response);
                    break;
                case "changePhoto":
                    updatePhoto(request, response);
                    break;
                default:
                    throw new NullPointerException("Invalid type");
            }
        } catch (NullPointerException | NumberFormatException ex) { // Some fields are null or invalid null
            ex.printStackTrace();
            Feedback.invalidRequestError("profile").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.existingUserWarning("profile").forward(request, response);
        } finally {
            // Rollback mechanism to restore correct infos of the user, including the hashed password
            try {
                if (user instanceof Citizen) {
                    request.getSession().setAttribute("user", citizenDAO.getByPrimaryKey(user.getId()));
                } else {
                    request.getSession().setAttribute("user", userDao.getByPrimaryKey(user.getId()));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private String getExtensionByStringHandling(String filename) {
        return filename.substring(filename.lastIndexOf(".") +1);
    }

    private void updatePhoto(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, DAOException {
        Part filePart = Objects.requireNonNull(request.getPart("file")); // Retrieves <input type="file" name="file">
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        String extension = getExtensionByStringHandling(fileName);
        String contentType = filePart.getContentType();
        User user = ((User) Objects.requireNonNull(request.getSession().getAttribute("user")));
        if ((contentType.equals("image/jpeg") || contentType.equals("image/png")) && (fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png"))) {
            long timestamp = System.currentTimeMillis();
            Timestamp date = new Timestamp(timestamp);
            String folder = "photos" + File.separator + user.getId() + File.separator;
            fileName = timestamp + "." + extension;
            (new File(getServletContext().getRealPath("") + folder)).mkdirs();
            OutputStream out = null;
            InputStream filecontent = null;
            File f = new File(getServletContext().getRealPath("") + folder + fileName);
            f.createNewFile();
            try {
                out = new FileOutputStream(f);
                filecontent = filePart.getInputStream();
                int read = 0;
                final byte[] bytes = new byte[1024];
                while ((read = filecontent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
            } catch (FileNotFoundException fne) {
                System.out.println("fnf");
            } finally {
                if (out != null) {
                    out.close();
                }
                if (filecontent != null) {
                    filecontent.close();
                }
            }
            Photo p = new Photo();
            p.setExtension(extension);
            p.setCreationTimestamp(date);
            p.setUserId(user.getId());
            if(user instanceof Citizen){
                photoDAO.insert(p);
            } else {
                photoDAO.replaceLast(p);
            }
            request.getSession().setAttribute("userPhoto", p);
            Toast.successInfoChange("Foto profilo").redirect(request, response);
        }
    }

    private void updateDoctor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DAOException {
        User user = (User) Objects.requireNonNull(request.getSession().getAttribute("user"));
        int doctorId = Integer.parseInt(request.getParameter("newDoctor"));
        // Doctor validation
        Objects.requireNonNull(doctorDAO.getByPrimaryKey(doctorId)); // Throw exception if not present
        if (doctorId == user.getId()) throw new NullPointerException("Doctor is not valid"); // Doctor of himself? Nope
        if (user instanceof Citizen) {
            ((Citizen) user).setDoctorId(doctorId);
            citizenDAO.update((Citizen) user);
        } else {
            throw new NullPointerException("User is not a citizen");
        }
        Toast.successInfoChange("Medico di Base").redirect(request, response);
    }

    private void updatePassword(HttpServletRequest request, HttpServletResponse response) throws DAOException, ServletException, IOException {
        User user = (User) Objects.requireNonNull(request.getSession().getAttribute("user"));
        String oldPassword = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "oldPassword"));
        String newPassword = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "password"));
        if (BCryptSingleton.Instance.verifyHash(oldPassword, user.getPassword())) {
            user.setPassword(newPassword);
            userDao.updatePassword(user);
            Toast.successInfoChange("Password").redirect(request, response);
        } else {
            Toast.wrongPassword().redirect(request, response);
        }
    }

    private void updateGeneral(HttpServletRequest request, HttpServletResponse response) throws DAOException, ServletException, IOException {
        User user = (User) Objects.requireNonNull(request.getSession().getAttribute("user"));
        String email = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "email"));
        String name = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "name"));
        int province = Integer.parseInt(request.getParameter("province"));
        // Province validation
        Objects.requireNonNull(provinceDAO.getByPrimaryKey(province)); // Throw exception if not present
        //Email Validation - reference n5 : https://howtodoinjava.com/regex/java-regex-validate-email-address/
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) throw new NullPointerException("Email not valid");
        // End of validations
        user.setName(name);
        user.setEmail(email);
        user.setProvince(province);
        if (user instanceof Citizen) {
            String surname = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "surname"));
            String gender = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "gender"));
            String cf = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "cf"));
            Date birthDate = Date.valueOf(Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "birthDate")));
            String birthTown = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "birthTown"));
            ((Citizen) user).setSurname(surname);
            ((Citizen) user).setGender(gender);
            ((Citizen) user).setCf(cf);
            ((Citizen) user).setBirthDate(birthDate);
            ((Citizen) user).setBirthTown(birthTown);
            citizenDAO.update((Citizen) user);
        } else {
            userDao.update(user);
        }
        Toast.successInfoChange("Dettagli Personali").redirect(request, response);
    }
}
