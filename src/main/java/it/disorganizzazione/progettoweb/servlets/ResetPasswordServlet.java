package it.disorganizzazione.progettoweb.servlets;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.PasswordRecoveryDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;
import it.disorganizzazione.progettoweb.persistence.entities.PasswordRecovery;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Sanitizer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that handles the password reset action.
 *
 * @author Matteo Rizzi
 */
@WebServlet(name = "resetPassword")
public class ResetPasswordServlet extends HttpServlet {

    private UserDAO userDao;
    private PasswordRecoveryDAO passwordRecoveryDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            passwordRecoveryDAO = daoFactory.getDAO(PasswordRecoveryDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            String token = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "token"));
            PasswordRecovery result = passwordRecoveryDAO.getByToken(token);
            if (result == null || !result.isValid()) {
                if (result != null) passwordRecoveryDAO.deleteByPrimaryKey(result.getUserId());
                Feedback.invalidTokenWarning("forgotPassword").forward(request, response);
            } else {
                request.setAttribute("token", token);
                request.getRequestDispatcher("/WEB-INF/reset-password.jsp").forward(request, response);
            }
        } catch (NullPointerException ex) { // The token is not present in the parameters
            Feedback.invalidRequestError("forgotPassword").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            String password = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "password"));
            String token = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "token"));
            PasswordRecovery result = passwordRecoveryDAO.getByToken(token);
            if (result == null || !result.isValid()) {
                Feedback.invalidTokenWarning("forgotPassword").forward(request, response);
            } else {
                User user = userDao.getByPrimaryKey(result.getUserId());
                user.setPassword(password);
                userDao.updatePassword(user);
                //token used
                passwordRecoveryDAO.deleteByPrimaryKey(result.getUserId());
                Feedback.requestSuccess("login").forward(request, response);
            }
        } catch (NullPointerException ex) { // Password o token null
            Feedback.invalidRequestError("forgotPassword").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
