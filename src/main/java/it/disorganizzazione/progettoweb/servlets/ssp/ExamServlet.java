package it.disorganizzazione.progettoweb.servlets.ssp;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.*;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Ssp;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Mailer;
import it.disorganizzazione.progettoweb.utils.Sanitizer;
import it.disorganizzazione.progettoweb.utils.Toast;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Objects;

public class ExamServlet extends HttpServlet {
    private ExamDAO examDAO;
    private ExamPrescriptionDAO examPrescriptionDAO;
    private CitizenDAO citizenDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            examDAO = daoFactory.getDAO(ExamDAO.class);
            examPrescriptionDAO = daoFactory.getDAO(ExamPrescriptionDAO.class);
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            ExamPrescription examPrescription = (ExamPrescription) Objects.requireNonNull(request.getSession().getAttribute("examPrescription"));
            request.setAttribute("examPrescriptionName", examDAO.getByPrimaryKey(examPrescription.getExamId()).getName());
            request.getRequestDispatcher("/WEB-INF/ssp/exam.jsp").forward(request, response);
        } catch (NullPointerException ex) { // No patient selected
            response.sendRedirect(response.encodeRedirectURL("home"));
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameterMap().containsKey("cancel")) {
            request.getSession().removeAttribute("examPrescription");
            response.sendRedirect(response.encodeRedirectURL("home"));
        } else if (request.getParameterMap().containsKey("send")) {
            try {
                Ssp ssp = (Ssp) Objects.requireNonNull(request.getSession().getAttribute("user"));
                ExamPrescription examPrescription = (ExamPrescription) Objects.requireNonNull(request.getSession().getAttribute("examPrescription"));
                String results = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "results"));
                Citizen patient = citizenDAO.getByPrimaryKey(examPrescription.getPatientId());

                //This exam performed by the ssp
                if (!results.trim().isEmpty()) {
                    examPrescription.setPerformerId(ssp.getId());
                    examPrescription.setResults(results);
                    examPrescription.setCompletionTimestamp(new Timestamp(System.currentTimeMillis()));
                    examPrescriptionDAO.complete(examPrescription);
                } else throw new NullPointerException("Empty results string");

                request.getSession().removeAttribute("examPrescription");

                Toast.successCompletion("Esame").setLink("home").redirect(request, response);
                String link = getServerURL(request) + request.getContextPath() + "/login";
                Mailer.Instance.Send(patient,"Hai un nuovo risultato d'esame","Gentile "+patient.getName()+" "+patient.getSurname()+",\nil Sistema Sanitario vuole comunicarLe che ci sono nuovi esami da visualizzare.\nAcceda al sito per prenderne visione:\n"+link);
            } catch (NullPointerException | NumberFormatException ex) { // Some fields are null or invalid null
                Feedback.invalidRequestError("exam").forward(request, response);
            } catch (DAOException ex) {
                ex.printStackTrace();
                Feedback.serverError("/").forward(request, response);
            }catch (MessagingException ex) {
                ex.printStackTrace();
                Feedback.mailerError("/").forward(request, response);
            }
        }
    }

    private String getServerURL(HttpServletRequest request) {
        return request.getScheme() + "://"
                + request.getServerName()
                + (request.getServerPort() == 80 || request.getServerPort() == 443 ? "" : ":" + request.getServerPort());
    }
}
