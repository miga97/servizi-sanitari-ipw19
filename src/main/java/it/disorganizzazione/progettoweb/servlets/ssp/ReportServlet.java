package it.disorganizzazione.progettoweb.servlets.ssp;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.ProvinceDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Ssp;
import it.disorganizzazione.progettoweb.utils.Feedback;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class ReportServlet extends HttpServlet {

    private ProvinceDAO provinceDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            provinceDAO = daoFactory.getDAO(ProvinceDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Ssp ssp = (Ssp) Objects.requireNonNull(request.getSession().getAttribute("user"));
        try {
            // We need the ssp province for the API (that uses the acronym of the province)
            request.setAttribute("sspProvinceAcronym", provinceDAO.getByPrimaryKey(ssp.getProvince()).getAcronym());
            request.getRequestDispatcher("/WEB-INF/ssp/report.jsp").forward(request, response);
        } catch (DAOException e) {
            e.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
