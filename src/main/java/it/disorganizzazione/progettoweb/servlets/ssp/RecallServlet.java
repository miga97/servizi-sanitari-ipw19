package it.disorganizzazione.progettoweb.servlets.ssp;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.*;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.Prescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Ssp;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Mailer;
import it.disorganizzazione.progettoweb.utils.Toast;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Objects;

public class RecallServlet extends HttpServlet {

    private CitizenDAO citizenDAO;
    private VisitDAO visitDAO;
    private ExamPrescriptionDAO examPrescriptionDAO;
    private VisitPrescriptionDAO visitPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            visitDAO = daoFactory.getDAO(VisitDAO.class);
            examPrescriptionDAO = daoFactory.getDAO(ExamPrescriptionDAO.class);
            visitPrescriptionDAO = daoFactory.getDAO(VisitPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ssp/recall.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // Controlli
            Ssp ssp = (Ssp) request.getSession().getAttribute("user");
            int minAge = Integer.parseInt(request.getParameter("minAge"));
            int maxAge = Integer.parseInt(request.getParameter("maxAge"));
            List<Citizen> citizens = citizenDAO.getByProvince(ssp.getProvince());
            Prescription prescription;
            if (request.getParameter("exam") != null) {
                prescription = new ExamPrescription();
                int examId = Integer.parseInt(request.getParameter("exam"));
                Objects.requireNonNull(visitDAO.getByPrimaryKey(examId)); // Throw ex if not in-range
                ((ExamPrescription) prescription).setExamId(examId);
            } else if (request.getParameter("visit") != null) {
                prescription = new VisitPrescription();
                int visitId = Integer.parseInt(request.getParameter("visit"));
                Objects.requireNonNull(visitDAO.getByPrimaryKey(visitId)); // Throw ex if not in-range
                ((VisitPrescription) prescription).setVisitId(visitId);
            } else throw new NullPointerException();
            prescription.setCost(0.0f);
            prescription.setPlannerId(ssp.getId());
            // Inserimento
            String link = Mailer.getServerURL(request) + request.getContextPath() + "/login";
            for (Citizen citizen : citizens) {
                int age = Period.between(citizen.getBirthDate().toLocalDate(), LocalDate.now()).getYears();
                if (minAge <= age && age <= maxAge) {
                    prescription.setPatientId(citizen.getId());
                    if (prescription instanceof VisitPrescription) {
                        visitPrescriptionDAO.insert(((VisitPrescription) prescription)); // Should always be a safe operation
                    } else {
                        examPrescriptionDAO.insert(((ExamPrescription) prescription));
                    }
                    Mailer.Instance.Send(citizen, "Hai un nuovo richiamo", "Gentile " + citizen.getName() + " " + citizen.getSurname() + ",\nil Sistema Sanitario vuole comunicarLe che il Servizio Sanitario Provinciale le ha prescritto un nuovo richiamo.\nAcceda al sito per prenderne visione:\n" + link);
                }
            }
            //Restituzione toast di successo
            if (prescription instanceof VisitPrescription) {
                Toast.successPrescription("Richiamo visita").redirect(request, response);
            } else {
                Toast.successPrescription("Richiamo esame").redirect(request, response);
            }
        } catch (NullPointerException | IllegalArgumentException ex) { // Some fields are null or invalid
            Feedback.invalidRequestError("recall").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        } catch (MessagingException e) {
            e.printStackTrace();
            Feedback.mailerError("/").forward(request, response);
        }
    }
}
