package it.disorganizzazione.progettoweb.servlets.ssp;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.ExamPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.utils.Feedback;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

public class HomeServlet extends HttpServlet {

    private ExamPrescriptionDAO examPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            examPrescriptionDAO = daoFactory.getDAO(ExamPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ssp/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int plannerId = Integer.parseInt(request.getParameter("plannerId"));
        int patientId = Integer.parseInt(request.getParameter("patientId"));

        String timestampAsString = Objects.requireNonNull(request.getParameter("prescriptionTimestamp"));
        LocalDateTime localDateTime = LocalDateTime.parse(timestampAsString);
        Timestamp prescriptionTimestamp = Timestamp.valueOf(localDateTime);

        PrescriptionId prescriptionId = new PrescriptionId(plannerId, patientId, prescriptionTimestamp);
        try {
            ExamPrescription examPrescription = Objects.requireNonNull(examPrescriptionDAO.getByPrimaryKey(prescriptionId));
            if (!examPrescription.isComplete()) {
                request.getSession().setAttribute("examPrescription", examPrescription);
                response.sendRedirect(response.encodeRedirectURL("exam"));
            } else throw new IllegalArgumentException("Invalid prescription");
        } catch (NullPointerException | IllegalArgumentException ex) {
            ex.printStackTrace();
            Feedback.invalidRequestError("home").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
