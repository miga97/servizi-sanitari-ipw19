package it.disorganizzazione.progettoweb.servlets.specialist;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.utils.Feedback;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class HomeServlet extends HttpServlet {

    private CitizenDAO citizenDAO;
    private VisitPrescriptionDAO visitPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            visitPrescriptionDAO = daoFactory.getDAO(VisitPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/specialist/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PrescriptionId pid = PrescriptionId.getFromRequest(request);
            VisitPrescription visitPrescription = Objects.requireNonNull(visitPrescriptionDAO.getByPrimaryKey(pid));
            Citizen patient = citizenDAO.getByPrimaryKey(Integer.parseInt(request.getParameter("patientId")));
            if (!visitPrescription.isComplete()) {
                request.getSession().setAttribute("patient", patient);
                request.getSession().setAttribute("visitPrescription", visitPrescription);
                response.sendRedirect(response.encodeRedirectURL("visit"));
            } else throw new IllegalArgumentException("Invalid prescription");
        } catch (NullPointerException | IllegalArgumentException ex) {
            ex.printStackTrace();
            Feedback.invalidRequestError("home").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
