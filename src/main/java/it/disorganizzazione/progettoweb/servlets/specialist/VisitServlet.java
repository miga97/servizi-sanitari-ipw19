package it.disorganizzazione.progettoweb.servlets.specialist;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.*;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Specialist;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Mailer;
import it.disorganizzazione.progettoweb.utils.Sanitizer;
import it.disorganizzazione.progettoweb.utils.Toast;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Objects;

public class VisitServlet extends HttpServlet {
    private PhotoDAO photoDAO;
    private VisitDAO visitDAO;
    private CitizenDAO citizenDAO;
    private VisitPrescriptionDAO visitPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            photoDAO = daoFactory.getDAO(PhotoDAO.class);
            visitDAO = daoFactory.getDAO(VisitDAO.class);
            visitPrescriptionDAO = daoFactory.getDAO(VisitPrescriptionDAO.class);
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            Citizen patient = (Citizen) Objects.requireNonNull(request.getSession().getAttribute("patient"));
            VisitPrescription visitPrescription = (VisitPrescription) Objects.requireNonNull(request.getSession().getAttribute("visitPrescription"));
            request.setAttribute("visitPrescriptionName", visitDAO.getByPrimaryKey(visitPrescription.getVisitId()).getName());
            Photo patientPhoto = photoDAO.getLastByUserId(patient.getId());
            request.setAttribute("patientPhoto", patientPhoto != null ? patientPhoto : Photo.defaultPhoto());
            request.getRequestDispatcher("/WEB-INF/specialist/visit.jsp").forward(request, response);
        } catch (NullPointerException ex) { // No patient selected
            response.sendRedirect(response.encodeRedirectURL("home"));
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameterMap().containsKey("cancel")) {
            request.getSession().removeAttribute("visitPrescription");
            request.getSession().removeAttribute("patient");
            response.sendRedirect(response.encodeRedirectURL("home"));
        } else if (request.getParameterMap().containsKey("send")) {
            try {
                Specialist specialist = (Specialist) Objects.requireNonNull(request.getSession().getAttribute("user"));
                VisitPrescription visitPrescription = (VisitPrescription) Objects.requireNonNull(request.getSession().getAttribute("visitPrescription"));
                String anamnesi = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "anamnesi"));
                Citizen patient = citizenDAO.getByPrimaryKey(visitPrescription.getPatientId());
                //This visit of the specialist
                if (!anamnesi.trim().isEmpty()) {
                    visitPrescription.setPerformerId(specialist.getId());
                    visitPrescription.setAnamnesi(anamnesi);
                    visitPrescription.setCompletionTimestamp(new Timestamp(System.currentTimeMillis()));
                    visitPrescriptionDAO.complete(visitPrescription);
                } else throw new NullPointerException("Empty anamnesi string");

                request.getSession().removeAttribute("visitPrescription");
                request.getSession().removeAttribute("patient");

                Toast.successCompletion("Visita specialistica").setLink("home").redirect(request, response);
                String link = Mailer.getServerURL(request) + request.getContextPath() + "/login";
                Mailer.Instance.Send(patient,"Hai una nuova anamnesi","Gentile "+patient.getName()+" "+patient.getSurname()+",\nil Sistema Sanitario vuole comunicarLe che ci sono nuove visite da visualizzare.\nAcceda al sito per prenderne visione:\n"+link);

            } catch (NullPointerException | NumberFormatException ex) { // Some fields are null or invalid null
                Feedback.invalidRequestError("visit").forward(request, response);
            } catch (DAOException ex) {
                ex.printStackTrace();
                Feedback.serverError("/").forward(request, response);
            }catch (MessagingException e) {
                e.printStackTrace();
                Feedback.mailerError("/").forward(request, response);
            }
        }
    }
}
