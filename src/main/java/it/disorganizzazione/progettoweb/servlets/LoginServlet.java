/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.servlets;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.PhotoDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.users.*;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Sanitizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

public class LoginServlet extends HttpServlet {
    private UserDAO userDao;
    private CitizenDAO citizenDAO;
    private PhotoDAO photoDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            photoDAO = daoFactory.getDAO(PhotoDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            User user = (User) Objects.requireNonNull(request.getSession().getAttribute("user"));
            redirectByUserType(response, user);
        } catch (NullPointerException | ClassCastException ex) {
            request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            String email = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "username"));
            String password = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "password"));
            boolean rememberMe = Objects.equals(Sanitizer.getSanitizedParameter(request, "rememberme"), "1");
            User user = userDao.getByEmailAndPassword(email, password);
            if (user == null) {
                throw new IllegalArgumentException("User not existing");
            } else if (user.isCitizen()) { // If the user is a citizen type, fetch the complete user (returns an instance of the correct type)
                request.getSession().setAttribute("user", citizenDAO.getByEmailAndPassword(email, password));
            } else { // Else use the already fetched user (of the correct type too)
                request.getSession().setAttribute("user", user);
            }
            // Set userPhoto in the session
            Photo photo = photoDAO.getLastByUserId(user.getId());
            request.getSession().setAttribute("userPhoto", photo != null ? photo : Photo.defaultPhoto());
            // Remember me
            if (rememberMe) request.getSession().setMaxInactiveInterval(1209600); //2 weeks in seconds
            redirectByUserType(response, user);
        } catch (NullPointerException ex) { // Email or password null
            Feedback.invalidRequestError("login").forward(request, response);
        } catch (IllegalArgumentException ex) { // Utente non esistente o password non corrispondente
            request.setAttribute("flash.loginError", "Nessun utente trovato con questa combinazione di email e password!");
            request.setAttribute("flash.username", Sanitizer.getSanitizedParameter(request, "username"));
            response.sendRedirect(response.encodeRedirectURL("login"));
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }

    private void redirectByUserType(HttpServletResponse response, User user) throws IOException {
        if (user instanceof Ssp) {
            response.sendRedirect(response.encodeRedirectURL("ssp/home"));
        } else if (user instanceof Pharmacy) {
            response.sendRedirect(response.encodeRedirectURL("pharmacy/home"));
        } else if (user instanceof Doctor) {
            response.sendRedirect(response.encodeRedirectURL("doctor/home"));
        } else if (user instanceof Specialist) {
            response.sendRedirect(response.encodeRedirectURL("specialist/home"));
        } else if (user instanceof Citizen) {
            response.sendRedirect(response.encodeRedirectURL("citizen/home"));
        }
    }
}
