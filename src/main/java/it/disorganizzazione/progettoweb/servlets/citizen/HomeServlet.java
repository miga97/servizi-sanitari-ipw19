package it.disorganizzazione.progettoweb.servlets.citizen;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfWriter;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.utils.Feedback;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class HomeServlet extends HttpServlet {
    private DrugPrescriptionDAO drugPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            drugPrescriptionDAO = daoFactory.getDAO(DrugPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/citizen/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (request.getParameterMap().containsKey("print")) {
                PrescriptionId pid = PrescriptionId.getFromRequest(request);
                DrugPrescription drugPrescription = Objects.requireNonNull(drugPrescriptionDAO.getByPrimaryKey(pid));
                Citizen citizen = (Citizen) Objects.requireNonNull(request.getSession().getAttribute("user"));
                String QRContent = "Prescrizione Numero: " + pid.getUniqueId() + "\nCodice Medico: " + pid.getPlannerId() +
                        "\nCodice Fiscale Paziente: " + citizen.getCf() + "\nData Prescrizione: " + pid.getPrescriptionTimestamp() +
                        "\nDescrizione Farmaco:\n" + drugPrescription.getDescription();

                Document document = new Document();
                response.setContentType("application/pdf");
                response.addHeader("Content-Disposition", "attachment; filename=" + pid.getUniqueId() + ".pdf");
                PdfWriter.getInstance(document, response.getOutputStream());

                document.open();
                Font fontValue = FontFactory.getFont(FontFactory.TIMES, 20, BaseColor.BLACK);
                Font fontTitle = FontFactory.getFont(FontFactory.TIMES_BOLD, 20, BaseColor.BLACK);

                Image QR = new BarcodeQRCode(QRContent, 1000, 1000, null).getImage();
                QR.scaleAbsolute(200, 200);
                float x = (PageSize.A4.getWidth() - QR.getScaledWidth()) / 2;
                QR.setAbsolutePosition(x, QR.getAbsoluteY());

                document.add(QR);
                for (String piece : QRContent.split("\n")) {
                    int indexSeparator = piece.indexOf(":");
                    Paragraph paragraph = new Paragraph();
                    if (indexSeparator != -1) {
                        String title = piece.substring(0, indexSeparator);
                        String value = piece.substring(indexSeparator);

                        paragraph.add(new Chunk(title, fontTitle));
                        paragraph.add(new Chunk(value, fontValue));

                    } else {
                        paragraph.add(new Chunk(piece, fontValue));
                    }
                    paragraph.setMultipliedLeading(3);
                    document.add(paragraph);
                }
                document.close();
            }
        } catch (NullPointerException | IllegalArgumentException ex) {
            ex.printStackTrace();
            Feedback.invalidRequestError("home").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        } catch (DocumentException e) {
            e.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
