package it.disorganizzazione.progettoweb.servlets.citizen;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ExamPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.TicketDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.utils.Feedback;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TicketServlet extends HttpServlet {

    private VisitPrescriptionDAO visitPrescriptionDAO;
    private ExamPrescriptionDAO examPrescriptionDAO;
    private DrugPrescriptionDAO drugPrescriptionDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            visitPrescriptionDAO = daoFactory.getDAO(VisitPrescriptionDAO.class);
            examPrescriptionDAO = daoFactory.getDAO(ExamPrescriptionDAO.class);
            drugPrescriptionDAO = daoFactory.getDAO(DrugPrescriptionDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Citizen citizen = ((Citizen) request.getSession().getAttribute("user"));
        try {
            request.setAttribute("visitsExpense", visitPrescriptionDAO.getExpenseByCitizenId(citizen.getId()));
            request.setAttribute("examsExpense", examPrescriptionDAO.getExpenseByCitizenId(citizen.getId()));
            request.setAttribute("drugsExpense", drugPrescriptionDAO.getExpenseByCitizenId(citizen.getId()));
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request,response);
        }
        request.getRequestDispatcher("/WEB-INF/citizen/ticket.jsp").forward(request, response);
    }
}
