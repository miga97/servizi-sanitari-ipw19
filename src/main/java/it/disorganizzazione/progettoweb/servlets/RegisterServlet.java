/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.servlets;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.ProvinceDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;
import it.disorganizzazione.progettoweb.persistence.factories.UserFactory;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Mailer;
import it.disorganizzazione.progettoweb.utils.Sanitizer;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterServlet extends HttpServlet {
    private UserDAO userDao;
    private CitizenDAO citizenDAO;
    private ProvinceDAO provinceDAO;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            userDao = daoFactory.getDAO(UserDAO.class);
            provinceDAO = daoFactory.getDAO(ProvinceDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String email = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "email"));
            String password = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "password"));
            String name = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "name"));
            int province = Integer.parseInt(request.getParameter("province"));
            int role = Integer.parseInt(request.getParameter("role"));
            // Province validation
            Objects.requireNonNull(provinceDAO.getByPrimaryKey(province)); // Throw exception if not present
            //Email Validation - reference n5 : https://howtodoinjava.com/regex/java-regex-validate-email-address/
            String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches()) throw new NullPointerException("Email not valid");
            // End of validations
            User user = new UserFactory().getUser(role);
            user.setName(name);
            user.setEmail(email);
            user.setPassword(password);
            user.setProvince(province);
            if (user instanceof Citizen) {
                // L'utente è un cittadino e quindi ci sono più campi da gestire
                String surname = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "surname"));
                String gender = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "gender"));
                String cf = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "cf"));
                Date birthDate = Date.valueOf(Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "birthDate")));
                String birthTown = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "birthTown"));
                ((Citizen) user).setSurname(surname);
                ((Citizen) user).setGender(gender);
                ((Citizen) user).setCf(cf);
                ((Citizen) user).setBirthDate(birthDate);
                ((Citizen) user).setBirthTown(birthTown);
                citizenDAO.insert((Citizen) user);
            } else {
                userDao.insert(user);
            }
            // Non vogliamo che acceda subito con un vecchio account!
            request.getSession().invalidate();
            // Se va tutto bene lo mando a fare il login
            Feedback.registrationSuccess("login").forward(request, response);
            String link = Mailer.getServerURL(request) + request.getContextPath() + "/login";
            Mailer.Instance.Send(user, "Nuova Registrazione", "Gentile " + user.getName() + " " + ((Citizen) user).getSurname() + ",\nil Sistema Sanitario vuole darLe il benvenuto.\nAcceda al sito tramite questo link:\n" + link);
        } catch (NullPointerException ex) { // Some fields are null or invalid null
            Feedback.invalidRequestError("register").forward(request, response);
        } catch (DAOException ex) { // User already in the system
            ex.printStackTrace();
            Feedback.existingUserWarning("#").forward(request, response);
        } catch (MessagingException e) {
            e.printStackTrace();
            Feedback.mailerError("/").forward(request, response);
        }
    }
}
