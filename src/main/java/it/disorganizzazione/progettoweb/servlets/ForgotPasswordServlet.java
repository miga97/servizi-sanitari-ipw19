package it.disorganizzazione.progettoweb.servlets;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.PasswordRecoveryDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;
import it.disorganizzazione.progettoweb.persistence.entities.PasswordRecovery;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;
import it.disorganizzazione.progettoweb.utils.Feedback;
import it.disorganizzazione.progettoweb.utils.Mailer;
import it.disorganizzazione.progettoweb.utils.RandomString;
import it.disorganizzazione.progettoweb.utils.Sanitizer;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that handles the password recovery action.
 *
 * @author Matteo Rizzi
 */
public class ForgotPasswordServlet extends HttpServlet {

    private UserDAO userDao;
    private PasswordRecoveryDAO passwordRecoveryDAO;
    private final RandomString gen = new RandomString();

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        try {
            userDao = daoFactory.getDAO(UserDAO.class);
            passwordRecoveryDAO = daoFactory.getDAO(PasswordRecoveryDAO.class);
        } catch (NullPointerException ex) {
            throw new ServletException("Impossible to get the daoFactory", ex);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get DAOs from the the daoFactory", ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/WEB-INF/forgot-password.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            String email = Objects.requireNonNull(Sanitizer.getSanitizedParameter(request, "email"));
            String token = gen.nextString();
            String link = Mailer.getServerURL(request) + request.getContextPath() + "/resetPassword?token=" + token;
            User user = userDao.getByEmail(email);
            if (user == null) throw new IllegalArgumentException("Unknown user");
            passwordRecoveryDAO.replace(new PasswordRecovery(user, token));
            String text = "Gentile " + user.getName() + ",\n" +
                    "abbiamo ricevuto una nuova richiesta per il recupero della tua password.\n" +
                    "Il tuo link è " + link + "\n\n" +
                    "Utilizzalo entro 24h oppure ignora questo messaggio se non hai effettuato alcuna richiesta.\n\n" +
                    "Il Servizio Sanitario";
            // Send the e-mail
            Mailer.Instance.Send(user, "Recupero password Servizi Sanitari", text);
            Feedback.emailSentSuccess("login").forward(request, response);
        } catch (NullPointerException ex) {
            Feedback.invalidRequestError("forgotPassword").forward(request, response);
        } catch (IllegalArgumentException ex) {
            Feedback.unknownUserWarning("forgotPassword").forward(request, response);
        } catch (MessagingException ex) {
            ex.printStackTrace();
            Feedback.mailerError("/").forward(request, response);
        } catch (DAOException ex) {
            ex.printStackTrace();
            Feedback.serverError("/").forward(request, response);
        }
    }
}
