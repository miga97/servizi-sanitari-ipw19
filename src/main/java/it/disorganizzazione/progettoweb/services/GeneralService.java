/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 12 - Shopping List Implementation
 * UniTN
 */
package it.disorganizzazione.progettoweb.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ExamPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.TicketDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;
import it.disorganizzazione.progettoweb.services.entities.Response;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * REST Web Service
 *
 * @author Matteo Rizzi
 */

@Path("dataTable")
public class GeneralService {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private ExamPrescriptionDAO examPrescriptionDAO;

    private VisitPrescriptionDAO visitPrescriptionDAO;

    private DrugPrescriptionDAO drugPrescriptionDAO;

    private TicketDAO ticketDAO;
    private Gson gson;

    private CitizenDAO citizenDAO;

    @Context
    public void setServletContext(ServletContext servletContext) {
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            examPrescriptionDAO = daoFactory.getDAO(ExamPrescriptionDAO.class);
            visitPrescriptionDAO = daoFactory.getDAO(VisitPrescriptionDAO.class);
            drugPrescriptionDAO = daoFactory.getDAO(DrugPrescriptionDAO.class);
            ticketDAO = daoFactory.getDAO(TicketDAO.class);
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
        } catch (NullPointerException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for storage system"));
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
    }

    /**
     * Retrieves representation of a view, by type.
     *
     * @param dataTable      Which datatable are we checking.
     * @param type           type of the search.
     * @param draw           conform to DataTables Standards.
     * @param queryTerm      the string used to search
     * @param size           return only the size if exist.
     * @param startParam     conform to DataTables Standards.
     * @param lengthParam    conform to DataTables Standards.
     * @param completedParam True if completed, false otherwise, null retrieves all.
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getDataTable(@QueryParam("dt") String dataTable, @QueryParam("type") String type, @QueryParam("draw") String draw, @QueryParam("search") String queryTerm, @QueryParam("size") String size, @QueryParam("start") String startParam, @QueryParam("length") String lengthParam, @QueryParam("completed") String completedParam) throws DAOException {
        if (dataTable == null || dataTable.isEmpty())
            return gson.toJson(new Response(new ArrayList<>()));
        if (queryTerm == null || queryTerm.isEmpty() || type == null || type.isEmpty()){
                type = "all";
        }
        Boolean completed = null;
        if (completedParam != null && !completedParam.isEmpty()) {
            completed = Boolean.parseBoolean(completedParam);
        }

        List data;
        List results;
        type = type.toLowerCase();
        switch (dataTable.toLowerCase()) {
            case "exam":
                switch (type) {
                    case "citizenid":
                        data = examPrescriptionDAO.getViewByCitizenId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "plannerid":
                        data = examPrescriptionDAO.getViewByPlannerId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "performerid":
                        data = examPrescriptionDAO.getViewByPerformerId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "all":
                        data = examPrescriptionDAO.getAllView(completed);
                        break;
                    default:
                        return gson.toJson(new Response(new ArrayList<>()));
                }
                break;
            case "visit":
                switch (type) {
                    case "citizenid":
                        data = visitPrescriptionDAO.getViewByCitizenId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "plannerid":
                        data = visitPrescriptionDAO.getViewByPlannerId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "performerid":
                        data = visitPrescriptionDAO.getViewByPerformerId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "all":
                        data = visitPrescriptionDAO.getAllView(completed);
                        break;
                    case "filtered": //queryTerm has to be set
                        User user;
                        try {
                            user = (User) Objects.requireNonNull(request.getSession().getAttribute("user"));
                        } catch (NullPointerException ex) { /* No drugs selected */
                            user = null;
                        }
                        data = visitPrescriptionDAO.getFilteredView(completed, user);
                        break;
                    default:
                        return gson.toJson(new Response(new ArrayList<>()));
                }
                break;
            case "drug":
                switch (type) {
                    case "citizenid":
                        data = drugPrescriptionDAO.getViewByCitizenId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "plannerid":
                        data = drugPrescriptionDAO.getViewByPlannerId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "performerid":
                        data = drugPrescriptionDAO.getViewByPerformerId(Integer.parseInt(queryTerm), completed);
                        break;
                    case "province":
                        data = drugPrescriptionDAO.getViewByCitizenProvince(queryTerm, completed);
                        break;
                    case "filtered":
                        List<DrugPrescriptionView> receipt;
                        try {
                            receipt = (List<DrugPrescriptionView>) Objects.requireNonNull(request.getSession().getAttribute("receipt"));
                        } catch (NullPointerException ex) { /* No drugs selected */
                            receipt = new ArrayList<>();
                        }
                        data = drugPrescriptionDAO.getFilteredView(Integer.parseInt(queryTerm), completed, receipt);
                        break;
                    case "all":
                        data = drugPrescriptionDAO.getAllView(completed);
                        break;
                    default:
                        return gson.toJson(new Response(new ArrayList<>()));
                }
                break;
            case "ticket":
                switch (type) {
                    case "citizenid":
                        data = ticketDAO.getViewByCitizenId(Integer.parseInt(queryTerm));
                        break;
                    case "plannerid":
                        data = ticketDAO.getViewByPlannerId(Integer.parseInt(queryTerm));
                        break;
                    case "performerid":
                        data = ticketDAO.getViewByPerformerId(Integer.parseInt(queryTerm));
                        break;
                    case "all":
                        data = ticketDAO.getAllView();
                        break;
                    default:
                        return gson.toJson(new Response(new ArrayList<>()));
                }
                break;
            case "user":
                if (queryTerm != null && !queryTerm.isEmpty()) {
                    data = citizenDAO.getViewByDoctorId(Integer.parseInt(queryTerm));
                } else {
                    data = citizenDAO.getAllView();
                }
                break;
            default:
                return gson.toJson(new Response(new ArrayList<>()));
        }
        results = new ArrayList<>(data);
        int start = 0;

        if (startParam != null && Integer.parseInt(startParam) >= 0 && Integer.parseInt(startParam) < results.size()) {
            start = Integer.parseInt(startParam);
        }

        int length = results.size() - start;
        if (lengthParam != null && Integer.parseInt(lengthParam) >= start && (start + Integer.parseInt(lengthParam)) < results.size())
            length = Integer.parseInt(lengthParam);
        if (size != null) {
            return gson.toJson(results.size());
        } else {
            if (draw != null && !draw.isEmpty())
                return gson.toJson(new Response(results.subList(start, start + length), Integer.parseInt(draw)));
            else return gson.toJson(new Response(results.subList(start, start + length)));

        }
    }

}
