/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 12 - Shopping List Implementation
 * UniTN
 */
package it.disorganizzazione.progettoweb.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.PhotoDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.services.entities.Response;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.*;

/**
 * REST Web Service
 *
 * @author Matteo Rizzi
 */

@Path("photos")
public class PhotosService {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private PhotoDAO photoDAO;
    private Gson gson;

    @Context
    public void setServletContext(ServletContext servletContext) {
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            photoDAO = daoFactory.getDAO(PhotoDAO.class);
        } catch (NullPointerException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for storage system"));
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    }

    /**
     * Retrieves representation of a collection of Photos
     *
     * @param queryTerm   the string used to search
     * @param size        return only the size if exist.
     * @param startParam  conform to DataTables Standards.
     * @param lengthParam conform to DataTables Standards.
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPhotos(@QueryParam("draw") String draw, @QueryParam("search") String queryTerm, @QueryParam("size") String size, @QueryParam("start") String startParam, @QueryParam("length") String lengthParam) throws DAOException {
        if (queryTerm == null || queryTerm.length() == 0) return new Gson().toJson(new Response(new ArrayList<>()));
        List<Photo> photoList = photoDAO.getByUserId(Integer.parseInt(queryTerm));
        List<PhotoView> results = new ArrayList<>();
        photoList.forEach((photo) -> results.add(new PhotoView(photo)));
        int start = 0;

        if (startParam != null && Integer.parseInt(startParam) >= 0 && Integer.parseInt(startParam) < results.size()) {
            start = Integer.parseInt(startParam);
        }

        int length = results.size() - start;
        if (lengthParam != null && Integer.parseInt(lengthParam) >= start && (start + Integer.parseInt(lengthParam)) < results.size())
            length = Integer.parseInt(lengthParam);
        if (size != null) {
            return new Gson().toJson(results.size());
        } else {
            if (draw != null && !draw.isEmpty())
                return gson.toJson(new Response(results.subList(start, start + length), Integer.parseInt(draw)));
            else return gson.toJson(new Response(results.subList(start, start + length)));

        }
    }

}
