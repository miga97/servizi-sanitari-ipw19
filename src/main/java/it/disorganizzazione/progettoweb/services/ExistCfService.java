package it.disorganizzazione.progettoweb.services;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author Matteo Zanella
 */

@Path("existCf")
public class ExistCfService {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private UserDAO userDAO;

    @Context
    public void setServletContext(ServletContext servletContext) {
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            userDAO = daoFactory.getDAO(UserDAO.class);
        } catch (NullPointerException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for storage system"));
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
    }

    /**
     * Retrieves existence condition of an user with a certain CF
     *
     * @param queryTerm the searched email
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers(@QueryParam("term") String queryTerm) throws DAOException {
        if (userDAO.getByCf(queryTerm) != null) {
            return "{\"exist\": true}";
        } else {
            return "{\"exist\": false}";
        }
    }
}
