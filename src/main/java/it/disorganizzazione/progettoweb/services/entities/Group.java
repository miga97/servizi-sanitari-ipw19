package it.disorganizzazione.progettoweb.services.entities;

import java.util.List;

public class Group {
    private String text;
    private List<Item> children;

    public Group(String text, List<Item> children) {
        this.text = text;
        this.children = children;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Item> getChildren() {
        return children;
    }

    public void setChildren(List<Item> children) {
        this.children = children;
    }
}
