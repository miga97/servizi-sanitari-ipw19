package it.disorganizzazione.progettoweb.services.entities;

import java.util.List;

public class Response {
    private Integer draw=null;
    private List results;

    public Response(List results) {
        this.results = results;
    }

    public Response(List results, int draw) {
        this.results = results;
        this.draw = draw;
    }

    public List getResults() {
        return results;
    }

    public void setResults(List results) {
        this.results = results;
    }
}
