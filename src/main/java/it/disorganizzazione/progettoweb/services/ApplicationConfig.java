package it.disorganizzazione.progettoweb.services;

import javax.ws.rs.core.Application;
import java.util.Set;

@javax.ws.rs.ApplicationPath("services")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(it.disorganizzazione.progettoweb.services.ProvincesService.class);
        resources.add(it.disorganizzazione.progettoweb.services.ExamsService.class);
        resources.add(it.disorganizzazione.progettoweb.services.VisitsService.class);
        resources.add(it.disorganizzazione.progettoweb.services.ExistCfService.class);
        resources.add(it.disorganizzazione.progettoweb.services.ExistEmailService.class);
        resources.add(it.disorganizzazione.progettoweb.services.PhotosService.class);
        resources.add(it.disorganizzazione.progettoweb.services.GeneralService.class);
    }

}
