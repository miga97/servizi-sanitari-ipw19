/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 12 - Shopping List Implementation
 * UniTN
 */
package it.disorganizzazione.progettoweb.services;

import com.google.gson.Gson;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Visit;
import it.disorganizzazione.progettoweb.services.entities.Item;
import it.disorganizzazione.progettoweb.services.entities.Response;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Matteo Rizzi
 */

@Path("visits")
public class VisitsService {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private VisitDAO visitDAO;

    @Context
    public void setServletContext(ServletContext servletContext) {
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            visitDAO = daoFactory.getDAO(VisitDAO.class);
        } catch (NullPointerException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for storage system"));
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
    }

    /**
     * Retrieves representation of a collection of Exams
     *
     * @param queryTerm the string used to search the province
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers(@QueryParam("term") String queryTerm) throws DAOException {
        List<Visit> visits = visitDAO.getAll();
        List<Item> results = new ArrayList<>();
        visits.stream()
                .filter((Visit visit) -> (queryTerm == null || visit.toString().toLowerCase().contains(queryTerm.toLowerCase())))
                .forEach((visit) -> results.add(new Item(visit.getId(), visit.toString())));
        return new Gson().toJson(new Response(results));
    }
}
