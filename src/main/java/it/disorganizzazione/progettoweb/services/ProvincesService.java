/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Lab 12 - Shopping List Implementation
 * UniTN
 */
package it.disorganizzazione.progettoweb.services;

import com.google.gson.Gson;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOFactoryException;
import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.ProvinceDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.services.entities.Item;
import it.disorganizzazione.progettoweb.services.entities.Response;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Matteo Zanella
 */

@Path("provinces")
public class ProvincesService {

    @Context
    private UriInfo context;

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private ProvinceDAO provinceDAO;

    @Context
    public void setServletContext(ServletContext servletContext) {
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            provinceDAO = daoFactory.getDAO(ProvinceDAO.class);
        } catch (NullPointerException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for storage system"));
        } catch (DAOFactoryException ex) {
            throw new RuntimeException(new ServletException("Impossible to get dao factory for user storage system", ex));
        }
    }

    /**
     * Retrieves representation of a collection of Provinces
     *
     * @param queryTerm the string used to search the province
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getUsers(@QueryParam("term") String queryTerm) throws DAOException {
        List<Province> provinces = provinceDAO.getAll();
        List<Item> results = new ArrayList<>();
        provinces.stream()
                .filter((Province province) -> queryTerm == null || province.toString().toLowerCase().contains(queryTerm.toLowerCase()))
                .forEach((province) -> results.add(new Item(province.getId(), province.toString())));
        return new Gson().toJson(new Response(results));
    }
}
