package it.disorganizzazione.progettoweb.filters;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * A filter implementation of the concept of Flash Scope, useful to use the post-redirect-get pattern
 * Inspired by http://smartkey.co.uk/development/implementing-flash-scope-in-java-web-applications/
 * <p>
 * This filter checks for request attribute names starting with ‘flash.‘. When it finds such an attribute,
 * it ensures that it is made available in the subsequent request by temporarily storing it in the user’s session,
 * and then reinstating it when the next request is received.
 * <p>
 * e.g.: request.setAttribute("flash.message", "Hello world!");
 */
public class FlashScopeFilter extends ProjectFilter {

    private static final String FLASH_SESSION_KEY = "FLASH_SESSION_KEY";

    @Override
    boolean doBeforeProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        //reinstate any flash scoped params from the users session
        //and clear the session
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();
            Map<String, Object> flashParams = (Map<String, Object>) session.getAttribute(FLASH_SESSION_KEY);
            if (flashParams != null) {
                for (Map.Entry<String, Object> flashEntry : flashParams.entrySet()) {
                    request.setAttribute(flashEntry.getKey(), flashEntry.getValue());
                }
                session.removeAttribute(FLASH_SESSION_KEY);
            }
        }
        return true;
    }

    @Override
    void doAfterProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            Map<String, Object> flashParams = new HashMap<>();
            Enumeration<String> e = httpRequest.getAttributeNames();
            while (e.hasMoreElements()) {
                String paramName = e.nextElement();
                if (paramName.startsWith("flash.")) {
                    Object value = request.getAttribute(paramName);
                    // removes the "flash." prefix for the next request
                    flashParams.put(paramName.substring(6), value);
                }
            }
            if (flashParams.size() > 0) {
                HttpSession session = httpRequest.getSession();
                session.setAttribute(FLASH_SESSION_KEY, flashParams);
            }
        }
    }
}
