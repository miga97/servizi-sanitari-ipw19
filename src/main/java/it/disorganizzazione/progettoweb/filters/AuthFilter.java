package it.disorganizzazione.progettoweb.filters;

import it.disorganizzazione.progettoweb.persistence.entities.users.*;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthFilter extends ProjectFilter {
    @Override
    protected boolean doBeforeProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8"); //importante per ottenere i caratteri giusti dai form
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            HttpSession session = httpRequest.getSession(false);
            if (session != null && session.getAttribute("user") instanceof User) {
                // Esiste un utente in sessione, è già avvenuto il login
                User user = (User) session.getAttribute("user");
                if (!matchingRoleAccess(user, httpRequest)) { // Wrong section
                    correctRedirect(user, httpRequest, httpResponse);
                    return false;
                } else setUserView(httpRequest); // Correct section
            } else {
                // Non c'è un utente salvato in sessione, quindi l'utente deve fare login
                httpResponse.sendRedirect(httpResponse.encodeRedirectURL(httpRequest.getContextPath() + "/login"));
                return false;
            }
        }
        return true;
    }

    @Override
    void doAfterProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
    }

    /**
     * We use this method to set an userView attribute in the request,
     * so the sidebar or other elements can correctly be displayed without parsing the URL
     */
    private void setUserView(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        if (requestURI.startsWith(request.getContextPath() + "/ssp/")) {
            request.setAttribute("userView", "ssp");
        } else if (requestURI.startsWith(request.getContextPath() + "/pharmacy/")) {
            request.setAttribute("userView", "pharmacy");
        } else if (requestURI.startsWith(request.getContextPath() + "/doctor/")) {
            request.setAttribute("userView", "doctor");
        } else if (requestURI.startsWith(request.getContextPath() + "/specialist/")) {
            request.setAttribute("userView", "specialist");
        } else if (requestURI.startsWith(request.getContextPath() + "/citizen/")) {
            request.setAttribute("userView", "citizen");
        }
    }

    private boolean matchingRoleAccess(User user, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        return (user instanceof Ssp && requestURI.startsWith(request.getContextPath() + "/ssp/")) ||
                (user instanceof Pharmacy && requestURI.startsWith(request.getContextPath() + "/pharmacy/")) ||
                (user instanceof Doctor && requestURI.startsWith(request.getContextPath() + "/doctor/")) ||
                (user instanceof Specialist && requestURI.startsWith(request.getContextPath() + "/specialist/")) ||
                (user instanceof Citizen && requestURI.startsWith(request.getContextPath() + "/citizen/"));
    }

    /**
     * The request is trying to access a restricted area without the correct permission, so we redirect the request
     * to the correct area matching the role of the user saved in the session
     */
    private void correctRedirect(User user, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (user instanceof Ssp) {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/ssp/home"));
        } else if (user instanceof Pharmacy) {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/pharmacy/home"));
        } else if (user instanceof Doctor) {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/doctor/home"));
        } else if (user instanceof Specialist) {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/specialist/home"));
        } else if (user instanceof Citizen) {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/citizen/home"));
        } else {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login"));
        }
    }
}
