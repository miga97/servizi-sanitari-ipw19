package it.disorganizzazione.progettoweb.filters;

import it.disorganizzazione.commons.persistence.dao.factories.DAOFactory;
import it.disorganizzazione.progettoweb.persistence.dao.general.PhotoDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoId;
import it.disorganizzazione.progettoweb.persistence.entities.users.*;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;

public class PhotoFilter extends ProjectFilter {

    private CitizenDAO citizenDAO;
    private UserDAO userDAO;
    private PhotoDAO photoDAO;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        DAOFactory daoFactory = (DAOFactory) filterConfig.getServletContext().getAttribute("daoFactory");
        try {
            citizenDAO = daoFactory.getDAO(CitizenDAO.class);
            userDAO = daoFactory.getDAO(UserDAO.class);
            photoDAO = daoFactory.getDAO(PhotoDAO.class);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected boolean doBeforeProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            HttpSession session = httpRequest.getSession(false);
            String[] servletPath = httpRequest.getServletPath().substring(1).split("/");
            if (session != null && session.getAttribute("user") instanceof User && servletPath.length == 3) {
                // Esiste un utente in sessione, è già avvenuto il login e il percorso è /photos/***/****
                if (!canAccessFolder((User) session.getAttribute("user"), servletPath[1], servletPath[2])) {
                    // L'utente sta cercando di accedere ad una foto che non puó vedere
                    httpResponse.sendError(401);
                    return false;
                }
            } else if (!httpRequest.getServletPath().equals(request.getServletContext().getInitParameter("defaultPhoto"))) {
                // Non la richiesta é strana o non c'è autenticazione, immagine di default.jpg
                httpResponse.sendError(404);
                return false;
            }
        }
        return true;
    }

    @Override
    void doAfterProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
    }

    private boolean canAccessFolder(User user, String folder, String file) {
        try {
            int targetUserId = Integer.parseInt(folder);
            Photo targetUserLastPhoto = photoDAO.getLastByUserId(targetUserId);
            boolean isTargetLastPhoto = targetUserLastPhoto.getFilename().equals(file);
            if (user.getId() == targetUserId || user instanceof Specialist) {
                // Specialists can see everything and one can see its data
                return true;
            }
            if (isTargetLastPhoto && (user instanceof Citizen || user instanceof Ssp || user instanceof Pharmacy)) {
                // Citizen/Ssp/Pharmacy want to see the last photo of someone
                return true;
            }
            if (user instanceof Doctor) {
                // True if the Doctor is trying to access all photos of only its patients
                Citizen targetUser = citizenDAO.getByPrimaryKey(targetUserId);
                return targetUser.getDoctorId().equals(user.getId());
            }
            return false;
        } catch (Exception ex) {
            return false;
        }
    }
}
