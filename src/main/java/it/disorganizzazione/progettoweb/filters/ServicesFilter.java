package it.disorganizzazione.progettoweb.filters;

import it.disorganizzazione.progettoweb.persistence.entities.users.User;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ServicesFilter extends ProjectFilter {
    @Override
    boolean doBeforeProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            HttpSession session = httpRequest.getSession(false);
            if (session != null && session.getAttribute("user") instanceof User) {
                return true;
            } else {
                // Non c'è un utente salvato in sessione, quindi l'utente deve fare login
                httpResponse.sendError(401);
                //httpResponse.sendRedirect(httpResponse.encodeRedirectURL(httpRequest.getContextPath() + "/login"));
                return false;
            }
        } else return true;
    }

    @Override
    void doAfterProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException {

    }
}
