package it.disorganizzazione.progettoweb.filters;

import javax.servlet.*;
import java.io.IOException;

public abstract class ProjectFilter implements Filter {

    private static final boolean DEBUG = false;
    private FilterConfig filterConfig;

    /**
     * @return a boolean that indicates if the request should be forwarded
     */
    abstract boolean doBeforeProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException;

    abstract void doAfterProcessing(ServletRequest request, ServletResponse response) throws IOException, ServletException;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        if (DEBUG) log("init");
    }

    @Override
    public void destroy() {
    }

    /**
     * @param request  The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain    The filter chain we are processing
     * @throws IOException      if an input/output error occurs
     * @throws ServletException if a servlet error occurs
     * @author Stefano Chirico
     * @since 1.0.0.190519
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (DEBUG) log("doBefore");
        if (doBeforeProcessing(request, response)) {
            if (DEBUG) log("doFilter");
            chain.doFilter(request, response);
        }
        if (DEBUG) log("doAfter");
        doAfterProcessing(request, response);
    }

    void log(String msg) {
        filterConfig.getServletContext().log(getClass().getSimpleName() + ": " + msg);
    }

}
