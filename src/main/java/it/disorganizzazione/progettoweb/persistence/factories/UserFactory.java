package it.disorganizzazione.progettoweb.persistence.factories;

import it.disorganizzazione.progettoweb.persistence.entities.users.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class UserFactory {
    public User getUser(Integer role) throws IllegalArgumentException {
        switch (role) {
            case 0:
                return new Citizen();
            case 1:
                return new Doctor();
            case 2:
                return new Specialist();
            case 3:
                return new Ssp();
            case 4:
                return new Pharmacy();
            default:
                throw new IllegalArgumentException("User role is invalid!");
        }
    }

    public Citizen getCitizen(Integer role) throws IllegalArgumentException {
        switch (role) {
            case 0:
                return new Citizen();
            case 1:
                return new Doctor();
            case 2:
                return new Specialist();
            default:
                throw new IllegalArgumentException("Citizen role is invalid!");
        }
    }

    public <USER extends User> USER getUser(Class<USER> userClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<USER> constructor = userClass.getConstructor();
        return constructor.newInstance();
    }

    public <USER_TYPE extends Citizen> USER_TYPE fillUser(USER_TYPE user, ResultSet rs) throws SQLException {
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("nome"));
        user.setSurname(rs.getString("cognome"));
        user.setEmail(rs.getString("email"));
        user.setPassword(rs.getString("password"));
        user.setProvince(rs.getInt("provincia"));
        user.setGender(rs.getString("sesso"));
        user.setCf(rs.getString("cf"));
        user.setBirthDate(rs.getDate("dataNascita"));
        user.setBirthTown(rs.getString("comuneNascita"));
        user.setDoctorId(rs.getObject("idMedicoBase", Integer.class));
        return user;
    }

    public <USER_TYPE extends User> USER_TYPE fillUser(USER_TYPE user, ResultSet rs) throws SQLException {
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("nome"));
        user.setEmail(rs.getString("email"));
        user.setPassword(rs.getString("password"));
        user.setProvince(rs.getInt("provincia"));
        return user;
    }
}

