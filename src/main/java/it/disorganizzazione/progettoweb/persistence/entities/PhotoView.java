package it.disorganizzazione.progettoweb.persistence.entities;

public class PhotoView extends PhotoId {
    private String path;

    public PhotoView(Photo photo) {
        super(photo.getUserId(), photo.getCreationTimestamp());
        path = photo.getPath();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
