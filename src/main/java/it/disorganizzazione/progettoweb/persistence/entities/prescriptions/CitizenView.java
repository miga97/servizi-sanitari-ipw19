package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;

public class CitizenView {
    private Citizen patient;
    private Province patientProvince;
    private PhotoView patientPhoto;

    public Citizen getPatient() {
        return patient;
    }

    public void setPatient(Citizen patient) {
        this.patient = patient;
    }

    public Province getPatientProvince() {
        return patientProvince;
    }

    public void setPatientProvince(Province patientProvince) {
        this.patientProvince = patientProvince;
    }

    public PhotoView getPatientPhoto() {
        return patientPhoto;
    }

    public void setPatientPhoto(PhotoView patientPhoto) {
        this.patientPhoto = patientPhoto;
    }

    public CitizenView(Citizen patient, Province patientProvince, PhotoView patientPhoto) {
        this.patient = patient;
        this.patientProvince = patientProvince;
        this.patientPhoto = patientPhoto;
    }
}
