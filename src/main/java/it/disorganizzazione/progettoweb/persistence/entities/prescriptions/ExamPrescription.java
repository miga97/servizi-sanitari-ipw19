package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

public class ExamPrescription extends Prescription {

    private Integer examId;
    private String results;

    public ExamPrescription() {
        this.setCost(11.00f);
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }
}
