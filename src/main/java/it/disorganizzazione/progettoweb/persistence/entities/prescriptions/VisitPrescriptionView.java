package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;

public class VisitPrescriptionView extends PrescriptionView {

    private String visitName;
    private VisitPrescription visitPrescription;
    private Citizen performer;

    public VisitPrescriptionView() {
    }

    public VisitPrescriptionView(Doctor planner, Province plannerProvince, PhotoView plannerPhoto, Citizen patient, Province patientProvince, PhotoView patientPhoto, Province performerProvince, PhotoView performerPhoto, String visitName, VisitPrescription visitPrescription, Citizen performer) {
        super(planner, plannerProvince, plannerPhoto, patient, patientProvince, patientPhoto, performerProvince, performerPhoto);
        this.visitName = visitName;
        this.visitPrescription = visitPrescription;
        this.performer = performer;
    }

    public String getVisitName() {
        return visitName;
    }

    public void setVisitName(String visitName) {
        this.visitName = visitName;
    }

    public VisitPrescription getVisitPrescription() {
        return visitPrescription;
    }

    public void setVisitPrescription(VisitPrescription visitPrescription) {
        this.visitPrescription = visitPrescription;
    }

    public void setPerformer(Citizen performer) {
        this.performer = performer;
    }

}
