package it.disorganizzazione.progettoweb.persistence.entities;

import java.sql.Timestamp;

public class PhotoId {
    private Integer userId;
    protected Timestamp creationTimestamp;

    public PhotoId() {
    }

    public PhotoId(Integer userId, Timestamp creationTimestamp) {
        this.userId = userId;
        this.creationTimestamp = creationTimestamp;
    }

    public PhotoId getPhotoId(){
        return this;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }
}
