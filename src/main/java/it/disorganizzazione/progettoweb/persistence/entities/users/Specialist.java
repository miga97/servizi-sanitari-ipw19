package it.disorganizzazione.progettoweb.persistence.entities.users;

/**
 * Classe che identifica il medico specialista
 */
public class Specialist extends Citizen {
    public Specialist() {
        role = 2;
    }
}
