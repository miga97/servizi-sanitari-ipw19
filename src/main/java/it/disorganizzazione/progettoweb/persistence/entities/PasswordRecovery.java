package it.disorganizzazione.progettoweb.persistence.entities;

import it.disorganizzazione.progettoweb.persistence.entities.users.User;

import java.sql.Timestamp;

public class PasswordRecovery {

    private Integer userId;
    private String token;
    private Timestamp generationTimestamp;
    public PasswordRecovery(){
    }
    public PasswordRecovery(User user, String token){
        this.userId=user.getId();
        this.token=token;
        this.generationTimestamp=new Timestamp(System.currentTimeMillis());
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getGenerationTimestamp() {
        return generationTimestamp;
    }

    public void setGenerationTimestamp(Timestamp generationTimestamp) {
        this.generationTimestamp = generationTimestamp;
    }

    public Boolean isValid() {
        return this.isValid(86400);
    }

    public Boolean isValid(int SECONDS) {
        Timestamp actual = new Timestamp(System.currentTimeMillis());
        long milliseconds1 = this.getGenerationTimestamp().getTime();
        long milliseconds2 = actual.getTime();

        long diff = milliseconds2 - milliseconds1;
        long diffSeconds = diff / 1000;

        return diffSeconds <= SECONDS;
    }
}
