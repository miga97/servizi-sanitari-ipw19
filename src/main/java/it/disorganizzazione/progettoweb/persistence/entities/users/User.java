package it.disorganizzazione.progettoweb.persistence.entities.users;

import java.util.Objects;

public abstract class User {
    private Integer id;
    private String name;
    private String email;
    private String password;
    private Integer province;
    protected Integer role;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getRole() { return role; }

    public boolean isOnlyCitizen() { return role == 0; }

    public boolean isCitizen() { return role >= 0 && role <= 2; }

    public boolean isMedic() { return role == 1 || role == 2; }

    public boolean isDoctor() { return role == 1; }

    public boolean isSpecialist() { return role == 2; }

    public boolean isSsp() { return role == 3; }

    public boolean isPharmacy() { return role == 4; }

    public void setRole(Integer role) { this.role = role; }

}

