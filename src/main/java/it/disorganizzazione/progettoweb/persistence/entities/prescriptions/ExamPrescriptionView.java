package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.persistence.entities.users.Ssp;

public class ExamPrescriptionView extends PrescriptionView {
    private String examName;
    private ExamPrescription examPrescription;
    private Ssp performer;

    public ExamPrescriptionView() {
    }

    public ExamPrescriptionView(Doctor planner, Province plannerProvince, PhotoView plannerPhoto, Citizen patient, Province patientProvince, PhotoView patientPhoto, Province performerProvince, PhotoView performerPhoto, String examName, ExamPrescription examPrescription, Ssp performer) {
        super(planner, plannerProvince, plannerPhoto, patient, patientProvince, patientPhoto, performerProvince, performerPhoto);
        this.examName = examName;
        this.examPrescription = examPrescription;
        this.performer = performer;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public ExamPrescription getExamPrescription() {
        return examPrescription;
    }

    public void setExamPrescription(ExamPrescription examPrescription) {
        this.examPrescription = examPrescription;
    }

    public void setPerformer(Ssp performer) {
        this.performer = performer;
    }

}
