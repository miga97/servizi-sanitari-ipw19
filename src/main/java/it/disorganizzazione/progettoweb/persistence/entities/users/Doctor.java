package it.disorganizzazione.progettoweb.persistence.entities.users;

/**
 * Classe che identifica il medico di base
 */
public class Doctor extends Citizen {
    public Doctor() {
        role = 1;
    }
}
