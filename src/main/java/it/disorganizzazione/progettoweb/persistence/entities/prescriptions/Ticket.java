package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

public class Ticket extends Prescription {
    private String description;
    private Integer performerRole;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) { this.description = description; }

    public Integer getPerformerRole() {
        return performerRole;
    }

    public void setPerformerRole(Integer performerRole) {
        this.performerRole = performerRole;
    }
}
