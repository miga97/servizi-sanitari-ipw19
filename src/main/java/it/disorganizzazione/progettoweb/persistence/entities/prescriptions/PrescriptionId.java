package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

public class PrescriptionId {
    private Integer plannerId;
    private Integer patientId;
    private Timestamp prescriptionTimestamp;

    public PrescriptionId() {
    }

    public PrescriptionId(Integer plannerId, Integer patientId, Timestamp prescriptionTimestamp) {
        this.plannerId = plannerId;
        this.patientId = patientId;
        this.prescriptionTimestamp = prescriptionTimestamp;
    }

    public PrescriptionId getPrescriptionId(){
        return this;
    }

    public Integer getPlannerId() {
        return plannerId;
    }

    public void setPlannerId(Integer plannerId) {
        this.plannerId = plannerId;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Timestamp getPrescriptionTimestamp() {
        return prescriptionTimestamp;
    }

    public void setPrescriptionTimestamp(Timestamp prescriptionTimestamp) {
        this.prescriptionTimestamp = prescriptionTimestamp;
    }
    public static PrescriptionId getFromRequest(HttpServletRequest request) throws NullPointerException{
        int plannerId = Integer.parseInt(request.getParameter("plannerId"));
        int patientId = Integer.parseInt(request.getParameter("patientId"));

        String timestampAsString = Objects.requireNonNull(request.getParameter("prescriptionTimestamp"));
        LocalDateTime localDateTime = LocalDateTime.parse(timestampAsString);
        Timestamp prescriptionTimestamp = Timestamp.valueOf(localDateTime);

        return new PrescriptionId(plannerId, patientId, prescriptionTimestamp);
    }
    public String getUniqueId() {
        return "" +prescriptionTimestamp.getTime() +patientId +plannerId;
    }
}
