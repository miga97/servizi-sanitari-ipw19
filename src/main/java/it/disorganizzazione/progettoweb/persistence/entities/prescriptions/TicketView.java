package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;

public class TicketView extends PrescriptionView {
    private Ticket ticket;
    private Citizen performer;

    public TicketView(Ticket ticket, Doctor planner, Province plannerProvince, PhotoView plannerPhoto, Citizen patient,
                      Province patientProvince, PhotoView patientPhoto, Province performerProvince, PhotoView performerPhoto, Citizen performer) {
        super(planner, plannerProvince, plannerPhoto, patient, patientProvince, patientPhoto, performerProvince, performerPhoto);
        this.performer = performer;
        this.ticket = ticket;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Citizen getPerformer() {
        return performer;
    }

    public void setPerformer(Citizen performer) {
        this.performer = performer;
    }
}
