package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import java.sql.Timestamp;

public abstract class Prescription extends PrescriptionId {
    private Float cost;
    private Integer performerId;
    private Timestamp completionTimestamp;

    public Prescription() {
        this.setPrescriptionTimestamp(new Timestamp(System.currentTimeMillis()));
    }

    public boolean isComplete() {
        return completionTimestamp != null;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Integer getPerformerId() {
        return performerId;
    }

    public void setPerformerId(Integer performerId) {
        this.performerId = performerId;
    }

    public Timestamp getCompletionTimestamp() {
        return completionTimestamp;
    }

    public void setCompletionTimestamp(Timestamp completionTimestamp) {
        this.completionTimestamp = completionTimestamp;
    }
}
