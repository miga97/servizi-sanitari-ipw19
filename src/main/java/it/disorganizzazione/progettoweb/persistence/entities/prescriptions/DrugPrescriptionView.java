package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.persistence.entities.users.Pharmacy;

public class DrugPrescriptionView extends PrescriptionView {

    private DrugPrescription drugPrescription;
    private Pharmacy performer;

    public DrugPrescriptionView() {
    }

    public DrugPrescriptionView(Doctor planner, Province plannerProvince, PhotoView plannerPhoto, Citizen patient, Province patientProvince, PhotoView patientPhoto, Province performerProvince, PhotoView performerPhoto, DrugPrescription drugPrescription, Pharmacy performer) {
        super(planner, plannerProvince, plannerPhoto, patient, patientProvince, patientPhoto, performerProvince, performerPhoto);
        this.drugPrescription = drugPrescription;
        this.performer = performer;
    }

    public DrugPrescription getDrugPrescription() {
        return drugPrescription;
    }

    public void setDrugPrescription(DrugPrescription drugPrescription) {
        this.drugPrescription = drugPrescription;
    }

    public void setPerformer(Pharmacy performer) {
        this.performer = performer;
    }

    public boolean equals(DrugPrescription dp) {
        if (drugPrescription.getPrescriptionTimestamp().getTime() != (dp.getPrescriptionTimestamp()).getTime())
            return false;
        if (drugPrescription.getPlannerId() != dp.getPlannerId())
            return false;
        return drugPrescription.getPatientId() == dp.getPatientId();
    }
    public boolean equals(DrugPrescriptionView dpv) {
        if (drugPrescription.getPrescriptionTimestamp().getTime() != (dpv.drugPrescription.getPrescriptionTimestamp()).getTime())
            return false;
        if (drugPrescription.getPlannerId().intValue() != dpv.drugPrescription.getPlannerId().intValue())
            return false;
        return drugPrescription.getPatientId().intValue() == dpv.drugPrescription.getPatientId().intValue();
    }

}
