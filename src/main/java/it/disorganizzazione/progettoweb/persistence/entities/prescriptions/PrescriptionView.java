package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;

public abstract class PrescriptionView {

    private Doctor planner;
    private Province plannerProvince;
    private PhotoView plannerPhoto;
    private Citizen patient;
    private Province patientProvince;
    private PhotoView patientPhoto;
    private Province performerProvince;
    private PhotoView performerPhoto;

    PrescriptionView() {
    }

    PrescriptionView(Doctor planner, Province plannerProvince, PhotoView plannerPhoto, Citizen patient, Province patientProvince, PhotoView patientPhoto, Province performerProvince, PhotoView performerPhoto) {
        this.planner = planner;
        this.plannerProvince = plannerProvince;
        this.plannerPhoto = plannerPhoto;
        this.patient = patient;
        this.patientProvince = patientProvince;
        this.patientPhoto = patientPhoto;
        this.performerProvince = performerProvince;
        this.performerPhoto = performerPhoto;
    }

    public Doctor getPlanner() {
        return planner;
    }

    public void setPlanner(Doctor planner) {
        this.planner = planner;
    }

    public Province getPlannerProvince() {
        return plannerProvince;
    }

    public void setPlannerProvince(Province plannerProvince) {
        this.plannerProvince = plannerProvince;
    }

    public PhotoView getPlannerPhoto() {
        return plannerPhoto;
    }

    public void setPlannerPhoto(PhotoView plannerPhoto) {
        this.plannerPhoto = plannerPhoto;
    }

    public Citizen getPatient() {
        return patient;
    }

    public void setPatient(Citizen patient) {
        this.patient = patient;
    }

    public Province getPatientProvince() {
        return patientProvince;
    }

    public void setPatientProvince(Province patientProvince) {
        this.patientProvince = patientProvince;
    }

    public PhotoView getPatientPhoto() {
        return patientPhoto;
    }

    public void setPatientPhoto(PhotoView patientPhoto) {
        this.patientPhoto = patientPhoto;
    }

    public Province getPerformerProvince() {
        return performerProvince;
    }

    public void setPerformerProvince(Province performerProvince) {
        this.performerProvince = performerProvince;
    }

    public PhotoView getPerformerPhoto() {
        return performerPhoto;
    }

    public void setPerformerPhoto(PhotoView performerPhoto) {
        this.performerPhoto = performerPhoto;
    }
}
