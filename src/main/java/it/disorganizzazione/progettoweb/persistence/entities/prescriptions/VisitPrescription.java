package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

public class VisitPrescription extends Prescription {

    private Integer visitId;
    private String anamnesi;

    public VisitPrescription() {
        this.setVisitId(80);
        this.setCost(50.00f);
    }

    public Integer getVisitId() {
        return visitId;
    }

    public void setVisitId(Integer visitId) {
        this.visitId = visitId;
    }

    public String getAnamnesi() {
        return anamnesi;
    }

    public void setAnamnesi(String anamnesi) {
        this.anamnesi = anamnesi;
    }
}
