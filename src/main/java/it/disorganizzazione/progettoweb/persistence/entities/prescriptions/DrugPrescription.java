package it.disorganizzazione.progettoweb.persistence.entities.prescriptions;

public class DrugPrescription extends Prescription {

    private String description;

    public DrugPrescription() {
        this.setCost(3.00f);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
