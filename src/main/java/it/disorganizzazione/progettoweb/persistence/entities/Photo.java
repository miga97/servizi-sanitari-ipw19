package it.disorganizzazione.progettoweb.persistence.entities;

import java.sql.Timestamp;

public class Photo extends PhotoId {
    private String extension;

    public Photo() {
    }

    public Photo(Integer userId, Timestamp creationTimestamp, String extension) {
        super(userId, creationTimestamp);
        this.extension = extension;
    }

    public static Photo defaultPhoto() {
        return new Photo();
    }

    public String getFilename() {
        return this.creationTimestamp.getTime() + "." + extension;
    }

    public void setExtension(String extension) { this.extension = extension; }

    public String getExtension() { return extension; }

    public String getPath() {
        if (extension != null) return "/photos/" + this.getUserId() + "/" + getFilename();
        else return "/photos/default.jpg";
    }

    @Override
    public String toString(){
        return getPath();
    }
}
