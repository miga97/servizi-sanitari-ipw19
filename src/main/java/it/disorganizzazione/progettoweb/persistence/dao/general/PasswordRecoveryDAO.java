package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.PasswordRecovery;

import javax.validation.constraints.NotNull;

public interface PasswordRecoveryDAO extends DAO<PasswordRecovery, Integer> {
    /**
     * Deletes the passwordRecovery with the userId passed as parameter from the storage system.
     *
     * @param userId the userId of the passwordRecovery that should be deleted
     * @throws DAOException if an error occurred during the delete
     * @author Matteo Zanella
     */
    void deleteByPrimaryKey(int userId) throws DAOException;

    /**
     * Gets the passwordRecovery with the passed token, that is unique.
     *
     * @param token the token of the PasswordRecovery we are trying to get from db
     * @return the PasswordRecovery object with the correct token.
     * @throws DAOException if an error occurred during the information retrieving
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    PasswordRecovery getByToken(@NotNull String token) throws DAOException;

    /**
     * Persists the new {@param PasswordRecovery passwordRecovery} passed as parameter to the storage system.
     * Deletes the row with the same primary key if it already exists and then inserts the new PasswordRecovery object
     *
     * @param passwordRecovery the new {@code passwordRecovery} to persist.
     * @return the inserted PasswordRecovery object.
     * @throws DAOException if an error occurred during the replace
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    PasswordRecovery replace(@NotNull PasswordRecovery passwordRecovery) throws DAOException;
}
