package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCVisitPrescriptionDAO extends JDBCDAO<VisitPrescription, PrescriptionId> implements VisitPrescriptionDAO {

    /**
     * The constructor for the JDBC PrescribedVisit DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCVisitPrescriptionDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM VisitePrescritte");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count prescribedVisits", ex);
        }
        return null;
    }

    @Override
    public VisitPrescription getByPrimaryKey(@NotNull PrescriptionId primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM VisitePrescritte WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey.getPlannerId()));
            stm.setInt(2, Objects.requireNonNull(primaryKey.getPatientId()));
            stm.setTimestamp(3, Objects.requireNonNull(primaryKey.getPrescriptionTimestamp()));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return getPrescribedVisit(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the prescribedVisit", ex);
        }
    }

    @Override
    public List<VisitPrescription> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM VisitePrescritte")) {
            List<VisitPrescription> visitPrescriptions = new ArrayList<>();
            while (rs.next()) {
                visitPrescriptions.add(getPrescribedVisit(rs));
            }
            return visitPrescriptions;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedVisits", ex);
        }
    }

    @Override
    public List<VisitPrescription> getByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM VisitePrescritte WHERE idPaziente = ? ORDER BY dataOraPrescrizione DESC")) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                List<VisitPrescription> visitPrescriptions = new ArrayList<>();
                while (rs.next()) {
                    visitPrescriptions.add(getPrescribedVisit(rs));
                }
                return visitPrescriptions;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedVisits", ex);
        }
    }

    @Override
    public List<VisitPrescription> getByCitizenId(int citizenId, boolean completed) throws DAOException {
        String query;
        if (completed) {
            query = "SELECT * FROM VisitePrescritte WHERE idPaziente = ? AND dataOraEvasione IS NOT NULL ORDER BY dataOraPrescrizione DESC";
        } else {
            query = "SELECT * FROM VisitePrescritte WHERE idPaziente = ? AND dataOraEvasione IS NULL ORDER BY dataOraPrescrizione DESC";
        }
        try (PreparedStatement stm = prepareStatement(query)) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                List<VisitPrescription> visitPrescriptions = new ArrayList<>();
                while (rs.next()) {
                    visitPrescriptions.add(getPrescribedVisit(rs));
                }
                return visitPrescriptions;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedVisits", ex);
        }
    }

    @Override
    public float getExpenseByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT COALESCE(sum(costo),0) AS spesa FROM VisitePrescritte WHERE idPaziente = ? AND dataOraEvasione IS NOT NULL")) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                return rs.getFloat("spesa");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the expense of prescribedVisits", ex);
        }
    }

    @Override
    public VisitPrescription insert(@NotNull VisitPrescription visitPrescription) throws DAOException {
        try (PreparedStatement ps = prepareStatement("INSERT INTO VisitePrescritte(idDottore,idPaziente,dataOraPrescrizione,idVisita,costo,idMedico,anamnesi,dataOraEvasione) VALUES(?, ?, ?, ?, ?, ?, ?, ?)")) {
            ps.setInt(1, Objects.requireNonNull(visitPrescription.getPlannerId()));
            ps.setInt(2, Objects.requireNonNull(visitPrescription.getPatientId()));
            ps.setTimestamp(3, Objects.requireNonNull(visitPrescription.getPrescriptionTimestamp()));
            ps.setInt(4, Objects.requireNonNull(visitPrescription.getVisitId()));
            ps.setFloat(5, Objects.requireNonNull(visitPrescription.getCost()));
            ps.setObject(6, visitPrescription.getPerformerId(), Types.INTEGER);
            ps.setString(7, visitPrescription.getAnamnesi());
            ps.setTimestamp(8, visitPrescription.getCompletionTimestamp());
            if (ps.executeUpdate() == 1) {
                return visitPrescription;
            } else {
                throw new DAOException("Impossible to insert the prescribedVisit");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the prescribedVisit", ex);
        }
    }

    @Override
    public VisitPrescription complete(@NotNull VisitPrescription visitPrescription) throws DAOException {
        try (PreparedStatement ps = prepareStatement("UPDATE VisitePrescritte SET idMedico = ?, anamnesi = ?, dataOraEvasione = ? WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            ps.setObject(1, visitPrescription.getPerformerId(), Types.INTEGER);
            ps.setString(2, visitPrescription.getAnamnesi());
            ps.setTimestamp(3, visitPrescription.getCompletionTimestamp());
            ps.setInt(4, Objects.requireNonNull(visitPrescription.getPlannerId()));
            ps.setInt(5, Objects.requireNonNull(visitPrescription.getPatientId()));
            ps.setTimestamp(6, Objects.requireNonNull(visitPrescription.getPrescriptionTimestamp()));
            if (ps.executeUpdate() == 1) {
                return visitPrescription;
            } else {
                throw new DAOException("Impossible to update the prescribedVisit");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the prescribedVisit", ex);
        }
    }

    @Override
    public List<VisitPrescriptionView> getAllView(Boolean status) throws DAOException {
        try (ResultSet rs = createStatement().executeQuery(queryfier("SELECT * FROM VisitePrescritteView", status))) {
            List<VisitPrescriptionView> visitPrescriptionViews = new ArrayList<>();
            while (rs.next()) {
                visitPrescriptionViews.add(getPrescribedVisitView(rs));
            }
            return visitPrescriptionViews;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of visitPrescriptionViews", ex);
        }
    }

    @Override
    public List<VisitPrescriptionView> getFilteredView(Boolean status, User user) throws DAOException {
        try (ResultSet rs = createStatement().executeQuery(queryfier("SELECT * FROM VisitePrescritteView", status))) {
            List<VisitPrescriptionView> visitPrescriptionViews = new ArrayList<>();
            VisitPrescriptionView visit;
            while (rs.next()) {
                visit = getPrescribedVisitView(rs);
                if (user != null) {
                    if (user.getId() != visit.getVisitPrescription().getPatientId())
                        visitPrescriptionViews.add(visit);
                } else visitPrescriptionViews.add(visit);
            }
            return visitPrescriptionViews;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of visitPrescriptionViews", ex);
        }
    }

    @Override
    public List<VisitPrescriptionView> getViewByPerformerId(int performerId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM VisitePrescritteView WHERE idMedico=?", status))) {
            return getVisitPrescriptionViews(performerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of visitPrescriptionViews", ex);
        }
    }

    @Override
    public List<VisitPrescriptionView> getViewByPlannerId(int plannerId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM VisitePrescritteView WHERE idDottore=?", status))) {
            return getVisitPrescriptionViews(plannerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of visitPrescriptionViews", ex);
        }
    }

    @Override
    public List<VisitPrescriptionView> getViewByCitizenId(int citizenId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM VisitePrescritteView WHERE idPaziente=?", status))) {
            return getVisitPrescriptionViews(citizenId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of visitPrescriptionViews", ex);
        }
    }

    private List<VisitPrescriptionView> getVisitPrescriptionViews(int id, PreparedStatement stm) throws SQLException {
        stm.setInt(1, id);
        try (ResultSet rs = stm.executeQuery()) {
            List<VisitPrescriptionView> visitPrescriptionViews = new ArrayList<>();
            while (rs.next()) {
                visitPrescriptionViews.add(getPrescribedVisitView(rs));
            }
            return visitPrescriptionViews;
        }
    }

    private VisitPrescriptionView getPrescribedVisitView(ResultSet rs) throws SQLException {
        String visitName = rs.getObject("nomeVisita", String.class);
        VisitPrescription visitPrescription = getPrescribedVisit(rs);
        //planner
        Doctor planner = new Doctor();
        planner.setId(rs.getObject("idDottore", Integer.class));
        planner.setName(rs.getObject("nomeDottore", String.class));
        planner.setSurname(rs.getObject("cognomeDottore", String.class));

        //fix for richiamo
        int costo = rs.getObject("costo",Integer.class);
        if (costo == 0){
            planner.setRole(3);
        }

        Province plannerProvince = new Province();
        plannerProvince.setAcronym(rs.getObject("siglaProvinciaDottore", String.class));
        plannerProvince.setName(rs.getObject("nomeProvinciaDottore", String.class));

        Photo plannerPhoto = new Photo();
        plannerPhoto.setExtension(rs.getObject("extensionFotoDottore", String.class));
        plannerPhoto.setCreationTimestamp(rs.getObject("DOCFotoDottore", Timestamp.class));
        plannerPhoto.setUserId(rs.getObject("idDottore", Integer.class));
        PhotoView plannerPhotoView = new PhotoView(plannerPhoto);

        //end planner -- start patient
        Citizen patient = new Citizen();
        patient.setDoctorId(rs.getObject("idDottore", Integer.class));
        patient.setCf(rs.getObject("cfPaziente", String.class));
        patient.setId(rs.getObject("idPaziente", Integer.class));
        patient.setName(rs.getObject("nomePaziente", String.class));
        patient.setSurname(rs.getObject("cognomePaziente", String.class));

        Province patientProvince = new Province();
        patientProvince.setAcronym(rs.getObject("siglaProvinciaPaziente", String.class));
        patientProvince.setName(rs.getObject("nomeProvinciaPaziente", String.class));

        Photo patientPhoto = new Photo();
        patientPhoto.setExtension(rs.getObject("extensionFotoPaziente", String.class));
        patientPhoto.setCreationTimestamp(rs.getObject("DOCFotoPaziente", Timestamp.class));
        patientPhoto.setUserId(rs.getObject("idPaziente", Integer.class));
        PhotoView patientPhotoView = new PhotoView(patientPhoto);

        //end patient -- start performer
        Citizen performer = new Citizen();
        performer.setId(rs.getObject("idMedico", Integer.class));
        performer.setName(rs.getObject("nomeErogatore", String.class));
        performer.setSurname(rs.getObject("cognomeErogatore", String.class));
        performer.setRole(rs.getObject("ruoloErogatore",Integer.class));

        Province performerProvince = new Province();
        performerProvince.setAcronym(rs.getObject("siglaProvinciaErogatore", String.class));
        performerProvince.setName(rs.getObject("nomeProvinciaErogatore", String.class));

        Photo performerPhoto = new Photo();
        performerPhoto.setExtension(rs.getObject("extensionFotoErogatore", String.class));
        performerPhoto.setCreationTimestamp(rs.getObject("DOCFotoErogatore", Timestamp.class));
        performerPhoto.setUserId(rs.getObject("idMedico", Integer.class));
        PhotoView performerPhotoView = new PhotoView(performerPhoto);
        //end performer

        return new VisitPrescriptionView(planner, plannerProvince, plannerPhotoView, patient, patientProvince, patientPhotoView,
                performerProvince, performerPhotoView, visitName, visitPrescription, performer);
    }

    private VisitPrescription getPrescribedVisit(ResultSet rs) throws SQLException {
        VisitPrescription visitPrescription = new VisitPrescription();
        visitPrescription.setPlannerId(rs.getInt("idDottore"));
        visitPrescription.setPatientId(rs.getInt("idPaziente"));
        visitPrescription.setPrescriptionTimestamp(rs.getTimestamp("dataOraPrescrizione"));
        visitPrescription.setVisitId(rs.getInt("idVisita"));
        visitPrescription.setCost(rs.getFloat("costo"));
        visitPrescription.setPerformerId(rs.getObject("idMedico", Integer.class));
        visitPrescription.setAnamnesi(rs.getString("anamnesi"));
        visitPrescription.setCompletionTimestamp(rs.getTimestamp("dataOraEvasione"));
        return visitPrescription;
    }

    private String queryfier(String query, Boolean status) {
        if (status != null) {
            String identifier = " WHERE";
            if (query.toLowerCase().contains(" where ")) identifier = " AND";
            if (status) query += identifier + " dataOraEvasione is NOT NULL";
            else query += identifier + " dataOraEvasione is NULL";
        }
        return query;
    }
}
