/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.persistence.dao.users.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.CitizenDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.CitizenView;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.factories.UserFactory;
import it.disorganizzazione.progettoweb.utils.BCryptSingleton;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCCitizenDAO extends JDBCDAO<Citizen, Integer> implements CitizenDAO {

    private final UserFactory userFactory = new UserFactory();

    /**
     * The constructor for the JDBC Citizen DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCCitizenDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stm = createStatement()) {
            ResultSet counter = stm.executeQuery("SELECT COUNT(*) FROM Utenti WHERE ruolo >=0 AND ruolo <= 2");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count citizens", ex);
        }
        return null;
    }

    @Override
    public Citizen getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE id = ? AND ruolo >=0 AND ruolo <= 2")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    Citizen citizen = userFactory.getCitizen(rs.getInt("ruolo"));
                    return userFactory.fillUser(citizen, rs);
                } else {
                    return null;
                }
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("Citizen role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the citizen", ex);
        }
    }

    @Override
    public List<Citizen> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Utenti WHERE ruolo >=0 AND ruolo <= 2 ORDER BY cognome")) {
            List<Citizen> citizens = new ArrayList<>();
            while (rs.next()) {
                Citizen citizen = userFactory.getCitizen(rs.getInt("ruolo"));
                citizens.add(userFactory.fillUser(citizen, rs));
            }
            return citizens;
        } catch (IllegalArgumentException ex) {
            throw new DAOException("Citizen role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of citizens", ex);
        }
    }

    @Override
    public List<Citizen> getByDoctorId(int doctorId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE idMedicoBase = ? AND ruolo >=0 AND ruolo <= 2")) {
            stm.setInt(1, doctorId);
            try (ResultSet rs = stm.executeQuery()) {
                List<Citizen> citizens = new ArrayList<>();
                while (rs.next()) {
                    Citizen citizen = userFactory.getCitizen(rs.getInt("ruolo"));
                    citizens.add(userFactory.fillUser(citizen, rs));
                }
                return citizens;
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("Citizen role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of citizens", ex);
        }
    }

    @Override
    public Citizen getByEmailAndPassword(@NotNull String email, @NotNull String password) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE email = ? AND ruolo >=0 AND ruolo <= 2")) {
            stm.setString(1, Objects.requireNonNull(email));
            /* Non si può ottenere la password direttamente dal BCrypt quindi otteniamo l'eventuale unico cittadino,
            verifichiamo la password e se abbiamo una password valida ritorniamo quel cittadino,
            altrimenti DAOException */
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next() && BCryptSingleton.Instance.verifyHash(password, rs.getString("password"))) {
                    Citizen citizen = userFactory.getCitizen(rs.getInt("ruolo"));
                    return userFactory.fillUser(citizen, rs);
                } else {
                    return null;
                }
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("Citizen role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the citizen", ex);
        }
    }

    @Override
    public List<Citizen> getByProvince(int provinceId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE provincia = ? AND ruolo >=0 AND ruolo <= 2")) {
            stm.setInt(1, provinceId);
            try (ResultSet rs = stm.executeQuery()) {
                List<Citizen> citizens = new ArrayList<>();
                while (rs.next()) {
                    Citizen citizen = userFactory.getCitizen(rs.getInt("ruolo"));
                    citizens.add(userFactory.fillUser(citizen, rs));
                }
                return citizens;
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("Citizen role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of citizens", ex);
        }
    }

    @Override
    public Citizen insert(@NotNull Citizen citizen) throws DAOException {
        try (PreparedStatement stm = prepareStatement("INSERT INTO Utenti(nome,cognome,email,password,provincia,sesso,cf,dataNascita,comuneNascita,ruolo,idMedicoBase) VALUES(?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
            stm.setString(1, Objects.requireNonNull(citizen.getName()));
            stm.setString(2, Objects.requireNonNull(citizen.getSurname()));
            stm.setString(3, Objects.requireNonNull(citizen.getEmail()));
            stm.setString(4, BCryptSingleton.Instance.hash(Objects.requireNonNull(citizen.getPassword())));
            stm.setInt(5, Objects.requireNonNull(citizen.getProvince()));
            stm.setString(6, Objects.requireNonNull(citizen.getGender()));
            stm.setString(7, Objects.requireNonNull(citizen.getCf()));
            stm.setDate(8, Objects.requireNonNull(citizen.getBirthDate()));
            stm.setString(9, Objects.requireNonNull(citizen.getBirthTown()));
            stm.setInt(10, Objects.requireNonNull(citizen.getRole()));
            stm.setObject(11, citizen.getDoctorId(), Types.INTEGER);
            if (stm.executeUpdate() == 1) {
                ResultSet generatedKeys = stm.getGeneratedKeys();
                generatedKeys.next();
                citizen.setId((int) generatedKeys.getLong(1));
                return citizen;
            } else {
                throw new DAOException("Impossible to insert the citizen");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the citizen", ex);
        }
    }

    @Override
    public Citizen update(@NotNull Citizen citizen) throws DAOException {
        try (PreparedStatement stm = prepareStatement("UPDATE Utenti SET nome = ?, cognome = ?, email = ?, provincia = ?, sesso = ?, cf = ?, dataNascita = ?, comuneNascita = ?, idMedicoBase = ? WHERE id = ?")) {
            stm.setString(1, Objects.requireNonNull(citizen.getName()));
            stm.setString(2, Objects.requireNonNull(citizen.getSurname()));
            stm.setString(3, Objects.requireNonNull(citizen.getEmail()));
            stm.setInt(4, Objects.requireNonNull(citizen.getProvince()));
            stm.setString(5, Objects.requireNonNull(citizen.getGender()));
            stm.setString(6, Objects.requireNonNull(citizen.getCf()));
            stm.setDate(7, Objects.requireNonNull(citizen.getBirthDate()));
            stm.setString(8, Objects.requireNonNull(citizen.getBirthTown()));
            stm.setObject(9, citizen.getDoctorId());
            stm.setInt(10, Objects.requireNonNull(citizen.getId()));
            if (stm.executeUpdate() == 1) {
                return getByPrimaryKey(citizen.getId());
            } else {
                throw new DAOException("Impossible to update the citizen");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the citizen", ex);
        }
    }

    @Override
    public List<CitizenView> getViewByDoctorId(int doctorId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM UtenteView WHERE idMedicoBase=?")) {
            return getCitizenViews(doctorId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of citizenViews", ex);
        }
    }

    @Override
    public List<CitizenView> getAllView() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM UtenteView")) {
            List<CitizenView> citizenViews = new ArrayList<>();
            while (rs.next()) {
                citizenViews.add(getCitizenView(rs));
            }
            return citizenViews;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of citizenViews", ex);
        }
    }

    private List<CitizenView> getCitizenViews(int id, PreparedStatement stm) throws SQLException {
        stm.setInt(1, id);
        try (ResultSet rs = stm.executeQuery()) {
            List<CitizenView> citizenViews = new ArrayList<>();
            while (rs.next()) {
                citizenViews.add(getCitizenView(rs));
            }
            return citizenViews;
        }
    }
    private CitizenView getCitizenView(ResultSet rs) throws SQLException {
        //start patient
        Citizen patient = new Citizen();
        patient.setDoctorId(rs.getObject("idMedicoBase",Integer.class));
        patient.setCf(rs.getObject("cf",String.class));
        patient.setId(rs.getObject("id",Integer.class));
        patient.setName(rs.getObject("nome",String.class));
        patient.setSurname(rs.getObject("cognome",String.class));
        patient.setGender(rs.getObject("sesso",String.class));
        patient.setBirthDate(rs.getObject("dataNascita",Date.class));
        patient.setBirthTown(rs.getObject("comuneNascita",String.class));


        Province patientProvince = new Province();
        patientProvince.setAcronym(rs.getObject("siglaProvincia",String.class));
        patientProvince.setName(rs.getObject("nomeProvincia",String.class));

        Photo patientPhoto = new Photo();
        patientPhoto.setExtension(rs.getObject("extensionFoto",String.class));
        patientPhoto.setCreationTimestamp(rs.getObject("dataOraCreazioneFoto",Timestamp.class));
        patientPhoto.setUserId(rs.getObject("id",Integer.class));
        PhotoView patientPhotoView = new PhotoView(patientPhoto);
        //end patient
        return new CitizenView(patient, patientProvince, patientPhotoView);
    }
}
