package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescription;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface DrugPrescriptionDAO extends DAO<DrugPrescription, PrescriptionId> {

    /**
     * Get a single record of DrugPrescriptionView
     *
     * @param primaryKey The 3 primaryKey
     * @return The DrugPrescriptionView that has been persisted in the DB
     * @throws DAOException if an error occurred during the information retrieving
     * @author Giulio Migani
     */
    DrugPrescriptionView getViewByPrimaryKey(@NotNull PrescriptionId primaryKey) throws DAOException;

    /**
     * Get the total cost of all completed drug prescriptions of a citizen.
     *
     * @param citizenId The Id of the citizen
     * @return The total cost of drugs prescriptions tickets
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    float getExpenseByCitizenId(int citizenId) throws DAOException;

    /**
     * Get all the prescribed drugs of a citizen, completed or not.
     * Most recently prescribed ones are listed first.
     *
     * @param citizenId The Id of the citizen
     * @return The list of prescribed drugs
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<DrugPrescription> getByCitizenId(int citizenId) throws DAOException;

    /**
     * Get the prescribed drugs of a citizen, choosing between completed or not ones.
     * Most recently prescribed ones are listed first.
     *
     * @param citizenId The Id of the citizen
     * @param completed If true, only completed ones are listed, if false only not already completed ones are listed
     * @return The list of prescribed drugs
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<DrugPrescription> getByCitizenId(int citizenId, boolean completed) throws DAOException;

    /**
     * Persists the new {@link DrugPrescription prescribedDrug} passed as parameter to the storage system.
     * Typically used to generate a new incomplete prescription.
     *
     * @param drugPrescription the new {@code prescribedDrug} to persist.
     * @return The prescribedDrug that has been persisted in the DB
     * @throws DAOException         if an error occurred during the insertion
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    DrugPrescription insert(@NotNull DrugPrescription drugPrescription) throws DAOException;

    /**
     * Complete infos about a prescribed drug, persisting the completionTimestamp and performerId fields of {@param prescribedDrug}
     * Typically used to complete the prescription after it's used
     *
     * @param drugPrescription The {@code prescribedDrug} that should be updated
     * @return The prescribedDrug that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    DrugPrescription complete(@NotNull DrugPrescription drugPrescription) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view.
     *
     * @param status True gets only completed.
     * @return The DrugPrescriptionView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<DrugPrescriptionView> getAllView(Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by performer ID.
     *
     * @param performerId The performer id
     * @param status      True gets only completed.
     * @return The List<DrugPrescriptionView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<DrugPrescriptionView> getViewByPerformerId(@NotNull int performerId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by planner ID.
     *
     * @param plannerId the planner id.
     * @param status    True gets only completed.
     * @return The List<DrugPrescriptionView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<DrugPrescriptionView> getViewByPlannerId(@NotNull int plannerId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by citizenId.
     *
     * @param citizenId the citizen id.
     * @param status    True gets only completed.
     * @return The List<DrugPrescriptionView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<DrugPrescriptionView> getViewByCitizenId(@NotNull int citizenId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by province acronym.
     *
     * @param acronym the acronym of the province.
     * @param status  True gets only completed.
     * @return The List<DrugPrescriptionView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<DrugPrescriptionView> getViewByCitizenProvince(@NotNull String acronym, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view filtered by a list
     *
     * @param status True gets only completed.
     * @param diff if diff == 1 return only the selected drugPrescriptionView in Session
     * @param receipt a list of items that should not be shown
     * @return The List<DrugPrescriptionView> that has been persisted in the DB
     * @throws DAOException if an error occurred during the update
     * @author Giulio Migani
     */
    List<DrugPrescriptionView> getFilteredView(int diff, Boolean status, List<DrugPrescriptionView> receipt) throws DAOException;

}
