package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.progettoweb.persistence.entities.Exam;

public interface ExamDAO extends DAO<Exam, Integer> {

}
