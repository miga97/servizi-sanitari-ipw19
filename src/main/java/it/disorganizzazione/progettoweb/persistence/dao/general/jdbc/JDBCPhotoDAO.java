package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.PhotoDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoId;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCPhotoDAO extends JDBCDAO<Photo, PhotoId> implements PhotoDAO {
    /**
     * The constructor for the JDBC Photo DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCPhotoDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Foto");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count photos", ex);
        }
        return null;
    }

    @Override
    public Photo getByPrimaryKey(@NotNull PhotoId primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Foto WHERE idUtente = ? AND dataOraCreazione = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey.getUserId()));
            stm.setTimestamp(2, Objects.requireNonNull(primaryKey.getCreationTimestamp()));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    return getPhoto(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the photo", ex);
        }
    }

    @Override
    public List<Photo> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Foto")) {
            List<Photo> photos = new ArrayList<>();
            while (rs.next()) {
                photos.add(getPhoto(rs));
            }
            return photos;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of photos", ex);
        }
    }

    @Override
    public List<Photo> getByUserId(int userId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Foto WHERE idUtente = ? ORDER BY dataOraCreazione DESC")) {
            stm.setInt(1, userId);
            try (ResultSet rs = stm.executeQuery()) {
                List<Photo> photos = new ArrayList<>();
                while (rs.next()) {
                    photos.add(getPhoto(rs));
                }
                return photos;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of photos", ex);
        }
    }

    @Override
    public Photo getLastByUserId(int userId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Foto WHERE idUtente = ? ORDER BY dataOraCreazione DESC LIMIT 1")) {
            stm.setInt(1, userId);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    return getPhoto(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the photo", ex);
        }
    }

    @Override
    public Photo insert(@NotNull Photo photo) throws DAOException {
        try (PreparedStatement ps = prepareStatement("INSERT INTO Foto(idUtente, dataOraCreazione, extension) VALUES(?, ?, ?)")) {
            ps.setInt(1, Objects.requireNonNull(photo.getPhotoId().getUserId()));
            ps.setTimestamp(2, Objects.requireNonNull(photo.getPhotoId().getCreationTimestamp()));
            ps.setString(3, Objects.requireNonNull(photo.getExtension()));
            if (ps.executeUpdate() == 1) {
                return photo;
            } else {
                throw new DAOException("Impossible to insert the photo");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the photo", ex);
        }
    }

    @Override
    public Photo replaceLast(@NotNull Photo photo) throws DAOException {
        // Deletes the last photo
        Photo lastPhoto = getLastByUserId(Objects.requireNonNull(photo.getPhotoId().getUserId()));
        if (lastPhoto != null) {
            deleteByPrimaryKey(lastPhoto.getPhotoId());
        }
        // Insert the parameter photo
        return insert(photo);
    }

    @Override
    public void deleteByPrimaryKey(@NotNull PhotoId photoId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("DELETE FROM Foto WHERE idUtente = ? AND dataOraCreazione = ?")) {
            stm.setInt(1, Objects.requireNonNull(photoId.getUserId()));
            stm.setTimestamp(2, Objects.requireNonNull(photoId.getCreationTimestamp()));
            if (stm.executeUpdate() != 1) {
                throw new DAOException("Impossible to delete the photo");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to delete the photo", ex);
        }
    }

    private Photo getPhoto(ResultSet rs) throws SQLException {
        Photo photo = new Photo();
        photo.getPhotoId().setUserId(rs.getInt("idUtente"));
        photo.getPhotoId().setCreationTimestamp(rs.getTimestamp("dataOraCreazione"));
        photo.setExtension(rs.getString("extension"));
        return photo;
    }
}
