/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.persistence.dao.users.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.PharmacyDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Pharmacy;
import it.disorganizzazione.progettoweb.persistence.factories.UserFactory;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCPharmacyDAO extends JDBCDAO<Pharmacy, Integer> implements PharmacyDAO {

    private final UserFactory userFactory = new UserFactory();

    /**
     * The constructor for the JDBC Pharmacy DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCPharmacyDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Utenti WHERE ruolo = 4");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count pharmacies", ex);
        }
        return null;
    }

    @Override
    public Pharmacy getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE id = ? AND ruolo = 4")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return userFactory.fillUser(new Pharmacy(), rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the pharmacy", ex);
        }
    }

    @Override
    public List<Pharmacy> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Utenti WHERE ruolo = 4 ORDER BY nome")) {
            List<Pharmacy> pharmacies = new ArrayList<>();
            while (rs.next()) {
                pharmacies.add(userFactory.fillUser(new Pharmacy(), rs));
            }
            return pharmacies;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of pharmacies", ex);
        }
    }
}
