package it.disorganizzazione.progettoweb.persistence.dao.users;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.CitizenView;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface CitizenDAO extends DAO<Citizen, Integer> {

    /**
     * Get all the patients of a specific family doctor
     *
     * @param doctorId The Id of the doctor citizen
     * @return The list of citizens followed by the doctor
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<Citizen> getByDoctorId(int doctorId) throws DAOException;

    /**
     * Get a citizen through its email and password.
     * Used for the login function.
     *
     * @param email    Login email of the citizen
     * @param password The password that the citizen is using
     * @return A citizen of the correct type
     * @throws DAOException         if an error occurred during the information retrieving
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    Citizen getByEmailAndPassword(@NotNull String email, @NotNull String password) throws DAOException;

    /**
     * Gets all citizens from a given province.
     * Used for recalls.
     *
     * @param provinceId province of the citizen
     * @return the list of citizen of the correct type, from the same {@param provinceId}
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<Citizen> getByProvince(int provinceId) throws DAOException;

    /**
     * Persists the new {@link Citizen citizen} passed as parameter to the storage system.
     * The password attribute is encrypted before the insertion.
     *
     * @param citizen the new {@code citizen} to persist.
     * @return the Citizen with the id of the new persisted record.
     * @throws DAOException         if an error occurred during the insertion
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    Citizen insert(@NotNull Citizen citizen) throws DAOException;

    /**
     * Update citizen fields using the passed citizen as parameter.
     * The password is not updated, use {@code userDAO.updatePassword(String password)} instead.
     *
     * @param citizen The citizen that should be updated
     * @return The citizen that is now loaded in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    Citizen update(@NotNull Citizen citizen) throws DAOException;
    /**
     * Needed for datatables. Gets all the values in the view by doctor ID.
     *
     * @param doctorId The doctor id
     * @return The List<CitizenView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<CitizenView> getViewByDoctorId(@NotNull int doctorId) throws DAOException;
    /**
     * Needed for datatables. Gets all the values in the view.
     *
     * @return The CitizenView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<CitizenView> getAllView() throws DAOException;
}

