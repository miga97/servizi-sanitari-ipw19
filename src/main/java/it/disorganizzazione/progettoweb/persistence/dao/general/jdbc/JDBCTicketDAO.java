package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.TicketDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.Ticket;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.TicketView;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCTicketDAO extends JDBCDAO<Ticket, PrescriptionId> implements TicketDAO {

    /**
     * The constructor for the JDBC Ticket DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCTicketDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Ticket");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count the tickets", ex);
        }
        return null;
    }

    @Override
    public Ticket getByPrimaryKey(@NotNull PrescriptionId primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Ticket WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey.getPlannerId()));
            stm.setInt(2, Objects.requireNonNull(primaryKey.getPatientId()));
            stm.setTimestamp(3, Objects.requireNonNull(primaryKey.getPrescriptionTimestamp()));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return getTicket(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the ticket", ex);
        }
    }

    @Override
    public List<Ticket> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Ticket")) {
            List<Ticket> tickets = new ArrayList<>();
            while (rs.next()) {
                tickets.add(getTicket(rs));
            }
            return tickets;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of tickets", ex);
        }
    }

    @Override
    public List<Ticket> getByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Ticket WHERE idPaziente = ? ORDER BY dataOraPrescrizione DESC")) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                List<Ticket> tickets = new ArrayList<>();
                while (rs.next()) {
                    tickets.add(getTicket(rs));
                }
                return tickets;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of tickets", ex);
        }
    }

    @Override
    public List<TicketView> getAllView() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM TicketView")) {
            List<TicketView> ticketViews = new ArrayList<>();
            while (rs.next()) {
                ticketViews.add(getTicketView(rs));
            }
            return ticketViews;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of ticketViews", ex);
        }
    }

    @Override
    public List<TicketView> getViewByPerformerId(int performerId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM TicketView WHERE idErogatore=?")) {
            return getTicketViews(performerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of ticketViews", ex);
        }
    }

    @Override
    public List<TicketView> getViewByPlannerId(int plannerId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM TicketView WHERE idDottore=?")) {
            return getTicketViews(plannerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of ticketViews", ex);
        }
    }

    @Override
    public List<TicketView> getViewByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM TicketView WHERE idPaziente=?")) {
            return getTicketViews(citizenId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of ticketViews", ex);
        }
    }

    private List<TicketView> getTicketViews(int id, PreparedStatement stm) throws SQLException {
        stm.setInt(1, id);
        try (ResultSet rs = stm.executeQuery()) {
            List<TicketView> ticketViews = new ArrayList<>();
            while (rs.next()) {
                ticketViews.add(getTicketView(rs));
            }
            return ticketViews;
        }
    }

    private TicketView getTicketView(ResultSet rs) throws SQLException {
        Ticket ticket = getTicket(rs);
        //planner
        Doctor planner = new Doctor();
        planner.setId(rs.getObject("idDottore", Integer.class));
        planner.setName(rs.getObject("nomeDottore", String.class));
        planner.setSurname(rs.getObject("cognomeDottore", String.class));

        Province plannerProvince = new Province();
        plannerProvince.setAcronym(rs.getObject("siglaProvinciaDottore", String.class));
        plannerProvince.setName(rs.getObject("nomeProvinciaDottore", String.class));

        Photo plannerPhoto = new Photo();
        plannerPhoto.setExtension(rs.getObject("extensionFotoDottore", String.class));
        plannerPhoto.setCreationTimestamp(rs.getObject("DOCFotoDottore", Timestamp.class));
        plannerPhoto.setUserId(rs.getObject("idDottore", Integer.class));
        PhotoView plannerPhotoView = new PhotoView(plannerPhoto);

        //end planner -- start patient
        Citizen patient = new Citizen();
        patient.setDoctorId(rs.getObject("idDottore", Integer.class));
        patient.setCf(rs.getObject("cfPaziente", String.class));
        patient.setId(rs.getObject("idPaziente", Integer.class));
        patient.setName(rs.getObject("nomePaziente", String.class));
        patient.setSurname(rs.getObject("cognomePaziente", String.class));

        Province patientProvince = new Province();
        patientProvince.setAcronym(rs.getObject("siglaProvinciaPaziente", String.class));
        patientProvince.setName(rs.getObject("nomeProvinciaPaziente", String.class));

        Photo patientPhoto = new Photo();
        patientPhoto.setExtension(rs.getObject("extensionFotoPaziente", String.class));
        patientPhoto.setCreationTimestamp(rs.getObject("DOCFotoPaziente", Timestamp.class));
        patientPhoto.setUserId(rs.getObject("idPaziente", Integer.class));
        PhotoView patientPhotoView = new PhotoView(patientPhoto);

        //end patient -- start performer
        Citizen performer = new Citizen();
        performer.setId(rs.getObject("idErogatore", Integer.class));
        performer.setName(rs.getObject("nomeErogatore", String.class));
        performer.setSurname(rs.getObject("cognomeErogatore", String.class));
        performer.setRole(rs.getObject("ruoloErogatore", Integer.class));

        Province performerProvince = new Province();
        performerProvince.setAcronym(rs.getObject("siglaProvinciaErogatore", String.class));
        performerProvince.setName(rs.getObject("nomeProvinciaErogatore", String.class));

        Photo performerPhoto = new Photo();
        performerPhoto.setExtension(rs.getObject("extensionFotoErogatore", String.class));
        performerPhoto.setCreationTimestamp(rs.getObject("DOCFotoErogatore", Timestamp.class));
        performerPhoto.setUserId(rs.getObject("idErogatore", Integer.class));
        PhotoView performerPhotoView = new PhotoView(performerPhoto);
        //end performer

        return new TicketView(ticket, planner, plannerProvince, plannerPhotoView, patient, patientProvince, patientPhotoView,
                performerProvince, performerPhotoView, performer);
    }

    private Ticket getTicket(ResultSet rs) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setPlannerId(rs.getInt("idDottore"));
        ticket.setPatientId(rs.getInt("idPaziente"));
        ticket.setPrescriptionTimestamp(rs.getTimestamp("dataOraPrescrizione"));
        ticket.setDescription((rs.getString("descrizione")));
        ticket.setPerformerRole(rs.getInt("ruoloErogatore"));
        ticket.setPerformerId(rs.getInt("idErogatore"));
        ticket.setCost(rs.getFloat("costo"));
        ticket.setCompletionTimestamp(rs.getTimestamp("dataOraEvasione"));
        return ticket;
    }
}
