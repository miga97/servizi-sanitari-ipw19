package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoId;

import java.util.List;

public interface PhotoDAO extends DAO<Photo, PhotoId> {
    /**
     * Get all the photos of a user.
     * Most recent ones are listed first.
     *
     * @param userId The Id of the user
     * @return The list of photos
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<Photo> getByUserId(int userId) throws DAOException;

    /**
     * Get the last photo of a user.
     *
     * @param userId The Id of the user
     * @return The last photo
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    Photo getLastByUserId(int userId) throws DAOException;

    /**
     * Insert a photo, without deleting any of the old photos
     *
     * @param photo the new photo to persist
     * @return The photo that has been persisted in the DB
     * @throws DAOException if an error occurred during the insertion
     * @author Matteo Zanella
     */
    Photo insert(Photo photo) throws DAOException;

    /**
     * Replace the last photo of an user with another photo.
     * Used mainly for SSPs and Pharmacies for replacing their last and only photo
     *
     * @param photo the new photo to persist
     * @return The photo that has been persisted in the DB
     * @throws DAOException if an error occurred during the replacing
     * @author Matteo Zanella
     */
    Photo replaceLast(Photo photo) throws DAOException;

    /**
     * Deletes the Photo passed as parameter
     *
     * @param photoId the photoId of the photo that should be deleted
     * @throws DAOException if an error occurred during the deletion
     * @author Matteo Zanella
     */
    void deleteByPrimaryKey(PhotoId photoId) throws DAOException;
}
