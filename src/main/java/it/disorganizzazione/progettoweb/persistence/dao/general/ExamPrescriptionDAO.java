package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ExamPrescriptionDAO extends DAO<ExamPrescription, PrescriptionId> {

    /**
     * Get the total cost of all completed exams of a citizen.
     *
     * @param citizenId The Id of the citizen
     * @return The total cost of exams tickets
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    float getExpenseByCitizenId(int citizenId) throws DAOException;

    /**
     * Get all the prescribed exams of a citizen, completed or not.
     * Most recently prescribed ones are listed first.
     *
     * @param citizenId The Id of the citizen
     * @return The list of prescribed exams
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<ExamPrescription> getByCitizenId(int citizenId) throws DAOException;

    /**
     * Get the prescribed exams of a citizen, choosing between completed or not ones.
     * Most recently prescribed ones are listed first.
     *
     * @param citizenId The Id of the citizen
     * @param completed If true, only completed ones are listed, if false only not already completed ones are listed
     * @return The list of prescribed exams
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<ExamPrescription> getByCitizenId(int citizenId, boolean completed) throws DAOException;

    /**
     * Persists the new {@link ExamPrescription prescribedExam} passed as parameter to the storage system.
     * Typically used to generate a new incomplete prescription.
     *
     * @param examPrescription the new {@code prescribedExam} to persist.
     * @return The PrescribedExam that has been persisted in the DB
     * @throws DAOException         if an error occurred during the insertion
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    ExamPrescription insert(@NotNull ExamPrescription examPrescription) throws DAOException;

    /**
     * Complete infos about a prescribed exam, persisting the completionTimestamp, performerId and results fields of {@param prescribedExam}
     * Typically used to complete the prescription after it's used
     *
     * @param examPrescription The {@code prescribedExam} that should be updated
     * @return The PrescribedExam that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    ExamPrescription complete(@NotNull ExamPrescription examPrescription) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view.
     *
     * @param status True gets only completed.
     * @return The PrescribedExamView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<ExamPrescriptionView> getAllView(Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by performer ID.
     *
     * @param performerId The performer id
     * @param status      True gets only completed.
     * @return The PrescribedExamView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<ExamPrescriptionView> getViewByPerformerId(@NotNull int performerId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by planner ID.
     *
     * @param plannerId the planner id.
     * @param status    True gets only completed.
     * @return The PrescribedExamView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<ExamPrescriptionView> getViewByPlannerId(@NotNull int plannerId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by citizenId.
     *
     * @param citizenId the citizen id.
     * @param status    True gets only completed.
     * @return The PrescribedExamView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<ExamPrescriptionView> getViewByCitizenId(@NotNull int citizenId, Boolean status) throws DAOException;

}
