package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.DrugPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.DrugPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.persistence.entities.users.Pharmacy;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCDrugPrescriptionDAO extends JDBCDAO<DrugPrescription, PrescriptionId> implements DrugPrescriptionDAO {
    /**
     * The constructor for the JDBC PrescribedDrug DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCDrugPrescriptionDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM RicettePrescritte");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count the prescribedDrugs", ex);
        }
        return null;
    }

    @Override
    public DrugPrescription getByPrimaryKey(@NotNull PrescriptionId primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM RicettePrescritte WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey.getPlannerId()));
            stm.setInt(2, Objects.requireNonNull(primaryKey.getPatientId()));
            stm.setTimestamp(3, Objects.requireNonNull(primaryKey.getPrescriptionTimestamp()));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return getPrescribedDrug(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the prescribedDrug", ex);
        }
    }

    public DrugPrescriptionView getViewByPrimaryKey(@NotNull PrescriptionId primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM RicettePrescritteView WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey.getPlannerId()));
            stm.setInt(2, Objects.requireNonNull(primaryKey.getPatientId()));
            stm.setTimestamp(3, Objects.requireNonNull(primaryKey.getPrescriptionTimestamp()));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return getPrescribedDrugView(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the prescribedDrug", ex);
        }
    }

    @Override
    public List<DrugPrescription> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM RicettePrescritte")) {
            List<DrugPrescription> drugPrescriptions = new ArrayList<>();
            while (rs.next()) {
                drugPrescriptions.add(getPrescribedDrug(rs));
            }
            return drugPrescriptions;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedDrugs", ex);
        }
    }

    @Override
    public List<DrugPrescription> getByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM RicettePrescritte WHERE idPaziente = ? ORDER BY dataOraPrescrizione DESC")) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                List<DrugPrescription> drugPrescriptions = new ArrayList<>();
                while (rs.next()) {
                    drugPrescriptions.add(getPrescribedDrug(rs));
                }
                return drugPrescriptions;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedDrugs", ex);
        }
    }

    @Override
    public List<DrugPrescription> getByCitizenId(int citizenId, boolean completed) throws DAOException {
        String query;
        if (completed) {
            query = "SELECT * FROM RicettePrescritte WHERE idPaziente = ? AND dataOraEvasione IS NOT NULL ORDER BY dataOraPrescrizione DESC";
        } else {
            query = "SELECT * FROM RicettePrescritte WHERE idPaziente = ? AND dataOraEvasione IS NULL ORDER BY dataOraPrescrizione DESC";
        }
        try (PreparedStatement stm = prepareStatement(query)) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                List<DrugPrescription> drugPrescriptions = new ArrayList<>();
                while (rs.next()) {
                    drugPrescriptions.add(getPrescribedDrug(rs));
                }
                return drugPrescriptions;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedDrugs", ex);
        }
    }

    @Override
    public float getExpenseByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT COALESCE(sum(costo),0) AS spesa FROM RicettePrescritte WHERE idPaziente = ? AND dataOraEvasione IS NOT NULL")) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                return rs.getFloat("spesa");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the expense of prescribedDrugs", ex);
        }
    }

    @Override
    public DrugPrescription insert(@NotNull DrugPrescription drugPrescription) throws DAOException {
        try (PreparedStatement ps = prepareStatement("INSERT INTO RicettePrescritte(idDottore,idPaziente,dataOraPrescrizione,descrizione,costo,idFarmacia,dataOraEvasione) VALUES(?,?,?,?,?,?,?)")) {
            ps.setInt(1, Objects.requireNonNull(drugPrescription.getPlannerId()));
            ps.setInt(2, Objects.requireNonNull(drugPrescription.getPatientId()));
            ps.setTimestamp(3, Objects.requireNonNull(drugPrescription.getPrescriptionTimestamp()));
            ps.setString(4, Objects.requireNonNull(drugPrescription.getDescription()));
            ps.setFloat(5, Objects.requireNonNull(drugPrescription.getCost()));
            ps.setObject(6, drugPrescription.getPerformerId(), Types.INTEGER);
            ps.setTimestamp(7, drugPrescription.getCompletionTimestamp());
            if (ps.executeUpdate() == 1) {
                return drugPrescription;
            } else {
                throw new DAOException("Impossible to insert the prescribedDrug");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the prescribedDrug", ex);
        }
    }

    @Override
    public DrugPrescription complete(@NotNull DrugPrescription drugPrescription) throws DAOException {
        try (PreparedStatement ps = prepareStatement("UPDATE RicettePrescritte SET idFarmacia = ?, dataOraEvasione = ? WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            ps.setObject(1, drugPrescription.getPerformerId(), Types.INTEGER);
            ps.setTimestamp(2, drugPrescription.getCompletionTimestamp());
            ps.setInt(3, Objects.requireNonNull(drugPrescription.getPlannerId()));
            ps.setInt(4, Objects.requireNonNull(drugPrescription.getPatientId()));
            ps.setTimestamp(5, Objects.requireNonNull(drugPrescription.getPrescriptionTimestamp()));
            if (ps.executeUpdate() == 1) {
                return drugPrescription;
            } else {
                throw new DAOException("Impossible to update the prescribedDrug");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the prescribedDrug", ex);
        }
    }

    @Override
    public List<DrugPrescriptionView> getAllView(Boolean status) throws DAOException {
        try (ResultSet rs = createStatement().executeQuery(queryfier("SELECT * FROM RicettePrescritteView", status))) {
            List<DrugPrescriptionView> drugPrescriptionViews = new ArrayList<>();
            while (rs.next()) {
                drugPrescriptionViews.add(getPrescribedDrugView(rs));
            }
            return drugPrescriptionViews;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of drugPrescriptionViews", ex);
        }
    }

    public List<DrugPrescriptionView> getFilteredView(int diff, Boolean status, List<DrugPrescriptionView> receipt) throws DAOException {
        if (diff != 1) {
            List<DrugPrescriptionView> drugPrescriptionViews = getAllView(status);
            //faccio una variabile temporanea perchè non posso eliminare elementi di una lista se itero su di essa
            List<DrugPrescriptionView> temp = new ArrayList<>();
            temp.addAll(drugPrescriptionViews);
            // rimuovo gli elementi che vanno filtrati
            for (DrugPrescriptionView drugSelected : receipt) {
                for (DrugPrescriptionView drugRequests : temp) {
                    if (drugRequests.equals(drugSelected)) {
                        drugPrescriptionViews.remove(drugRequests);
                        break;
                    }
                }
            }
            return drugPrescriptionViews;
        } else return receipt;
    }

    @Override
    public List<DrugPrescriptionView> getViewByPerformerId(int performerId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM RicettePrescritteView WHERE idFarmacia=?", status))) {
            return getDrugPrescriptionViews(performerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of drugPrescriptionViews", ex);
        }
    }

    @Override
    public List<DrugPrescriptionView> getViewByPlannerId(int plannerId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM RicettePrescritteView WHERE idDottore=?", status))) {
            return getDrugPrescriptionViews(plannerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of drugPrescriptionViews", ex);
        }
    }

    @Override
    public List<DrugPrescriptionView> getViewByCitizenId(int citizenId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM RicettePrescritteView WHERE idPaziente=?", status))) {
            return getDrugPrescriptionViews(citizenId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of drugPrescriptionViews", ex);
        }
    }

    @Override
    public List<DrugPrescriptionView> getViewByCitizenProvince(String acronym, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM RicettePrescritteView WHERE siglaProvinciaPaziente=?", status))) {
            stm.setString(1, acronym);
            try (ResultSet rs = stm.executeQuery()) {
                List<DrugPrescriptionView> drugPrescriptionViews = new ArrayList<>();
                while (rs.next()) {
                    drugPrescriptionViews.add(getPrescribedDrugView(rs));
                }
                return drugPrescriptionViews;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of drugPrescriptionViews", ex);
        }
    }

    private List<DrugPrescriptionView> getDrugPrescriptionViews(int id, PreparedStatement stm) throws SQLException {
        stm.setInt(1, id);
        try (ResultSet rs = stm.executeQuery()) {
            List<DrugPrescriptionView> drugPrescriptionViews = new ArrayList<>();
            while (rs.next()) {
                drugPrescriptionViews.add(getPrescribedDrugView(rs));
            }
            return drugPrescriptionViews;
        }
    }

    private DrugPrescriptionView getPrescribedDrugView(ResultSet rs) throws SQLException {
        DrugPrescription prescribedDrug = getPrescribedDrug(rs);
        //planner
        Doctor planner = new Doctor();
        planner.setId(rs.getObject("idDottore", Integer.class));
        planner.setName(rs.getObject("nomeDottore", String.class));
        planner.setSurname(rs.getObject("cognomeDottore", String.class));

        Province plannerProvince = new Province();
        plannerProvince.setAcronym(rs.getObject("siglaProvinciaDottore", String.class));
        plannerProvince.setName(rs.getObject("nomeProvinciaDottore", String.class));

        Photo plannerPhoto = new Photo();
        plannerPhoto.setExtension(rs.getObject("extensionFotoDottore", String.class));
        plannerPhoto.setCreationTimestamp(rs.getObject("DOCFotoDottore", Timestamp.class));
        plannerPhoto.setUserId(rs.getObject("idDottore", Integer.class));
        PhotoView plannerPhotoView = new PhotoView(plannerPhoto);
        //end planner -- start patient
        Citizen patient = new Citizen();
        patient.setDoctorId(rs.getObject("idDottore", Integer.class));
        patient.setCf(rs.getObject("cfPaziente", String.class));
        patient.setId(rs.getObject("idPaziente", Integer.class));
        patient.setName(rs.getObject("nomePaziente", String.class));
        patient.setSurname(rs.getObject("cognomePaziente", String.class));

        Province patientProvince = new Province();
        patientProvince.setAcronym(rs.getObject("siglaProvinciaPaziente", String.class));
        patientProvince.setName(rs.getObject("nomeProvinciaPaziente", String.class));

        Photo patientPhoto = new Photo();
        patientPhoto.setExtension(rs.getObject("extensionFotoPaziente", String.class));
        patientPhoto.setCreationTimestamp(rs.getObject("DOCFotoPaziente", Timestamp.class));
        patientPhoto.setUserId(rs.getObject("idPaziente", Integer.class));
        PhotoView patientPhotoView = new PhotoView(patientPhoto);
        //end patient -- start performer
        Pharmacy performer = new Pharmacy();
        performer.setId(rs.getObject("idFarmacia", Integer.class));
        performer.setName(rs.getObject("nomeErogatore", String.class));

        Province performerProvince = new Province();
        performerProvince.setAcronym(rs.getObject("siglaProvinciaErogatore", String.class));
        performerProvince.setName(rs.getObject("nomeProvinciaErogatore", String.class));

        Photo performerPhoto = new Photo();
        performerPhoto.setExtension(rs.getObject("extensionFotoErogatore", String.class));
        performerPhoto.setCreationTimestamp(rs.getObject("DOCFotoErogatore", Timestamp.class));
        performerPhoto.setUserId(rs.getObject("idFarmacia", Integer.class));
        PhotoView performerPhotoView = new PhotoView(performerPhoto);
        //end performer

        return new DrugPrescriptionView(planner, plannerProvince, plannerPhotoView, patient, patientProvince, patientPhotoView,
                performerProvince, performerPhotoView, prescribedDrug, performer);
    }

    private DrugPrescription getPrescribedDrug(ResultSet rs) throws SQLException {
        DrugPrescription drugPrescription = new DrugPrescription();
        drugPrescription.setPlannerId(rs.getInt("idDottore"));
        drugPrescription.setPatientId(rs.getInt("idPaziente"));
        drugPrescription.setPrescriptionTimestamp(rs.getTimestamp("dataOraPrescrizione"));
        drugPrescription.setDescription(rs.getString("descrizione"));
        drugPrescription.setCost(rs.getFloat("costo"));
        drugPrescription.setPerformerId(rs.getObject("idFarmacia", Integer.class));
        drugPrescription.setCompletionTimestamp(rs.getTimestamp("dataOraEvasione"));
        return drugPrescription;
    }

    private String queryfier(String query, Boolean status) {
        if (status != null) {
            String identifier = " WHERE";
            if (query.toLowerCase().contains(" where ")) identifier = " AND";
            if (status) query += identifier + " dataOraEvasione is NOT NULL";
            else query += identifier + " dataOraEvasione is NULL";
        }
        return query;
    }
}

