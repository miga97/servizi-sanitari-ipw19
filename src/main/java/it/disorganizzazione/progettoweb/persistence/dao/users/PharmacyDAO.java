package it.disorganizzazione.progettoweb.persistence.dao.users;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Pharmacy;

public interface PharmacyDAO extends DAO<Pharmacy,Integer> {

}
