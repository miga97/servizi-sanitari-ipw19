package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.PasswordRecoveryDAO;
import it.disorganizzazione.progettoweb.persistence.entities.PasswordRecovery;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCPasswordRecoveryDAO extends JDBCDAO<PasswordRecovery, Integer> implements PasswordRecoveryDAO {

    /**
     * The constructor for the JDBC Password Recovery DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Rizzi
     * @since 1.0.0.190406
     */
    public JDBCPasswordRecoveryDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM RecuperoPassword");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count passwordRecoveries", ex);
        }
        return null;
    }

    @Override
    public PasswordRecovery getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM RecuperoPassword WHERE idUtente = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    return getPasswordRecovery(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the passwordRecovery", ex);
        }
    }

    @Override
    public List<PasswordRecovery> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM RecuperoPassword")) {
            List<PasswordRecovery> passwordRecoveries = new ArrayList<>();
            while (rs.next()) {
                passwordRecoveries.add(getPasswordRecovery(rs));
            }
            return passwordRecoveries;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of passwordRecoveries", ex);
        }
    }

    @Override
    public void deleteByPrimaryKey(int userId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("DELETE FROM RecuperoPassword WHERE idUtente = ?")) {
            stm.setInt(1, userId);
            if (stm.executeUpdate() != 1) {
                throw new DAOException("Impossible to delete the passwordRecovery");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to delete the passwordRecovery", ex);
        }
    }

    public PasswordRecovery getByToken(@NotNull String token) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM RecuperoPassword WHERE token = ?")) {
            stm.setString(1, Objects.requireNonNull(token));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    return getPasswordRecovery(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the passwordRecovery", ex);
        }
    }

    @Override
    public PasswordRecovery replace(@NotNull PasswordRecovery passwordRecovery) throws DAOException {
        try (PreparedStatement ps = prepareStatement("REPLACE INTO RecuperoPassword(idUtente,token,dataOraGenerazione) VALUES(?, ?, ?)")) {
            ps.setInt(1, passwordRecovery.getUserId());
            ps.setString(2, Objects.requireNonNull(passwordRecovery.getToken()));
            ps.setTimestamp(3, Objects.requireNonNull(passwordRecovery.getGenerationTimestamp()));
            int res = ps.executeUpdate();
            if (res == 1 || res == 2) {
                return passwordRecovery;
            } else {
                throw new DAOException("Impossible to replace the passwordRecovery");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to replace the passwordRecovery", ex);
        }
    }

    private PasswordRecovery getPasswordRecovery(ResultSet rs) throws SQLException {
        PasswordRecovery passwordRecovery = new PasswordRecovery();
        passwordRecovery.setUserId(rs.getInt("idUtente"));
        passwordRecovery.setToken(rs.getString("token"));
        passwordRecovery.setGenerationTimestamp(rs.getTimestamp("dataOraGenerazione"));
        return passwordRecovery;
    }
}

