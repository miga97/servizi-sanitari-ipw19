package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ExamDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Exam;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCExamDAO extends JDBCDAO<Exam, Integer> implements ExamDAO {

    /**
     * The constructor for the JDBC Exam DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCExamDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Esami");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count exams", ex);
        }
        return null;
    }

    @Override
    public Exam getByPrimaryKey(Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Esami WHERE id = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    Exam exam = new Exam();
                    exam.setId(rs.getInt("id"));
                    exam.setName(rs.getString("nome"));
                    return exam;
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam", ex);
        }
    }

    @Override
    public List<Exam> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Esami ORDER BY nome")) {
            List<Exam> exams = new ArrayList<>();
            while (rs.next()) {
                Exam exam = new Exam();
                exam.setId(rs.getInt("id"));
                exam.setName(rs.getString("nome"));
                exams.add(exam);
            }
            return exams;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of exams", ex);
        }
    }
}

