package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ProvinceDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Province;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCProvinceDAO extends JDBCDAO<Province, Integer> implements ProvinceDAO {

    /**
     * The constructor for the JDBC Province DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCProvinceDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Province");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count provinces", ex);
        }
        return null;
    }

    @Override
    public Province getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Province WHERE id = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    Province province = new Province();
                    province.setId(rs.getInt("id"));
                    province.setName(rs.getString("nome"));
                    province.setRegion(rs.getString("regione"));
                    province.setAcronym(rs.getString("sigla"));
                    return province;
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the province", ex);
        }
    }

    @Override
    public List<Province> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Province ORDER BY nome")) {
            List<Province> provinces = new ArrayList<>();
            while (rs.next()) {
                Province province = new Province();
                province.setId(rs.getInt("id"));
                province.setName(rs.getString("nome"));
                province.setRegion(rs.getString("regione"));
                province.setAcronym(rs.getString("sigla"));
                provinces.add(province);
            }
            return provinces;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of provinces", ex);
        }
    }
}
