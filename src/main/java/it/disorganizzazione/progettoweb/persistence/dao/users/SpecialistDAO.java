package it.disorganizzazione.progettoweb.persistence.dao.users;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Specialist;

public interface SpecialistDAO extends DAO<Specialist, Integer> {
}
