package it.disorganizzazione.progettoweb.persistence.dao.users;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;

import javax.validation.constraints.NotNull;

public interface UserDAO extends DAO<User, Integer> {

    /**
     * Get an user through its email and password.
     * Used for the login function.
     *
     * @param email    Login email of the {@code user}
     * @param password The password that the user is using
     * @return An User of the correct type
     * @throws DAOException         if an error occurred during the information retrieving
     * @throws NullPointerException if parameters are null
     * @author Alberto Xamin
     */
    User getByEmailAndPassword(@NotNull String email, @NotNull String password) throws DAOException;

    /**
     * Get the User using its email
     *
     * @param email Login email of the {@code user}
     * @return An User of the correct type
     * @throws DAOException         if an error occurred during the information retrieving
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    User getByEmail(@NotNull String email) throws DAOException;

    /**
     * Get the user using its cf
     *
     * @param cf CF of the {@code user}
     * @return An User of the correct type
     * @throws DAOException         if an error occurred during the information retrieving
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    User getByCf(@NotNull String cf) throws DAOException;

    /**
     * Updates all infos about an user, coping all attributes except the password.
     * You should use {@code CitizenDAO.update(Citizen citizen)} to update a citizen.
     *
     * @param user The {@code user} that should be updated
     * @return The user that is now loaded in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Alberto Xamin
     */
    User update(User user) throws DAOException;

    /**
     * Updates the password of an user, hashing the plain text password attribute of {@param user} and persisting it in the DB
     *
     * @param user The {@code User user} that should be updated, storing the plain text password
     * @return The user that is now loaded in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Alberto Xamin
     */
    User updatePassword(@NotNull User user) throws DAOException;

    /**
     * Persists the new {@code User user} passed as parameter to the storage system.
     *
     * @param user the new {@code user} to persist.
     * @return the User with the id of the new persisted record.
     * @throws DAOException         if an error occurred during the insertion
     * @throws NullPointerException if parameters are null
     * @author Alberto Xamin
     */
    User insert(@NotNull User user) throws DAOException;
}
