package it.disorganizzazione.progettoweb.persistence.dao.users;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;

import java.util.List;

public interface DoctorDAO extends DAO<Doctor, Integer> {

    /**
     * Gets all doctors from a given province.
     * Used for changing doctor.
     *
     * @param provinceId province of the doctor
     * @return the list of doctors of the correct type, from the same {@param provinceId}
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<Doctor> getByProvince(int provinceId) throws DAOException;
}
