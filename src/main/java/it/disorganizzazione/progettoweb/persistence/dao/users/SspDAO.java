package it.disorganizzazione.progettoweb.persistence.dao.users;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Ssp;

public interface SspDAO extends DAO<Ssp, Integer> {
}
