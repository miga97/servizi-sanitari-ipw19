package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.ExamPrescriptionDAO;
import it.disorganizzazione.progettoweb.persistence.entities.PhotoView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.Photo;
import it.disorganizzazione.progettoweb.persistence.entities.Province;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.ExamPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.users.Citizen;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.persistence.entities.users.Ssp;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCExamPrescriptionDAO extends JDBCDAO<ExamPrescription, PrescriptionId> implements ExamPrescriptionDAO {
    /**
     * The constructor for the JDBC PrescribedExam DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCExamPrescriptionDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM EsamiPrescritti");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count prescribedExams", ex);
        }
        return null;
    }

    @Override
    public ExamPrescription getByPrimaryKey(@NotNull PrescriptionId primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM EsamiPrescritti WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey.getPlannerId()));
            stm.setInt(2, Objects.requireNonNull(primaryKey.getPatientId()));
            stm.setTimestamp(3, Objects.requireNonNull(primaryKey.getPrescriptionTimestamp()));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return getPrescribedExam(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the prescribedExam", ex);
        }
    }

    @Override
    public List<ExamPrescription> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM EsamiPrescritti")) {
            List<ExamPrescription> examPrescriptions = new ArrayList<>();
            while (rs.next()) {
                examPrescriptions.add(getPrescribedExam(rs));
            }
            return examPrescriptions;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedExams", ex);
        }
    }

    @Override
    public float getExpenseByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT COALESCE(sum(costo),0) AS spesa FROM EsamiPrescritti WHERE idPaziente = ? AND dataOraEvasione IS NOT NULL")) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                return rs.getFloat("spesa");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the expense of prescribedExams", ex);
        }
    }

    @Override
    public List<ExamPrescription> getByCitizenId(int citizenId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM EsamiPrescritti WHERE idPaziente = ? ORDER BY dataOraPrescrizione DESC")) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                List<ExamPrescription> examPrescriptions = new ArrayList<>();
                while (rs.next()) {
                    examPrescriptions.add(getPrescribedExam(rs));
                }
                return examPrescriptions;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedExams", ex);
        }
    }

    @Override
    public List<ExamPrescription> getByCitizenId(int citizenId, boolean completed) throws DAOException {
        String query;
        if (completed) {
            query = "SELECT * FROM EsamiPrescritti WHERE idPaziente = ? AND dataOraEvasione IS NOT NULL ORDER BY dataOraPrescrizione DESC";
        } else {
            query = "SELECT * FROM EsamiPrescritti WHERE idPaziente = ? AND dataOraEvasione IS NULL ORDER BY dataOraPrescrizione DESC";
        }
        try (PreparedStatement stm = prepareStatement(query)) {
            stm.setInt(1, citizenId);
            try (ResultSet rs = stm.executeQuery()) {
                List<ExamPrescription> examPrescriptions = new ArrayList<>();
                while (rs.next()) {
                    examPrescriptions.add(getPrescribedExam(rs));
                }
                return examPrescriptions;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of prescribedExams", ex);
        }
    }

    @Override
    public ExamPrescription insert(@NotNull ExamPrescription examPrescription) throws DAOException {
        try (PreparedStatement ps = prepareStatement("INSERT INTO EsamiPrescritti(idDottore,idPaziente,dataOraPrescrizione,idEsame,costo,idSsp,risultati,dataOraEvasione) VALUES(?, ?, ?, ?, ?, ?, ?, ?)")) {
            ps.setInt(1, Objects.requireNonNull(examPrescription.getPlannerId()));
            ps.setInt(2, Objects.requireNonNull(examPrescription.getPatientId()));
            ps.setTimestamp(3, Objects.requireNonNull(examPrescription.getPrescriptionTimestamp()));
            ps.setInt(4, Objects.requireNonNull(examPrescription.getExamId()));
            ps.setFloat(5, Objects.requireNonNull(examPrescription.getCost()));
            ps.setObject(6, examPrescription.getPerformerId(), Types.INTEGER);
            ps.setString(7, examPrescription.getResults());
            ps.setTimestamp(8, examPrescription.getCompletionTimestamp());
            if (ps.executeUpdate() == 1) {
                return examPrescription;
            } else {
                throw new DAOException("Impossible to insert the prescribedExam");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the prescribedExam", ex);
        }
    }

    @Override
    public ExamPrescription complete(@NotNull ExamPrescription examPrescription) throws DAOException {
        try (PreparedStatement ps = prepareStatement("UPDATE EsamiPrescritti SET idSsp = ?, risultati = ?, dataOraEvasione = ? WHERE idDottore = ? AND idPaziente = ? AND dataOraPrescrizione = ?")) {
            ps.setObject(1, examPrescription.getPerformerId(), Types.INTEGER);
            ps.setString(2, examPrescription.getResults());
            ps.setTimestamp(3, examPrescription.getCompletionTimestamp());
            ps.setInt(4, Objects.requireNonNull(examPrescription.getPlannerId()));
            ps.setInt(5, Objects.requireNonNull(examPrescription.getPatientId()));
            ps.setTimestamp(6, Objects.requireNonNull(examPrescription.getPrescriptionTimestamp()));
            if (ps.executeUpdate() == 1) {
                return examPrescription;
            } else {
                throw new DAOException("Impossible to update the prescribedExam");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the prescribedExam", ex);
        }
    }

    @Override
    public List<ExamPrescriptionView> getAllView(Boolean status) throws DAOException {
        try (ResultSet rs = createStatement().executeQuery(queryfier("SELECT * FROM EsamiPrescrittiView",status))) {
            List<ExamPrescriptionView> examPrescriptionViews = new ArrayList<>();
            while (rs.next()) {
                examPrescriptionViews.add(getPrescribedExamView(rs));
            }
            return examPrescriptionViews;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of examPrescriptionViews", ex);
        }
    }

    @Override
    public List<ExamPrescriptionView> getViewByPerformerId(int performerId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM EsamiPrescrittiView WHERE idSsp=?",status))) {
            return getExamPrescriptionViews(performerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of examPrescriptionViews", ex);
        }
    }

    @Override
    public List<ExamPrescriptionView> getViewByPlannerId(int plannerId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM EsamiPrescrittiView WHERE idDottore=?",status))) {
            return getExamPrescriptionViews(plannerId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of examPrescriptionViews", ex);
        }
    }

    @Override
    public List<ExamPrescriptionView> getViewByCitizenId(int citizenId, Boolean status) throws DAOException {
        try (PreparedStatement stm = prepareStatement(queryfier("SELECT * FROM EsamiPrescrittiView WHERE idPaziente=?",status))) {
            return getExamPrescriptionViews(citizenId, stm);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of examPrescriptionViews", ex);
        }
    }

    private List<ExamPrescriptionView> getExamPrescriptionViews(int id, PreparedStatement stm) throws SQLException {
        stm.setInt(1, id);
        try (ResultSet rs = stm.executeQuery()) {
            List<ExamPrescriptionView> examPrescriptionViews = new ArrayList<>();
            while (rs.next()) {
                examPrescriptionViews.add(getPrescribedExamView(rs));
            }
            return examPrescriptionViews;
        }
    }


    private ExamPrescriptionView getPrescribedExamView(ResultSet rs) throws SQLException {
        String examName = rs.getObject("nomeEsame",String.class);
        ExamPrescription examPrescription = getPrescribedExam(rs);

        //planner
        Doctor planner = new Doctor();
        planner.setId(rs.getObject("idDottore",Integer.class));
        planner.setName(rs.getObject("nomeDottore",String.class));
        planner.setSurname(rs.getObject("cognomeDottore",String.class));

        //fix for richiamo
        int costo = rs.getObject("costo",Integer.class);
        if (costo == 0){
            planner.setRole(3);
        }

        Province plannerProvince = new Province();
        plannerProvince.setAcronym(rs.getObject("siglaProvinciaDottore",String.class));
        plannerProvince.setName(rs.getObject("nomeProvinciaDottore",String.class));

        Photo plannerPhoto = new Photo();
        plannerPhoto.setExtension(rs.getObject("extensionFotoDottore", String.class));
        plannerPhoto.setCreationTimestamp(rs.getObject("DOCFotoDottore", Timestamp.class));
        plannerPhoto.setUserId(rs.getObject("idDottore", Integer.class));
        PhotoView plannerPhotoView = new PhotoView(plannerPhoto);

        //end planner -- start patient
        Citizen patient = new Citizen();
        patient.setDoctorId(rs.getObject("idDottore",Integer.class));
        patient.setCf(rs.getObject("cfPaziente",String.class));
        patient.setId(rs.getObject("idPaziente",Integer.class));
        patient.setName(rs.getObject("nomePaziente",String.class));
        patient.setSurname(rs.getObject("cognomePaziente",String.class));

        Province patientProvince = new Province();
        patientProvince.setAcronym(rs.getObject("siglaProvinciaPaziente",String.class));
        patientProvince.setName(rs.getObject("nomeProvinciaPaziente",String.class));

        Photo patientPhoto = new Photo();
        patientPhoto.setExtension(rs.getObject("extensionFotoPaziente", String.class));
        patientPhoto.setCreationTimestamp(rs.getObject("DOCFotoPaziente", Timestamp.class));
        patientPhoto.setUserId(rs.getObject("idPaziente", Integer.class));
        PhotoView patientPhotoView = new PhotoView(patientPhoto);

        //end patient -- start performer
        Ssp performer = new Ssp();
        performer.setId(rs.getObject("idSsp",Integer.class));
        performer.setName(rs.getObject("nomeErogatore",String.class));

        Province performerProvince = new Province();
        performerProvince.setAcronym(rs.getObject("siglaProvinciaErogatore",String.class));
        performerProvince.setName(rs.getObject("nomeProvinciaErogatore",String.class));

        Photo performerPhoto = new Photo();
        performerPhoto.setExtension(rs.getObject("extensionFotoErogatore", String.class));
        performerPhoto.setCreationTimestamp(rs.getObject("DOCFotoErogatore", Timestamp.class));
        performerPhoto.setUserId(rs.getObject("idSsp", Integer.class));
        PhotoView performerPhotoView = new PhotoView(performerPhoto);
        //end performer

        return new ExamPrescriptionView(planner, plannerProvince, plannerPhotoView, patient, patientProvince, patientPhotoView,
                performerProvince, performerPhotoView, examName, examPrescription, performer);
    }

    private ExamPrescription getPrescribedExam(ResultSet rs) throws SQLException {
        ExamPrescription examPrescription = new ExamPrescription();
        examPrescription.setPlannerId(rs.getInt("idDottore"));
        examPrescription.setPatientId(rs.getInt("idPaziente"));
        examPrescription.setPrescriptionTimestamp(rs.getTimestamp("dataOraPrescrizione"));
        examPrescription.setExamId(rs.getInt("idEsame"));
        examPrescription.setCost(rs.getFloat("costo"));
        examPrescription.setPerformerId(rs.getObject("idSsp", Integer.class));
        examPrescription.setResults(rs.getString("risultati"));
        examPrescription.setCompletionTimestamp(rs.getTimestamp("dataOraEvasione"));
        return examPrescription;
    }
    private String queryfier(String query, Boolean status) {
        if (status != null) {
            String identifier =" WHERE";
            if (query.toLowerCase().contains(" where ")) identifier=" AND";
            if (status) query += identifier+" dataOraEvasione is NOT NULL";
            else query += identifier+" dataOraEvasione is NULL";
        }
        return query;
    }
}
