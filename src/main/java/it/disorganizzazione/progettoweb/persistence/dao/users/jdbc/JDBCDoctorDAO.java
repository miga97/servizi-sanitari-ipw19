/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.persistence.dao.users.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.DoctorDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Doctor;
import it.disorganizzazione.progettoweb.persistence.factories.UserFactory;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCDoctorDAO extends JDBCDAO<Doctor, Integer> implements DoctorDAO {

    private final UserFactory userFactory = new UserFactory();

    /**
     * The constructor for the JDBC Doctor DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCDoctorDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Utenti WHERE ruolo = 1");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count doctors", ex);
        }
        return null;
    }

    @Override
    public Doctor getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE id = ? AND ruolo = 1")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return userFactory.fillUser(new Doctor(), rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the doctor", ex);
        }
    }

    @Override
    public List<Doctor> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Utenti WHERE ruolo = 1 ORDER BY cognome")) {
            List<Doctor> doctors = new ArrayList<>();
            while (rs.next()) {
                doctors.add(userFactory.fillUser(new Doctor(), rs));
            }
            return doctors;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of doctors", ex);
        }
    }

    @Override
    public List<Doctor> getByProvince(int provinceId) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE provincia = ? AND ruolo = 1")) {
            stm.setInt(1, provinceId);
            try (ResultSet rs = stm.executeQuery()) {
                List<Doctor> doctors = new ArrayList<>();
                while (rs.next()) {
                    doctors.add(userFactory.fillUser(new Doctor(), rs));
                }
                return doctors;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of doctors", ex);
        }
    }
}
