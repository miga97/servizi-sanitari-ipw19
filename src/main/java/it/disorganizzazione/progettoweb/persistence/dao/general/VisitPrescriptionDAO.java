package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescription;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescriptionView;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface VisitPrescriptionDAO extends DAO<VisitPrescription, PrescriptionId> {

    /**
     * Get the total cost of all completed visits of a citizen.
     *
     * @param citizenId The Id of the citizen
     * @return The total cost of visits tickets
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    float getExpenseByCitizenId(int citizenId) throws DAOException;

    /**
     * Get all the prescribed visits of a citizen, completed or not.
     * Most recently prescribed ones are listed first.
     *
     * @param citizenId The Id of the citizen
     * @return The list of prescribed visits
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<VisitPrescription> getByCitizenId(int citizenId) throws DAOException;

    /**
     * Get the prescribed visits of a citizen, choosing between completed or not ones.
     * Most recently prescribed ones are listed first.
     *
     * @param citizenId The Id of the citizen
     * @param completed If true, only completed ones are listed, if false only not already completed ones are listed
     * @return The list of prescribed visits
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<VisitPrescription> getByCitizenId(int citizenId, boolean completed) throws DAOException;

    /**
     * Persists the new {@link VisitPrescription prescribedVisit} passed as parameter to the storage system.
     * Typically used to generate a new incomplete prescription.
     *
     * @param visitPrescription the new {@code prescribedVisit} to persist.
     * @return The PrescribedVisit that has been persisted in the DB
     * @throws DAOException         if an error occurred during the insertion
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    VisitPrescription insert(@NotNull VisitPrescription visitPrescription) throws DAOException;

    /**
     * Complete infos about a prescribed visit, persisting the performerId, anamnesis and completionTimestamp fields of {@param prescribedExam}
     * Typically used to complete the prescription after it's used
     *
     * @param visitPrescription The {@code prescribedVisit} that should be updated
     * @return The PrescribedVisit that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Zanella
     */
    VisitPrescription complete(@NotNull VisitPrescription visitPrescription) throws DAOException;
    /**
     * Needed for datatables. Gets all the values in the view.
     *
     * @param status True gets only completed.
     * @return The VisitPrescriptionView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<VisitPrescriptionView> getAllView(Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by performer ID.
     *
     * @param performerId The performer id
     * @param status      True gets only completed.
     * @return The List<VisitPrescriptionView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<VisitPrescriptionView> getViewByPerformerId(@NotNull int performerId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by planner ID.
     *
     * @param plannerId the planner id.
     * @param status    True gets only completed.
     * @return The List<VisitPrescriptionView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<VisitPrescriptionView> getViewByPlannerId(@NotNull int plannerId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by citizenId.
     *
     * @param citizenId the citizen id.
     * @param status    True gets only completed.
     * @return The List<VisitPrescriptionView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<VisitPrescriptionView> getViewByCitizenId(@NotNull int citizenId, Boolean status) throws DAOException;

    /**
     * Needed for datatables. show the prescriptions without all the prescription of one user
     *
     * @param status True gets only completed.
     * @param user the user in sessione scope
     * @return The List<DrugPrescriptionView> that has been persisted in the DB
     * @throws DAOException if an error occurred during the update
     * @author Giulio Migani
     */
    List<VisitPrescriptionView> getFilteredView(Boolean status, User user) throws DAOException;

}
