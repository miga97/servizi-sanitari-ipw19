/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.persistence.dao.users.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.SspDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Ssp;
import it.disorganizzazione.progettoweb.persistence.factories.UserFactory;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCSspDAO extends JDBCDAO<Ssp, Integer> implements SspDAO {

    private final UserFactory userFactory = new UserFactory();

    /**
     * The constructor for the JDBC Ssp DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCSspDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Utenti WHERE ruolo = 3");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count SSPs", ex);
        }
        return null;
    }

    @Override
    public Ssp getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE id = ? AND ruolo = 3")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return userFactory.fillUser(new Ssp(), rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the SSP", ex);
        }
    }

    @Override
    public List<Ssp> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Utenti WHERE ruolo = 3 ORDER BY nome")) {
            List<Ssp> ssps = new ArrayList<>();
            while (rs.next()) {
                ssps.add(userFactory.fillUser(new Ssp(), rs));
            }
            return ssps;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of SSPs", ex);
        }
    }

}
