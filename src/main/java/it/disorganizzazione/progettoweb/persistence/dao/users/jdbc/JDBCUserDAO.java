/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.persistence.dao.users.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.UserDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.User;
import it.disorganizzazione.progettoweb.persistence.factories.UserFactory;
import it.disorganizzazione.progettoweb.utils.BCryptSingleton;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCUserDAO extends JDBCDAO<User, Integer> implements UserDAO {

    private final UserFactory userFactory = new UserFactory();

    /**
     * The constructor for the JDBC User DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCUserDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Utenti");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count users", ex);
        }
        return null;
    }

    @Override
    public User getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE id = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    User user = userFactory.getUser(rs.getInt("ruolo"));
                    return userFactory.fillUser(user, rs);
                } else {
                    return null;
                }
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("User role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user", ex);
        }
    }

    @Override
    public List<User> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Utenti ORDER BY nome")) {
            List<User> users = new ArrayList<>();
            while (rs.next()) {
                User user = userFactory.getUser(rs.getInt("ruolo"));
                users.add(userFactory.fillUser(user, rs));
            }
            return users;
        } catch (IllegalArgumentException ex) {
            throw new DAOException("User role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of citizens", ex);
        }
    }

    @Override
    public User getByEmailAndPassword(@NotNull String email, @NotNull String password) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE email = ?")) {
            stm.setString(1, Objects.requireNonNull(email));
            /* Non si può ottenere la password direttamente dal BCrypt quindi otteniamo l'eventuale unico utente,
            verifichiamo la password e se abbiamo una password valida ritorniamo quell'utente,
            altrimenti DAOException */
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next() && BCryptSingleton.Instance.verifyHash(password, rs.getString("password"))) {
                    User user = userFactory.getUser(rs.getInt("ruolo"));
                    return userFactory.fillUser(user, rs);
                } else {
                    return null;
                }
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("User role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user", ex);
        }
    }

    @Override
    public User getByEmail(@NotNull String email) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE email = ?")) {
            stm.setString(1, Objects.requireNonNull(email));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    User user = userFactory.getUser(rs.getInt("ruolo"));
                    return userFactory.fillUser(user, rs);
                } else {
                    return null;
                }
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("User role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user", ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getByCf(@NotNull String cf) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE cf = ?")) {
            stm.setString(1, Objects.requireNonNull(cf));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    User user = userFactory.getUser(rs.getInt("ruolo"));
                    return userFactory.fillUser(user, rs);
                } else {
                    return null;
                }
            }
        } catch (IllegalArgumentException ex) {
            throw new DAOException("User role not recognised", ex);
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user", ex);
        }
    }

    @Override
    public User update(User user) throws DAOException {
        try (PreparedStatement std = prepareStatement("UPDATE Utenti SET email = ?, nome = ?, provincia = ?, ruolo=?  WHERE id = ?")) {
            std.setString(1, Objects.requireNonNull(user.getEmail()));
            std.setString(2, Objects.requireNonNull(user.getName()));
            std.setInt(3, Objects.requireNonNull(user.getProvince()));
            std.setInt(4, Objects.requireNonNull(user.getRole()));
            std.setInt(5, Objects.requireNonNull(user.getId()));
            if (std.executeUpdate() == 1) {
                return getByPrimaryKey(user.getId());
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public User updatePassword(User user) throws DAOException {
        try (PreparedStatement std = prepareStatement("UPDATE Utenti SET password = ? WHERE id = ?")) {
            std.setString(1, BCryptSingleton.Instance.hash(Objects.requireNonNull(user.getPassword())));
            std.setInt(2, Objects.requireNonNull(user.getId()));
            if (std.executeUpdate() == 1) {
                return getByPrimaryKey(user.getId());
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public User insert(User user) throws DAOException {
        try (PreparedStatement ps = prepareStatement("INSERT INTO Utenti(nome,email,password,provincia,ruolo) VALUES(?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, Objects.requireNonNull(user.getName()));
            ps.setString(2, Objects.requireNonNull(user.getEmail()));
            ps.setString(3, BCryptSingleton.Instance.hash(Objects.requireNonNull(user.getPassword())));
            ps.setInt(4, Objects.requireNonNull(user.getProvince()));
            ps.setInt(5, Objects.requireNonNull(user.getRole()));
            if (ps.executeUpdate() == 1) {
                ResultSet generatedKeys = ps.getGeneratedKeys();
                generatedKeys.next();
                user.setId((int) generatedKeys.getLong(1));
                return user;
            } else {
                throw new DAOException("Impossible to insert the new user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the new user", ex);
        }
    }
}
