package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.PrescriptionId;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.Ticket;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.TicketView;
import it.disorganizzazione.progettoweb.persistence.entities.prescriptions.VisitPrescription;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface TicketDAO extends DAO<Ticket, PrescriptionId> {
    /**
     * Get all the tickets of a citizen. Only a completed and not-free prescription generates a Ticket
     * Most recently prescribed ones are listed first.
     *
     * @param citizenId The Id of the citizen
     * @return The list of prescribed visits
     * @throws DAOException if an error occurred during the information retrieving
     * @author Matteo Zanella
     */
    List<Ticket> getByCitizenId(int citizenId) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view.
     *
     * @return The TicketView that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<TicketView> getAllView() throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by performer ID.
     *
     * @param performerId The performer id
     * @return The List<TicketView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<TicketView> getViewByPerformerId(@NotNull int performerId) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by planner ID.
     *
     * @param plannerId the planner id.
     * @return The List<TicketView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<TicketView> getViewByPlannerId(@NotNull int plannerId) throws DAOException;

    /**
     * Needed for datatables. Gets all the values in the view by citizenId.
     *
     * @param citizenId the citizen id.
     * @return The List<TicketView> that has been persisted in the DB
     * @throws DAOException         if an error occurred during the update
     * @throws NullPointerException if parameters are null
     * @author Matteo Rizzi
     */
    List<TicketView> getViewByCitizenId(@NotNull int citizenId) throws DAOException;
}
