/*
 * Copyright (c) Alberto Xamin 2019.
 */

package it.disorganizzazione.progettoweb.persistence.dao.users.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.users.SpecialistDAO;
import it.disorganizzazione.progettoweb.persistence.entities.users.Specialist;
import it.disorganizzazione.progettoweb.persistence.factories.UserFactory;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCSpecialistDAO extends JDBCDAO<Specialist, Integer> implements SpecialistDAO {

    private final UserFactory userFactory = new UserFactory();

    /**
     * The constructor for the JDBC Specialist DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCSpecialistDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Utenti WHERE ruolo = 2");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count specialists", ex);
        }
        return null;
    }

    @Override
    public Specialist getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Utenti WHERE id = ? AND ruolo = 2")) {
            stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return userFactory.fillUser(new Specialist(), rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the specialist", ex);
        }
    }

    @Override
    public List<Specialist> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Utenti WHERE ruolo = 2 ORDER BY cognome")) {
            List<Specialist> specialists = new ArrayList<>();
            while (rs.next()) {
                specialists.add(userFactory.fillUser(new Specialist(), rs));
            }
            return specialists;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of specialists", ex);
        }
    }
}
