package it.disorganizzazione.progettoweb.persistence.dao.general.jdbc;

import it.disorganizzazione.commons.persistence.dao.exceptions.DAOException;
import it.disorganizzazione.commons.persistence.dao.jdbc.JDBCDAO;
import it.disorganizzazione.progettoweb.persistence.dao.general.VisitDAO;
import it.disorganizzazione.progettoweb.persistence.entities.Visit;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JDBCVisitDAO extends JDBCDAO<Visit, Integer> implements VisitDAO {
    /**
     * The constructor for the JDBC Visit DAO.
     *
     * @param con the internal {@code Connection}.
     * @author Matteo Zanella
     * @since 1.0.0.190406
     */
    public JDBCVisitDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM Visite");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count visits", ex);
        }
        return null;
    }

    @Override
    public Visit getByPrimaryKey(@NotNull Integer primaryKey) throws DAOException {
        try (PreparedStatement stm = prepareStatement("SELECT * FROM Visite WHERE id = ?")) {
            stm.setInt(1, Objects.requireNonNull(primaryKey));
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    Visit visit = new Visit();
                    visit.setId(rs.getInt("id"));
                    visit.setName(rs.getString("nome"));
                    return visit;
                } else {
                    return null;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the visit", ex);
        }
    }

    @Override
    public List<Visit> getAll() throws DAOException {
        try (ResultSet rs = createStatement().executeQuery("SELECT * FROM Visite ORDER BY nome")) {
            List<Visit> visits = new ArrayList<>();
            while (rs.next()) {
                Visit visit = new Visit();
                visit.setId(rs.getInt("id"));
                visit.setName(rs.getString("nome"));
                visits.add(visit);
            }
            return visits;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of visits", ex);
        }
    }
}
