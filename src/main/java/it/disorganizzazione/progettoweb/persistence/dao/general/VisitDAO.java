package it.disorganizzazione.progettoweb.persistence.dao.general;

import it.disorganizzazione.commons.persistence.dao.DAO;
import it.disorganizzazione.progettoweb.persistence.entities.Visit;

public interface VisitDAO extends DAO<Visit, Integer> {

}
