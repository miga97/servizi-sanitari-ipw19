# Progetto

[TOC]

### Versioni

|      Nome       |  Funzione   | Versione |
| :-------------: | :---------: | :------: |
|     Tomcat      | Web Servlet |  9.0.24  |
| Maria DB(mySQL) |  Database   |    5     |
|       JS        | Javascript  |  latest  |
|      Java       |    Java     |    11    |



### To-Do Resoconto

|        FrontEnd         |    BackEnd    |
| :---------------------: | :-----------: |
|       Login Page        | *equivalente* |
|  Gestione Farmaci[^1]   | *equivalente* |
|     Gestione Utenti     | *equivalente* |
|  Gestione Ricette[^2]   | *equivalente* |
|   Gestione Esami[^2]    | *equivalente* |
| Gestione Profilo Utente | *equivalente* |
|    Pagine di Ricerca    | *equivalente* |
|   Gestioni Visite[^2]   | *equivalente* |
|          *---*          |  API DAO DB   |
|          *---*          |  API Ricerca  |

[^1]: Farmacia
[^2]: Medico

#### Priorità:

*In ordine di importanza*

- [x] DBSchema
- [x] API Dao
- [x] AUTH(Login Page) e Gestione Utenti(Medico, Utente, Farmacia)
- [x] Gestione farmaci, ricette e visite
- [x] API Ricerca
- [x] Pagine di Ricerca
- [x] Grafica e Profili

### FlowChart

#### Generico

```mermaid
graph LR
Auth[Auth] -->Tipo{Tipo}
Tipo --> Farmacia[Farmacia]
Tipo --> Medico[Medico]
Tipo --> Utente[Utente]
Tipo --> SSP[SSP]
Medico-->Base[Di Base]
Medico-->Specialistico[Specialistico]
```

#### Medico

```mermaid
graph LR
Medico-->Base
Base-->Report(Report prescritta/erogata)
Base-->Vista(Vista Pazienti Propri)
Vista-->Dettaglio(Dettaglio Singolo Paziente)
Dettaglio-->Ultima(Ultima Visita)
Dettaglio-->Ricetta(Ricetta Prescritta)
Dettaglio-->Scheda(Scheda Paziente con totale prescrizioni)
Base-->Prescrive{Prescrive}
Prescrive-->Esame(Esame)
Prescrive-->Farmaco(Farmaco)
Prescrive-->Visita(Visita Medica)
Medico-->Specialistico
Specialistico-->Eroga(Eroga Visite Specialistiche)
Specialistico-->VistaGlob(Vista Pazienti Globale)
```

#### Farmacia

```mermaid
graph LR
Farmacia-->Eroga(Eroga Medicinali)
Farmacia-->Vista{Vista}
Vista-->Farmaci(Farmaci)
Vista-->Prescr(Prescrizioni)
```

#### Utente

```mermaid
graph LR
Utente-->Seleziona{Sceglie}
Seleziona-->Medico[Medico di Base]
Seleziona-->Dati[Dati Profilo]
Dati-->Foto(Foto)
Dati-->Generico(Altri Dati Utente)
Utente-->Ricerca{Ricerca}
Ricerca-->Esami(Esami Per Parola Chiave)
Utente-->Vista{Vista}
Vista-->Visite(Visite Proprie)
Vista-->Esamip(Esami Propri)
Vista-->Prescrizioni(Prescrizioni Proprie)
```

#### SSP

```mermaid
graph LR
SSP-->Richiami(Richiami Esami)
SSP-->Eroga(Eroga esami)
SSP-->|XLS|Report(Report per Giorno)
```

#### Completo

```mermaid
graph LR
Auth[Auth] -->Tipo{Tipo}
Tipo --> Farmacia[Farmacia]
Tipo --> Medico[Medico]
Tipo --> Utente[Utente]
Tipo --> SSP[SSP]
Medico-->Base[Di Base]
Medico-->Specialistico[Specialistico]

Farmacia-->ErogaFAR(Eroga Medicinali)
Farmacia-->VistaFAR{Vista}
VistaFAR-->FarmaciFAR(Farmaci)
VistaFAR-->PrescrFAR(Prescrizioni)

SSP-->RichiamiSSP(Richiami Esami)
SSP-->ErogaSSP(Eroga esami)
SSP-->|XLS|ReportSSP(Report per Giorno)

Utente-->SelezionaU{Sceglie}
SelezionaU-->Base
SelezionaU-->DatiU[Dati Profilo]
DatiU-->FotoU(Foto)
DatiU-->GenericoU(Altri Dati Utente)
Utente-->RicercaU{Ricerca}
RicercaU-->EsamiU(Esami Per Parola Chiave)
Utente-->VistaU{Vista}
VistaU-->VisiteU(Visite Proprie)
VistaU-->EsamipU(Esami Propri)
VistaU-->PrescrizioniU(Prescrizioni Proprie)

Base-->Report(Report prescritta/erogata)
Base-->Vista(Vista Pazienti Propri)
Vista-->Dettaglio(Dettaglio Singolo Paziente)
Dettaglio-->Ultima(Ultima Visita)
Dettaglio-->Ricetta(Ricetta Prescritta)
Dettaglio-->Scheda(Scheda Paziente con totale prescrizioni)
Base-->Prescrive{Prescrive}
Prescrive-->Esame(Esame)
Prescrive-->Farmaco(Farmaco)
Prescrive-->Visita(Visita Medica)
Specialistico-->Eroga(Eroga Visite Specialistiche)
Specialistico-->VistaGlob(Vista Pazienti Globale)
```

### Struttura DB

![tg_image_1501332751](assets/tg_image_1501332751.jpeg)

### Dettagli tecnici



#### Login e Registrazione

Per il login è stato utilizzato un filtro autenticativo, dove in base all'utente loggato viene reindirizzato in un path relativo diverso (es `/ssp/home`)



Il filtro agisce per ogni pagina la quale necessita di dati sensibili e/o zone private.



La sessione scade dopo un tempo base di **30 minuti,** mentre può essere estesa a **2 settimane.**



Nonostante la registrazione non fosse prevista, è stato scelto di farla comunque per praticità. La registrazione è comunque disattivabile semplicemente rimuovendo le JSP e Servlet nella quale operano.



Le **password** ovviamente rispettano delle policy per fare in modo che la password sia sicura, con più di X caratteri, con almeno un simbolo e un numero(oltre alle lettere).

Le **password** sono salvate sul database hashate con **bcrypt** e salt.



### Gestione Foto

Le foto sono state gestite attraverso un oggetto `Photo` che contiene `timestamp`, `idutente` e `ext`(estensione).

con il metodo `getPath()` si ottiene il path composto da `idutente/timestamp.ext`, residente nella cartella `Photos`.



#### Ruoli 

I ruoli sono:

* **Cittadino**
* **Medico di Base**
* **Medico Specialista**
* **SSP**
* **Farmacia**

*Sia il medico di base che lo specialista possono premere "naviga come paziente" per accedere come cittadino.*



##### Cittadino

Il cittadino ha funzioni strettamente di base:

* Controllare le proprie Visite
* Controllare i propri Esami
* Vedere le proprie ricette
* Vedere la propria cartella clinica con prescrizioni/esami evasi ed inevasi, ricette, propri dati e lista di foto precedenti.
* Ricerca di prestazioni globalmente(tra visite e esami)
* Vista e stampa di ticket
* Vista del profilo, con:
  * Scelta del medico di base;
  * Scelta della foto profilo;
  * Modifica dei dettagli personali.



##### Medico di Base

Un medico di base ha 

* Vista del profilo , come il cittadino;
* Vista dei propri pazienti;
  * Per ogni paziente, può iniziare una **visita**.
* Vista delle attività recenti effettuate (ultime 3 azioni).



Nelle **Visite**:

* Il medico avrà accesso alla scheda clinica del paziente, dove vedrà
  * Generalità del paziente;
  * Visite del paziente;
  * Esami del paziente;
  * Ricette del paziente;
  * Foto del paziente.
* Avrà accesso ad una sezione apposita per effettuare una **visita di base**. La visita di base ha:
  * Una Anamnesi;
  * Esami prescritti;
  * Visite specialistiche;
  * Farmaci prescritti.

*Sia visite ed esami prescritti sono ad inserimento multiplo usando i valori del database, mentre i farmaci prescritti sono a inserimento libero, confermabili in blocco premendo invio, visto le infinite varianti di dosaggio e nome/tipo farmaco.*

*Una visita di base ha un ID standard 80* (numero arbitrario che ci permette di capire quale visita è di base).



Dopo aver effettuato una visita, il cittadino riceve una mail con invito a loggare per vedere i dati.

Abbiamo deciso di non esporre o inviare direttamente la visita nella mail perchè sono comunque **dati sensibili.**

> Il medico di base è sia **prescrittore che erogatore** della visita, mentre il medico specialista è **erogatore** di eventuali visite fornite dal medico di base nel campo **visite specialistiche**.



##### Medico Specialista

Un medico specialista, come il medico, ha la possibilità di navigare come utente normale (cittadino).

Di funzioni base ha:

* Cambio di dati del proprio profilo come un utente normale;
* Inziare una visita specialistica

Nella **visita specialistica**:

* Il medico può vedere la Cartella clinica con visite, esami e ricette.
* Completare la visita specialistica scrivendo una anamnesi.

*Una visita specialistica è gesitita con un ID particolare in base alla visita scelta.*



##### SSP

Un SSP può

* Erogare gli Esami
* Prescrivere i richiami di visite ed esami
* Avere report delle ricette erogate e poterli stampare in PDF/XLS
* Avere un profilo modificabile come un utente.



##### Farmacia

Una farmacia può:

* Leggere le ricette erogabili e successivamente erogarle. Può erogare anche più di una ricetta per volta.
* Avere un profilo modificabile come un utente.



#### Email

L'utente viene notificato via mail *durante il recupero password* e *alla prescrizione di una visita*.

Le email saranno sempre e solo **notifiche**, non verranno mai trasmessi dati sensibili via mail.



##### Recupero password

Il recupero password è effettuato via token. 

Il token è generato in modo sicuro utilizzando `java.security.SecureRandom` con uno snippet ottenuto [qui](https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string/41156#41156), dove viene specificato perchè non usare **UUID**.

In sostanza, abbiamo preferito non usare **UUID** per la minor entropia.

La lunghezza del token è di 21 alfanumerici.

L'utente otterrà un link ed in `get` passerà il token se valido. La validità è stata settata a 24 ore per convenzione. Se un token è più vecchio di 24 ore, il token verrà invalidato.

Richiedere il recupero password non lockerà l'account, ma permetterà comunque l'accesso con la password (nel caso l'utente se la ricordi, non ho bisogno di forzarlo comunque a recuperare la password).



#### Uso di viste

Per utilizzare efficientemente ed efficacemente le Datatable con uso di richieste AJAX, il sistema utilizza delle View a livello database e oggetti equivalenti che uniscono più tipi a livello java.

Le view (ed i rispettivi oggetti sono):

|        View DB        |       View JAVA       |                         Composizione                         | Utilizzo |
| :-------------------: | :-------------------: | :----------------------------------------------------------: | :------: |
|      TicketView       |      TicketView       | (Dottore + Paziente + Erogatore) + Corrispettive Foto e Province + Ticket |    *     |
|      UtenteView       |      CitizenView      |                  Utente + Provincia + Foto                   |    *     |
|       *-------*       |  *PrescriptionView*   |                *----------------------------*                | Extends  |
|  EsamiPrescrittiView  | ExamPrescriptionView  | (Dottore + Paziente + Erogatore) + Corrispettive Foto e Province + Prescrizione |    *     |
| VisitePrescritteView  | VisitPrescriptionView | (Dottore + Paziente + Erogatore) + Corrispettive Foto e Province + Prescrizione |    *     |
| RicettePrescritteView | DrugPrescriptionView  | (Dottore + Paziente + Erogatore) + Corrispettive Foto e Province + Prescrizione |    *     |

*\* Vengono utilizzate nella API Generale* 



Le datatable andranno a prendersi i risultati con richieste AJAX (ad una API Generale) appena l'utente richiede e al caricamento di pagina .



#### API Generale

Tra i diversi servizi, abbiamo creato una API unica per gestire le richieste da datatable.

*Ovviamente la API non sarà accessibile se non loggato.*

|  Parametro  |                      Valori assumibili                       |                        Comportamento                         |                          Se `null`                           |
| :---------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|    `dt`     |              `exam, visit, drug, ticket, user`               | In base alla datatable scelta, restituirà risultati della datatable corrispondente. |                   Ritorna un array vuoto.                    |
|   `type`    | `citizenid`<br/>`plannerid`<br/>`performerid`<br/>`all`<br/>`province`\*<br/>`filtered`\* | Decido a che campo mi riferisco nel parametro `search`<br /><br />*\*solo nella `dt=drug`* |                 Lo considera come tipo `all`                 |
|  `search`   |                       stringa generica                       | Cerco quello di cui ho bisogno<br /> (in base al tipo scelto) |                 Lo considera come tipo `all`                 |
|   `size`    |                            numero                            | Se presente, restituirà la grandezza dei risultati cercati.  |  Esegue la query normalmente in base al `type` e `search`.   |
|   `start`   |                            numero                            | Filtra da dove partire nei risultati ricevuti, *conforme agli standard DataTable.* |                  Considera `0` come start.                   |
|  `length`   |                            numero                            | Filtra quanti risultati restituire nei risultati ricevuti, *conforme agli standard DataTable.* | Considera <br />`len(array)-start`<br /> come lunghezza. <br />(fine array, tutti i risultati dallo start in poi) |
|   `draw`    |                       stringa generica                       | Utilizzato automaticamente dalle datatable per identificare quale AJAX si riferisce, *conforme agli standard DataTable.* |            Non viene restituito con i risultati.             |
| `completed` |                     `true`<br />`false`                      | Se `true`, restituisce solo quelle erogate, modificandone la query a DB.<br />Se `false`, restituisce solo quelle non erogate, modificandone la query a DB. | Restituisce tutti i risultati, indipendentemente se erogati o meno. |

*`Filtered` è utilizzato nella home della farmacia (per filtrare quelli esistenti da quelli selezionati per una selezione multipla) e nella home dello specialista (per filtrare le prescrizioni che hanno come paziente lo specialista stesso)*



#### Parametri e sicurezza

Per ogni parametro che non sia parsato ad Int `Integer.parseInt` o float `Float.parseFloat` , avviene una *sanitizzazione dei parametri*utilizzando il singleton `Sanitizer`, dove vengono rimossi dalla stringa tutti i valori che possono essere **dannosi** e che possono fare `xss`.



Inoltre per il database utilizziamo solo `prepareStatement` e `DAOPattern`.



#### Flash Scope

È stato utilizzato un flash scope per evitare i messaggi di reinvio modulo grazie all'uso del *redirect* invece del *forward*, ma allo stesso tempo fornire informazioni riguardo ad *errori, successi, informazioni* relativi all'operazione di **Post** appena effettuata dall'utente. Abbiamo raggiunto questo obiettivo implementando il pattern **Post/Redirect/Get**.

È stato utilizzato il concetto di **flashscope** per salvare gli attributi, in modo che questi fossero disponibili nella richiesta immediatamente successiva a quella in cui sono stati aggiunti.

1. È stato creato un filtro che si occupa di prendere gli attributi della richiesta che iniziano con `flash.` al momento della restituzione al client, *salvandoli nella sessione in una mappa appositamente creata.* 

2. Ad ogni nuova richiesta del *client*, il filtro si occupa di svuotare la mappa di tutti gli attributi e aggiungerli alla richiesta eliminando il prefisso `flash.`, in modo che nelle richieste *successive* questi attributi *non siano più presenti.*



#### Pagine di errore e avvisi

Per avvisare l'utente del corretto avvenimento di una operazione, abbiamo utilizzato un singleton di avviso chiamato `feedback` e un altro singleton chiamato `toast`.

La differenza, entrambi implementati con il **builder pattern**, è sostanzialmente nel tipo di feedback visivo ottenuto.

* `feedback` ci permette di usare anche messaggi di errore complessi, in pagine diverse da quelle nella quale è avvenuta la richiesta;
* `toast` utilizza i toast di **bootstrap** per avvisare l'utente del corretto avvenimento di una operazione, senza *reindirizzarlo ad un altra pagina* ma mantenendosi nella pagina dove è avvenuta la richiesta (utilizzando *Post-Redirect-Get pattern,* cioè `Flash Scope`).

Per gli errori `uncaught` e qualsiasi altra cosa non avvenuta correttamente che non passa per queste pagine, è stata implementata una JSP generica che restituisce il codice di pagina (`500`,`404`, ... ) ed un eventuale messaggio di errore settabile attraverso `setAttribute()` nella richiesta.



Entrambi i singleton utilizzano lo stesso metodo(`setAttribute` nella richiesta, invio alla pagina di JSP), solo utilizzando un builder pattern, più elegantemente. 



#### Stampa ricette

Per la stampa delle ricette il file **PDF** viene generato al momento con i dati richiesti + QR Generato al momento con i dati richiesti presenti in esso.

Il file non viene salvato, ma viene effettuata *direttamente una scrittura nell'outputStream della richiesta,* evitando un utilizzo macchinoso di filtri per controllare chi effettivamente richieda il determinato file salvato nel FileSystem.