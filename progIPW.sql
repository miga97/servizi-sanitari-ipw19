-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Ott 13, 2019 alle 21:47
-- Versione del server: 5.5.62-MariaDB
-- Versione PHP: 5.6.40

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `TESTprogIPW`
--
CREATE DATABASE IF NOT EXISTS `ProgIPW` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ProgIPW`;

-- --------------------------------------------------------

--
-- Struttura della tabella `Esami`
--
-- Creazione: Ago 17, 2019 alle 08:19
-- Ultimo aggiornamento: Ago 17, 2019 alle 08:19
-- Ultimo controllo: Ott 06, 2019 alle 00:07
--

DROP TABLE IF EXISTS `Esami`;
CREATE TABLE `Esami` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `Esami`:
--

--
-- Svuota la tabella prima dell'inserimento `Esami`
--

TRUNCATE TABLE `Esami`;
--
-- Dump dei dati per la tabella `Esami`
--

INSERT INTO `Esami` (`id`, `nome`) VALUES
(1, '(Laser) dopplergrafia degli arti superiori o inferiori'),
(2, '11 deossicortisolo'),
(3, '17 Chetosteroidi'),
(4, '17 alfa idrossiprogesterone (17 OH-P)'),
(5, '17 idrossicorticoidi [du]'),
(6, '3 metil istidina'),
(7, '7-Deidrocolesterolo. Dosaggio'),
(8, 'Aberrometria oculare'),
(9, 'Acidi biliari'),
(10, 'Acidi grassi a catena molto lunga (VLCFA)'),
(11, 'Acidi nucleici di microrganismi (batteri, virus, miceti, protozoi) ricerca in materiali biologici vari multiplex'),
(12, 'Acidi organici urinari (NAS) in gc/ms. Dosaggio'),
(13, 'Acido 5 idrossi 3 indolacetico'),
(14, 'Acido acetoacetico'),
(15, 'Acido citrico'),
(16, 'Acido delta aminolevulinico (ALA)'),
(17, 'Acido fitanico. Dosaggio'),
(18, 'Acido guanidino acetico. Dosaggio'),
(19, 'Acido ippurico'),
(20, 'Acido lattico'),
(21, 'Acido omovanillico'),
(22, 'Acido orotico urinario. Dosaggio'),
(23, 'Acido para aminoippurico (pai)'),
(24, 'Acido pipecolico. Dosaggio'),
(25, 'Acido piruvico'),
(26, 'Acido pristanico. Dosaggio'),
(27, 'Acido sialico'),
(28, 'Acido sialico libero. Dosaggio'),
(29, 'Acido sialico totale. Dosaggio'),
(30, 'Acido valproico'),
(31, 'Acido vanilmandelico'),
(32, 'Actinomiceti in materiali biologici esame colturale'),
(33, 'Acufenometria, prove audiometriche sopraliminari'),
(34, 'Adiuretina [vasopressina] (ADH)'),
(35, 'Adrenalina - noradrenalina [p]'),
(36, 'Adrenalina - noradrenalina [u]'),
(37, 'Adrenalina-Noradrenalina'),
(38, 'Aeromonas nelle feci esame colturale'),
(39, 'Agglutinine a freddo'),
(40, 'Agoaspirato della mammella'),
(41, 'Agoaspirato della mammella in stereotassi'),
(42, 'Agoaspirato ecoguidato della mammella'),
(43, 'Agoaspirazione di cisti dell\'ovaio'),
(44, 'Agoaspirazione ecoguidata dei follicoli [pick up ovocitario]. Prelievo ovociti'),
(45, 'Agoaspirazione testicolare (TESA)'),
(46, 'Agoaspirazione/Drenaggio nella regione tiroidea'),
(47, 'Agobiopsia ecoguidata della prostata (approccio transperineale o transrettale)'),
(48, 'Agobiopsia ecoguidata delle vescicole seminali'),
(49, 'Agobiopsia ecoguidata transtracheale o transbronchiale di linfonodi ilo mediastinici'),
(50, 'Agobiopsia linfonodale'),
(51, 'Agobiopsia orbitaria'),
(52, 'Agobiopsia percutanea o agoaspirato del fegato'),
(53, 'Ala deidrasi eritrocitaria'),
(54, 'Alanina aminotransferasi (ALT) (GPT)'),
(55, 'Albumina'),
(56, 'Albuminuria [microalbuminuria]'),
(57, 'Aldolasi'),
(58, 'Aldosterone'),
(59, 'Alfa 1 antitripsina'),
(60, 'Alfa 1 fetoproteina'),
(61, 'Alfa 1 glicoproteina acida'),
(62, 'Alfa 1 microglobulina'),
(63, 'Alfa 2 antiplasmina'),
(64, 'Alfa 2 macroglobulina'),
(65, 'Alfa amilasi isoenzimi'),
(66, 'Alfa amilasi totale'),
(67, 'Alluminio'),
(68, 'Amebe a vita libera esame colturale'),
(69, 'Amebe a vita libera esame colturale'),
(70, 'Amebe a vita libera esame microscopico (con colorazioni specifiche)'),
(71, 'Aminoacidi totali, frazionamento cromatografico'),
(72, 'Aminoacidi. Dosaggio'),
(73, 'Amitriptilina'),
(74, 'Ammonio'),
(75, 'Amniocentesi'),
(76, 'Amplificazione gene n-myc'),
(77, 'Amplificazione mdm2'),
(78, 'Analisi cinematica dell\'arto superiore o inferiore o del tronco'),
(79, 'Analisi citogenetica molecolare. Incluso: fish con sonda di DNA su metafasi/nuclei interfasici/mlpa e coltura del materiale biologico da analizzare'),
(80, 'Analisi citogenetica per patologia da fragilità cromosomica'),
(81, 'Analisi citogenetica per ricerca siti fragili'),
(82, 'Analisi citogenetica per scambi di cromatidi fratelli'),
(83, 'Analisi citogenetica per studio mosaicismo cromosomico'),
(84, 'Analisi citogenetica per studio riarrangiamenti cromosomici indotti'),
(85, 'Analisi citogenetica postnatale. Analisi del cariotipo'),
(86, 'Analisi citogenetica prenatale. Analisi del cariotipo'),
(87, 'Analisi citogenetica prenatale. Ricerca aneuploidie dei cromosomi 13, 18, 21, X e Y'),
(88, 'Analisi citogenetica prenatale. Ricerca aneuploidie dei cromosomi 15, 16, 22'),
(89, 'Analisi del DNA cellulare per lo studio citometrico del ciclo cellulare e della ploidia'),
(90, 'Analisi del DNA e ibridazione con sonda molecolare (southern blot)'),
(91, 'Analisi del DNA per polimorfismo'),
(92, 'Analisi del carattere'),
(93, 'Analisi della cinematica dell\'arto superiore'),
(94, 'Analisi della cinematica e della dinamica del passo'),
(95, 'Analisi della superficie oculare [test di Schirmer, break up time (BUT), esame con coloranti]'),
(96, 'Analisi di contaminazione materna. Zigosità, PCR qualitativa (real-time PCR)'),
(97, 'Analisi di mutazione del DNA con elettroforesi'),
(98, 'Analisi di mutazione del DNA con ibridazione con sonde'),
(99, 'Analisi di mutazione nota. Ricerca di mutazione identificata in caso di familiarità'),
(100, 'Analisi di mutazioni del DNA'),
(101, 'Analisi di polimorfismi (str, VNTR)'),
(102, 'Analisi di regione cromosomica mediante southern blot (blotting)'),
(103, 'Analisi di segmenti di DNA mediante sequenziamento'),
(104, 'Analisi di sequenza geniche mediante next generation sequencing (NGS) e tecniche assimilabili'),
(105, 'Analisi dinamometrica dell\'arto superiore o inferiore o del tronco'),
(106, 'Analisi dinamometrica isocinetica segmentale'),
(107, 'Analisi microsatelliti tx-eterologo'),
(108, 'Analisi mutazionale di malattia che necessita del sequenziamento del DNA mitocondriale per la diagnosi'),
(109, 'Analisi mutazionale di malattia che necessita di più di un gene per la diagnosi. Sequenziamento ed eventuale metodica quantitativa.'),
(110, 'Analisi mutazionale di malattia che necessita di un solo gene per la diagnosi'),
(111, 'Analisi quantitativa di acidi nucleici umani mediante PCR real time (RT-PCR) o tecniche assimilabili'),
(112, 'Analisi strumentale della voce / esame fonetografico'),
(113, 'Androstanediolo glucuronide'),
(114, 'Angio RM coronarica'),
(115, 'Angio RM dei vasi del collo'),
(116, 'Angio RM dei vasi del collo senza e con MDC'),
(117, 'Angio RM del distretto toracico'),
(118, 'Angio RM del distretto vascolare intracranico'),
(119, 'Angio RM del distretto vascolare intracranico senza e con MDC'),
(120, 'Angio RM dell\'addome inferiore'),
(121, 'Angio RM dell\'addome superiore'),
(122, 'Angio RM dell\'arto inferiore'),
(123, 'Angio RM dell\'arto superiore'),
(124, 'Angio RM dell\'arto superiore o inferiore'),
(125, 'Angio RM midollo spinale con mezzo di contrasto'),
(126, 'Angio TC [arteriosa e venosa] degli arti inferiori'),
(127, 'Angio TC [arteriosa e venosa] degli arti superiori'),
(128, 'Angio TC dei vasi del collo [carotidi]'),
(129, 'Angio TC dei vasi intracranici'),
(130, 'Angio TC dei vasi intracranici e del collo [carotidi]'),
(131, 'Angio TC del circolo polmonare'),
(132, 'Angio TC dell\'aorta addominale'),
(133, 'Angio TC dell\'aorta toracica'),
(134, 'Angio TC dell\'aorta toraco addominale'),
(135, 'Angio TC di aorta addominale e arterie renali'),
(136, 'Angio TC di aorta addominale e arti inferiori'),
(137, 'Angiocardiografia combinata del cuore destro e sinistro'),
(138, 'Angiocardiografia del cuore destro'),
(139, 'Angiocardiografia del cuore sinistro'),
(140, 'Angiocardiografia della vena cava'),
(141, 'Angiocardioscintigrafia all\'equilibrio'),
(142, 'Angiocardioscintigrafia di primo passaggio (first pass)'),
(143, 'Angiografia con fluoresceina o angioscopia oculare'),
(144, 'Angiografia oculistica con verde indocianina'),
(145, 'Angioscintigrafia [angiografia, venografia radioisotopica]'),
(146, 'Angioscopia percutanea'),
(147, 'Angiotensina ii'),
(148, 'Anisakis anticorpi'),
(149, 'Anoscopia'),
(150, 'Ansogramma colico per atresia anorettale'),
(151, 'Antibiotici'),
(152, 'Anticoagulante lupus-like (LAC)'),
(153, 'Anticoagulanti acquisiti ricerca'),
(154, 'Anticorpi IgG avidity per rosolia, citomegalovirus.'),
(155, 'Anticorpi anti DNA nativo'),
(156, 'Anticorpi anti HLA contro sospensioni linfocitarie (almeno 10 soggetti)'),
(157, 'Anticorpi anti a/b'),
(158, 'Anticorpi anti antigeni nucleari estraibili (ENA). Test di screening'),
(159, 'Anticorpi anti canale del calcio'),
(160, 'Anticorpi anti cardiolipina [igg, IgM ed eventuali iga]'),
(161, 'Anticorpi anti cellule parietali gastriche (PCA)'),
(162, 'Anticorpi anti centromero'),
(163, 'Anticorpi anti citoplasma dei neutrofili (ANCA: P-ANCA e C-ANCA)'),
(164, 'Anticorpi anti citrullina (peptide)'),
(165, 'Anticorpi anti endomisio'),
(166, 'Anticorpi anti endotelio'),
(167, 'Anticorpi anti eritrocitari (caratterizzazione del range termico)'),
(168, 'Anticorpi anti eritrocitari (con mezzo potenziante)'),
(169, 'Anticorpi anti eritrocitari (identificazione)'),
(170, 'Anticorpi anti eritrocitari (titolazione)'),
(171, 'Anticorpi anti eritrocitari [test di Coombs diretto]'),
(172, 'Anticorpi anti eritrociti [test di Coombs indiretto]'),
(173, 'Anticorpi anti fattore viii'),
(174, 'Anticorpi anti fosfolipidi (IgG, IgM)'),
(175, 'Anticorpi anti gliadina deamidata (DPG-AGA) IgG'),
(176, 'Anticorpi anti glicoproteina oligodendrocitaria mielinica (mog)'),
(177, 'Anticorpi anti glutammico decarbossilasi (GAD)'),
(178, 'Anticorpi anti gm1 (monosialoganglioside)'),
(179, 'Anticorpi anti hla (cross-match, singolo individuo)'),
(180, 'Anticorpi anti insula pancreatica (ICA)'),
(181, 'Anticorpi anti insulina (AIAA)'),
(182, 'Anticorpi anti interferone'),
(183, 'Anticorpi anti istoni'),
(184, 'Anticorpi anti jo1. Determinazione singola'),
(185, 'Anticorpi anti leucociti'),
(186, 'Anticorpi anti mag'),
(187, 'Anticorpi anti membrana basale glomerulare'),
(188, 'Anticorpi anti microsomi (AbTMS) o anti tireoperossidasi (AbTPO)'),
(189, 'Anticorpi anti microsomi epatici e renali (LKMA)'),
(190, 'Anticorpi anti mieloperossidasi (MPO)'),
(191, 'Anticorpi anti mitocondri (AMA)'),
(192, 'Anticorpi anti muscolo liscio (ASMA)'),
(193, 'Anticorpi anti muscolo striato (cuore)'),
(194, 'Anticorpi anti nucleo (ANA)'),
(195, 'Anticorpi anti organo'),
(196, 'Anticorpi anti ovaio'),
(197, 'Anticorpi anti piastrine'),
(198, 'Anticorpi anti piastrine adese'),
(199, 'Anticorpi anti piastrine circolanti'),
(200, 'Anticorpi anti piastrine identificazione'),
(201, 'Anticorpi anti proteinasi 3 (PR3)'),
(202, 'Anticorpi anti recettore acetilcolina'),
(203, 'Anticorpi anti recettore del tsh'),
(204, 'Anticorpi anti recettore nicotinico muscolare'),
(205, 'Anticorpi anti rnp. Determinazione singola'),
(206, 'Anticorpi anti saccaromyces cerevisiae'),
(207, 'Anticorpi anti scl-70. Determinazione singola'),
(208, 'Anticorpi anti sm. Determinazione singola'),
(209, 'Anticorpi anti spermatozoi'),
(210, 'Anticorpi anti ssa. Determinazione singola'),
(211, 'Anticorpi anti ssb. Determinazione singola'),
(212, 'Anticorpi anti surrene'),
(213, 'Anticorpi anti testicolo (ATLA)'),
(214, 'Anticorpi anti tireoglobulina (ABTG)'),
(215, 'Anticorpi anti transglutaminasi (IgG, IgA)'),
(216, 'Anticorpi emolitici anti eritrocitari'),
(217, 'Anticorpi in fase solida anticorpi anti-at1r'),
(218, 'Antigene carboidratico 125 (CA 125)'),
(219, 'Antigene carboidratico 15.3 (CA 15.3)'),
(220, 'Antigene carboidratico 19.9 (CA 19.9)'),
(221, 'Antigene carboidratico 195 (CA 195)'),
(222, 'Antigene carboidratico 50 (CA 50)'),
(223, 'Antigene carboidratico 72-4 (CA 72-4)'),
(224, 'Antigene carboidratico mucinoso [MCA]'),
(225, 'Antigene carcinoembrionario [CEA]'),
(226, 'Antigene polipeptidico tissutale [TPA]'),
(227, 'Antigene prostatico specifico [PSA]'),
(228, 'Antigene prostatico specifico [PSA] reflex'),
(229, 'Antigene ta 4 [SCC]'),
(230, 'Antigeni eritrocitari cd55/cd59'),
(231, 'Antigeni hla'),
(232, 'Antigeni piastrinici'),
(233, 'Antitrombina funzionale [AT3]'),
(234, 'Aortografia addominale'),
(235, 'Aortografia toracica'),
(236, 'Apicocardiogramma (con elettrodo ECG)'),
(237, 'Apolipoproteina b'),
(238, 'Apolipoproteina-A1'),
(239, 'Aptoglobina'),
(240, 'Arteriografia coronarica con catetere doppio'),
(241, 'Arteriografia coronarica con catetere singolo'),
(242, 'Arteriografia di arterie cerebrali'),
(243, 'Arteriografia monolaterale dell\'arto superiore'),
(244, 'Artro RM: RMN (risonanza magnetica nucleare) muscoloscheletrica'),
(245, 'Artro RM: RMN (risonanza magnetica nucleare) muscoloscheletrica senza e con mezzo di contrasto'),
(246, 'Artro TC (tomografia computerizzata) spalla o gomito o ginocchio'),
(247, 'Artrografia con mezzo di contrasto'),
(248, 'Artrografia temporomandibolare con contrasto bilaterale'),
(249, 'Artrografia temporomandibolare con contrasto monolaterale'),
(250, 'Aspartato aminotransferasi (AST) (GOT)'),
(251, 'Aspirazione percutanea di cisti nella ghiandola del Bartolino'),
(252, 'Attivatore tissutale del plasminogeno [TPA]'),
(253, 'Audiometria'),
(254, 'Audiometria automatica'),
(255, 'BRCA1 e 2 reflex: sequenziamento completo e riarrangiamento con consulenza genetica pre-post test'),
(256, 'Barbiturici'),
(257, 'Bartonella hanselae anticorpi, IgG e IgM per classe di anticorpi'),
(258, 'Bartonella ricerca anticorpi IgG e IgM'),
(259, 'Batteri acidi nucleici in materiali biologici ibridazione'),
(260, 'Batteri acidi nucleici in materiali biologici ibridazione diretta'),
(261, 'Batteri acidi nucleici in materiali biologici ricerca qualitativa/quantitativa'),
(262, 'Batteri anaerobi antibiogramma da coltura (M.I.C., almeno 10 antibiotici)'),
(263, 'Batteri anaerobi da coltura identificazione biochimica'),
(264, 'Batteri anaerobi in materiali biologici esame colturale'),
(265, 'Batteri antibiogramma da coltura (M.I.C., almeno 10 antibiotici)'),
(266, 'Batteri antibiogramma da coltura (attività associazioni antibiotiche)'),
(267, 'Batteri antibiogramma da coltura (attività battericida c.m.b.)'),
(268, 'Batteri antibiogramma da coltura (kirby bauer, almeno 10 antibiotici)'),
(269, 'Batteri anticorpi immunoblotting (saggio di conferma)'),
(270, 'Batteri antigeni cellulari ed extracellulari identificazione diretta'),
(271, 'Batteri antigeni cellulari ed extracellulari ricerca diretta in materiali biologici'),
(272, 'Batteri da coltura identificazione biochimica'),
(273, 'Batteri da coltura identificazione sierologica'),
(274, 'Batteri determinazione carica microbica in liquidi biologici diversi'),
(275, 'Batteri in campioni biologici diversi ricerca microscopica'),
(276, 'Batteri potere antibatterico residuo in materiali biologici diversi'),
(277, 'Batteri potere battericida del siero sull\'isolato clinico'),
(278, 'Batteri prodotti metabolici in materiali biologici diversi identificazione'),
(279, 'Batteri respiratori ricerca acidi nucleici multiplex. Almeno tre microrganismi'),
(280, 'Benzodiazepine'),
(281, 'Benzolo'),
(282, 'Beta tromboglobulina'),
(283, 'Beta-Idrossibutirrato'),
(284, 'Beta2 microglobulina'),
(285, 'Bicarbonati (idrogenocarbonato)'),
(286, 'Bilirubina (curva spettrofotometrica nel liquido amniotico)'),
(287, 'Bilirubina diretta'),
(288, 'Bilirubina reflex'),
(289, 'Bilirubina totale'),
(290, 'Bioimpedenziometria, valutazione dello stato di idratazione'),
(291, 'Biomicroscopia corneale con conta cellule endoteliali'),
(292, 'Biopsia RM della mammella con retroaspirazione'),
(293, 'Biopsia [agobiopsia] del midollo osseo'),
(294, 'Biopsia [agobiopsia] della lingua'),
(295, 'Biopsia [agobiopsia] di ghiandola o dotto salivare'),
(296, 'Biopsia [endoscopica] del retto'),
(297, 'Biopsia [endoscopica] dell\'esofago'),
(298, 'Biopsia [endoscopica] dell\'intestino tenue'),
(299, 'Biopsia [endoscopica] dello stomaco'),
(300, 'Biopsia [percutanea] [agobiopsia] [aspirazione] del pancreas TC guidata'),
(301, 'Biopsia [percutanea] [agobiopsia] [aspirazione] del pancreas ecoguidata'),
(302, 'Biopsia [percutanea] [agobiopsia] dei nervi periferici'),
(303, 'Biopsia [percutanea] [agobiopsia] del mediastino'),
(304, 'Biopsia [percutanea] [agobiopsia] del testicolo'),
(305, 'Biopsia [percutanea] [agobiopsia] della colecisti e dei dotti biliari'),
(306, 'Biopsia [percutanea] [agobiopsia] della tiroide'),
(307, 'Biopsia [percutanea] [agobiopsia] delle ghiandole surrenali'),
(308, 'Biopsia [percutanea] [agobiopsia] di massa intraddominale ECO guidata'),
(309, 'Biopsia [percutanea] [agobiopsia] di massa intraddominale TC guidata'),
(310, 'Biopsia a celo aperto del retto'),
(311, 'Biopsia a cielo aperto dei bronchi'),
(312, 'Biopsia a cielo aperto dei legamenti uterini'),
(313, 'Biopsia a cielo aperto dei nervi cranici o periferici o dei gangli'),
(314, 'Biopsia a cielo aperto del fegato'),
(315, 'Biopsia a cielo aperto del pancreas'),
(316, 'Biopsia a cielo aperto del polmone'),
(317, 'Biopsia a cielo aperto del testicolo'),
(318, 'Biopsia a cielo aperto dell\'intestino tenue'),
(319, 'Biopsia a cielo aperto dell\'uretere'),
(320, 'Biopsia a cielo aperto dell\'utero'),
(321, 'Biopsia a cielo aperto della colecisti e dei dotti biliari'),
(322, 'Biopsia a cielo aperto della lingua'),
(323, 'Biopsia a cielo aperto della mammella'),
(324, 'Biopsia a cielo aperto della milza'),
(325, 'Biopsia a cielo aperto della prostata'),
(326, 'Biopsia a cielo aperto della tiroide'),
(327, 'Biopsia a cielo aperto delle ghiandole surrenali'),
(328, 'Biopsia a cielo aperto delle vescicole seminali'),
(329, 'Biopsia a cielo aperto di ghiandola o dotto salivare'),
(330, 'Biopsia a cielo aperto di lesione cerebrale'),
(331, 'Biopsia aspirativa dell\'ovaio'),
(332, 'Biopsia bronchiale [endoscopica]'),
(333, 'Biopsia chirurgica della mammella con o senza reperaggio stereotassico'),
(334, 'Biopsia chiusa [percutanea] [agobiopsia] del polmone'),
(335, 'Biopsia dei legamenti uterini'),
(336, 'Biopsia dei seni nasali'),
(337, 'Biopsia dei tessuti molli/muscoli'),
(338, 'Biopsia dei vasi sanguigni'),
(339, 'Biopsia del cordone spermatico, dell\'epididimo o dei dotti deferenti'),
(340, 'Biopsia del cul de sac'),
(341, 'Biopsia del cuore'),
(342, 'Biopsia del diaframma'),
(343, 'Biopsia del labbro'),
(344, 'Biopsia del naso'),
(345, 'Biopsia del palato osseo'),
(346, 'Biopsia del pene'),
(347, 'Biopsia del pericardio'),
(348, 'Biopsia del peritoneo'),
(349, 'Biopsia del sacco lacrimale'),
(350, 'Biopsia del tessuto periprostatico'),
(351, 'Biopsia del tessuto perirenale o perivescicale'),
(352, 'Biopsia del tessuto periuretrale'),
(353, 'Biopsia del timo'),
(354, 'Biopsia dell\'alveolo'),
(355, 'Biopsia dell\'ano o dei tessuti perianali'),
(356, 'Biopsia dell\'intestino tenue in corso di enteroscopia'),
(357, 'Biopsia dell\'iride'),
(358, 'Biopsia dell\'orecchio esterno'),
(359, 'Biopsia dell\'orecchio medio'),
(360, 'Biopsia dell\'osso in sede non specificata'),
(361, 'Biopsia dell\'uretra'),
(362, 'Biopsia dell\'utero'),
(363, 'Biopsia dell\'utero con dilatazione del canale cervicale'),
(364, 'Biopsia della bocca (cavo orale)'),
(365, 'Biopsia della congiuntiva'),
(366, 'Biopsia della cornea'),
(367, 'Biopsia della cute e del tessuto sottocutaneo'),
(368, 'Biopsia della gengiva'),
(369, 'Biopsia della ghiandola lacrimale'),
(370, 'Biopsia della ghiandola pineale'),
(371, 'Biopsia della laringe'),
(372, 'Biopsia della mammella con tru-cut'),
(373, 'Biopsia della palpebra'),
(374, 'Biopsia della parete addominale o dell\'ombelico'),
(375, 'Biopsia della parete toracica'),
(376, 'Biopsia della pleura'),
(377, 'Biopsia della vulva o della cute perineale'),
(378, 'Biopsia delle ossa facciali'),
(379, 'Biopsia delle paratiroidi'),
(380, 'Biopsia delle pareti vaginali a guida colposcopica'),
(381, 'Biopsia delle strutture articolari, sede non specificata'),
(382, 'Biopsia delle tube'),
(383, 'Biopsia dello scroto o della tunica vaginale'),
(384, 'Biopsia di strutture linfatiche'),
(385, 'Biopsia di tessuti perirettali'),
(386, 'Biopsia endocervicale'),
(387, 'Biopsia endometriale'),
(388, 'Biopsia endoscopica dell\'albero biliare o dello sfintere di Oddi'),
(389, 'Biopsia faringea'),
(390, 'Biopsia in sede multipla in corso di ileocolonscopia retrograda'),
(391, 'Biopsia laparoscopica dell\'ovaio'),
(392, 'Biopsia mediastinica a cielo aperto'),
(393, 'Biopsia mirata della portio e dell\'endocervice'),
(394, 'Biopsia mirata endocervicale a guida isteroscopica'),
(395, 'Biopsia per aspirazione [percutanea] [agobiopsia] della milza'),
(396, 'Biopsia percutanea dell\'uretere'),
(397, 'Biopsia percutanea mammaria \'vacuum assisted\''),
(398, 'Biopsia renale a cielo aperto'),
(399, 'Biopsia transparietale [ecoendoscopica] biliopancreatica'),
(400, 'Biopsia transparietale [ecoendoscopica] del colon'),
(401, 'Biopsia transparietale [ecoendoscopica] del retto-sigma'),
(402, 'Biopsia transureteroscopica'),
(403, 'Biopsia vaginale / delle pareti vaginali'),
(404, 'Biopsia vescicale a cielo aperto'),
(405, 'Biotinidasi. Dosaggio'),
(406, 'Bordetella anticorpi (E.I.A.)'),
(407, 'Bordetella anticorpi IgG e IgM. Incluso: IgA se IgM negative'),
(408, 'Bordetella esame colturale'),
(409, 'Borrelia analisi qualitativa DNA o RNA'),
(410, 'Borrelia anticorpi IgG e IgM. Incluso: eventuale immunoblotting'),
(411, 'Borrelia burgdorferi anticorpi (E.I.A.)'),
(412, 'Borrelia burgdorferi anticorpi (I.F.)'),
(413, 'Breath test per determinazione di colonizzazione batterica anomala'),
(414, 'Breath test per determinazione tempo di transito intestinale, svuotamento gastrico'),
(415, 'Breath test per helycobacter pylori (Urea C13)'),
(416, 'Breath test per intolleranza al lattosio'),
(417, 'Breath test per lo studio della funzionalità epatica'),
(418, 'Breath test per lo studio della funzionalità pancreatica'),
(419, 'Broncoscopia'),
(420, 'Brucelle anticorpi (E.I.A.)'),
(421, 'Brucelle anticorpi (titolazione mediante agglutinazione) [Wright]'),
(422, 'Brucelle anticorpi incompleti (coombs)'),
(423, 'Brushing nasale per battito ciliare'),
(424, 'C peptide'),
(425, 'C peptide. Dosaggio basale e dopo stimolo'),
(426, 'Cadmio'),
(427, 'Calcio ionizzato (calcolo indiretto)'),
(428, 'Calcio ionizzato determinazione diretta'),
(429, 'Calcio totale'),
(430, 'Calcitonina'),
(431, 'Calcoli e concrezioni (ricerca semiquantitativa)'),
(432, 'Calcoli esame chimico di base (ricerca qualitativa)'),
(433, 'Calprotectina fecale'),
(434, 'Campylobacter antibiogramma'),
(435, 'Campylobacter da coltura identificazione biochimica'),
(436, 'Campylobacter esame colturale'),
(437, 'Capacitazione del materiale seminale [swim up]'),
(438, 'Capillaroscopia con videoregistrazione'),
(439, 'Captazione tiroidea'),
(440, 'Carbamazepina'),
(441, 'Carbossiemoglobina [(Sg)Hb/(Sg)Er]'),
(442, 'Cardiotocografia esterna'),
(443, 'Cariotipo ad alta risoluzione'),
(444, 'Cariotipo da metafasi di fibroblasti o di altri tessuti (materiale abortivo, ecc.)'),
(445, 'Cariotipo da metafasi di liquido amniotico'),
(446, 'Cariotipo da metafasi linfocitarie'),
(447, 'Cariotipo da metafasi spontanee di midollo osseo'),
(448, 'Cariotipo da metafasi spontanee di villi coriali'),
(449, 'Carnitina esterificata'),
(450, 'Carnitina libera'),
(451, 'Catecolamine totali urinarie'),
(452, 'Catene kappa e lamba [s/u] dosaggio (per ogni dosaggio)'),
(453, 'Cavografia inferiore'),
(454, 'Cavografia superiore'),
(455, 'Ceruloplasmina'),
(456, 'Cheratoestesiometria'),
(457, 'Chimotripsina'),
(458, 'Chlamydia pneumoniae anticorpi IgG e IgM. Incluso: IgA se IgM negative'),
(459, 'Chlamydia psittaci anticorpi IgG e IgM. Incluso: IgA se IgM negative'),
(460, 'Chlamydia trachomatis anticorpi IgG e IgM. Incluso: IgA se IgM negative'),
(461, 'Chlamydie anticorpi (E.I.A.)'),
(462, 'Chlamydie anticorpi (titolazione mediante F.C.)'),
(463, 'Chlamydie da coltura identificazione (I.F.)'),
(464, 'Chlamydie da coltura identificazione microscopica (colorazione iodio, giemsa)'),
(465, 'Chlamydie esame colturale'),
(466, 'Chlamydie ricerca diretta (E.I.A.)'),
(467, 'Chlamydie ricerca diretta (I.F.)'),
(468, 'Chlamydie ricerca diretta (mediante ibridazione)'),
(469, 'Chlamydie ricerca qualitativa DNA'),
(470, 'Ciclosporina'),
(471, 'Cine RM (risonanza magnetica) del cuore'),
(472, 'Cine RM (risonanza magnetica) del cuore senza e con mezzo di contrasto'),
(473, 'Cine RM (risonanza magnetica) del cuore senza e con stress funzionale'),
(474, 'Cistatina C'),
(475, 'Cistina intraleucocitaria'),
(476, 'Cistografia'),
(477, 'Cistografia con doppio contrasto'),
(478, 'Cistoscopia attraverso stoma artificiale'),
(479, 'Cistosonografia con mezzo di contrasto'),
(480, 'Cistouretrografia minzionale'),
(481, 'Cistouretrografia retrograda'),
(482, 'Citotossicità CTL'),
(483, 'Citotossicità LAK'),
(484, 'Citotossicità con antigeni specifici'),
(485, 'Citotossicità spontanea NK'),
(486, 'Clisma con doppio contrasto dell\'intestino'),
(487, 'Clisma del tenue con doppio contrasto dell\'intestino'),
(488, 'Clisma opaco semplice dell\'intestino'),
(489, 'Cloruro'),
(490, 'Cloruro, sodio e potassio [sd] (stimolazione con pilocarpina)'),
(491, 'Clostridium difficile da coltura identificazione biochimica'),
(492, 'Clostridium difficile esame colturale'),
(493, 'Clostridium difficile tossine nelle feci ricerca diretta'),
(494, 'Cobalammina (vit. b12)'),
(495, 'Cobalto'),
(496, 'Codelezione 1p/19q'),
(497, 'Cofattore ristocetinico'),
(498, 'Colangio RM'),
(499, 'Colangiografia epatica percutanea'),
(500, 'Colangiografia intraoperatoria'),
(501, 'Colangiografia intravenosa'),
(502, 'Colangiografia retrograda endoscopica [ERC]'),
(503, 'Colangiografia transcatetere'),
(504, 'Colangiopancreatografia retrograda endoscopica [ERCP]'),
(505, 'Colecistografia'),
(506, 'Colesterolo HDL'),
(507, 'Colesterolo LDL'),
(508, 'Colesterolo LDL. Determinazione diretta'),
(509, 'Colesterolo LDL. Determinazione indiretta'),
(510, 'Colesterolo totale'),
(511, 'Colinesterasi (pseudo-che)'),
(512, 'Colonscopia - ileoscopia retrograda'),
(513, 'Colonscopia con endoscopio flessibile'),
(514, 'Colonscopia con endoscopio flessibile con biopsia'),
(515, 'Color doppler transcranico'),
(516, 'Colorazione aggiuntiva in bande: actinomicina D'),
(517, 'Colorazione aggiuntiva in bande: bandeggio C'),
(518, 'Colorazione aggiuntiva in bande: bandeggio G'),
(519, 'Colorazione aggiuntiva in bande: bandeggio G ad alta risoluzione'),
(520, 'Colorazione aggiuntiva in bande: bandeggio NOR'),
(521, 'Colorazione aggiuntiva in bande: bandeggio Q'),
(522, 'Colorazione aggiuntiva in bande: bandeggio R'),
(523, 'Colorazione aggiuntiva in bande: bandeggio T'),
(524, 'Colorazione aggiuntiva in bande: distamicina A'),
(525, 'Colorazioni vitali in corso di EGDS o colonscopia'),
(526, 'Colpografia'),
(527, 'Colposcopia'),
(528, 'Coltura di amniociti'),
(529, 'Coltura di cellule di altri tessuti'),
(530, 'Coltura di fibroblasti'),
(531, 'Coltura di linee cellulari stabilizzate con virus'),
(532, 'Coltura di linee linfocitarie stabilizzate con virus o interleuchina'),
(533, 'Coltura di linfociti fetali con PHA'),
(534, 'Coltura di linfociti periferici con PHA o altri mitogeni'),
(535, 'Coltura di materiale abortivo'),
(536, 'Coltura di villi coriali'),
(537, 'Coltura di villi coriali (a breve termine)'),
(538, 'Coltura mista linfocitaria unidirezionale'),
(539, 'Coltura per studio del cromosoma X a replicazione tardiva'),
(540, 'Coltura semisolida di cellule emopoietiche'),
(541, 'Complemento (C1 inibitore) quantitativo'),
(542, 'Complemento: c3, c3 naf, c4, ch50'),
(543, 'Compliance polmonare statica e dinamica'),
(544, 'Conservazione di campioni di DNA o di RNA'),
(545, 'Consulenza genetica associata al test'),
(546, 'Controllo del set-up iniziale per prima seduta, controllo portale e/o della ripetibilità del set up del paziente'),
(547, 'Controllo della forma dell\'onda del pace-maker'),
(548, 'Controllo della impedenza dell\'elettrodo del pace-maker'),
(549, 'Controllo della soglia di voltaggio o amperaggio del pace-maker'),
(550, 'Controllo di cardiovertore/defibrillatore impiantabile automatico (AICD)'),
(551, 'Controllo di frequenza del pace-maker'),
(552, 'Controllo e programmazione defibrillatore impiantabile'),
(553, 'Controllo radiologico di derivazioni liquorali'),
(554, 'Coproporfirine'),
(555, 'Corneometria'),
(556, 'Coronarografia (angiografia coronarica)'),
(557, 'Corpi chetonici'),
(558, 'Corticotropina (ACTH)'),
(559, 'Cortisolo'),
(560, 'Corynebacterium diphteriae esame colturale'),
(561, 'Coxiella burneti anticorpi'),
(562, 'Creatina. Dosaggio'),
(563, 'Creatinchinasi (CPK o CK)'),
(564, 'Creatinchinasi isoenzima MB (CK-MB)'),
(565, 'Creatinchinasi isoenzimi'),
(566, 'Creatinchinasi isoforme'),
(567, 'Creatinina'),
(568, 'Creatinina clearance'),
(569, 'Crioconservazione cellule staminali [placentari] per trapianto'),
(570, 'Crioconservazione in azoto liquido di cellule e tessuti'),
(571, 'Crioconservazione in azoto liquido di colture cellulari'),
(572, 'Crioconservazione siero pre-trapianto'),
(573, 'Crioconservazione sospensioni linfocitarie'),
(574, 'Crioglobuline ricerca'),
(575, 'Crioglobuline tipizzazione'),
(576, 'Criptococco ricerca diretta'),
(577, 'Cromatografia zuccheri urinari'),
(578, 'Cromo'),
(579, 'Cromogranina a'),
(580, 'Cross-Match citofluorietrico linfociti t IgM'),
(581, 'Cross-Match citofluorimetrico linfociti B IgG'),
(582, 'Cross-Match citofluorimetrico linfociti t IgG'),
(583, 'Cross-Match citotossico con linfociti b'),
(584, 'Cross-Match citotossico con linfociti t'),
(585, 'Cryptosporidium ricerca diretta nelle feci'),
(586, 'Culdoscopia'),
(587, 'Curettage della cornea per striscio o coltura'),
(588, 'Cyfra 21-1'),
(589, 'D-Dimero. Dosaggio con metodo immunometrico'),
(590, 'D-dimero (test al latice)'),
(591, 'Dacriocistografia'),
(592, 'Decarbossiprotrombina'),
(593, 'Defecografia'),
(594, 'Deferentografia con contrasto'),
(595, 'Deidroepiandrosterone (dea)'),
(596, 'Deidroepiandrosterone solfato (DEA-S)'),
(597, 'Delta 4 androstenedione'),
(598, 'Desipramina'),
(599, 'Desossipiridinolina'),
(600, 'Determinazione del pattern respiratorio a riposo'),
(601, 'Determinazione del volume plasmatico o del volume eritrocitario'),
(602, 'Determinazione della P 0.1'),
(603, 'Determinazione della capacità vitale'),
(604, 'Determinazione delle massime pressioni inspiratorie ed espiratorie o transdiaframmatiche'),
(605, 'Determinazione e localizzazione perdite di LCR'),
(606, 'Diffusione alveolo-capillare del co'),
(607, 'Digestione di DNA con enzimi di restrizione'),
(608, 'Diidrotestosterone (DHT)'),
(609, 'Dopamina [s/u]'),
(610, 'Dosaggio attività anticoagulante dei farmaci inibitori fattore X attivato (eparina, rivaroxaban, etc.)'),
(611, 'Dosaggio dell\'attività anticoagulante fattore anti II a (dabigatran, etc)'),
(612, 'Dosaggio quantitativo di un singolo acido organico in liquidi biologici mediante gc/ms con isotopi stabili'),
(613, 'Dosimetria in vivo'),
(614, 'Doxepina'),
(615, 'Droghe d\'abuso'),
(616, 'Du variante ricerca'),
(617, 'Duplicazione di MLL'),
(618, 'EMG (elettromiografia) dinamica dell\'arto superiore'),
(619, 'Echinococco [idatidosi] anticorpi'),
(620, 'Eco(color)doppler dei reni e dei surreni'),
(621, 'Eco(color)doppler del fegato e delle vie biliari'),
(622, 'Eco(color)doppler del pancreas'),
(623, 'Eco(color)doppler dell\'addome inferiore'),
(624, 'Eco(color)doppler della mammella'),
(625, 'Eco(color)doppler della milza'),
(626, 'Eco(color)dopplergrafia degli arti superiori o inferiori o distrettuale'),
(627, 'Ecobiometria, esame biometrico interferometrico'),
(628, 'Ecocolordoppler arterie renali'),
(629, 'Ecocolordoppler cardiaco'),
(630, 'Ecocolordoppler cardiaco a riposo'),
(631, 'Ecocolordoppler cardiaco transesofagea'),
(632, 'Ecocolordoppler degli arti inferiori o superiori arterioso e/o venoso'),
(633, 'Ecocolordoppler dei grossi vasi addominali arteriosi o venosi'),
(634, 'Ecocolordoppler dei tronchi sovraaortici'),
(635, 'Ecocolordoppler paratiroidi'),
(636, 'Ecocolordoppler penieno dinamico con stimolazione farmacologica'),
(637, 'Ecocolordoppler scrotale'),
(638, 'Ecocolordoppler vasi splancnici'),
(639, 'Ecocolordoppler vasi viscerali'),
(640, 'Ecocolordopplergrafia cardiaca fetale / ecocardiogramma fetale'),
(641, 'Ecocolordopplergrafia dei vasi venosi del collo'),
(642, 'Ecodoppler fistola arterovenosa'),
(643, 'Ecoencefalografia. Ecografia transfontanellare'),
(644, 'Ecoendoscopia biliopancreatica [ERP]'),
(645, 'Ecoendoscopia del colon'),
(646, 'Ecoendoscopia del pancreas'),
(647, 'Ecoendoscopia del pancreas con biopsia'),
(648, 'Ecoendoscopia del retto-sigma'),
(649, 'Ecoendoscopia di esofago, stomaco, duodeno'),
(650, 'Ecoendoscopia di esofago, stomaco, duodeno con biopsia'),
(651, 'Ecografia cardiaca'),
(652, 'Ecografia cardiaca senza e con contrasto'),
(653, 'Ecografia degli arti superiori o inferiori o distrettuale, arteriosa'),
(654, 'Ecografia del capo, del collo e della tiroide'),
(655, 'Ecografia del pene'),
(656, 'Ecografia dell\'addome completo'),
(657, 'Ecografia dell\'addome inferiore (inclusi ureteri, vescica e pelvi maschile o femminile)'),
(658, 'Ecografia dell\'addome superiore (inclusi fegato e vie biliari, pancreas, milza, reni e surreni, retroperineo)'),
(659, 'Ecografia della cute e del tessuto sottocutaneo'),
(660, 'Ecografia della mammella (bilaterale)'),
(661, 'Ecografia della mammella (monolaterale)'),
(662, 'Ecografia delle anche nel neonato'),
(663, 'Ecografia delle anse intestinali'),
(664, 'Ecografia delle vie digestive'),
(665, 'Ecografia di grossi vasi addominali'),
(666, 'Ecografia di grossi vasi addominali senza e con contrasto'),
(667, 'Ecografia endoanale'),
(668, 'Ecografia ginecologica'),
(669, 'Ecografia mediastinica transesofagea'),
(670, 'Ecografia muscolotendinea e osteoarticolare'),
(671, 'Ecografia oculare'),
(672, 'Ecografia organo mirata con mezzo di contrasto'),
(673, 'Ecografia ostetrica'),
(674, 'Ecografia ostetrica con translucenza nucale'),
(675, 'Ecografia ostetrica morfologica'),
(676, 'Ecografia parete addominale'),
(677, 'Ecografia regione inguino-crurale'),
(678, 'Ecografia scrotale e dei testicoli'),
(679, 'Ecografia stazioni linfonodali pluridistrettuali (laterocervicale, sovraclaveare, ascellare, inguinale)'),
(680, 'Ecografia toracica / polmonare'),
(681, 'Ecografia transesofagea del torace'),
(682, 'Ecografia transrettale'),
(683, 'Ecografia transvaginale'),
(684, 'Elastasi 1 pancreatica'),
(685, 'Elastometria'),
(686, 'Elettrocardiogramma'),
(687, 'Elettrocardiogramma dinamico (secondo Holter)'),
(688, 'Elettrocardiogramma per lo studio del ritmo'),
(689, 'Elettrocardiogramma transesofageo'),
(690, 'Elettrococleografia'),
(691, 'Elettroencefalogramma (EEG)'),
(692, 'Elettroencefalogramma con analisi spettrale'),
(693, 'Elettroencefalogramma con privazione del sonno'),
(694, 'Elettroencefalogramma con sonno farmacologico'),
(695, 'Elettroencefalogramma con videoregistrazione'),
(696, 'Elettroencefalogramma dinamico [12-24 ore]'),
(697, 'Elettromiografia (EMG) dell\'occhio'),
(698, 'Elettromiografia (EMG) di muscoli speciali [laringei, faringei, diaframma, perineali]'),
(699, 'Elettromiografia (EMG) di una singola fibra'),
(700, 'Elettromiografia (EMG) di unità motoria analisi quantitativa. Con esame ad ago'),
(701, 'Elettromiografia semplice (EMG)'),
(702, 'Elettronistagmografia (ENG)'),
(703, 'Elettrooculografia (EOG)'),
(704, 'Elettroretinografia (ERG, flash-pattern)'),
(705, 'Emazie (conteggio), emoglobina'),
(706, 'Emissioni otoacustiche'),
(707, 'Emocromo con formula'),
(708, 'Emogasanalisi arteriosa prima e durante somministrazione di ossigeno'),
(709, 'Emogasanalisi arteriosa sistemica'),
(710, 'Emogasanalisi continua intra-arteriosa'),
(711, 'Emogasanalisi di sangue misto venoso'),
(712, 'Emogasanalisi durante respirazione di O2 a bassa concentrazione. Test dell\'ipossia'),
(713, 'Emogasanalisi durante respirazione di O2 ad alta concentrazione. Test dell\'iperossia'),
(714, 'Emogasanalisi prima e dopo iperventilazione'),
(715, 'Emolisina bifasica'),
(716, 'Endoscopia dell\'intestino crasso attraverso stoma artificiale'),
(717, 'Endoscopia dell\'intestino tenue attraverso stoma artificiale'),
(718, 'Endoscopia transaddominale dell\'intestino crasso'),
(719, 'Endoscopia transaddominale dell\'intestino tenue'),
(720, 'Enolasi neuronespecifica (NSE)'),
(721, 'Entamoeba histolytica anticorpi'),
(722, 'Entamoeba histolytica nelle feci esame colturale (coltura xenica)'),
(723, 'Entamoeba histolytica/dispar antigeni ricerca diretta nelle feci'),
(724, 'Enterobius vermicularis [ossiuri] ricerca microscopica su materiale perianale. Scotch test o tampone perianale'),
(725, 'Enterocolpocistodefecografia'),
(726, 'Enteroscopia con microcamera ingeribile [VCE]'),
(727, 'Enteroscopia per via anterograda con pallone [BAE]'),
(728, 'Enteroscopia per via anterograda con pallone [BAE] con biopsia'),
(729, 'Enzima di conversione dell\'angiotensina (ACE)'),
(730, 'Enzimi del metabolismo glicidico. Dosaggio'),
(731, 'Enzimi della beta-ossidazione. Dosaggio'),
(732, 'Enzimi eritrocitari'),
(733, 'Enzimi glicosilazione delle proteine. Dosaggio'),
(734, 'Enzimi lisosomiali. Dosaggio'),
(735, 'Enzimi metabolismo degli amminoacidi. Dosaggio'),
(736, 'Enzimi metabolismo delle porfirine. Dosaggio'),
(737, 'Enzimi metabolismo lipidico. Dosaggio'),
(738, 'Enzimi metabolismo pirimidinico. Dosaggio'),
(739, 'Enzimi metabolismo purinico. Dosaggio'),
(740, 'Enzimi mitocondriali. Dosaggio'),
(741, 'Eosinofili (conteggio)[alb]'),
(742, 'Epididimografia con contrasto'),
(743, 'Eritrociti: antigeni non abo e non rh'),
(744, 'Eritropoietina'),
(745, 'Esame allergologico strumentale per orticarie da agenti fisici'),
(746, 'Esame approfondito dell\'occhio'),
(747, 'Esame audiometrico condizionato infantile'),
(748, 'Esame audiometrico tonale'),
(749, 'Esame audiometrico vocale'),
(750, 'Esame citologico da agoaspirato apparato digerente'),
(751, 'Esame citologico da agoaspirato apparato respiratorio'),
(752, 'Esame citologico da agoaspirato della tiroide'),
(753, 'Esame citologico da agoaspirato di tessuto emopoietico'),
(754, 'Esame citologico da agoaspirazione'),
(755, 'Esame citologico da agoaspirazione ecoguidata: sedi multiple'),
(756, 'Esame citologico da agoaspirazione: sedi multiple'),
(757, 'Esame citologico di espettorato'),
(758, 'Esame citologico di versamenti (fino a 5 vetrini e/o colorazioni)'),
(759, 'Esame citologico esfoliativo apparato digerente'),
(760, 'Esame citologico esfoliativo apparato respiratorio'),
(761, 'Esame citologico esfoliativo cute'),
(762, 'Esame citologico esfoliativo mammella'),
(763, 'Esame citologico esfoliativo sierose'),
(764, 'Esame citologico urine per ricerca cellule neoplastiche'),
(765, 'Esame colturale broncolavaggio [prelievo protetto di secrezioni respiratorie]'),
(766, 'Esame colturale campioni apparato genitourinario / Tampone cervico vaginale'),
(767, 'Esame colturale campioni biologici diversi'),
(768, 'Esame colturale del sangue [emocoltura]'),
(769, 'Esame colturale dell\'urina [urinocoltura]'),
(770, 'Esame colturale delle feci [coprocoltura]'),
(771, 'Esame colturale espettorato'),
(772, 'Esame colturale essudati [pleurico, peritoneale, articolare, pericardico]'),
(773, 'Esame colturale essudati purulenti [pus] da lesioni profonde'),
(774, 'Esame colturale essudati purulenti [pus] da lesioni superficiali'),
(775, 'Esame colturale essudato auricolare otite esterna monolaterale'),
(776, 'Esame colturale essudato auricolare otite media acuta monolaterale / Tampone auricolare'),
(777, 'Esame colturale essudato oculare monolaterale'),
(778, 'Esame colturale essudato oro-faringo-nasale / Tampone nasale'),
(779, 'Esame colturale seriato di: urine primo mitto, urine mitto intermedio, liquido prostatico e/o urine dopo massaggio prostatico [test di Stamey] ricerca batteri patogeni'),
(780, 'Esame colturale seriato di: urine primo mitto, urine mitto intermedio, liquido prostatico o seminale [test di stamey]'),
(781, 'Esame dei denti'),
(782, 'Esame del midollo osseo per apposizione e/o striscio'),
(783, 'Esame dell\'afasia'),
(784, 'Esame dell\'occhio sotto anestesia'),
(785, 'Esame istocitopatologico SNP (biopsia di nervo periferico)'),
(786, 'Esame istocitopatologico altri organi da agobiopsia'),
(787, 'Esame istocitopatologico apparato digerente. Mapping per malattia infiammatoria cronica intestinale (IBD)'),
(788, 'Esame istocitopatologico apparato digerente: agobiopsia epatica'),
(789, 'Esame istocitopatologico apparato digerente: biopsia endoscopica (sede unica)'),
(790, 'Esame istocitopatologico apparato digerente: biopsia endoscopica (sedi multiple)'),
(791, 'Esame istocitopatologico apparato digerente: biopsia endoscopica per celiachia'),
(792, 'Esame istocitopatologico apparato digerente: biopsia endoscopica per studio gastrite cronica'),
(793, 'Esame istocitopatologico apparato digerente: biopsia ghiandola salivare'),
(794, 'Esame istocitopatologico apparato digerente: biopsia semplice'),
(795, 'Esame istocitopatologico apparato digerente: escissione di neoformazione'),
(796, 'Esame istocitopatologico apparato digerente: mucosectomia'),
(797, 'Esame istocitopatologico apparato digerente: polipectomia endoscopica (sedi multiple)'),
(798, 'Esame istocitopatologico apparato digerente: polipectomia endoscopica (singola)'),
(799, 'Esame istocitopatologico apparato genitale maschile: agobiopsia prostatica su prelievi multipli'),
(800, 'Esame istocitopatologico apparato genitale: biopsia semplice'),
(801, 'Esame istocitopatologico apparato genitale: da conizzazione cervice uterina'),
(802, 'Esame istocitopatologico apparato genitale: escissione di neoformazione'),
(803, 'Esame istocitopatologico apparato muscolo scheletrico: biopsia incisionale o punch'),
(804, 'Esame istocitopatologico apparato muscolo scheletrico: biopsia semplice ossea'),
(805, 'Esame istocitopatologico apparato respiratorio: agobiopsia pleurica'),
(806, 'Esame istocitopatologico apparato respiratorio: biopsia cavità nasali'),
(807, 'Esame istocitopatologico apparato respiratorio: biopsia endobronchiale (sede unica)'),
(808, 'Esame istocitopatologico apparato respiratorio: biopsia endobronchiale (sedi multiple)'),
(809, 'Esame istocitopatologico apparato respiratorio: biopsia laringea'),
(810, 'Esame istocitopatologico apparato respiratorio: biopsia semplice'),
(811, 'Esame istocitopatologico apparato respiratorio: biopsia vie aeree (sedi multiple)'),
(812, 'Esame istocitopatologico apparato respiratorio: polipectomia endoscopica'),
(813, 'Esame istocitopatologico apparato urinario per lesione diffusa: biopsia renale'),
(814, 'Esame istocitopatologico apparato urinario per lesione focale: biopsia renale'),
(815, 'Esame istocitopatologico apparato urinario: biopsia semplice'),
(816, 'Esame istocitopatologico apparato urinario: mapping da biopsia endoscopica vescicale'),
(817, 'Esame istocitopatologico apparato urogenitale femminile: biopsia cervicale e endometriale'),
(818, 'Esame istocitopatologico apparato urogenitale femminile: biopsia cervice uterina'),
(819, 'Esame istocitopatologico apparato urogenitale femminile: biopsia endometriale (VABRA)'),
(820, 'Esame istocitopatologico apparato urogenitale femminile: biopsia vaginale'),
(821, 'Esame istocitopatologico apparato urogenitale femminile: biopsia vulvare (sede unica)'),
(822, 'Esame istocitopatologico apparato urogenitale femminile: biopsia vulvare (sedi multiple)'),
(823, 'Esame istocitopatologico apparato urogenitale femminile: biopsie cervicali (sedi multiple)'),
(824, 'Esame istocitopatologico apparato urogenitale femminile: conizzazione della cervice'),
(825, 'Esame istocitopatologico apparato urogenitale femminile: polipectomia endocervicale'),
(826, 'Esame istocitopatologico apparato urogenitale femminile: raschiamento endometriale'),
(827, 'Esame istocitopatologico apparato urogenitale maschile: biopsia annessi testicolari'),
(828, 'Esame istocitopatologico apparato urogenitale maschile: biopsia pene'),
(829, 'Esame istocitopatologico apparato urogenitale maschile: biopsia testicolare'),
(830, 'Esame istocitopatologico apparato urogenitale: agobiopsia ovarica'),
(831, 'Esame istocitopatologico apparato urogenitale: biopsia endoscopica vescicale (sede unica)'),
(832, 'Esame istocitopatologico apparato urogenitale: biopsia endoscopica vescicale (sedi multiple)'),
(833, 'Esame istocitopatologico articolazioni. Biopsia semplice'),
(834, 'Esame istocitopatologico articolazioni: biopsia sinoviale, biopsia tendinea'),
(835, 'Esame istocitopatologico articolazioni: tessuto fibrotendineo'),
(836, 'Esame istocitopatologico bulbo oculare. Biopsia semplice'),
(837, 'Esame istocitopatologico cavo orale. Biopsia semplice'),
(838, 'Esame istocitopatologico cavo orale. Escissione di neoformazione'),
(839, 'Esame istocitopatologico cavo orale: biopsia multiple'),
(840, 'Esame istocitopatologico cute (shave o punch)'),
(841, 'Esame istocitopatologico cute e/o tessuti molli. Con biopsia o escissione di neoformazione'),
(842, 'Esame istocitopatologico cute e/o tessuti molli: biopsia incisionale'),
(843, 'Esame istocitopatologico cute e/o tessuti molli: biopsie multiple'),
(844, 'Esame istocitopatologico cute e/o tessuti molli: escissione neoformazioni multiple'),
(845, 'Esame istocitopatologico da agobiopsia di organo/tessuto superficiale o profondo'),
(846, 'Esame istocitopatologico della mammella: biopsia vacuum assisted'),
(847, 'Esame istocitopatologico di cute e/o tessuti molli. Escissione allargata di neoplasia maligna'),
(848, 'Esame istocitopatologico mammella: biopsia semplice'),
(849, 'Esame istocitopatologico mammella: biopsia stereotassica'),
(850, 'Esame istocitopatologico mammella: escissione di neoformazione'),
(851, 'Esame istocitopatologico mammella: nodulectomia'),
(852, 'Esame istocitopatologico naso e cavità nasali. Escissione di neoformazione'),
(853, 'Esame istocitopatologico orecchio. Biopsia semplice'),
(854, 'Esame istocitopatologico sierose. Biopsia semplice'),
(855, 'Esame istocitopatologico sierose: escissione di neoformazione'),
(856, 'Esame istocitopatologico sistema circolatorio'),
(857, 'Esame istocitopatologico sistema emopoietico: agobiopsia linfonodale'),
(858, 'Esame istocitopatologico sistema emopoietico: agobiopsia linfonodale (sedi multiple)'),
(859, 'Esame istocitopatologico sistema emopoietico: asportazione di linfonodo unico superficiale'),
(860, 'Esame istocitopatologico sistema emopoietico: biopsia osteo midollare'),
(861, 'Esame istocitopatologico sistema emopoietico: biopsia osteo-midollare (BOM)'),
(862, 'Esame istocitopatologico sistema endocrino: agobiopsia tiroidea'),
(863, 'Esame istocitopatologico sistema endocrino: biopsia semplice'),
(864, 'Esame istocitopatologico sistema linfoemopoietico: da agobiopsia linfonodale'),
(865, 'Esame istocitopatologico ultrastrutturale (SEM, TEM)'),
(866, 'Esame istocitopatologico. Biopsia semplice di organo/tessuto superficiale o profondo'),
(867, 'Esame microbiologico del secreto endocervicale / Tampone vaginale'),
(868, 'Esame microbiologico del secreto uretrale/urine primo mitto'),
(869, 'Esame microbiologico del secreto vaginale'),
(870, 'Esame microscopico del sangue periferico'),
(871, 'Esame microscopico di campione di ferita operatoria - coltura'),
(872, 'Esame microscopico di campione di ferita operatoria - coltura e sensibilità'),
(873, 'Esame microscopico di campione di ferita operatoria - gruppo di cellule e striscio di Papanicolaou'),
(874, 'Esame microscopico di campione di ferita operatoria - parassitologia'),
(875, 'Esame microscopico di campione di ferita operatoria - striscio batterico'),
(876, 'Esame microscopico di campione di ferita operatoria - tossicologia'),
(877, 'Esame microscopico di campione di sistema muscoloscheletrico e liquido articolare - coltura'),
(878, 'Esame microscopico di campione di sistema muscoloscheletrico e liquido articolare - coltura e sensibilità'),
(879, 'Esame microscopico di campione di sistema muscoloscheletrico e liquido articolare - gruppo di cellule e striscio di Papanicolaou'),
(880, 'Esame microscopico di campione di sistema muscoloscheletrico e liquido articolare - parassitologia'),
(881, 'Esame microscopico di campione di sistema muscoloscheletrico e liquido articolare - striscio batterico'),
(882, 'Esame microscopico di campione di sistema muscoloscheletrico e liquido articolare - tossicologia'),
(883, 'Esame microscopico di campione di vie genitali femminili, feto, sacco amniotico - tossicologia'),
(884, 'Esame microscopico di striscio o apposizione di citoaspirato linfoghiandolare'),
(885, 'Esame urodinamico invasivo'),
(886, 'Esame urodinamico non invasivo (uroflussometria)'),
(887, 'Esame urovideodinamico'),
(888, 'Escherichia coli nelle feci esame colturale'),
(889, 'Escherichia coli patogeni da coltura identificazione biochimica'),
(890, 'Escherichia coli patogeni da coltura identificazione sierologica'),
(891, 'Esofagogastroduodenoscopia [EGDS] con biopsia dell\'esofago'),
(892, 'Esoftalmometria'),
(893, 'Esplorazione digitale di stoma di enterostomia'),
(894, 'Esplorazione retroperitoneo'),
(895, 'Esplorazione rettale digitale'),
(896, 'Esteri organofosforici'),
(897, 'Estradiolo (E2)'),
(898, 'Estrazione di DNA o di RNA (nucleare o mitocondriale)'),
(899, 'Estriolo'),
(900, 'Estriolo non coniugato'),
(901, 'Estrone'),
(902, 'Etanolo'),
(903, 'Etosuccimide'),
(904, 'Faringografia'),
(905, 'Farmaci anti epilettici'),
(906, 'Farmaci antiaritmici'),
(907, 'Farmaci antiinfiammatori'),
(908, 'Farmaci antitumorali'),
(909, 'Farmaci con metodi cromatografici'),
(910, 'Farmaci con tecniche non cromatografiche'),
(911, 'Farmaci digitalici'),
(912, 'Farmaci dosaggio quantitativo in cromatografia'),
(913, 'Fattore natriuretico atriale (ANP)'),
(914, 'Fattore reumatoide'),
(915, 'Fattore von willebrand'),
(916, 'Fattore vwf'),
(917, 'Fattore vwf antigene (EIA)'),
(918, 'Fattore vwf cba (EIA)'),
(919, 'Fattori della coagulazione (ii, v, vii, viii, ix, x, xi, xii, xiii)'),
(920, 'Feci esame chimico e microscopico (grassi, prodotto di digestione, parassiti)'),
(921, 'Feci sangue occulto'),
(922, 'Fenilalanina'),
(923, 'Fenitoina'),
(924, 'Fenolo [u]'),
(925, 'Fenotipo rh'),
(926, 'Ferritina'),
(927, 'Ferro'),
(928, 'Ferro [du]'),
(929, 'Fibrina/Fibrinogeno'),
(930, 'Fibrinogeno clauss'),
(931, 'Fibrinogeno funzionale'),
(932, 'Fibrinogeno immunologico'),
(933, 'Fish bcl6'),
(934, 'Fistolografia'),
(935, 'Fistolografia dell\'arto inferiore'),
(936, 'Fistolografia dell\'arto superiore'),
(937, 'Fistulografia retroperitoneale'),
(938, 'Flebografia a impedenza'),
(939, 'Flebografia con mezzo di contrasto del sistema portale'),
(940, 'Flebografia con mezzo di contrasto delle vene del capo e del collo'),
(941, 'Flebografia con mezzo di contrasto delle vene polmonari'),
(942, 'Flebografia degli arti inferiori bilaterale'),
(943, 'Flebografia degli arti inferiori monolaterale'),
(944, 'Flebografia giugulare'),
(945, 'Flebografia iliaca'),
(946, 'Flebografia monolaterale dell\'arto superiore'),
(947, 'Flebografia orbitaria'),
(948, 'Flebografia renale'),
(949, 'Flebografia spermatica'),
(950, 'Flebografia spinale'),
(951, 'Fluoro'),
(952, 'Flussimetria (Ecocolordoppler fetoplacentare)'),
(953, 'Folato'),
(954, 'Follitropina (FSH)'),
(955, 'Fondo oculare (Esame del fundus oculi)'),
(956, 'Fonocardiogramma con elettrodo ECG'),
(957, 'Fosfatasi acida'),
(958, 'Fosfatasi alcalina'),
(959, 'Fosfatasi alcalina isoenzima osseo'),
(960, 'Fosfatasi prostatica'),
(961, 'Fosfato inorganico (fosforo)'),
(962, 'Fosfoesosoisomerasi (PHI)'),
(963, 'Fosforo'),
(964, 'Fotografia del fondo oculare (fundus oculi)'),
(965, 'Fotografia del segmento anteriore'),
(966, 'Fotopletismografia degli arti superiori o inferiori'),
(967, 'Fotopletismografia di altri distretti'),
(968, 'Fragilità del cromosoma X (fraxa). Test di primo livello'),
(969, 'Fragilità del cromosoma X (fraxa). Test di secondo livello'),
(970, 'Francisella tularensis [tularemia] anticorpi'),
(971, 'Fruttosamina (proteine glicate) [s]'),
(972, 'Fruttosio [ls]'),
(973, 'Funicolocentesi per la diagnosi prenatale di patologie del feto'),
(974, 'Gait analysis: valutazione clinica della menomazione degli arti inferiori, incluso EMG dinamica del cammino'),
(975, 'Galattografia [duttografia, duttogalattografia]'),
(976, 'Galattosio (prova da carico)'),
(977, 'Galattosio 1-fosfato uridil transferasi (GALT)'),
(978, 'Galattosio [s/u]'),
(979, 'Galattosio-1-Fosfato. Dosaggio'),
(980, 'Gamma-Glutamiltransferasi (Gamma GT)'),
(981, 'Gastrina');
INSERT INTO `Esami` (`id`, `nome`) VALUES
(982, 'Gastroscopia attraverso stoma artificiale'),
(983, 'Gastroscopia transaddominale'),
(984, 'Giardia antigene nelle feci ricerca diretta (I.F.)'),
(985, 'Giardia antigeni ricerca diretta nelle feci'),
(986, 'Glicoproteina ricca in istidina'),
(987, 'Globulina da trasporto ormoni sessuali (SHBG)'),
(988, 'Globulina legante la tiroxina (tbg)'),
(989, 'Glucagone'),
(990, 'Glucosio'),
(991, 'Glucosio (Curva glicemica da carico 3 determinazioni)'),
(992, 'Glucosio (Curva glicemica da carico 6 determinazioni)'),
(993, 'Glucosio 6 fosfato deidrogenasi (G6PDH)'),
(994, 'Glutammato deidrogenasi'),
(995, 'Gonadotropina corionica (prova immunologica di gravidanza)'),
(996, 'Gonadotropina corionica (subunità beta, frazione libera)'),
(997, 'Gonadotropina corionica (subunità beta, molecola intera)'),
(998, 'Gruppo sanguigno ABO (agglutinogeni e agglutinine) e RH (D)'),
(999, 'Gruppo sanguigno abo/rh ii controllo'),
(1000, 'Gustometria [olfattometria]'),
(1001, 'Hb - biosintesi in vitro'),
(1002, 'Hb - emoglobina A2'),
(1003, 'Hb - emoglobina [Sg/La]'),
(1004, 'Hb - emoglobina fetale (dosaggio)'),
(1005, 'Hb - emoglobina plasmatica libera'),
(1006, 'Hb - emoglobina. Dosaggio frazioni (HbA2, HbF, Hb anomale)'),
(1007, 'Hb - emoglobine anomale (HbS, HbD, HbH, ecc.)'),
(1008, 'Hb - isoelettrofocalizzazione'),
(1009, 'Hb - ricerca mutazioni delle catene globiniche (cromatografia)'),
(1010, 'Hb - test di stabilità [(Sg)Er]'),
(1011, 'Hb-Emoglobina glicata'),
(1012, 'HcG frazione libera e Papp-a'),
(1013, 'Head up tilt test'),
(1014, 'Helicobacter pylori anticorpi (E.I.A.)'),
(1015, 'Helicobacter pylori antigene nelle feci ricerca diretta'),
(1016, 'Helicobacter pylori in materiali biologici esame colturale'),
(1017, 'Helicobacter pylori ureasi nel materiale bioptico (saggio mediante prova biochimica)'),
(1018, 'INV(16). Test qualitativo'),
(1019, 'INV(16). Test quantitativo'),
(1020, 'Ibridazione con sonda molecolare'),
(1021, 'Ibridazione genomica comparativa su Array (Array - CGH)'),
(1022, 'Ibridazione genomica comparativa su microarray. Incluso: estrazione DNA, CGH-Array, SNPS-Array, coltura del materiale biologico da analizzare'),
(1023, 'Ibridazione in situ (FISH) su metafasi, nuclei interfasici, tessuti - mediante sequenze genomiche in YAC'),
(1024, 'Ibridazione in situ (FISH) su metafasi, nuclei interfasici, tessuti - mediante sonde molecolari'),
(1025, 'Identificazione di specificità anti HLA contro pannello linfocitario-(1 siero/30 cellule a antigenicità nota)'),
(1026, 'Identificazione in fase solida anticorpi anti-HLA fissanti il complemento classe I'),
(1027, 'Identificazione in fase solida anticorpi anti-HLA fissanti il complemento classe II'),
(1028, 'Identificazione in fase solida specificità anti-HLA classe I. Isotipo IgG'),
(1029, 'Identificazione in fase solida specificità anti-HLA classe I. Isotipo IgM'),
(1030, 'Identificazione in fase solida specificità anti-HLA classe II. Isotipo IgG'),
(1031, 'Identificazione in fase solida specificità anti-HLA classe II. Isotipo IgM'),
(1032, 'Idrossiprolina [u]'),
(1033, 'IgA secretorie'),
(1034, 'IgE specifiche allergologiche quantitativo'),
(1035, 'IgE specifiche allergologiche quantitativo per farmaci e veleni. Fino a 12 allergeni'),
(1036, 'IgE specifiche allergologiche quantitativo per inalanti e alimenti. Fino a 12 allergeni'),
(1037, 'IgE specifiche allergologiche: screening multiallergenico qualitativo'),
(1038, 'IgE specifiche pannello per alimenti'),
(1039, 'IgE specifiche pannello per inalanti'),
(1040, 'IgE specifiche per allergeni singoli ricombinanti molecolari'),
(1041, 'IgE totali'),
(1042, 'IgG specifiche allergologiche'),
(1043, 'Igg, IgA sottoclassi'),
(1044, 'Ileocolonscopia per via retrograda con pallone [BAE]'),
(1045, 'Ileocolonscopia per via retrograda con pallone [BAE] con biopsia'),
(1046, 'Imaging intravascolare dei vasi cerebrali extracranici'),
(1047, 'Imaging intravascolare dei vasi coronarici'),
(1048, 'Imaging intravascolare dei vasi intratoracici'),
(1049, 'Imaging intravascolare dei vasi periferici'),
(1050, 'Imaging intravascolare dei vasi renali'),
(1051, 'Imipramina'),
(1052, 'Immunocomplessi circolanti'),
(1053, 'Immunofenotipizzazione leuco/linfocitaria'),
(1054, 'Immunofissazione'),
(1055, 'Immunoglobuline IgA, IgD, IgG o IgM'),
(1056, 'Immunoglobuline di superficie linfocitarie'),
(1057, 'Immunoglobuline: catene k e lambda libere'),
(1058, 'Immunoglobuline: catene kappa e lambda'),
(1059, 'Impedenziometria esofagea (24 ore)'),
(1060, 'Impedenzometria'),
(1061, 'Individuazione del volume bersaglio e organi critici'),
(1062, 'Individuazione del volume bersaglio e simulazione con PET-TC'),
(1063, 'Inibina b'),
(1064, 'Inibitore attivatore del plasminogeno (pai i)'),
(1065, 'Inibitori dei fattori della coagulazione'),
(1066, 'Instabilità microsatellitare'),
(1067, 'Insulin growth factor [IGF-1 o somatomedina C]'),
(1068, 'Insulin growth factor binding protein 3 [IGF-BP3]'),
(1069, 'Insulina'),
(1070, 'Insulina (curva da carico o dopo test farmacologici, max. 5)'),
(1071, 'Interferone'),
(1072, 'Interleuchina 2'),
(1073, 'Intradermoreazione con ppd (intradermoreazione con tubercolina secondo mantoux)'),
(1074, 'Ioduria'),
(1075, 'Isolelettrofocusing della transferrina sierica'),
(1076, 'Isterografia percutanea'),
(1077, 'Isterosalpingografia'),
(1078, 'Isterosalpingografia con contrasto gassoso'),
(1079, 'Isterosalpingosonografia'),
(1080, 'Isteroscopia'),
(1081, 'Isteroscopia diagnostica con o senza biopsia dell\'endometrio'),
(1082, 'Isterosonografia [idrosonografia]'),
(1083, 'Laringografia con contrasto'),
(1084, 'Laringoscopia'),
(1085, 'Laringostroboscopia'),
(1086, 'Lattato deidrogenasi (LDH)'),
(1087, 'Latte muliebre'),
(1088, 'Lattosio [u/ls]'),
(1089, 'Legionella pneumophila antigene urinario ricerca diretta (eia o immunocromatografico)'),
(1090, 'Legionelle anticorpi (E.I.A.)'),
(1091, 'Legionelle anticorpi (titolazione mediante I.F.)'),
(1092, 'Legionelle antigene nelle urine'),
(1093, 'Legionelle in materiali biologici esame colturale'),
(1094, 'Leishmania acidi nucleici'),
(1095, 'Leishmania anticorpi'),
(1096, 'Leishmania esame colturale'),
(1097, 'Leishmania ricerca microscopica previa colorazione specifica'),
(1098, 'Leptospire anticorpi'),
(1099, 'Leptospire esame colturale'),
(1100, 'Leucin amino peptidasi (lap) [s]'),
(1101, 'Leucociti (conteggio e formula leucocitaria microscopica) [Sg]'),
(1102, 'Leucociti (conteggio) [(Sg)]'),
(1103, 'Levodopa'),
(1104, 'Linfangiografia intratoracica'),
(1105, 'Linfografia addominale'),
(1106, 'Linfografia arto inferiore'),
(1107, 'Linfografia arto superiore'),
(1108, 'Linfografia cervicale'),
(1109, 'Lipasi pancreatica'),
(1110, 'Lipoproteina (A)'),
(1111, 'Liquidi da versamenti esame chimico fisico e microscopico'),
(1112, 'Liquido amniotico enzimi (cellobiasi, lattasi, maltasi, palatinasi, saccarasi, trealasi, acetil-colinesterasi)'),
(1113, 'Liquido amniotico fosfolipidi (cromatografia)'),
(1114, 'Liquido amniotico rapporto lecitina/sfingomielina'),
(1115, 'Liquido amniotico test alla schiuma di clements'),
(1116, 'Liquido seminale [spermiogramma]. Esame macroscopico e microscopico'),
(1117, 'Liquido seminale [spermiogramma]. Test di vitalità previa colorazione con eosina'),
(1118, 'Liquido seminale esame morfologico e indice di fertilità'),
(1119, 'Liquido seminale profilo biochimico (fruttosio, carnitina, fosfatasi prostatica o zinco o acido citrico, alfa glucosidasi o maltasi)'),
(1120, 'Liquido seminale prove di valutazione della fertilità'),
(1121, 'Liquido sinoviale esame chimico fisico e microscopico'),
(1122, 'Lisozima/S'),
(1123, 'Listeria in materiali biologici esame colturale'),
(1124, 'Listeria monocytogenes anticorpi (titolazione mediante agglutinazione)'),
(1125, 'Litio'),
(1126, 'Localizzazione radiologica di corpo estraneo'),
(1127, 'Luteotropina (LH) e follitropina (FSH): dosaggi seriati dopo GNRH o altro stimolo (da 3 a 6)'),
(1128, 'Luteotropina (lh) [s/u]'),
(1129, 'MOC (densitometria ossea) a ultrasuoni'),
(1130, 'MOC (densitometria ossea) con TC'),
(1131, 'MOC (densitometria ossea) con tecnica di assorbimento a fotone'),
(1132, 'MOC (densitometria ossea). DXA lombare, femorale, ultradistale'),
(1133, 'MOC (densitometria ossea). DXA total body'),
(1134, 'Macroprolattina'),
(1135, 'Magnesio totale'),
(1136, 'Mammografia bilaterale'),
(1137, 'Mammografia monolaterale'),
(1138, 'Manganese [s]'),
(1139, 'Manometria ano-rettale'),
(1140, 'Manometria del colon'),
(1141, 'Manometria esofagea'),
(1142, 'Manometria urinaria'),
(1143, 'Mappatura dei nevi (nei) - Osservazione delle lesioni cutanee o annessi cutanei con videodermatoscopio'),
(1144, 'Marcatura di lesione del tubo digerente'),
(1145, 'Massaggio cardiaco a torace chiuso'),
(1146, 'Meprobamato'),
(1147, 'Mercurio'),
(1148, 'Messa a punto della dentatura'),
(1149, 'Metaemoglobina [(Sg)er]'),
(1150, 'Metanefrine frazionate'),
(1151, 'Metilazione promotore MGMT'),
(1152, 'Miceti [lieviti] antimicogramma da coltura (M.I.C., fino a 5 antimicotici)'),
(1153, 'Miceti [lieviti] identificazione biochimica'),
(1154, 'Miceti anticorpi (D.I.D.)'),
(1155, 'Miceti anticorpi (titolazione mediante F.C.)'),
(1156, 'Miceti anticorpi (titolazione mediante agglutinazione)'),
(1157, 'Miceti antigeni (metodi immunologici)'),
(1158, 'Miceti antigeni cellulari ed extracellulari in materiali biologici diversi'),
(1159, 'Miceti da coltura identificazione microscopica (osservazione morfologica)'),
(1160, 'Miceti da coltura identificazione sierologica'),
(1161, 'Miceti ricerca acidi nucleici in materiali biologici ricerca qualitativa/quantitativa'),
(1162, 'Miceti ricerca in materiali biologici diversi. Incluso: esame microscopico ed esame colturale'),
(1163, 'Micobatteri antibiogramma da coltura (Metodo tradizionale, almeno 3 antibiotici)'),
(1164, 'Micobatteri antibiogramma da coltura in terreno liquido'),
(1165, 'Micobatteri da coltura identificazione (Biochimica e metodo radiometrico)'),
(1166, 'Micobatteri da coltura identificazione di specie'),
(1167, 'Micobatteri da coltura identificazione mediante ibridazione (previa reazione polimerasica a catena)'),
(1168, 'Micobatteri diagnosi immunologica di infezione tubercolare latente [IGRA]'),
(1169, 'Micobatteri in campioni biologici diversi esame colturale (Metodo radiometrico)'),
(1170, 'Micobatteri in campioni biologici diversi esame colturale (Metodo tradizionale)'),
(1171, 'Micobatteri in campioni biologici ricerca microscopica (Ziehl-Neelsen, Kinyiun)'),
(1172, 'Micobatteri ricerca acidi nucleici di m. tuberculosis complex in materiali biologici'),
(1173, 'Micobatteri ricerca in campioni biologici vari'),
(1174, 'Micoplasma pneumoniae anticorpi (E.I.A.)'),
(1175, 'Micoplasma pneumoniae anticorpi (titolazione mediante I.F.)'),
(1176, 'Micoplasma pneumoniae da coltura identificazione biochimica'),
(1177, 'Micoplasma pneumoniae da coltura identificazione sierologica'),
(1178, 'Micoplasma pneumoniae in materiali biologici diversi esame colturale'),
(1179, 'Micoplasmi urogenitali esame colturale'),
(1180, 'Microbatteri anticorpi (E.I.A.)'),
(1181, 'Microdelezione del cromosoma Y'),
(1182, 'Microfilarie [W. Bancrofti] antigeni ricerca diretta'),
(1183, 'Microfilarie nel sangue (Giemsa) dopo concentrazione o arricchimento'),
(1184, 'Microsporidi esame colturale su linee cellulari'),
(1185, 'Microsporidi esame microscopico dopo concentrazione (colorazioni specifiche)'),
(1186, 'Mielografia con contrasto'),
(1187, 'Mioglobina'),
(1188, 'Misura della perdita transepidermica di acqua (tewl)'),
(1189, 'Misura ossido nitrico esalato'),
(1190, 'Misura transcutanea della PO2 e della PCO2 con utilizzo di membrane monouso'),
(1191, 'Misurazione di pressione dello sfintere di oddi'),
(1192, 'Misurazione indice pressorio caviglia/braccio (ABI) [indice di Winsor]'),
(1193, 'Misure protesiche in situ'),
(1194, 'Monitoraggio cardiorespiratorio notturno completo'),
(1195, 'Monitoraggio continuo [24 ore] della pressione arteriosa - Holter pressorio'),
(1196, 'Monitoraggio del flusso ematico coronarico'),
(1197, 'Monitoraggio della gettata cardiaca mediante la tecnica del consumo di ossigeno (metodo di Fick)'),
(1198, 'Monitoraggio della pressione arteriosa sistemica'),
(1199, 'Monitoraggio della pressione dell\'arteria polmonare'),
(1200, 'Monitoraggio della pressione venosa centrale'),
(1201, 'Monitoraggio dinamico della glicemia (Holter glicemico)'),
(1202, 'Monitoraggio doppler transcranico per il microembolismo [Mesh]'),
(1203, 'Monitoraggio ecografico del ciclo ovulatorio / Ecografia ovarica'),
(1204, 'Monitoraggio elettrocardiografico'),
(1205, 'Monitoraggio incruento della saturazione arteriosa / pulsossimetria'),
(1206, 'Monitoraggio transcutaneo di O2 e CO2'),
(1207, 'Monomeri solubili di fibrina (fs test)'),
(1208, 'Mucopolisaccaridi'),
(1209, 'Mucopolisaccaridi urinari test di screening'),
(1210, 'Mucopolisaccaridi urinari totali. Dosaggio'),
(1211, 'Mutazione di BRAF'),
(1212, 'Mutazione di C-KIT'),
(1213, 'Mutazione di EGFR'),
(1214, 'Mutazione di KRAS'),
(1215, 'Mutazione di PDGFRA'),
(1216, 'Mutazione di PIK3CA'),
(1217, 'Mutazione jak2 v617f test qualitativo'),
(1218, 'Mutazione jak2 v617f test quantitativo'),
(1219, 'Mutazioni CEBPA'),
(1220, 'Mutazioni FLT-3 (D385)'),
(1221, 'Mutazioni FLT-3 (ITD)'),
(1222, 'Mutazioni del fattore II'),
(1223, 'Mutazioni del fattore V Leiden'),
(1224, 'Mutazioni della alfa-talassemia'),
(1225, 'Mutazioni della beta-talassemia'),
(1226, 'Mutazioni della connessina 26 in familiari. Test mirato'),
(1227, 'Mutazioni della connessina 26. Test completo'),
(1228, 'Mutazioni della connessina 30 in familiari. Test mirato'),
(1229, 'Mutazioni della connessina 30. Test completo'),
(1230, 'Mutazioni della emocromatosi'),
(1231, 'Mutazioni della fibrosi cistica in familiari. Test mirato'),
(1232, 'Mutazioni della fibrosi cistica. Test di secondo livello'),
(1233, 'Mutazioni della fibrosi cistica. test di primo livello'),
(1234, 'Mutazioni di BRCA1 in familiari. Test mirato'),
(1235, 'Mutazioni di BRCA1. Test completo'),
(1236, 'Mutazioni di BRCA2 in familiari. Test mirato'),
(1237, 'Mutazioni di BRCA2. Test completo'),
(1238, 'Mutazioni di MTHFR'),
(1239, 'Mutazioni gene IgHV'),
(1240, 'Mutazioni idh1-2'),
(1241, 'Mutazioni nucleofosmina test qualitativo'),
(1242, 'Mutazioni nucleofosmina test quantitativo'),
(1243, 'Mycoplasma pneumoniae anticorpi IgG e IgM. Incluso: IgA se IgM negative'),
(1244, 'Mycoplasma/Ureaplasma urogenitali esame colturale'),
(1245, 'N-Acetilaspartato'),
(1246, 'N-Acetilglucosaminidasi'),
(1247, 'Nefa (acidi grassi non esterificati)'),
(1248, 'Neisseria gonorrhoeae esame colturale'),
(1249, 'Neisseria meningitidis esame colturale'),
(1250, 'Neisseriae identificazione biochimica'),
(1251, 'Neisseriae identificazione sierologica'),
(1252, 'Neopterina'),
(1253, 'Neurotrasmettitori. Dosaggio liquor'),
(1254, 'Nichel'),
(1255, 'Nortriptilina'),
(1256, 'Oligoelementi. Dosaggio per ciascun oligoelemento'),
(1257, 'Oligosaccaridi urinari'),
(1258, 'Omocisteina'),
(1259, 'Ormone anti-mulleriano (AMH). Dosaggio'),
(1260, 'Ormone lattogeno placentare o somatomammotropina (hpl) [s]'),
(1261, 'Ormone somatotropo (GH)'),
(1262, 'Ormoni: dosaggi seriati dopo stimolo (17 OH-P, FSH, LH, TSH, ACTH, cortisolo, GH, aldosterone, PRL, renina o altri ormoni)'),
(1263, 'Ortopanoramica delle arcate dentarie'),
(1264, 'Osmolalità'),
(1265, 'Osmolalità/Osmolarità. Determinazione diretta'),
(1266, 'Ossalati [U]'),
(1267, 'Osservazione dermatologica in epidiascopia'),
(1268, 'Osservazione dermatologica in epiluminescenza'),
(1269, 'Osteocalcina (BGP)'),
(1270, 'Otomicroscopia'),
(1271, 'PAP test - screening per il tumore dell\'utero - esame citologico cervico vaginale'),
(1272, 'PH ematico'),
(1273, 'PH-Metria esofagea (24 ore)'),
(1274, 'PH-Metria telemetrica'),
(1275, 'Pachimetria corneale'),
(1276, 'Palpazione del seno'),
(1277, 'Pannello di immunofenotipizzazione di fattori prognostici e predittivi per melanoma'),
(1278, 'Pannello di immunofenotipizzazione di fattori prognostici e predittivi per patologia tumorale maligna del polmone'),
(1279, 'Pannello di immunofenotipizzazione di fattori prognostici e predittivi per patologia tumorale maligna del sistema nervoso centrale'),
(1280, 'Pannello di immunofenotipizzazione di fattori prognostici e predittivi per patologia tumorale maligna dell\'apparato gastroenterico'),
(1281, 'Pannello di immunofenotipizzazione di fattori prognostici e predittivi per patologia tumorale maligna della mammella'),
(1282, 'Parassiti [elminti, protozoi] acidi nucleici in materiali biologici. Ricerca qualitativa/quantitativa'),
(1283, 'Parassiti [elminti, protozoi] anticorpi'),
(1284, 'Parassiti [elminti, protozoi] nel sangue esame microscopico (Giemsa)'),
(1285, 'Parassiti in materiali biologici, ricerca macro e microscopica'),
(1286, 'Parassiti intestinali [elminti, protozoi] ricerca macro e microscopica'),
(1287, 'Parassiti intestinali [elminti, protozoi] ricerca macro e microscopica (esame diretto e dopo concentrazione o arricchimento)'),
(1288, 'Parassiti intestinali [protozoi] esame colturale (coltura xenica)'),
(1289, 'Parassiti intestinali ricerca microscopica'),
(1290, 'Paratormone (PTH). Molecola intatta'),
(1291, 'Paratormone related peptide [S]'),
(1292, 'Pelvimetria'),
(1293, 'Pepsinogeno A (I)'),
(1294, 'Pepsinogeno C (II)'),
(1295, 'Peptide natriuretico tipo B (BNP o N-probnp)'),
(1296, 'Piastrine (conteggio) [(Sg)]'),
(1297, 'Pielografia percutanea'),
(1298, 'Pielografia retrograda'),
(1299, 'Pielografia transpielostomica'),
(1300, 'Pink test'),
(1301, 'Piombo'),
(1302, 'Piridinolina'),
(1303, 'Piruvatochinasi (PK)'),
(1304, 'Plasminogeno'),
(1305, 'Plasmodi della malaria nel sangue ricerca diretta antigeni'),
(1306, 'Plasmodi della malaria nel sangue ricerca microscopica (Giemsa) / Goccia spessa'),
(1307, 'Plasmodi della malaria nel sangue ricerca microscopica (striscio sottile e goccia spessa previa colorazione specifica) e ricerca diretta antigeni (metodi immunologici)'),
(1308, 'Plasmodio falciparum anticorpi (titolazione mediante I.F.)'),
(1309, 'Pletismografia ad occlusione venosa degli arti superiori o inferiori'),
(1310, 'Pletismografia di altri distretti'),
(1311, 'Pletismografia di un arto'),
(1312, 'Pletismografia peniena'),
(1313, 'Pletismogramma'),
(1314, 'Pneumocistigrafia mammaria'),
(1315, 'Pneumocistis carinii nel broncolavaggio'),
(1316, 'Pneumocystis jirovecii in secrezioni respiratorie acidi nucleici'),
(1317, 'Pneumocystis jirovecii in secrezioni respiratorie esame microscopico e/o ricerca diretta'),
(1318, 'Pneumografia retroperitoneale'),
(1319, 'Pneumomediastino'),
(1320, 'Poligrafia con videoregistrazione'),
(1321, 'Poligrafia dinamica'),
(1322, 'Polipeptide intestinale vasoattivo (VIP)'),
(1323, 'Polipeptide specifico tissutale (pps)'),
(1324, 'Polisonnografia diurna o notturna'),
(1325, 'Porfirine totali e frazionate'),
(1326, 'Porfobilinogeno'),
(1327, 'Post coital test'),
(1328, 'Potassio'),
(1329, 'Potenziali evocati acustici'),
(1330, 'Potenziali evocati da stimolo laser (LEP)'),
(1331, 'Potenziali evocati motori. Arto superiore o inferiore'),
(1332, 'Potenziali evocati somato sensoriali. Per nervo o dermatomero'),
(1333, 'Potenziali evocati stimolo ed evento correlati'),
(1334, 'Potenziali evocati uditivi da stimolo elettrico'),
(1335, 'Potenziali evocati vestibolari (VEMPS)'),
(1336, 'Potenziali evocati visivi (VEP)'),
(1337, 'Prealbumina'),
(1338, 'Prelievo citologico'),
(1339, 'Prelievo dei villi coriali - villocentesi'),
(1340, 'Prelievo di sangue arterioso'),
(1341, 'Prelievo di sangue capillare'),
(1342, 'Prelievo di sangue venoso'),
(1343, 'Prelievo microbiologico'),
(1344, 'Prick by prick con allergeni freschi'),
(1345, 'Primidone'),
(1346, 'Proctorettosigmoidoscopia con endoscopio rigido'),
(1347, 'Proctorettosigmoidoscopia con endoscopio rigido con biopsia'),
(1348, 'Proctosigmoidoscopia attraverso orifizio artificiale'),
(1349, 'Profilo acilcarnitine plasmatiche con ms/ms'),
(1350, 'Progesterone'),
(1351, 'Prolattina (PRL)'),
(1352, 'Prolattina (PRL): Dosaggi seriati dopo TRH (5)'),
(1353, 'Propeptide ammino-terminale del procollagene tipo 1 (P1NP)'),
(1354, 'Proteina 4 dell\'epididimo umano (HE4). Dosaggio'),
(1355, 'Proteina C anticoagulante antigene [P]'),
(1356, 'Proteina C anticoagulante funzionale [P]'),
(1357, 'Proteina C reattiva (quantitativa)'),
(1358, 'Proteina cationica eosinofila (ECP)'),
(1359, 'Proteina legante il retinolo'),
(1360, 'Proteina s 100'),
(1361, 'Proteina s libera'),
(1362, 'Proteina s totale'),
(1363, 'Proteine plasmatiche (elettroforesi delle)'),
(1364, 'Proteine totali'),
(1365, 'Proteine urinarie (elettroforesi)'),
(1366, 'Protoporfirina ix eritrocitaria'),
(1367, 'Protozoi enterici ricerca acidi nucleici multiplex'),
(1368, 'Protozoi enterici ricerca diretta multipla antigeni fecali'),
(1369, 'Protozoi in materiali biologici diversi esame colturale'),
(1370, 'Protrombina frammenti 1, 2'),
(1371, 'Prova crociata di compatibilità trasfusionale'),
(1372, 'Prova crociata piastrinica'),
(1373, 'Prova di compatibilità molecolare pre-trapianto (reazione polimerasica a catena-fingerprint)'),
(1374, 'Pterine plasmatiche e urinarie'),
(1375, 'Pulsossimetria notturna'),
(1376, 'Pupillometria'),
(1377, 'Purine e loro metaboliti'),
(1378, 'RM (risonanza magnetica) addome inferiore con studio dinamico del pavimento pelvico'),
(1379, 'RM (risonanza magnetica) addome inferiore e zona pelvica'),
(1380, 'RM (risonanza magnetica) addome inferiore e zona pelvica, senza e con contrasto'),
(1381, 'RM (risonanza magnetica) cervello e tronco encefalico'),
(1382, 'RM (risonanza magnetica) colonna vertebrale'),
(1383, 'RM (risonanza magnetica) del bacino'),
(1384, 'RM (risonanza magnetica) del braccio'),
(1385, 'RM (risonanza magnetica) del collo'),
(1386, 'RM (risonanza magnetica) del ginocchio'),
(1387, 'RM (risonanza magnetica) del gomito'),
(1388, 'RM (risonanza magnetica) del massiccio facciale'),
(1389, 'RM (risonanza magnetica) del massiccio facciale complessivo'),
(1390, 'RM (risonanza magnetica) del piede'),
(1391, 'RM (risonanza magnetica) del polso'),
(1392, 'RM (risonanza magnetica) del rachide cervicale'),
(1393, 'RM (risonanza magnetica) del rachide dorsale'),
(1394, 'RM (risonanza magnetica) del rachide lombosacrale'),
(1395, 'RM (risonanza magnetica) del rachide sacrococcigeo'),
(1396, 'RM (risonanza magnetica) del torace'),
(1397, 'RM (risonanza magnetica) dell\'addome superiore'),
(1398, 'RM (risonanza magnetica) dell\'articolazione coxofemorale mono e/o bilaterale'),
(1399, 'RM (risonanza magnetica) dell\'articolazione temporomandibolare monolaterale e/o bilaterale'),
(1400, 'RM (risonanza magnetica) dell\'avambraccio'),
(1401, 'RM (risonanza magnetica) della caviglia'),
(1402, 'RM (risonanza magnetica) della colonna, senza e con contrasto'),
(1403, 'RM (risonanza magnetica) della coscia [RM del femore]'),
(1404, 'RM (risonanza magnetica) della gamba'),
(1405, 'RM (risonanza magnetica) della mammella bilaterale'),
(1406, 'RM (risonanza magnetica) della mammella monolaterale'),
(1407, 'RM (risonanza magnetica) della mammella, senza e con contrasto, bilaterale'),
(1408, 'RM (risonanza magnetica) della mammella, senza e con contrasto, monolaterale'),
(1409, 'RM (risonanza magnetica) della mano'),
(1410, 'RM (risonanza magnetica) della sella turcica'),
(1411, 'RM (risonanza magnetica) della spalla'),
(1412, 'RM (risonanza magnetica) delle orbite'),
(1413, 'RM (risonanza magnetica) delle rocche petrose. Incluso: relativo distretto vascolare'),
(1414, 'RM (risonanza magnetica) delle vie digestive'),
(1415, 'RM (risonanza magnetica) di inguine, scroto e/o pene'),
(1416, 'RM (risonanza magnetica) diffusione'),
(1417, 'RM (risonanza magnetica) endocavitaria'),
(1418, 'RM (risonanza magnetica) fetale'),
(1419, 'RM (risonanza magnetica) flussimetria liquorale quantitativa'),
(1420, 'RM (risonanza magnetica) perfusione'),
(1421, 'RM (risonanza magnetica) spettroscopia'),
(1422, 'RM (risonanza magnetica) studi funzionali attivazione corticale'),
(1423, 'RM (risonanza magnetica) urografia'),
(1424, 'RMN (risonanza magnetica nucleare) di pelvi, prostata e vescica'),
(1425, 'Radiografia articolazione temporomandibolare monolaterale'),
(1426, 'Radiografia assiale della rotula'),
(1427, 'Radiografia completa degli arti inferiori e del bacino sotto carico'),
(1428, 'Radiografia completa del lattante'),
(1429, 'Radiografia completa del tubo digerente con mezzo di contrasto'),
(1430, 'Radiografia completa della colonna e del bacino sotto carico'),
(1431, 'Radiografia completa delle arcate dentarie'),
(1432, 'Radiografia con occlusale delle arcate dentarie'),
(1433, 'Radiografia degli arti inferiori (femore ginocchio e gamba)'),
(1434, 'Radiografia dei tessuti molli della faccia, del capo e del collo'),
(1435, 'Radiografia del braccio'),
(1436, 'Radiografia del gomito'),
(1437, 'Radiografia del gomito e dell\'avambraccio'),
(1438, 'Radiografia del pancreas con contrasto'),
(1439, 'Radiografia del piede'),
(1440, 'Radiografia del piede e della caviglia'),
(1441, 'Radiografia del polso'),
(1442, 'Radiografia del polso e della mano'),
(1443, 'Radiografia del torace'),
(1444, 'Radiografia del tratto faringo-crico-esofageo-cardiale'),
(1445, 'Radiografia del tratto gastrointestinale superiore con mezzo di contrasto'),
(1446, 'Radiografia dell\'addome'),
(1447, 'Radiografia dell\'apparato urinario'),
(1448, 'Radiografia dell\'arto superiore, senza altra indicazione'),
(1449, 'Radiografia dell\'avambraccio'),
(1450, 'Radiografia dell\'esofago con mezzo di contrasto'),
(1451, 'Radiografia dell\'esofago, stomaco e duodeno con doppio mezzo di contrasto'),
(1452, 'Radiografia dell\'utero gravido'),
(1453, 'Radiografia della caviglia'),
(1454, 'Radiografia della clavicola'),
(1455, 'Radiografia della colonna cervicale'),
(1456, 'Radiografia della colonna lombosacrale'),
(1457, 'Radiografia della colonna toracica (dorsale)'),
(1458, 'Radiografia della mano'),
(1459, 'Radiografia della pelvi con contrasto gassoso'),
(1460, 'Radiografia della pelvi con contrasto opaco'),
(1461, 'Radiografia della spalla'),
(1462, 'Radiografia della spalla e degli arti superiori'),
(1463, 'Radiografia della trachea'),
(1464, 'Radiografia dello scheletro in toto'),
(1465, 'Radiografia dello sterno'),
(1466, 'Radiografia dello stretto toracico superiore-studio dell\'articolazione sternoclaveare'),
(1467, 'Radiografia di arcata dentaria'),
(1468, 'Radiografia di bacino e anca'),
(1469, 'Radiografia di coste, sterno e clavicola'),
(1470, 'Radiografia di vescica ileale'),
(1471, 'Radiografia emimandibola'),
(1472, 'Radiografia endorale'),
(1473, 'Radiografia ghiandole salivari con mezzo di contrasto'),
(1474, 'Radiografia morfometria vertebrale dorsale'),
(1475, 'Radiografia morfometria vertebrale lombare'),
(1476, 'Radiografia orbitale con contrasto'),
(1477, 'Radiografia sinusale con contrasto'),
(1478, 'Radiografia standard del cranio'),
(1479, 'Radiografia standard sacrococcige'),
(1480, 'Rame'),
(1481, 'Rame tissutale. Dosaggio'),
(1482, 'Reazione di Waaler Rose'),
(1483, 'Recettore solubile transferrina'),
(1484, 'Recettori degli estrogeni'),
(1485, 'Recettori del progesterone'),
(1486, 'Renina'),
(1487, 'Resistenza osmotica eritrocitaria (test di simmel)'),
(1488, 'Resistenze delle vie aeree'),
(1489, 'Resistenze osmotico globulari (curva)'),
(1490, 'Reticolociti. Conteggio'),
(1491, 'Riarrangiamenti (delezioni e duplicazioni) di altri geni umani mediante MLPA e tecniche assimilabili (per ciascun gene)'),
(1492, 'Riarrangiamenti in BRCA1 mediante MLPA'),
(1493, 'Riarrangiamenti in BRCA2 mediante MLPA'),
(1494, 'Riarrangiamento ALK. In caso di negatività incluso: ROS1'),
(1495, 'Riarrangiamento IgH test qualitativo'),
(1496, 'Riarrangiamento IgH test quantitativo'),
(1497, 'Riarrangiamento IgK test qualitativo'),
(1498, 'Riarrangiamento IgK test quantitativo'),
(1499, 'Riarrangiamento TCR B test qualitativo'),
(1500, 'Riarrangiamento TCR B test quantitativo'),
(1501, 'Riarrangiamento TCR D test qualitativo'),
(1502, 'Riarrangiamento TCR D test quantitativo'),
(1503, 'Riarrangiamento TCR G test qualitativo'),
(1504, 'Riarrangiamento TCR G test quantitativo'),
(1505, 'Riarrangiamento del recettore delle cellule t (TCR)'),
(1506, 'Riarrangiamento ewsr1'),
(1507, 'Riarrangiamento gene ddit3'),
(1508, 'Riarrangiamento gene fox01'),
(1509, 'Riarrangiamento geni delle immunoglobuline'),
(1510, 'Ricerca autoanticorpi immunoblotting'),
(1511, 'Ricerca di metastasi di tumori tiroidei'),
(1512, 'Ricerca di mucosa gastrica ectopica'),
(1513, 'Ricerca di mutazioni note/polimorfismi noti. Farmacogenetica dei geni del metabolismo dei farmaci: CYP2C19'),
(1514, 'Ricerca di mutazioni note/polimorfismi noti. Farmacogenetica dei geni del metabolismo dei farmaci: CYP2D6'),
(1515, 'Ricerca di mutazioni note/polimorfismi noti. Farmacogenetica in oncologia: ugt1a1'),
(1516, 'Ricerca mutazione (DGGE)'),
(1517, 'Ricerca mutazione (SSCP)'),
(1518, 'Rickettsie anticorpi (titolazione mediante I.F.)'),
(1519, 'Rickettsie anticorpi [anti proteus spp.] (titolazione mediante agglutinazione) [Weil-Felix]'),
(1520, 'Rickettsie conorii anticorpi IgG e IgM'),
(1521, 'Ricostruzione tridimensionale TC'),
(1522, 'Rinomanometria con o senza test di provocazione'),
(1523, 'Risposte riflesse. H, F, blink reflex, riflesso bulbocavernoso, riflessi esterocettivi agli arti'),
(1524, 'Roentgengrafia cardiaca con contrasto negativo'),
(1525, 'S-Adenosilmetionina. Dosaggio'),
(1526, 'S-Adenosilomocisteina. Dosaggio'),
(1527, 'Salmonella nelle feci esame colturale'),
(1528, 'Salmonelle anticorpi (E.I.A.)'),
(1529, 'Salmonelle anticorpi (titolazione mediante agglutinazione) [Widal]'),
(1530, 'Salmonelle da coltura identificazione biochimica e sierologica di gruppo'),
(1531, 'Salmonelle da coltura identificazione sierologica'),
(1532, 'Salmonelle e brucelle anticorpi (titolazione mediante agglutinazione) [Widal-Wright]'),
(1533, 'Schistosoma anticorpi o ricerca urinaria degli antigeni circolanti'),
(1534, 'Schistosoma haematobium in campioni urinari'),
(1535, 'Scintigrafia del midollo osseo total body'),
(1536, 'Scintigrafia delle paratiroidi'),
(1537, 'Scintigrafia delle paratiroidi con indagine tomografica'),
(1538, 'Scintigrafia epatica per ricerca di lesioni angiomatose con indagine tomografica'),
(1539, 'Scintigrafia globale corporea con indicatori positivi di neoplasia o di flogosi'),
(1540, 'Scintigrafia linfatica e linfoghiandolare segmentaria'),
(1541, 'Scintigrafia mammaria con indicatori positivi di neoplasia'),
(1542, 'Scintigrafia ossea o articolare'),
(1543, 'Scintigrafia ossea o articolare o segmentaria trifasica'),
(1544, 'Scintigrafia ossea o articolare segmentaria'),
(1545, 'Scintigrafia ossea o articolare segmentaria polifasica'),
(1546, 'Scintigrafia polmonare'),
(1547, 'Scintigrafia renale'),
(1548, 'Scintigrafia segmentaria con indicatori positivi di neoplasia o di flogosi'),
(1549, 'Scintigrafia sequenziale delle ghiandole salivari con studio funzionale'),
(1550, 'Scintigrafia sequenziale epatobiliare con valutazione della funzione colecistica e/o del reflusso duodeno-gastrico'),
(1551, 'Scintigrafia sequenziale renale'),
(1552, 'Scintigrafia surrenalica corticale'),
(1553, 'Scintigrafia surrenalica midollare'),
(1554, 'Scintigrafia tiroidea'),
(1555, 'Scintigrafia tiroidea con captazione, con o senza prove farmacologiche'),
(1556, 'Scintigrafia tiroidea con iodio-123'),
(1557, 'Screening allergologico per inalanti e alimenti [prick test]'),
(1558, 'Screening in fase solida anticorpi anti-HLA classe I e II. Isotipo IgG'),
(1559, 'Screening in fase solida anticorpi anti-hla classe I e II. Isotipo IgM'),
(1560, 'Screening in fase solida anticorpi anti-mica'),
(1561, 'Screening in fase solida specificità anticorpi anti-mica'),
(1562, 'Screening urinari errori congeniti del metabolismo'),
(1563, 'Screening/Identificazione mediante citotossicità di anticorpi anti-hla con pannello di linfociti B'),
(1564, 'Screening/Identificazione mediante citotossicità di anticorpi anti-hla con pannello di linfociti T'),
(1565, 'Sebometria'),
(1566, 'Selenio'),
(1567, 'Selezione nemaspermica per migrazione o su gradiente'),
(1568, 'Shigella nelle feci esame colturale'),
(1569, 'Shigelle da coltura identificazione biochimica e sierologica'),
(1570, 'Sigmoidoscopia con endoscopio flessibile'),
(1571, 'Sintesi di oligonucleotidi (ciascuno)'),
(1572, 'Sirolimus'),
(1573, 'Sodio'),
(1574, 'Somatostatina'),
(1575, 'Sondaggio gastrico frazionato'),
(1576, 'Sostanza amiloide ricerca'),
(1577, 'Sostanze d\'abuso identificazione e/o dosaggio di singole sostanze e relativi metaboliti'),
(1578, 'Sostanze d\'abuso test di screening'),
(1579, 'Spirometria'),
(1580, 'Spirometria globale con tecnica pletismografica'),
(1581, 'Stato her2-neu'),
(1582, 'Stato mutazionale b-raf'),
(1583, 'Stato mutazionale c-kit'),
(1584, 'Stato mutazionale egfr'),
(1585, 'Stato mutazionale h-ras'),
(1586, 'Stato mutazionale k-ras'),
(1587, 'Stato mutazionale k-ras, n-ras'),
(1588, 'Stato mutazionale pdgfra'),
(1589, 'Stato mutazionale ret'),
(1590, 'Steroli. Dosaggio plasma'),
(1591, 'Stimolazione ripetitiva'),
(1592, 'Stimolazioni vestibolari rotatorie. Prove rotatorie, prove pendolari a smorzamento meccanico'),
(1593, 'Stratigrafia dell\'articolazione temporomandibolare bilaterale'),
(1594, 'Stratigrafia dell\'articolazione temporomandibolare monolaterale'),
(1595, 'Streptococco agalactiae nel tampone vagino-rettale esame colturale'),
(1596, 'Streptococco anticorpi anti DNAsi b'),
(1597, 'Streptococco anticorpi anti antistreptolisina-o [TAS]'),
(1598, 'Streptococcus pneumoniae antigeni nelle urine ricerca diretta'),
(1599, 'Streptococcus pyogenes nel tampone orofaringeo esame colturale'),
(1600, 'Strongyloides stercoralis anticorpi'),
(1601, 'Strongyloides stercoralis ricerca larve nelle feci'),
(1602, 'Studio articolare dinamico sotto stress e/o sottocarico'),
(1603, 'Studio del campo visivo'),
(1604, 'Studio del reflusso gastro-esofageo'),
(1605, 'Studio del reflusso vescico-ureterale'),
(1606, 'Studio del transito esofageo'),
(1607, 'Studio dell\'adattabilità al buio'),
(1608, 'Studio dell\'età ossea'),
(1609, 'Studio della sensibilità al colore'),
(1610, 'Studio della sensibilità al contrasto'),
(1611, 'Studio della topografia corneale'),
(1612, 'Studio ecografico del reflusso gastroesofageo'),
(1613, 'Studio ecografico del tempo di svuotamento gastrico'),
(1614, 'Studio fisico-dosimetrico'),
(1615, 'Studio fisico-dosimetrico con elaboratore per calcolo dose da somministrare'),
(1616, 'Studio registrato del nistagmo (spontaneo, posizionale, provocato) / Videoculografia'),
(1617, 'Studio seriato dell\'intestino tenue con singolo contrasto'),
(1618, 'Studio strumentale della conformazione della papilla ottica [HRT o GDX o OCT]'),
(1619, 'Succinilacetone urinario'),
(1620, 'Succo gastrico esame chimico completo'),
(1621, 'Sudore (esame con determinazione di na+ e k+)'),
(1622, 'Sulfiti. Screening urine mediante sulfitest'),
(1623, 'Svuotamento gastrico: valutazione della funzione motoria gastrica'),
(1624, 'Swelling test'),
(1625, 'T(11:14)'),
(1626, 'T(12:21) test qualitativo'),
(1627, 'T(12:21) test quantitativo'),
(1628, 'T(14:18) test qualitativo'),
(1629, 'T(14:18) test quantitativo'),
(1630, 'T(15:17) test qualitativo'),
(1631, 'T(15:17) test quantitativo'),
(1632, 'T(1:19) test qualitativo'),
(1633, 'T(1:19) test quantitativo'),
(1634, 'T(4:11) test qualitativo'),
(1635, 'T(4:11) test quantitativo'),
(1636, 'T(8:21) test qualitativo'),
(1637, 'T(8:21) test quantitativo'),
(1638, 'T(9:22) test qualitativo'),
(1639, 'T(9:22) test quantitativo'),
(1640, 'TAC (tomografia assiale computerizzata) del rene'),
(1641, 'TAC (tomografia assiale computerizzata) dell\'addome'),
(1642, 'TC (tomografia computerizzata) dell\'addome completo'),
(1643, 'TC (tomografia computerizzata) dei reni, senza e con contrasto'),
(1644, 'TC (tomografia computerizzata) del bacino'),
(1645, 'TC (tomografia computerizzata) del braccio'),
(1646, 'TC (tomografia computerizzata) del collo'),
(1647, 'TC (tomografia computerizzata) del colon'),
(1648, 'TC (tomografia computerizzata) del cranio-encefalo'),
(1649, 'TC (tomografia computerizzata) del cuore'),
(1650, 'TC (tomografia computerizzata) del fegato multifasica'),
(1651, 'TC (tomografia computerizzata) del ginocchio'),
(1652, 'TC (tomografia computerizzata) del gomito'),
(1653, 'TC (tomografia computerizzata) del massiccio facciale'),
(1654, 'TC (tomografia computerizzata) del piede'),
(1655, 'TC (tomografia computerizzata) del polso'),
(1656, 'TC (tomografia computerizzata) del rachide e dello speco vertebrale'),
(1657, 'TC (tomografia computerizzata) del rachide e dello speco vertebrale cervicale'),
(1658, 'TC (tomografia computerizzata) del rachide e dello speco vertebrale toracico'),
(1659, 'TC (tomografia computerizzata) del rachide e dello speco vertebrale, senza e con contrasto'),
(1660, 'TC (tomografia computerizzata) del rachide, dello speco vertebrale lombosacrale e del sacro coccige'),
(1661, 'TC (tomografia computerizzata) del torace'),
(1662, 'TC (tomografia computerizzata) dell\'addome inferiore'),
(1663, 'TC (tomografia computerizzata) dell\'addome superiore'),
(1664, 'TC (tomografia computerizzata) dell\'articolazione coxofemorale'),
(1665, 'TC (tomografia computerizzata) dell\'arto inferiore'),
(1666, 'TC (tomografia computerizzata) dell\'arto inferiore, senza e con contrasto'),
(1667, 'TC (tomografia computerizzata) dell\'arto superiore'),
(1668, 'TC (tomografia computerizzata) dell\'arto superiore, senza e con contrasto'),
(1669, 'TC (tomografia computerizzata) dell\'avambraccio'),
(1670, 'TC (tomografia computerizzata) dell\'intestino tenue (con enteroclisi)'),
(1671, 'TC (tomografia computerizzata) dell\'orecchio'),
(1672, 'TC (tomografia computerizzata) della caviglia'),
(1673, 'TC (tomografia computerizzata) della caviglia e del piede'),
(1674, 'TC (tomografia computerizzata) della coscia [TC del femore]'),
(1675, 'TC (tomografia computerizzata) della gamba'),
(1676, 'TC (tomografia computerizzata) della mano'),
(1677, 'TC (tomografia computerizzata) della sella turcica'),
(1678, 'TC (tomografia computerizzata) della spalla'),
(1679, 'TC (tomografia computerizzata) delle arcate dentarie [DENTALSCAN]'),
(1680, 'TC (tomografia computerizzata) delle ghiandole salivari'),
(1681, 'TC (tomografia computerizzata) delle orbite'),
(1682, 'TC (tomografia computerizzata) di ginocchio e gamba'),
(1683, 'TC (tomografia computerizzata) di gomito e avambraccio'),
(1684, 'TC (tomografia computerizzata) di polso e mano'),
(1685, 'TC (tomografia computerizzata) di spalla e braccio'),
(1686, 'TC (tomografia computerizzata) total body'),
(1687, 'TC (tomografia computerizzata) urografia'),
(1688, 'Taenia solium [cisticercosi] anticorpi'),
(1689, 'Taenia solium [cisticercosi] immunoblotting (saggio di conferma)'),
(1690, 'Teleradiografia del cranio'),
(1691, 'Tempo di botroxina (reptilase)'),
(1692, 'Tempo di emorragia sec. mielke'),
(1693, 'Tempo di lisi euglobulinica'),
(1694, 'Tempo di protrombina (PT)'),
(1695, 'Tempo di transito intestinale'),
(1696, 'Tempo di trombina (TT)'),
(1697, 'Tempo di tromboplastina parziale attivata (APTT)'),
(1698, 'Teofillina'),
(1699, 'Test cardiovascolare da sforzo al tallio'),
(1700, 'Test cardiovascolare da sforzo con cicloergometro o con pedana mobile'),
(1701, 'Test cardiovascolari per valutazione di neuropatia autonomica'),
(1702, 'Test clinico della funzionalità vestibolare'),
(1703, 'Test da sforzo cardiopolmonare'),
(1704, 'Test da sforzo dei due gradini di masters'),
(1705, 'Test del cammino con valutazione della saturazione arteriosa [walking test]'),
(1706, 'Test del siero autologo'),
(1707, 'Test del sudore'),
(1708, 'Test della memoria. Memoria implicita, esplicita, a breve e lungo termine, test di attenzione, test di abilità di lettura (somministrazione e interpretazione)'),
(1709, 'Test della scala di memoria di Wechsler [WMS] (somministrazione e interpretazione)'),
(1710, 'Test delle abilità visuo spaziali (somministrazione e interpretazione)'),
(1711, 'Test delle funzioni esecutive (somministrazione e interpretazione)'),
(1712, 'Test di Bernstein'),
(1713, 'Test di aggregazione piastrinica'),
(1714, 'Test di broncodilatazione farmacologica'),
(1715, 'Test di deterioramento o sviluppo intellettivo (somministrazione e interpretazione)'),
(1716, 'Test di distribuzione della ventilazione con gas non radioattivi'),
(1717, 'Test di emolisi al saccarosio'),
(1718, 'Test di equilibrazione peritoneale'),
(1719, 'Test di falcizzazione'),
(1720, 'Test di funzionaltà piastrinica (PFA)'),
(1721, 'Test di ham'),
(1722, 'Test di inibizione delle IgE specifiche con allergene specifico'),
(1723, 'Test di intelligenza (somministrazione e interpretazione)'),
(1724, 'Test di intolleranze o allergie sulla congiuntiva'),
(1725, 'Test di ischemia prolungata'),
(1726, 'Test di kleihauer (ricerca emazie fetali)'),
(1727, 'Test di permeabilità intestinale (test del lattulosio e mannitolo)'),
(1728, 'Test di provocazione bronchiale con agente broncocostrittore'),
(1729, 'Test di provocazione e curva tonometrica per glaucoma'),
(1730, 'Test di resistenza alla proteina C attivata'),
(1731, 'Test di stimolazione elettrica al promontorio'),
(1732, 'Test di stimolazione linfocitaria (per mitogeno)'),
(1733, 'Test di stimolazione linfocitaria con antigeni specifici'),
(1734, 'Test di tolleranza/provocazione con farmaci, alimenti e additivi'),
(1735, 'Test di valutazione del carico familiare e delle strategie di coping (somministrazione e interpretazione)'),
(1736, 'Test di valutazione della disabilità sociale (somministrazione e interpretazione)'),
(1737, 'Test di valutazione della risposta motoria alla levodopa/apomorfina'),
(1738, 'Test epicutanei a lettura ritardata [patch test]'),
(1739, 'Test epicutaneo in aperto [open test]'),
(1740, 'Test funzionali obiettivi dell\'occhio (test di Hess-Lancaster)'),
(1741, 'Test funzionali pre-trapianto (htlp, ctlp)'),
(1742, 'Test neurofisiologici per la valutazione del sistema nervoso vegetativo'),
(1743, 'Test per tetania latente'),
(1744, 'Test percutanei e intracutanei a lettura immediata (fino a 12 allergeni)'),
(1745, 'Test percutanei e intracutanei a lettura immediata e ritardata per farmaci'),
(1746, 'Test percutanei e intracutanei a lettura immediata per veleno di imenotteri'),
(1747, 'Test polisonnografici del livello di vigilanza'),
(1748, 'Test posturografico'),
(1749, 'Test proiettivi e della personalità (somministrazione e interpretazione)'),
(1750, 'Test stabilometrico statico e/o dinamico su pedana'),
(1751, 'Testosterone'),
(1752, 'Testosterone libero'),
(1753, 'Tine test (reazione cutanea alla turbecolina)'),
(1754, 'Tipizzazione eritrocitaria per D variant'),
(1755, 'Tipizzazione geni KIR in trapianto mismatch'),
(1756, 'Tipizzazione genomica locus A (HLA-A) - alta risoluzione'),
(1757, 'Tipizzazione genomica locus A (HLA-A) - bassa risoluzione'),
(1758, 'Tipizzazione genomica locus A (HLA-A) mediante sequenziamento diretto'),
(1759, 'Tipizzazione genomica locus B (HLA-B) - alta risoluzione'),
(1760, 'Tipizzazione genomica locus B (HLA-B) - bassa risoluzione'),
(1761, 'Tipizzazione genomica locus B (HLA-B) mediante sequenziamento diretto'),
(1762, 'Tipizzazione genomica locus C (HLA-C) - alta risoluzione'),
(1763, 'Tipizzazione genomica locus C (HLA-C) - bassa risoluzione'),
(1764, 'Tipizzazione genomica locus C (HLA-C) mediante sequenziamento diretto'),
(1765, 'Tipizzazione genomica locus DP (HLA-DP) mediante sequenziamento diretto'),
(1766, 'Tipizzazione genomica locus DPA1 (HLA-DPA1) - alta risoluzione'),
(1767, 'Tipizzazione genomica locus DPB1 (HLA-DPB1) - alta risoluzione'),
(1768, 'Tipizzazione genomica locus DQ (HLA-DQ) mediante sequenziamento diretto'),
(1769, 'Tipizzazione genomica locus DQA1 (HLA-DQA1) - alta risoluzione'),
(1770, 'Tipizzazione genomica locus DQB1 (HLA-DQB1) - alta risoluzione'),
(1771, 'Tipizzazione genomica locus DQB1 (HLA-DQB1) - bassa risoluzione'),
(1772, 'Tipizzazione genomica locus DR (HLA-DR) mediante sequenziamento diretto'),
(1773, 'Tipizzazione genomica locus DRB (HLA-DRB1 e DRB3, DRB4, DRB5) - alta risoluzione'),
(1774, 'Tipizzazione genomica locus DRB (HLA-DRB1 e DRB3, DRB4, DRB5) - bassa risoluzione'),
(1775, 'Tipizzazione genomica locus DRB1. Alta risoluzione'),
(1776, 'Tipizzazione genomica locus DRB3. Alta risoluzione'),
(1777, 'Tipizzazione genomica locus DRB4. Alta risoluzione'),
(1778, 'Tipizzazione genomica locus DRB5. Alta risoluzione'),
(1779, 'Tipizzazione sierologica HLA classe I (Fenot. compl. loci A, B, C, o loci A, B)'),
(1780, 'Tipizzazione sierologica HLA classe II (Fenot. compl. loci DR, DQ o locus DP)'),
(1781, 'Tipizzazione sottopopolazioni di cellule del sangue (per ciascun anticorpo)'),
(1782, 'Tireoglobulina (TG)'),
(1783, 'Tireotropina (TSH)'),
(1784, 'Tireotropina (tsh): dosaggi seriati dopo trh ( 4 )'),
(1785, 'Tireotropina [TSH] test reflex'),
(1786, 'Tiroxina libera (FT4)'),
(1787, 'Tomografia [stratigrafia] articolazione temporomandibolare'),
(1788, 'Tomografia [stratigrafia] contemporanea a esame di: Ghiandole salivari, Trachea'),
(1789, 'Tomografia [stratigrafia] del mediastino'),
(1790, 'Tomografia [stratigrafia] della laringe'),
(1791, 'Tomografia [stratigrafia] delle arcate dentarie'),
(1792, 'Tomografia [stratigrafia] di segmento scheletrico'),
(1793, 'Tomografia [stratigrafia] renale'),
(1794, 'Tomografia [stratigrafia] toracica bilaterale'),
(1795, 'Tomografia [stratigrafia] toracica monolaterale'),
(1796, 'Tomografia a emissione di positroni [PET TC] miocardica'),
(1797, 'Tomografia a emissione di positroni [PET] cerebrale qualitativa / quantitativa'),
(1798, 'Tomografia ad emissione di positroni [PET TC] miocardica di perfusione a riposo e da stimolo'),
(1799, 'Tomografia ad emissione di positroni [PET] cerebrale con altri radiofarmaci'),
(1800, 'Tomografia ad emissione di positroni [PET] segmentaria ai fini di piano dosimetrico'),
(1801, 'Tomografia retinica (OCT) a luce coerente'),
(1802, 'Tomoscintigrafia [G-SPET] miocardica di perfusione a riposo'),
(1803, 'Tomoscintigrafia [SPET] cerebrale con tracciante di perfusione'),
(1804, 'Tomoscintigrafia [SPET] cerebrale con traccianti recettoriali o indicatori positivi di neoplasia'),
(1805, 'Tomoscintigrafia [SPET] miocardica con tracciante di innervazione o recettoriale o neurorecettoriale o di metabolismo'),
(1806, 'Tomoscintigrafia [SPET] miocardica di perfusione a riposo o dopo stimolo'),
(1807, 'Tomoscintigrafia [SPET] segmentaria ai fini di piano dosimetrico'),
(1808, 'Tomscintigrafia [PET] globale corporea'),
(1809, 'Tonometria'),
(1810, 'Tossina difterica anticorpi'),
(1811, 'Tossina tetanica anticorpi'),
(1812, 'Toxocara anticorpi'),
(1813, 'Toxoplasma acidi nucleici in materiali biologici ricerca qualitativa/quantitativa'),
(1814, 'Toxoplasma anticorpi (E.I.A.)'),
(1815, 'Toxoplasma anticorpi (titolazione mediante I.F.)'),
(1816, 'Toxoplasma anticorpi (titolazione mediante agglutinazione) [test di Fulton]'),
(1817, 'Toxoplasma anticorpi IgG e IgM'),
(1818, 'Toxoplasma anticorpi immunoblotting per IgG e IgM (saggio di conferma) per classe di anticorpi'),
(1819, 'Tracciato dell\'impulso carotideo con elettrodo ECG'),
(1820, 'Transferrina'),
(1821, 'Transferrina (capacità ferrolegante)'),
(1822, 'Transferrina desialata (CDT)'),
(1823, 'Transilluminazione di cranio di neonato'),
(1824, 'Traslocazione (11:14)'),
(1825, 'Traslocazione (2:17)'),
(1826, 'Traslocazione (2:5), (1:2)'),
(1827, 'Traslocazione (7:16)'),
(1828, 'Traslocazione (8:14), (2:8), (8:22), (8:9), (3:8)'),
(1829, 'Traslocazione (9:14)'),
(1830, 'Traslocazione (x:18)'),
(1831, 'Traslocazione der (17) t (x:17)'),
(1832, 'Traslocazione t (11:18), t (1:14), t (3:14)'),
(1833, 'Traslocazione t (12:15)'),
(1834, 'Traslocazione t (14:18)'),
(1835, 'Traslocazione t (2:12)'),
(1836, 'Treponema pallidum anticorpi (E.I.A.)'),
(1837, 'Treponema pallidum anticorpi (I.F.) [fta-abs]'),
(1838, 'Treponema pallidum anticorpi (ricerca qualitatativa mediante emoagglutinazione passiva) [tpha]'),
(1839, 'Treponema pallidum anticorpi (ricerca quantitativa mediante emoagglutinazione passiva) [tpha]'),
(1840, 'Treponema pallidum anticorpi anti cardiolipina (flocculazione) [vdrl] [rpr]'),
(1841, 'Treponema pallidum anticorpi anti cardiolipina (flocculazione) [vdrl] [rpr] quantitativa'),
(1842, 'Treponema pallidum sierologia della sifilide. Anticorpi EIA/CLIA e/o TPHA [TPPA] più VDRL [RPR]'),
(1843, 'Tri test: alfafetoproteina (AFP), HCG totale o frazione libera, estriolo (E3). Determinazioni di rischio prenatale per anomalie cromosomiche e difetti del tubo neurale'),
(1844, 'Trichinella anticorpi'),
(1845, 'Trichomonas vaginalis nel secreto vaginale esame colturale'),
(1846, 'Trichomonas vaginalis, esame colturale e/o ricerca diretta antigeni'),
(1847, 'Trigliceridi'),
(1848, 'Triodotironina libera (FT3)'),
(1849, 'Tripanosoma cruzi anticorpi'),
(1850, 'Tripanosomi nel sangue ricerca microscopica'),
(1851, 'Tripsina'),
(1852, 'Triptasi'),
(1853, 'Trombina - Antitrombina III complesso (TAT)'),
(1854, 'Trombossano b2'),
(1855, 'Troponina I'),
(1856, 'Troponina t'),
(1857, 'Urato'),
(1858, 'Urea'),
(1859, 'Ureteroscopia'),
(1860, 'Uretrocistoscopia'),
(1861, 'Uretrocistoscopia con biopsia'),
(1862, 'Uretrografia'),
(1863, 'Uretroscopia'),
(1864, 'Uretroscopia trans-perineale'),
(1865, 'Urine conta di addis'),
(1866, 'Urine esame completo'),
(1867, 'Urine esame morfologico a fresco'),
(1868, 'Urine esame parziale (acetone e glucosio quantitativo)'),
(1869, 'Urine ricerca di spermatozoi'),
(1870, 'Urografia endovenosa'),
(1871, 'Valore ematocrito'),
(1872, 'Valutazione EMG dinamica del cammino'),
(1873, 'Valutazione del livello di autonomia nella cura della propria persona (ADL primarie o di base)'),
(1874, 'Valutazione del livello di autonomia nelle attività di vita domestica e aree di vita principale (ADL secondarie e\\o IADL)'),
(1875, 'Valutazione del ricircolo di fistola arterovenosa'),
(1876, 'Valutazione della portata della fistola arterovenosa'),
(1877, 'Valutazione della soglia di sensibilità vibratoria'),
(1878, 'Valutazione della ventilazione e dei gas espirati e relativi parametri'),
(1879, 'Valutazione delle gastroenterorragie'),
(1880, 'Valutazione e assistenza protesica'),
(1881, 'Valutazione funzionale delle funzioni corticali superiori'),
(1882, 'Valutazione funzionale globale'),
(1883, 'Valutazione globale del livello di autonomia (ADL primarie o di base e ADL secondarie e\\o IADL)'),
(1884, 'Valutazione monofunzionale del dolore'),
(1885, 'Valutazione monofunzionale del movimento'),
(1886, 'Valutazione monofunzionale del sistema cardiovascolare e dell\'apparato respiratorio'),
(1887, 'Valutazione monofunzionale dell\'apparato digerente [disfagia-turbe della defecazione]'),
(1888, 'Valutazione monofunzionale della voce e dell\'eloquio [afasia-disartria]'),
(1889, 'Valutazione monofunzionale delle funzioni delle articolazioni e delle ossa'),
(1890, 'Valutazione monofunzionale delle funzioni genito urinarie [turbe vescico minzionali-perineali]'),
(1891, 'Valutazione monofunzionale delle funzioni mentali globali'),
(1892, 'Valutazione monofunzionale delle funzioni muscolari [forza-tono-resistenza]'),
(1893, 'Valutazione monofunzionale delle funzioni vestibolari [equilibrio]'),
(1894, 'Valutazione ortesica'),
(1895, 'Valutazione ortottica con studio completo della motilità oculare'),
(1896, 'Valutazione psichiatrica dello stato mentale'),
(1897, 'Velocità di conduzione nervosa motoria'),
(1898, 'Velocità di conduzione nervosa sensitiva'),
(1899, 'Velocità di sedimentazione delle emazie (VES)'),
(1900, 'Verifica beneficio protesico. Audiometria tonale/vocale protesica');
INSERT INTO `Esami` (`id`, `nome`) VALUES
(1901, 'Vesciculografia seminale con contrasto'),
(1902, 'Vettocardiografia'),
(1903, 'Vibrio da coltura identificazione biochimica e sierologica'),
(1904, 'Vibrio nelle feci esame colturale'),
(1905, 'Videoendoscopia delle vie aeree e digestive superiori (VADS)'),
(1906, 'Virus HBV [HBV] reflex. Antigene HBsAg + anticorpi anti HBsAg + anticorpi anti HBcAg'),
(1907, 'Virus [echo, polio, coxsackie, enterovirus] anticorpi IgG e IgM'),
(1908, 'Virus acidi nucleici in materiali biologici ibridazione (previa reazione polimerasica a catena)'),
(1909, 'Virus acidi nucleici in materiali biologici ibridazione (previa retrotrascrizione-reazione polimerasica a catena)'),
(1910, 'Virus acidi nucleici in materiali biologici ibridazione diretta'),
(1911, 'Virus acidi nucleici in materiali biologici. ricerca qualitativa/quantitativa'),
(1912, 'Virus adenovirus anticorpi (E.I.A.)'),
(1913, 'Virus adenovirus anticorpi (titolazione mediante F.C.)'),
(1914, 'Virus adenovirus antigeni ricerca diretta nelle feci'),
(1915, 'Virus adenovirus in materiali biologici esame colturale'),
(1916, 'Virus anticorpi (Titolazione mediante F.C.)'),
(1917, 'Virus anticorpi immunoblotting Virus anticorpi immunoblotting'),
(1918, 'Virus antigeni ricerca diretta in materiali biologici (Agglutinazione passiva) (Adenovirus, Rotavirus, Virus dell\'apparato gastroenterico)'),
(1919, 'Virus antigeni ricerca diretta in materiali biologici (E.I.A.) (Adenovirus, Parvovirus B19, Rotavirus)'),
(1920, 'Virus antigeni ricerca diretta in materiali biologici (I.F.) (Citomegalovirus, Herpes, Virus dell\'apparato respiratorio)'),
(1921, 'Virus antigeni ricerca diretta in materiali biologici non altrimenti specificato'),
(1922, 'Virus astrovirus ricerca antigene diretta nelle feci'),
(1923, 'Virus citomegalovirus anticorpi (E.I.A.)'),
(1924, 'Virus citomegalovirus anticorpi (titolazione mediante F.C.)'),
(1925, 'Virus citomegalovirus anticorpi IgG e IgM'),
(1926, 'Virus citomegalovirus anticorpi IgM (E.I.A.)'),
(1927, 'Virus citomegalovirus da coltura identificazione mediante ibridazione'),
(1928, 'Virus citomegalovirus in materiali biologici diversi ricerca mediante esame colturale'),
(1929, 'Virus citomegalovirus in materiali biologici diversi ricerca mediante esame colturale (metodo rapido)'),
(1930, 'Virus citomegalovirus nel latte materno e nel tampone faringeo esame colturale (metodo tradizionale)'),
(1931, 'Virus citomegalovirus nel sangue acidi nucleici identificazione mediante ibridazione'),
(1932, 'Virus citomegalovirus nel sangue esame colturale (metodo tradizionale)'),
(1933, 'Virus citomegalovirus nell\'urina acidi nucleici identificazione mediante ibridazione'),
(1934, 'Virus citomegalovirus nell\'urina esame colturale (metodo tradizionale)'),
(1935, 'Virus citomegalovirus. ricerca antigeni su granulociti (antigenemia) (IF o EIA)'),
(1936, 'Virus citomegalovirus: analisi qualitativa del DNA'),
(1937, 'Virus citomegalovirus: analisi quantitativa del DNA'),
(1938, 'Virus coxsackie [b1, b2, b3, b4, b5, b6] anticorpi (titolazione mediante F.C.)'),
(1939, 'Virus coxsackie [b1, b2, b3, b4, b5, b6] anticorpi (titolazione mediante I.F.)'),
(1940, 'Virus da coltura identificazione (mediante I.F.)'),
(1941, 'Virus da coltura identificazione (mediante M.E.)'),
(1942, 'Virus da coltura identificazione (mediante neutralizzazione)'),
(1943, 'Virus echo (titolazione mediante I.F.)'),
(1944, 'Virus enterici ricerca acidi nucleici multiplex'),
(1945, 'Virus epatite A [HAV] anticorpi'),
(1946, 'Virus epatite A [HAV] anticorpi IgG e IgM per sospetta infezione acuta'),
(1947, 'Virus epatite A [HAV] anticorpi IgG per controllo stato immunitario'),
(1948, 'Virus epatite A [HAV] anticorpi IgM'),
(1949, 'Virus epatite B [HBV] DNA-polimerasi'),
(1950, 'Virus epatite B [HBV] acidi nucleici ibridazione (previa reazione polimerasica a catena)'),
(1951, 'Virus epatite B [HBV] acidi nucleici ibridazione diretta'),
(1952, 'Virus epatite B [HBV] analisi di mutazione del DNA per rilevamento resistenze ai farmaci antivirali'),
(1953, 'Virus epatite B [HBV] analisi qualitativa di HBV DNA'),
(1954, 'Virus epatite B [HBV] anticorpi HBcAg'),
(1955, 'Virus epatite B [HBV] anticorpi HBcAg IgM'),
(1956, 'Virus epatite B [HBV] anticorpi HBeAg'),
(1957, 'Virus epatite B [HBV] anticorpi HBsAg'),
(1958, 'Virus epatite B [HBV] antigene HBeAg'),
(1959, 'Virus epatite B [HBV] antigene HBsAg'),
(1960, 'Virus epatite B [HBV] antigene hbsag (saggio di conferma)'),
(1961, 'Virus epatite B [HBV] tipizzazione genomica'),
(1962, 'Virus epatite C [HCV] analisi di mutazione del DNA per rilevamento resistenze ai farmaci antivirali'),
(1963, 'Virus epatite C [HCV] analisi qualitativa di hcv RNA'),
(1964, 'Virus epatite C [HCV] analisi quantitativa di hcv RNA'),
(1965, 'Virus epatite C [HCV] anticorpi'),
(1966, 'Virus epatite C [HCV] immunoblotting (saggio di conferma)'),
(1967, 'Virus epatite C [HCV] tipizzazione genomica'),
(1968, 'Virus epatite C antigene'),
(1969, 'Virus epatite delta [HDV] anticorpi'),
(1970, 'Virus epatite delta [HDV] anticorpi IgG e IgM'),
(1971, 'Virus epatite delta [HDV] anticorpi IgM'),
(1972, 'Virus epatite delta [HDV] antigene HDVAg'),
(1973, 'Virus epatite e [HEV] anticorpi'),
(1974, 'Virus epstein barr [EBV] analisi qualitativa/quantitativa del DNA'),
(1975, 'Virus epstein barr [EBV] anticorpi (EA o EBNA o VCA) (E.I.A.)'),
(1976, 'Virus epstein barr [EBV] anticorpi (EA o EBNA o VCA) (titolazione mediante I.F.)'),
(1977, 'Virus epstein barr [EBV] anticorpi EBNA + VCA IgG + VCA IgM'),
(1978, 'Virus epstein barr [EBV] anticorpi eterofili (test rapido)'),
(1979, 'Virus epstein barr [EBV] anticorpi eterofili [R. Paul Bunnel Davidsohn]'),
(1980, 'Virus genotipizzazione'),
(1981, 'Virus herpes anticorpi (titolazione mediante F.C.)'),
(1982, 'Virus herpes simplex (tipo 1 e 2) anticorpi IgG'),
(1983, 'Virus herpes simplex (tipo 1 o 2) anticorpi'),
(1984, 'Virus immunodef. acquisita [HIV 1-2] anticorpi'),
(1985, 'Virus immunodef. acquisita [HIV 1] antigene P24 (E.I.A.)'),
(1986, 'Virus immunodef. acquisita [HIV 1] antigene P24 da colture linfocitarie (E.I.A.)'),
(1987, 'Virus immunodef. acquisita [HIV 2] anticorpi immunoblotting (saggio di conferma)'),
(1988, 'Virus immunodef. acquisita [HIV]: Analisi di mutazione dell\'acido nucleico per rilevamento resistenze ai farmaci antivirali'),
(1989, 'Virus immunodeficenza acquisita [HIV 1-2] .Test combinato anticorpi e antigene P24'),
(1990, 'Virus immunodeficenza acquisita [HIV] analisi qualitativa di DNA provirale'),
(1991, 'Virus immunodeficenza acquisita [HIV] analisi quantitativa di RNA'),
(1992, 'Virus immunodeficienza acquisita [HIV 1-2] anticorpi immunoblotting (saggio di conferma)'),
(1993, 'Virus immunodeficienza acquisita [HIV 1] anticorpi anti antigene P24 (E.I.A.)'),
(1994, 'Virus immunodeficienza acquisita [HIV 1] anticorpi immunoblotting (saggio di conferma)'),
(1995, 'Virus in materiali biologici esame colturale'),
(1996, 'Virus in materiali biologici esame colturale (metodo rapido)'),
(1997, 'Virus in materiali biologici esame colturale (metodo tradizionale)'),
(1998, 'Virus morbillo anticorpi (E.I.A.)'),
(1999, 'Virus morbillo anticorpi (I.F.)'),
(2000, 'Virus morbillo anticorpi (titolazione mediante F.C.)'),
(2001, 'Virus morbillo anticorpi IgG e IgM'),
(2002, 'Virus norovirus antigeni ricerca diretta nelle feci'),
(2003, 'Virus papillomavirus [HPV] in materiali biologici mediante ibridazione diretta'),
(2004, 'Virus papillomavirus [HPV] tipizzazione genomica'),
(2005, 'Virus papillomavirus [HPV]. Analisi qualitativa/quantitativa DNA'),
(2006, 'Virus parotite anticorpi (E.I.A.)'),
(2007, 'Virus parotite anticorpi (I.F.)'),
(2008, 'Virus parotite anticorpi (titolazione mediante F.C.)'),
(2009, 'Virus parotite anticorpi IgG e IgM'),
(2010, 'Virus parvovirus b19 anticorpi IgG e IgM'),
(2011, 'Virus respiratori ricerca acidi nucleici multiplex'),
(2012, 'Virus respiratorio sinciziale anticorpi (I.F.)'),
(2013, 'Virus respiratorio sinciziale anticorpi (titolazione mediante F.C.)'),
(2014, 'Virus respiratorio sinciziale ricerca anticorpi (E.I.A.)'),
(2015, 'Virus respiratorio sinciziale ricerca diretta in materiali biologici'),
(2016, 'Virus retrovirus ricerca anticorpi anti HTLV1-HTLV2'),
(2017, 'Virus rosolia IgG e IgM (incluso test di avidità IgG)'),
(2018, 'Virus rosolia anticorpi (titolazione mediante i.h.a.)'),
(2019, 'Virus rosolia anticorpi IgG per controllo stato immunitario'),
(2020, 'Virus rotavirus antigeni ricerca diretta nelle feci'),
(2021, 'Virus varicella zoster anticorpi (E.I.A.)'),
(2022, 'Virus varicella zoster anticorpi (I.F.)'),
(2023, 'Virus varicella zoster anticorpi (titolazione mediante F.C.)'),
(2024, 'Virus varicella zoster anticorpi IgG ed eventuali IgM'),
(2025, 'Viscosità ematica'),
(2026, 'Viscosità plasmatica'),
(2027, 'Vitamina D (1,25 OH)'),
(2028, 'Vitamina D (25 OH)'),
(2029, 'Vitamine idrosolubili: dosaggio plasmatico'),
(2030, 'Vitamine liposolubili: dosaggio plasmatico'),
(2031, 'Wilms tumor1 test quantitativo'),
(2032, 'Xilosio (test di assorbimento)'),
(2033, 'Yersinia da coltura identificazione biochimica'),
(2034, 'Yersinia nelle feci esame colturale'),
(2035, 'Zinco'),
(2036, 'Zincoprotoporfirina [(Sg)er]'),
(2037, '[CTX] Telopeptide c-terminale del collagene tipo 1');

-- --------------------------------------------------------

--
-- Struttura della tabella `EsamiPrescritti`
--
-- Creazione: Ott 04, 2019 alle 08:55
-- Ultimo aggiornamento: Ott 10, 2019 alle 19:42
--

DROP TABLE IF EXISTS `EsamiPrescritti`;
CREATE TABLE `EsamiPrescritti` (
  `idDottore` int(11) NOT NULL,
  `idPaziente` int(11) NOT NULL,
  `dataOraPrescrizione` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idEsame` int(11) NOT NULL,
  `costo` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '€',
  `idSsp` int(11) DEFAULT NULL,
  `risultati` text,
  `dataOraEvasione` timestamp(3) NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `EsamiPrescritti`:
--   `idDottore`
--       `Utenti` -> `id`
--   `idEsame`
--       `Esami` -> `id`
--   `idMedico`
--       `Utenti` -> `id`
--   `idPaziente`
--       `Utenti` -> `id`
--   `idSsp`
--       `Utenti` -> `id`
--

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `EsamiPrescrittiView`
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `EsamiPrescrittiView`;
CREATE TABLE `EsamiPrescrittiView` (
`idDottore` int(11)
,`nomeDottore` varchar(100)
,`cognomeDottore` varchar(100)
,`nomeProvinciaDottore` varchar(60)
,`siglaProvinciaDottore` varchar(5)
,`DOCFotoDottore` timestamp(3)
,`extensionFotoDottore` varchar(10)
,`idPaziente` int(11)
,`nomePaziente` varchar(100)
,`cognomePaziente` varchar(100)
,`cfPaziente` varchar(17)
,`nomeProvinciaPaziente` varchar(60)
,`siglaProvinciaPaziente` varchar(5)
,`DOCFotoPaziente` timestamp(3)
,`extensionFotoPaziente` varchar(10)
,`dataOraPrescrizione` timestamp(3)
,`idEsame` int(11)
,`nomeEsame` varchar(200)
,`costo` float(10,2)
,`idSsp` int(11)
,`nomeErogatore` varchar(100)
,`nomeProvinciaErogatore` varchar(60)
,`siglaProvinciaErogatore` varchar(5)
,`DOCFotoErogatore` timestamp(3)
,`extensionFotoErogatore` varchar(10)
,`ruoloErogatore` int(3)
,`risultati` text
,`dataOraEvasione` timestamp(3)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `Foto`
--
-- Creazione: Ott 10, 2019 alle 22:22
-- Ultimo aggiornamento: Ott 11, 2019 alle 06:44
--

DROP TABLE IF EXISTS `Foto`;
CREATE TABLE `Foto` (
  `idUtente` int(11) NOT NULL,
  `dataOraCreazione` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `extension` varchar(10) NOT NULL COMMENT 'timestamp = nome del file'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `Foto`
--

INSERT INTO `Foto` (`idUtente`, `dataOraCreazione`, `extension`) VALUES
(6, '2019-11-06 23:25:18.093', 'jpg'),
(18, '2019-11-06 23:30:47.561', 'jpg'),
(19, '2019-11-06 23:30:49.938', 'jpg'),
(31, '2019-11-06 23:36:18.314', 'jpg'),
(32, '2019-11-06 23:36:21.221', 'jpg'),
(44, '2019-11-06 23:41:48.261', 'jpg'),
(45, '2019-11-06 23:41:50.608', 'jpg'),
(57, '2019-11-06 23:47:19.859', 'jpg'),
(58, '2019-11-06 23:47:22.308', 'jpg'),
(70, '2019-11-06 23:52:49.208', 'jpg'),
(71, '2019-11-06 23:52:51.565', 'jpg'),
(83, '2019-11-06 23:58:20.886', 'jpg'),
(84, '2019-11-06 23:58:23.749', 'jpg'),
(96, '2019-11-07 00:03:50.308', 'jpg'),
(97, '2019-11-07 00:03:52.667', 'jpg'),
(109, '2019-11-07 00:09:21.688', 'jpg'),
(110, '2019-11-07 00:09:24.238', 'jpg'),
(122, '2019-11-07 00:14:53.765', 'jpg'),
(123, '2019-11-07 00:14:56.237', 'jpg'),
(135, '2019-11-07 00:20:26.385', 'jpg'),
(7, '2019-11-06 23:25:20.623', 'jpg'),
(9, '2019-11-06 23:25:23.794', 'jpg'),
(10, '2019-11-06 23:25:26.668', 'jpg'),
(11, '2019-11-06 23:25:30.039', 'jpg'),
(12, '2019-11-06 23:25:32.611', 'jpg'),
(13, '2019-11-06 23:25:34.960', 'jpg'),
(14, '2019-11-06 23:25:37.326', 'jpg'),
(15, '2019-11-06 23:25:39.757', 'jpg'),
(16, '2019-11-06 23:25:42.225', 'jpg'),
(17, '2019-11-06 23:25:44.596', 'jpg'),
(20, '2019-11-06 23:30:52.307', 'jpg'),
(22, '2019-11-06 23:30:54.751', 'jpg'),
(23, '2019-11-06 23:30:57.121', 'jpg'),
(24, '2019-11-06 23:30:59.462', 'jpg'),
(25, '2019-11-06 23:31:01.820', 'jpg'),
(26, '2019-11-06 23:31:04.283', 'jpg'),
(27, '2019-11-06 23:31:06.727', 'jpg'),
(28, '2019-11-06 23:31:09.193', 'jpg'),
(29, '2019-11-06 23:31:11.733', 'jpg'),
(30, '2019-11-06 23:36:15.554', 'jpg'),
(33, '2019-11-06 23:36:23.635', 'jpg'),
(35, '2019-11-06 23:36:26.368', 'jpg'),
(36, '2019-11-06 23:36:28.758', 'jpg'),
(37, '2019-11-06 23:36:31.211', 'jpg'),
(38, '2019-11-06 23:36:33.553', 'jpg'),
(39, '2019-11-06 23:36:35.912', 'jpg'),
(40, '2019-11-06 23:36:38.281', 'jpg'),
(41, '2019-11-06 23:36:40.619', 'jpg'),
(42, '2019-11-06 23:41:43.433', 'jpg'),
(43, '2019-11-06 23:41:45.795', 'jpg'),
(46, '2019-11-06 23:41:52.961', 'jpg'),
(48, '2019-11-06 23:41:55.320', 'jpg'),
(49, '2019-11-06 23:41:57.674', 'jpg'),
(50, '2019-11-06 23:42:00.058', 'jpg'),
(51, '2019-11-06 23:42:02.543', 'jpg'),
(52, '2019-11-06 23:42:05.357', 'jpg'),
(53, '2019-11-06 23:42:07.792', 'jpg'),
(54, '2019-11-06 23:47:11.746', 'jpg'),
(55, '2019-11-06 23:47:14.227', 'jpg'),
(56, '2019-11-06 23:47:16.679', 'jpg'),
(59, '2019-11-06 23:47:24.690', 'jpg'),
(61, '2019-11-06 23:47:27.242', 'jpg'),
(62, '2019-11-06 23:47:29.784', 'jpg'),
(63, '2019-11-06 23:47:32.145', 'jpg'),
(64, '2019-11-06 23:47:34.622', 'jpg'),
(65, '2019-11-06 23:47:36.969', 'jpg'),
(66, '2019-11-06 23:52:39.769', 'jpg'),
(67, '2019-11-06 23:52:42.153', 'jpg'),
(68, '2019-11-06 23:52:44.500', 'jpg'),
(69, '2019-11-06 23:52:46.851', 'jpg'),
(72, '2019-11-06 23:52:53.928', 'jpg'),
(74, '2019-11-06 23:52:56.399', 'jpg'),
(75, '2019-11-06 23:52:58.748', 'jpg'),
(76, '2019-11-06 23:53:01.087', 'jpg'),
(77, '2019-11-06 23:53:03.445', 'jpg'),
(78, '2019-11-06 23:58:06.835', 'jpg'),
(79, '2019-11-06 23:58:09.804', 'jpg'),
(80, '2019-11-06 23:58:12.261', 'jpg'),
(81, '2019-11-06 23:58:15.050', 'jpg'),
(82, '2019-11-06 23:58:17.418', 'jpg'),
(85, '2019-11-06 23:58:26.108', 'jpg'),
(87, '2019-11-06 23:58:28.452', 'jpg'),
(88, '2019-11-06 23:58:30.819', 'jpg'),
(89, '2019-11-06 23:58:33.280', 'jpg'),
(90, '2019-11-07 00:03:35.986', 'jpg'),
(91, '2019-11-07 00:03:38.352', 'jpg'),
(92, '2019-11-07 00:03:40.686', 'jpg'),
(93, '2019-11-07 00:03:43.157', 'jpg'),
(94, '2019-11-07 00:03:45.521', 'jpg'),
(95, '2019-11-07 00:03:47.949', 'jpg'),
(98, '2019-11-07 00:03:55.023', 'jpg'),
(100, '2019-11-07 00:03:57.373', 'jpg'),
(101, '2019-11-07 00:03:59.662', 'jpg'),
(102, '2019-11-07 00:09:02.507', 'jpg'),
(103, '2019-11-07 00:09:05.210', 'jpg'),
(104, '2019-11-07 00:09:07.668', 'jpg'),
(105, '2019-11-07 00:09:10.515', 'jpg'),
(106, '2019-11-07 00:09:12.975', 'jpg'),
(107, '2019-11-07 00:09:16.246', 'jpg'),
(108, '2019-11-07 00:09:18.719', 'jpg'),
(111, '2019-11-07 00:09:26.900', 'jpg'),
(113, '2019-11-07 00:09:29.865', 'jpg'),
(114, '2019-11-07 00:14:34.182', 'jpg'),
(115, '2019-11-07 00:14:36.788', 'jpg'),
(116, '2019-11-07 00:14:39.628', 'jpg'),
(117, '2019-11-07 00:14:41.991', 'jpg'),
(118, '2019-11-07 00:14:44.348', 'jpg'),
(119, '2019-11-07 00:14:46.703', 'jpg'),
(120, '2019-11-07 00:14:49.055', 'jpg'),
(121, '2019-11-07 00:14:51.414', 'jpg'),
(124, '2019-11-07 00:14:58.608', 'jpg'),
(126, '2019-11-07 00:20:01.353', 'jpg'),
(127, '2019-11-07 00:20:03.730', 'jpg'),
(128, '2019-11-07 00:20:06.225', 'jpg'),
(129, '2019-11-07 00:20:09.034', 'jpg'),
(130, '2019-11-07 00:20:11.541', 'jpg'),
(131, '2019-11-07 00:20:14.699', 'jpg'),
(132, '2019-11-07 00:20:17.635', 'jpg'),
(133, '2019-11-07 00:20:20.133', 'jpg'),
(134, '2019-11-07 00:20:23.497', 'jpg');
--
-- RELAZIONI PER TABELLA `Foto`:
--   `idUtente`
--       `Utenti` -> `id`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Province`
--
-- Creazione: Ott 08, 2019 alle 18:12
-- Ultimo aggiornamento: Ott 08, 2019 alle 18:12
-- Ultimo controllo: Ott 08, 2019 alle 18:12
--

DROP TABLE IF EXISTS `Province`;
CREATE TABLE `Province` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `regione` varchar(30) NOT NULL,
  `sigla` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `Province`:
--

--
-- Svuota la tabella prima dell'inserimento `Province`
--

TRUNCATE TABLE `Province`;
--
-- Dump dei dati per la tabella `Province`
--

INSERT INTO `Province` (`id`, `nome`, `regione`, `sigla`) VALUES
(1, 'Agrigento', 'Sicilia', 'AG'),
(2, 'Alessandria', 'Piemonte', 'AL'),
(3, 'Ancona', 'Marche', 'AN'),
(4, 'Aosta', 'Valle d\'Aosta', 'AO'),
(5, 'Arezzo', 'Toscana', 'AR'),
(6, 'Ascoli Piceno', 'Marche', 'AP'),
(7, 'Asti', 'Piemonte', 'AT'),
(8, 'Avellino', 'Campania', 'AV'),
(9, 'Bari', 'Puglia', 'BA'),
(10, 'Barletta-Andria-Trani', 'Puglia', 'BT'),
(11, 'Belluno', 'Veneto', 'BL'),
(12, 'Benevento', 'Campania', 'BN'),
(13, 'Bergamo', 'Lombardia', 'BG'),
(14, 'Biella', 'Piemonte', 'BI'),
(15, 'Bologna', 'Emilia-Romagna', 'BO'),
(16, 'Bolzano', 'Trentino-Alto Adige', 'BZ'),
(17, 'Brescia', 'Lombardia', 'BS'),
(18, 'Brindisi', 'Puglia', 'BR'),
(19, 'Cagliari', 'Sardegna', 'CA'),
(20, 'Caltanissetta', 'Sicilia', 'CL'),
(21, 'Campobasso', 'Molise', 'CB'),
(22, 'Caserta', 'Campania', 'CE'),
(23, 'Catania', 'Sicilia', 'CT'),
(24, 'Catanzaro', 'Calabria', 'CZ'),
(25, 'Chieti', 'Abruzzo', 'CH'),
(26, 'Como', 'Lombardia', 'CO'),
(27, 'Cosenza', 'Calabria', 'CS'),
(28, 'Cremona', 'Lombardia', 'CR'),
(29, 'Crotone', 'Calabria', 'KR'),
(30, 'Cuneo', 'Piemonte', 'CN'),
(31, 'Enna', 'Sicilia', 'EN'),
(32, 'Fermo', 'Marche', 'FM'),
(33, 'Ferrara', 'Emilia-Romagna', 'FE'),
(34, 'Firenze', 'Toscana', 'FI'),
(35, 'Foggia', 'Puglia', 'FG'),
(36, 'Forlì-Cesena', 'Emilia-Romagna', 'FC'),
(37, 'Frosinone', 'Lazio', 'FR'),
(38, 'Genova', 'Liguria', 'GE'),
(39, 'Gorizia', 'Friuli-Venezia Giulia', 'GO'),
(40, 'Grosseto', 'Toscana', 'GR'),
(41, 'Imperia', 'Liguria', 'IM'),
(42, 'Isernia', 'Molise', 'IS'),
(43, 'L\'Aquila', 'Abruzzo', 'AQ'),
(44, 'La Spezia', 'Liguria', 'SP'),
(45, 'Latina', 'Lazio', 'LT'),
(46, 'Lecce', 'Puglia', 'LE'),
(47, 'Lecco', 'Lombardia', 'LC'),
(48, 'Livorno', 'Toscana', 'LI'),
(49, 'Lodi', 'Lombardia', 'LO'),
(50, 'Lucca', 'Toscana', 'LU'),
(51, 'Macerata', 'Marche', 'MC'),
(52, 'Mantova', 'Lombardia', 'MN'),
(53, 'Massa-Carrara', 'Toscana', 'MS'),
(54, 'Matera', 'Basilicata', 'MT'),
(55, 'Messina', 'Sicilia', 'ME'),
(56, 'Milano', 'Lombardia', 'MI'),
(57, 'Modena', 'Emilia-Romagna', 'MO'),
(58, 'Monza e Brianza', 'Lombardia', 'MB'),
(59, 'Napoli', 'Campania', 'NA'),
(60, 'Novara', 'Piemonte', 'NO'),
(61, 'Nuoro', 'Sardegna', 'NU'),
(62, 'Oristano', 'Sardegna', 'OR'),
(63, 'Padova', 'Veneto', 'PD'),
(64, 'Palermo', 'Sicilia', 'PA'),
(65, 'Parma', 'Emilia-Romagna', 'PR'),
(66, 'Pavia', 'Lombardia', 'PV'),
(67, 'Perugia', 'Umbria', 'PG'),
(68, 'Pesaro e Urbino', 'Marche', 'PU'),
(69, 'Pescara', 'Abruzzo', 'PE'),
(70, 'Piacenza', 'Emilia-Romagna', 'PC'),
(71, 'Pisa', 'Toscana', 'PI'),
(72, 'Pistoia', 'Toscana', 'PT'),
(73, 'Pordenone', 'Friuli-Venezia Giulia', 'PN'),
(74, 'Potenza', 'Basilicata', 'PZ'),
(75, 'Prato', 'Toscana', 'PO'),
(76, 'Ragusa', 'Sicilia', 'RG'),
(77, 'Ravenna', 'Emilia-Romagna', 'RA'),
(78, 'Reggio Calabria', 'Calabria', 'RC'),
(79, 'Reggio Emilia', 'Emilia-Romagna', 'RE'),
(80, 'Rieti', 'Lazio', 'RI'),
(81, 'Rimini', 'Emilia-Romagna', 'RN'),
(82, 'Roma', 'Lazio', 'RM'),
(83, 'Rovigo', 'Veneto', 'RO'),
(84, 'Salerno', 'Campania', 'SA'),
(85, 'Sassari', 'Sardegna', 'SS'),
(86, 'Savona', 'Liguria', 'SV'),
(87, 'Siena', 'Toscana', 'SI'),
(88, 'Siracusa', 'Sicilia', 'SR'),
(89, 'Sondrio', 'Lombardia', 'SO'),
(90, 'Sud Sardegna', 'Sardegna', 'SU'),
(91, 'Taranto', 'Puglia', 'TA'),
(92, 'Teramo', 'Abruzzo', 'TE'),
(93, 'Terni', 'Umbria', 'TR'),
(94, 'Torino', 'Piemonte', 'TO'),
(95, 'Trapani', 'Sicilia', 'TP'),
(96, 'Trento', 'Trentino-Alto Adige', 'TN'),
(97, 'Treviso', 'Veneto', 'TV'),
(98, 'Trieste', 'Friuli-Venezia Giulia', 'TS'),
(99, 'Udine', 'Friuli-Venezia Giulia', 'UD'),
(100, 'Varese', 'Lombardia', 'VA'),
(101, 'Venezia', 'Veneto', 'VE'),
(102, 'Verbano-Cusio-Ossola', 'Piemonte', 'VB'),
(103, 'Vercelli', 'Piemonte', 'VC'),
(104, 'Verona', 'Veneto', 'VR'),
(105, 'Vibo Valentia', 'Calabria', 'VV'),
(106, 'Vicenza', 'Veneto', 'VI'),
(107, 'Viterbo', 'Lazio', 'VT');

-- --------------------------------------------------------

--
-- Struttura della tabella `RecuperoPassword`
--
-- Creazione: Set 28, 2019 alle 14:35
-- Ultimo aggiornamento: Ott 02, 2019 alle 14:33
--

DROP TABLE IF EXISTS `RecuperoPassword`;
CREATE TABLE `RecuperoPassword` (
  `idUtente` int(11) NOT NULL,
  `token` varchar(100) NOT NULL,
  `dataOraGenerazione` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `RecuperoPassword`:
--   `idUtente`
--       `Utenti` -> `id`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `RicettePrescritte`
--
-- Creazione: Set 28, 2019 alle 14:35
-- Ultimo aggiornamento: Ott 10, 2019 alle 19:41
--

DROP TABLE IF EXISTS `RicettePrescritte`;
CREATE TABLE `RicettePrescritte` (
  `idDottore` int(11) NOT NULL,
  `idPaziente` int(11) NOT NULL,
  `dataOraPrescrizione` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descrizione` text NOT NULL,
  `costo` float(10,2) NOT NULL DEFAULT '3.00' COMMENT '€',
  `idFarmacia` int(11) DEFAULT NULL,
  `dataOraEvasione` timestamp(3) NULL DEFAULT NULL COMMENT 'Null = non ancora evaso'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Prescrizioni';

--
-- RELAZIONI PER TABELLA `RicettePrescritte`:
--   `idDottore`
--       `Utenti` -> `id`
--   `idFarmacia`
--       `Utenti` -> `id`
--   `idMedico`
--       `Utenti` -> `id`
--   `idPaziente`
--       `Utenti` -> `id`
--

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `RicettePrescritteView`
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `RicettePrescritteView`;
CREATE TABLE `RicettePrescritteView` (
`idDottore` int(11)
,`nomeDottore` varchar(100)
,`cognomeDottore` varchar(100)
,`nomeProvinciaDottore` varchar(60)
,`siglaProvinciaDottore` varchar(5)
,`DOCFotoDottore` timestamp(3)
,`extensionFotoDottore` varchar(10)
,`idPaziente` int(11)
,`nomePaziente` varchar(100)
,`cognomePaziente` varchar(100)
,`cfPaziente` varchar(17)
,`nomeProvinciaPaziente` varchar(60)
,`siglaProvinciaPaziente` varchar(5)
,`DOCFotoPaziente` timestamp(3)
,`extensionFotoPaziente` varchar(10)
,`dataOraPrescrizione` timestamp(3)
,`costo` float(10,2)
,`idFarmacia` int(11)
,`nomeErogatore` varchar(100)
,`nomeProvinciaErogatore` varchar(60)
,`siglaProvinciaErogatore` varchar(5)
,`DOCFotoErogatore` timestamp(3)
,`extensionFotoErogatore` varchar(10)
,`ruoloErogatore` int(3)
,`descrizione` text
,`dataOraEvasione` timestamp(3)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `Ruoli`
--
-- Creazione: Ago 17, 2019 alle 08:19
-- Ultimo aggiornamento: Ago 22, 2019 alle 14:34
-- Ultimo controllo: Ott 06, 2019 alle 00:08
--

DROP TABLE IF EXISTS `Ruoli`;
CREATE TABLE `Ruoli` (
  `idRuolo` int(11) NOT NULL,
  `descrizione` varchar(30) NOT NULL,
  `isPaziente` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `Ruoli`:
--

--
-- Svuota la tabella prima dell'inserimento `Ruoli`
--

TRUNCATE TABLE `Ruoli`;
--
-- Dump dei dati per la tabella `Ruoli`
--

INSERT INTO `Ruoli` (`idRuolo`, `descrizione`, `isPaziente`) VALUES
(0, 'Cittadino', 1),
(1, 'Medico di Base', 1),
(2, 'Medico Specialista', 1),
(3, 'Servizio Sanitario Provinciale', 0),
(4, 'Farmacia', 0),
(5, 'Servizio Sanitario Nazionale', 0);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `Ticket`
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `Ticket`;
CREATE TABLE `Ticket` (
`idDottore` int(11)
,`idPaziente` int(11)
,`dataOraPrescrizione` timestamp(3)
,`descrizione` mediumtext
,`ruoloErogatore` int(11)
,`idErogatore` int(11)
,`costo` float(10,2)
,`dataOraEvasione` timestamp(3)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `TicketView`
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `TicketView`;
CREATE TABLE `TicketView` (
`idDottore` int(11)
,`nomeDottore` varchar(100)
,`cognomeDottore` varchar(100)
,`nomeProvinciaDottore` varchar(60)
,`siglaProvinciaDottore` varchar(5)
,`DOCFotoDottore` timestamp(3)
,`extensionFotoDottore` varchar(10)
,`idPaziente` int(11)
,`nomePaziente` varchar(100)
,`cognomePaziente` varchar(100)
,`cfPaziente` varchar(17)
,`nomeProvinciaPaziente` varchar(60)
,`siglaProvinciaPaziente` varchar(5)
,`DOCFotoPaziente` timestamp(3)
,`extensionFotoPaziente` varchar(10)
,`dataOraPrescrizione` timestamp(3)
,`costo` float(10,2)
,`idErogatore` int(11)
,`nomeErogatore` varchar(100)
,`cognomeErogatore` varchar(100)
,`nomeProvinciaErogatore` varchar(60)
,`siglaProvinciaErogatore` varchar(5)
,`DOCFotoErogatore` timestamp(3)
,`extensionFotoErogatore` varchar(10)
,`ruoloErogatore` int(11)
,`descrizione` mediumtext
,`dataOraEvasione` timestamp(3)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `UltimaFoto`
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `UltimaFoto`;
CREATE TABLE `UltimaFoto` (
`idUtente` int(11)
,`dataOraCreazione` timestamp(3)
,`extension` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `UtenteView`
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `UtenteView`;
CREATE TABLE `UtenteView` (
`id` int(11)
,`nome` varchar(100)
,`cognome` varchar(100)
,`email` varchar(100)
,`password` varchar(255)
,`provincia` int(11)
,`nomeProvincia` varchar(60)
,`siglaProvincia` varchar(5)
,`sesso` varchar(2)
,`cf` varchar(17)
,`dataNascita` date
,`comuneNascita` varchar(100)
,`ruolo` int(3)
,`dataOraCreazioneFoto` timestamp(3)
,`extensionFoto` varchar(10)
,`idMedicoBase` int(11)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `Utenti`
--
-- Creazione: Ott 04, 2019 alle 08:55
-- Ultimo aggiornamento: Ott 11, 2019 alle 06:49
--

DROP TABLE IF EXISTS `Utenti`;
CREATE TABLE `Utenti` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cognome` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `provincia` int(11) NOT NULL,
  `sesso` varchar(2) DEFAULT NULL COMMENT 'M - F - A',
  `cf` varchar(17) DEFAULT NULL,
  `dataNascita` date DEFAULT NULL,
  `comuneNascita` varchar(100) DEFAULT NULL,
  `ruolo` int(3) NOT NULL DEFAULT '0',
  `idMedicoBase` int(11) DEFAULT NULL COMMENT 'diverso da se stesso'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `Utenti`:
--   `idMedicoBase`
--       `Utenti` -> `id`
--   `provincia`
--       `Province` -> `id`
--   `ruolo`
--       `Ruoli` -> `idRuolo`
--
--
-- Svuota la tabella prima dell'inserimento `Utenti`
--

TRUNCATE TABLE `Utenti`;
--
-- Dump dei dati per la tabella `Utenti`
--

INSERT INTO `Utenti` (`id`, `nome`, `cognome`, `email`, `password`, `provincia`, `sesso`, `cf`, `dataNascita`, `comuneNascita`, `ruolo`, `idMedicoBase`) VALUES
(6, 'Ortensio', 'Fremantle', 'frmrns56m23a378a@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'FRMRNS56M23A378A', '1956-08-23', 'Acquanegra Cremonese', 1, 0),
(18, 'Luigi', 'Frontini', 'frnlgu41m27b476t@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'FRNLGU41M27B476T', '1941-08-27', 'Acquanegra Cremonese', 2, 6),
(19, 'Giacinto', 'Pofferi', 'pffgnt92t03c804k@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'PFFGNT92T03C804K', '1992-12-03', 'Agnadello', 1, 0),
(31, 'Miriam', 'Marcedone', 'mrcmrm32p58f744n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'MRCMRM32P58F744N', '1932-09-18', 'Agnadello', 2, 19),
(32, 'Chantal', 'Louis', 'lsocnt83b41f702x@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'LSOCNT83B41F702X', '1983-02-01', 'Annicco', 1, 0),
(44, 'Isacco', 'Jeridi', 'jrdscc59d29e133g@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'JRDSCC59D29E133G', '1959-04-29', 'Annicco', 2, 32),
(45, 'Alfiero', 'Cobau', 'cbolfr39t09i051t@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'CBOLFR39T09I051T', '1939-12-09', 'Azzanello', 1, 0),
(57, 'Gustavina', 'Fonzeca', 'fnzgtv31m50l843h@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'FNZGTV31M50L843H', '1931-08-10', 'Azzanello', 2, 45),
(58, 'Attilia', 'Medde', 'mddttl84r44c445o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'MDDTTL84R44C445O', '1984-10-04', 'Bagnolo Cremasco', 1, 0),
(70, 'Vincenza', 'Cendret', 'cndvcn45s63l053x@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'CNDVCN45S63L053X', '1945-11-23', 'Bagnolo Cremasco', 2, 58),
(71, 'Emilio', 'Bosia', 'bsomle39d21g210j@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'BSOMLE39D21G210J', '1939-04-21', 'Bonemerse', 1, 0),
(83, 'Viviano', 'Gallusi', 'gllvvn42e02f120d@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'GLLVVN42E02F120D', '1942-05-02', 'Bonemerse', 2, 71),
(84, 'Dorotea', 'Pivato', 'pvtdrt36h52i091w@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'PVTDRT36H52I091W', '1936-06-12', 'Andria', 1, 0),
(96, 'Assunta', 'Zadeo', 'zdasnt68a61c723m@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'ZDASNT68A61C723M', '1968-01-21', 'Andria', 2, 84),
(97, 'Morena', 'Romanati', 'rmnmrn85c65e164k@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'RMNMRN85C65E164K', '1985-03-25', 'Barletta', 1, 0),
(109, 'Aurelio', 'Fonini', 'fnnrla86t01h451x@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'FNNRLA86T01H451X', '1986-12-01', 'Barletta', 2, 97),
(110, 'Alida', 'Ciabochi', 'cbclda79r56d837v@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'CBCLDA79R56D837V', '1979-10-16', 'Bisceglie', 1, 0),
(122, 'Edvige', 'Marcolin', 'mrcdvg89r68f333m@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'MRCDVG89R68F333M', '1989-10-28', 'Bisceglie', 2, 110),
(123, 'Geltrude', 'Bedin', 'bdngtr90d51i663z@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'BDNGTR90D51I663Z', '1990-04-11', 'Canosa di Puglia', 1, 0),
(135, 'Clelia', 'Grandilli', 'grncll89m68i878l@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'GRNCLL89M68I878L', '1989-08-28', 'Canosa di Puglia', 2, 123),
(7, 'Flaviano', 'Dassa', 'dssfvn98r17h861w@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'DSSFVN98R17H861W', '1998-10-17', 'San Daniele Po', 0, 6),
(9, 'Ottavia', 'Randolino', 'rndttv42h67b013n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'RNDTTV42H67B013N', '1942-06-27', 'Bordolano', 0, 6),
(10, 'Lucia', 'Finatzer', 'fntlcu35r54a725m@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'FNTLCU35R54A725M', '1935-10-14', 'Rivarolo del Re ed Uniti', 0, 6),
(11, 'Loretta', 'Pedercini', 'pdrltt63l43e439y@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'PDRLTT63L43E439Y', '1963-07-03', 'Cappella Cantone', 0, 6),
(12, 'Concetta', 'Pulicari', 'plccct39r45c840i@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'PLCCCT39R45C840I', '1939-10-05', 'Isola Dovarese', 0, 6),
(13, 'Tranquillo', 'Chameides', 'chmtnq66a23f543p@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'CHMTNQ66A23F543P', '1966-01-23', 'San Martino del Lago', 0, 6),
(14, 'Eustachia', 'Fratianni', 'frtsch82a56h960v@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'FRTSCH82A56H960V', '1982-01-16', 'Paderno Ponchielli', 0, 6),
(15, 'Leo', 'Pedraza', 'pdrleo91l10h461u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'PDRLEO91L10H461U', '1991-07-10', 'Robecco d\'Oglio', 0, 6),
(16, 'Attilia', 'Cianfaglioni', 'cnfttl43r61c494a@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'CNFTTL43R61C494A', '1943-10-21', 'Scandolara Ravara', 0, 6),
(17, 'Bianca', 'Anelotti', 'nltbnc65a47i981r@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'NLTBNC65A47I981R', '1965-01-07', 'Capergnanica', 0, 6),
(20, 'Tea', 'Grigorio', 'grgtea75s47b355a@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'GRGTEA75S47B355A', '1975-11-07', 'Pieve d\'Olmi', 0, 19),
(22, 'Allegra', 'Dulli', 'dlllgr95p62g764n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'DLLLGR95P62G764N', '1995-09-22', 'Calvatone', 0, 19),
(23, 'Sofia', 'Gigliozzi', 'gglsfo96h70l976u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'GGLSFO96H70L976U', '1996-06-30', 'Izano', 0, 19),
(24, 'Germano', 'Lormini', 'lrmgmn77p21c737o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'LRMGMN77P21C737O', '1977-09-21', 'Pianengo', 0, 19),
(25, 'Maurilio', 'Parbotti', 'prbmrl49s02c888e@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'PRBMRL49S02C888E', '1949-11-02', 'Pieve San Giacomo', 0, 19),
(26, 'Baldassarre', 'Buzzella', 'bzzbds80b23h577q@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'BZZBDS80B23H577Q', '1980-02-23', 'Capergnanica', 0, 19),
(27, 'Generosa', 'Jetti', 'jttgrs32e59a251k@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'JTTGRS32E59A251K', '1932-05-19', 'Bonemerse', 0, 19),
(28, 'Fermo', 'Bammannage', 'bmmfrm91l29a034n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'BMMFRM91L29A034N', '1991-07-29', 'Camisano', 0, 19),
(29, 'Anastasio', 'Mezulic', 'mzlnts66h11e421q@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'MZLNTS66H11E421Q', '1966-06-11', 'Quintano', 0, 19),
(30, 'Livio', 'Santorinakis', 'sntlvi81m16d497e@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'SNTLVI81M16D497E', '1981-08-16', 'Casaletto Ceredano', 0, 19),
(33, 'Barbara', 'Janiszewska', 'jnsbbr57r45f067i@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'JNSBBR57R45F067I', '1957-10-05', 'Volongo', 0, 32),
(35, 'Valentina', 'Crambi', 'crmvnt67l54l572u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'CRMVNT67L54L572U', '1967-07-14', 'Montodine', 0, 32),
(36, 'Prudenzio', 'Ghazal', 'ghzpdn35m14h147s@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'GHZPDN35M14H147S', '1935-08-14', 'Chieve', 0, 32),
(37, 'Euridice', 'Lovascio', 'lvsrdc71l62c880w@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'LVSRDC71L62C880W', '1971-07-22', 'Vaiano Cremasco', 0, 32),
(38, 'Ovidio', 'Cucchetti', 'cccvdo65a18l015r@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'CCCVDO65A18L015R', '1965-01-18', 'Pieve d\'Olmi', 0, 32),
(39, 'Raoul', 'Angioi', 'ngarla64d17h858m@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'NGARLA64D17H858M', '1964-04-17', 'Ripalta Guerina', 0, 32),
(40, 'Adone', 'Ligreci', 'lgrdna60d03b928j@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'LGRDNA60D03B928J', '1960-04-03', 'Offanengo', 0, 32),
(41, 'Guglielmina', 'Forcisi', 'frcgll65b56e630p@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'FRCGLL65B56E630P', '1965-02-16', 'Cappella de\' Picenardi', 0, 32),
(42, 'Albino', 'Giorlando', 'grllbn42m17c127y@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'GRLLBN42M17C127Y', '1942-08-17', 'Torricella del Pizzo', 0, 32),
(43, 'Dorotea', 'Scomodon', 'scmdrt80m41b941z@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'SCMDRT80M41B941Z', '1980-08-01', 'Bordolano', 0, 32),
(46, 'Macario', 'Balsanelli', 'blsmcr58s24a512q@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'BLSMCR58S24A512Q', '1958-11-24', 'San Martino del Lago', 0, 45),
(48, 'Germana', 'Sallam', 'sllgmn82l59i538c@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'SLLGMN82L59I538C', '1982-07-19', 'Torlino Vimercati', 0, 45),
(49, 'Agata', 'Santipolo', 'sntgta89m53l634z@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'SNTGTA89M53L634Z', '1989-08-13', 'Soresina', 0, 45),
(50, 'Susanna', 'Buttinelli', 'bttsnn37a47h278c@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'BTTSNN37A47H278C', '1937-01-07', 'Ostiano', 0, 45),
(51, 'Filomena', 'Perier', 'prrfmn97p69f847l@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'PRRFMN97P69F847L', '1997-09-29', 'Gerre de\' Caprioli', 0, 45),
(52, 'Tosca', 'Castiglione', 'csttsc33a51a116y@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'CSTTSC33A51A116Y', '1933-01-11', 'San Bassano', 0, 45),
(53, 'Aladino', 'Areddia', 'rddldn86e19e219g@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'RDDLDN86E19E219G', '1986-05-19', 'Offanengo', 0, 45),
(54, 'Gastone', 'Tracz', 'trcgtn58d25b278o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'TRCGTN58D25B278O', '1958-04-25', 'Izano', 0, 45),
(55, 'Albino', 'Betto', 'bttlbn39l20g764o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'BTTLBN39L20G764O', '1939-07-20', 'Soncino', 0, 45),
(56, 'Rosalia', 'Cartechini', 'crtrsl33l67a517o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'CRTRSL33L67A517O', '1933-07-27', 'Vescovato', 0, 45),
(59, 'Lorena', 'Abbruzzese', 'bbrlrn54a51f266r@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'BBRLRN54A51F266R', '1954-01-11', 'Castel Gabbiano', 0, 58),
(61, 'Porzia', 'Behin', 'bhnprz77p56d160c@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'BHNPRZ77P56D160C', '1977-09-16', 'Ricengo', 0, 58),
(62, 'Clorinda', 'Casizzone', 'cszcrn82m41l470k@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'CSZCRN82M41L470K', '1982-08-01', 'Solarolo Rainerio', 0, 58),
(63, 'Emiliano', 'Folmi', 'flmmln63p17d157f@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'FLMMLN63P17D157F', '1963-09-17', 'Malagnino', 0, 58),
(64, 'Goffredo', 'Insetta', 'nstgfr35r10h203z@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'NSTGFR35R10H203Z', '1935-10-10', 'Ricengo', 0, 58),
(65, 'Romilda', 'Pelomoro', 'plmrld33d54g962v@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'PLMRLD33D54G962V', '1933-04-14', 'Vaiano Cremasco', 0, 58),
(66, 'Gerolama', 'Cornier', 'crnglm74h49a343p@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'CRNGLM74H49A343P', '1974-06-09', 'Moscazzano', 0, 58),
(67, 'Amos', 'Vignola', 'vgnmsa90d10g654v@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'VGNMSA90D10G654V', '1990-04-10', 'Pozzaglio ed Uniti', 0, 58),
(68, 'Eugenio', 'Criscuoli', 'crsgne53l17a463u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'CRSGNE53L17A463U', '1953-07-17', 'Trescore Cremasco', 0, 58),
(69, 'Domenica', 'Bestagini', 'bstdnc54e44h465b@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'BSTDNC54E44H465B', '1954-04-05', 'Rocchetta Ligure', 0, 58),
(72, 'Odetta', 'Koc', 'kcodtt94r65f486k@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'KCODTT94R65F486K', '1994-10-25', 'Olmeneta', 0, 71),
(74, 'Ruth', 'Ganner', 'gnnrth72e43l320q@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'GNNRTH72E43L320Q', '1972-05-03', 'Casalmaggiore', 0, 71),
(75, 'Baldo', 'Briatico', 'brtbld74d13a760v@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'BRTBLD74D13A760V', '1974-04-13', 'Montodine', 0, 71),
(76, 'Decio', 'Ambuchi', 'mbcdce34e19e826o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'MBCDCE34E19E826O', '1934-05-19', 'Casalmaggiore', 0, 71),
(77, 'Rosalinda', 'Mangiapelo', 'mngrln84a45a421c@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'MNGRLN84A45A421C', '1984-01-05', 'Robecco d\'Oglio', 0, 71),
(78, 'Fiorenzo', 'Sibiloni', 'sblfnz89r15m148w@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'SBLFNZ89R15M148W', '1989-10-15', 'Trigolo', 0, 71),
(79, 'Modesto', 'Setari', 'strmst52s17c732y@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'M', 'STRMST52S17C732Y', '1952-11-17', 'San Bassano', 0, 71),
(80, 'Samantha', 'Nakada', 'nkdsnt63s57i936l@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'NKDSNT63S57I936L', '1963-11-17', 'Camisano', 0, 71),
(81, 'Betta', 'Giubbilini', 'gbbbtt58b50c889x@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'GBBBTT58B50C889X', '1958-02-10', 'Soresina', 0, 71),
(82, 'Palmira', 'Podgajski', 'pdgpmr34c64f562b@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, 'F', 'PDGPMR34C64F562B', '1934-03-24', 'Pieranica', 0, 71),
(85, 'Salvatore', 'Lagosante', 'lgssvt68c21a906f@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'LGSSVT68C21A906F', '1968-03-21', 'San Ferdinando di Puglia', 0, 84),
(87, 'Porzia', 'Cattaino', 'cttprz32s45g081j@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'CTTPRZ32S45G081J', '1932-11-05', 'Spinazzola', 0, 84),
(88, 'Fabia', 'Garruccio', 'grrfba71t44b037i@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'GRRFBA71T44B037I', '1971-12-04', 'Minervino Murge', 0, 84),
(89, 'Diodoro', 'Fidelfi', 'fdlddr94t04e204j@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'FDLDDR94T04E204J', '1994-12-04', 'Andria', 0, 84),
(90, 'Fulgenzio', 'Arsan', 'rsnfgn56e04f148o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'RSNFGN56E04F148O', '1956-05-04', 'Canosa di Puglia', 0, 84),
(91, 'Amabile', 'Ardenni', 'rdnmbl60b21a233i@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'RDNMBL60B21A233I', '1960-02-21', 'Minervino Murge', 0, 84),
(92, 'Altea', 'Ogliarulo', 'glrlta70m52b699u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'GLRLTA70M52B699U', '1970-08-12', 'Minervino Murge', 0, 84),
(93, 'Massimiliano', 'Arontaldi', 'rntmsm55d03a038w@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'RNTMSM55D03A038W', '1955-04-03', 'San Ferdinando di Puglia', 0, 84),
(94, 'Fulgenzio', 'Rissotto', 'rssfgn82r29g681w@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'RSSFGN82R29G681W', '1982-10-29', 'Trani', 0, 84),
(95, 'Dacio', 'Trettl', 'trtdca54t04c851p@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'TRTDCA54T04C851P', '1954-12-04', 'Margherita di Savoia', 0, 84),
(98, 'Eufrasia', 'Peccenati', 'pccfrs41m54g385n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'PCCFRS41M54G385N', '1941-08-14', 'San Ferdinando di Puglia', 0, 97),
(100, 'Iva', 'Guenzi', 'gnzvia89m57b586o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'GNZVIA89M57B586O', '1989-08-17', 'Minervino Murge', 0, 97),
(101, 'Simeone', 'Nomieri', 'nmrsmn95m22l237u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'NMRSMN95M22L237U', '1995-08-22', 'Andria', 0, 97),
(102, 'Rosalia', 'Sferragatti', 'sfrrsl67a51l646i@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'SFRRSL67A51L646I', '1967-01-11', 'Barletta', 0, 97),
(103, 'Onesta', 'Turska', 'trsnst75c60c885h@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'TRSNST75C60C885H', '1975-03-20', 'Trani', 0, 97),
(104, 'Alice', 'Caserto', 'csrlca64m52f282n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'CSRLCA64M52F282N', '1964-08-12', 'San Ferdinando di Puglia', 0, 97),
(105, 'Giacobbe', 'Muscat', 'mscgbb99h28h212e@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'MSCGBB99H28H212E', '1999-06-28', 'Bisceglie', 0, 97),
(106, 'Diogene', 'Giovinali', 'gvndgn41a03h807s@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'GVNDGN41A03H807S', '1941-01-03', 'Minervino Murge', 0, 97),
(107, 'Zefiro', 'Macumelli', 'mcmzfr54a07m377p@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'MCMZFR54A07M377P', '1954-01-07', 'Bisceglie', 0, 97),
(108, 'Decio', 'Guagnin', 'ggndce88h13f208e@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'GGNDCE88H13F208E', '1988-06-13', 'Spinazzola', 0, 97),
(111, 'Eufemia', 'Konemann', 'knmfme52l43g476x@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'KNMFME52L43G476X', '1952-07-03', 'Trani', 0, 110),
(113, 'Salomone', 'Barausse', 'brssmn91c16e034i@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'BRSSMN91C16E034I', '1991-03-16', 'Barletta', 0, 110),
(114, 'Tosca', 'Iantoli', 'ntltsc87b68g821y@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'NTLTSC87B68G821Y', '1987-02-28', 'Bisceglie', 0, 110),
(115, 'Ulderico', 'Merluccio', 'mrllrc76p24g063n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'MRLLRC76P24G063N', '1976-09-24', 'Canosa di Puglia', 0, 110),
(116, 'Rosaria', 'Franforte', 'frnrsr52p41d194j@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'FRNRSR52P41D194J', '1952-09-01', 'Spinazzola', 0, 110),
(117, 'Battista', 'Meisterhans', 'mstbts63r17b157u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'MSTBTS63R17B157U', '1963-10-17', 'Trani', 0, 110),
(118, 'Celeste', 'Bues', 'bsucst47m52m074k@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'BSUCST47M52M074K', '1947-08-12', 'Margherita di Savoia', 0, 110),
(119, 'Berenice', 'Tersitti', 'trsbnc75r62b833h@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'TRSBNC75R62B833H', '1975-10-22', 'Trani', 0, 110),
(120, 'Nico', 'Gromme', 'grmnci69d12c289t@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'GRMNCI69D12C289T', '1969-04-12', 'Trani', 0, 110),
(121, 'Stefania', 'Biancastelli', 'bncsfn65b62a256o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'BNCSFN65B62A256O', '1965-02-22', 'Andria', 0, 110),
(124, 'Generoso', 'Campagnini', 'cmpgrs00r23e391m@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'CMPGRS00R23E391M', '2000-10-23', 'Spinazzola', 0, 123),
(126, 'Quirina', 'Jahja', 'jhjqrn60t43b257y@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'JHJQRN60T43B257Y', '1960-12-03', 'Barletta', 0, 123),
(127, 'Renata', 'Alushaj', 'lshrnt97s60c200u@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'LSHRNT97S60C200U', '1997-11-20', 'Minervino Murge', 0, 123),
(128, 'Fosca', 'Plazzi', 'plzfsc93a57f010h@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'PLZFSC93A57F010H', '1993-01-17', 'Bisceglie', 0, 123),
(129, 'Osvaldo', 'Manula', 'mnlsld44p17h016o@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'MNLSLD44P17H016O', '1944-09-17', 'Margherita di Savoia', 0, 123),
(130, 'Eusebio', 'Rossut', 'rsssbe95e22h882l@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'RSSSBE95E22H882L', '1995-05-22', 'Canosa di Puglia', 0, 123),
(131, 'Ersilia', 'Boaretto', 'brtrsl54a61g611n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'BRTRSL54A61G611N', '1954-01-21', 'Trani', 0, 123),
(132, 'Estella', 'Prigoliti', 'prgsll76m59h890p@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'PRGSLL76M59H890P', '1976-08-19', 'Trani', 0, 123),
(133, 'Gherardo', 'Sagoleo', 'sglgrr32d16g740j@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'M', 'SGLGRR32D16G740J', '1932-04-16', 'San Ferdinando di Puglia', 0, 123),
(134, 'Ignazia', 'Pelà', 'plegnz50p57e063a@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, 'F', 'PLEGNZ50P57E063A', '1950-09-17', 'Canosa di Puglia', 0, 123),
(8, 'Farmacia di San Daniele Po', NULL, 'farmacia.dssfvn98r17h861w@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, NULL, NULL, NULL, NULL, 4, NULL),
(21, 'Farmacia di Pieve d\'Olmi', NULL, 'farmacia.grgtea75s47b355a@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, NULL, NULL, NULL, NULL, 4, NULL),
(34, 'Farmacia di Volongo', NULL, 'farmacia.jnsbbr57r45f067i@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, NULL, NULL, NULL, NULL, 4, NULL),
(47, 'Farmacia di San Martino del Lago', NULL, 'farmacia.blsmcr58s24a512q@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, NULL, NULL, NULL, NULL, 4, NULL),
(60, 'Farmacia di Castel Gabbiano', NULL, 'farmacia.bbrlrn54a51f266r@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, NULL, NULL, NULL, NULL, 4, NULL),
(73, 'Farmacia di Olmeneta', NULL, 'farmacia.kcodtt94r65f486k@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, NULL, NULL, NULL, NULL, 4, NULL),
(86, 'Farmacia di San Ferdinando di Puglia', NULL, 'farmacia.lgssvt68c21a906f@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, NULL, NULL, NULL, NULL, 4, NULL),
(99, 'Farmacia di San Ferdinando di Puglia', NULL, 'farmacia.pccfrs41m54g385n@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, NULL, NULL, NULL, NULL, 4, NULL),
(112, 'Farmacia di Trani', NULL, 'farmacia.knmfme52l43g476x@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, NULL, NULL, NULL, NULL, 4, NULL),
(125, 'Farmacia di Spinazzola', NULL, 'farmacia.cmpgrs00r23e391m@mailstop.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, NULL, NULL, NULL, NULL, 4, NULL),
(136, 'Agrigento', NULL, 'agrigento@ag.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 1, NULL, NULL, NULL, NULL, 3, NULL),
(137, 'Alessandria', NULL, 'alessandria@al.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 2, NULL, NULL, NULL, NULL, 3, NULL),
(138, 'Ancona', NULL, 'ancona@an.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 3, NULL, NULL, NULL, NULL, 3, NULL),
(139, 'Aosta', NULL, 'aosta@ao.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 4, NULL, NULL, NULL, NULL, 3, NULL),
(140, 'Arezzo', NULL, 'arezzo@ar.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 5, NULL, NULL, NULL, NULL, 3, NULL),
(141, 'Ascoli Piceno', NULL, 'ascolipiceno@ap.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 6, NULL, NULL, NULL, NULL, 3, NULL),
(142, 'Asti', NULL, 'asti@at.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 7, NULL, NULL, NULL, NULL, 3, NULL),
(143, 'Avellino', NULL, 'avellino@av.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 8, NULL, NULL, NULL, NULL, 3, NULL),
(144, 'Bari', NULL, 'bari@ba.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 9, NULL, NULL, NULL, NULL, 3, NULL),
(145, 'Barletta-Andria-Trani', NULL, 'barletta-andria-trani@bt.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 10, NULL, NULL, NULL, NULL, 3, NULL),
(146, 'Belluno', NULL, 'belluno@bl.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 11, NULL, NULL, NULL, NULL, 3, NULL),
(147, 'Benevento', NULL, 'benevento@bn.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 12, NULL, NULL, NULL, NULL, 3, NULL),
(148, 'Bergamo', NULL, 'bergamo@bg.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 13, NULL, NULL, NULL, NULL, 3, NULL),
(149, 'Biella', NULL, 'biella@bi.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 14, NULL, NULL, NULL, NULL, 3, NULL),
(150, 'Bologna', NULL, 'bologna@bo.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 15, NULL, NULL, NULL, NULL, 3, NULL),
(151, 'Bolzano', NULL, 'bolzano@bz.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 16, NULL, NULL, NULL, NULL, 3, NULL),
(152, 'Brescia', NULL, 'brescia@bs.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 17, NULL, NULL, NULL, NULL, 3, NULL),
(153, 'Brindisi', NULL, 'brindisi@br.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 18, NULL, NULL, NULL, NULL, 3, NULL),
(154, 'Cagliari', NULL, 'cagliari@ca.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 19, NULL, NULL, NULL, NULL, 3, NULL),
(155, 'Caltanissetta', NULL, 'caltanissetta@cl.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 20, NULL, NULL, NULL, NULL, 3, NULL),
(156, 'Campobasso', NULL, 'campobasso@cb.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 21, NULL, NULL, NULL, NULL, 3, NULL),
(157, 'Caserta', NULL, 'caserta@ce.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 22, NULL, NULL, NULL, NULL, 3, NULL),
(158, 'Catania', NULL, 'catania@ct.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 23, NULL, NULL, NULL, NULL, 3, NULL),
(159, 'Catanzaro', NULL, 'catanzaro@cz.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 24, NULL, NULL, NULL, NULL, 3, NULL),
(160, 'Chieti', NULL, 'chieti@ch.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 25, NULL, NULL, NULL, NULL, 3, NULL),
(161, 'Como', NULL, 'como@co.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 26, NULL, NULL, NULL, NULL, 3, NULL),
(162, 'Cosenza', NULL, 'cosenza@cs.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 27, NULL, NULL, NULL, NULL, 3, NULL),
(163, 'Cremona', NULL, 'cremona@cr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 28, NULL, NULL, NULL, NULL, 3, NULL),
(164, 'Crotone', NULL, 'crotone@kr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 29, NULL, NULL, NULL, NULL, 3, NULL),
(165, 'Cuneo', NULL, 'cuneo@cn.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 30, NULL, NULL, NULL, NULL, 3, NULL),
(166, 'Enna', NULL, 'enna@en.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 31, NULL, NULL, NULL, NULL, 3, NULL),
(167, 'Fermo', NULL, 'fermo@fm.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 32, NULL, NULL, NULL, NULL, 3, NULL),
(168, 'Ferrara', NULL, 'ferrara@fe.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 33, NULL, NULL, NULL, NULL, 3, NULL),
(169, 'Firenze', NULL, 'firenze@fi.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 34, NULL, NULL, NULL, NULL, 3, NULL),
(170, 'Foggia', NULL, 'foggia@fg.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 35, NULL, NULL, NULL, NULL, 3, NULL),
(171, 'Forlì-Cesena', NULL, 'forlì-cesena@fc.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 36, NULL, NULL, NULL, NULL, 3, NULL),
(172, 'Frosinone', NULL, 'frosinone@fr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 37, NULL, NULL, NULL, NULL, 3, NULL),
(173, 'Genova', NULL, 'genova@ge.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 38, NULL, NULL, NULL, NULL, 3, NULL),
(174, 'Gorizia', NULL, 'gorizia@go.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 39, NULL, NULL, NULL, NULL, 3, NULL),
(175, 'Grosseto', NULL, 'grosseto@gr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 40, NULL, NULL, NULL, NULL, 3, NULL),
(176, 'Imperia', NULL, 'imperia@im.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 41, NULL, NULL, NULL, NULL, 3, NULL),
(177, 'Isernia', NULL, 'isernia@is.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 42, NULL, NULL, NULL, NULL, 3, NULL),
(178, 'L\'Aquila', NULL, 'l\'aquila@aq.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 43, NULL, NULL, NULL, NULL, 3, NULL),
(179, 'La Spezia', NULL, 'laspezia@sp.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 44, NULL, NULL, NULL, NULL, 3, NULL),
(180, 'Latina', NULL, 'latina@lt.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 45, NULL, NULL, NULL, NULL, 3, NULL),
(181, 'Lecce', NULL, 'lecce@le.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 46, NULL, NULL, NULL, NULL, 3, NULL),
(182, 'Lecco', NULL, 'lecco@lc.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 47, NULL, NULL, NULL, NULL, 3, NULL),
(183, 'Livorno', NULL, 'livorno@li.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 48, NULL, NULL, NULL, NULL, 3, NULL),
(184, 'Lodi', NULL, 'lodi@lo.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 49, NULL, NULL, NULL, NULL, 3, NULL),
(185, 'Lucca', NULL, 'lucca@lu.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 50, NULL, NULL, NULL, NULL, 3, NULL),
(186, 'Macerata', NULL, 'macerata@mc.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 51, NULL, NULL, NULL, NULL, 3, NULL),
(187, 'Mantova', NULL, 'mantova@mn.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 52, NULL, NULL, NULL, NULL, 3, NULL),
(188, 'Massa-Carrara', NULL, 'massa-carrara@ms.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 53, NULL, NULL, NULL, NULL, 3, NULL),
(189, 'Matera', NULL, 'matera@mt.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 54, NULL, NULL, NULL, NULL, 3, NULL),
(190, 'Messina', NULL, 'messina@me.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 55, NULL, NULL, NULL, NULL, 3, NULL),
(191, 'Milano', NULL, 'milano@mi.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 56, NULL, NULL, NULL, NULL, 3, NULL),
(192, 'Modena', NULL, 'modena@mo.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 57, NULL, NULL, NULL, NULL, 3, NULL),
(193, 'Monza e Brianza', NULL, 'monzaebrianza@mb.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 58, NULL, NULL, NULL, NULL, 3, NULL),
(194, 'Napoli', NULL, 'napoli@na.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 59, NULL, NULL, NULL, NULL, 3, NULL),
(195, 'Novara', NULL, 'novara@no.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 60, NULL, NULL, NULL, NULL, 3, NULL),
(196, 'Nuoro', NULL, 'nuoro@nu.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 61, NULL, NULL, NULL, NULL, 3, NULL),
(197, 'Oristano', NULL, 'oristano@or.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 62, NULL, NULL, NULL, NULL, 3, NULL),
(198, 'Padova', NULL, 'padova@pd.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 63, NULL, NULL, NULL, NULL, 3, NULL),
(199, 'Palermo', NULL, 'palermo@pa.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 64, NULL, NULL, NULL, NULL, 3, NULL),
(200, 'Parma', NULL, 'parma@pr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 65, NULL, NULL, NULL, NULL, 3, NULL),
(201, 'Pavia', NULL, 'pavia@pv.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 66, NULL, NULL, NULL, NULL, 3, NULL),
(202, 'Perugia', NULL, 'perugia@pg.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 67, NULL, NULL, NULL, NULL, 3, NULL),
(203, 'Pesaro e Urbino', NULL, 'pesaroeurbino@pu.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 68, NULL, NULL, NULL, NULL, 3, NULL),
(204, 'Pescara', NULL, 'pescara@pe.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 69, NULL, NULL, NULL, NULL, 3, NULL),
(205, 'Piacenza', NULL, 'piacenza@pc.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 70, NULL, NULL, NULL, NULL, 3, NULL),
(206, 'Pisa', NULL, 'pisa@pi.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 71, NULL, NULL, NULL, NULL, 3, NULL),
(207, 'Pistoia', NULL, 'pistoia@pt.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 72, NULL, NULL, NULL, NULL, 3, NULL),
(208, 'Pordenone', NULL, 'pordenone@pn.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 73, NULL, NULL, NULL, NULL, 3, NULL),
(209, 'Potenza', NULL, 'potenza@pz.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 74, NULL, NULL, NULL, NULL, 3, NULL),
(210, 'Prato', NULL, 'prato@po.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 75, NULL, NULL, NULL, NULL, 3, NULL),
(211, 'Ragusa', NULL, 'ragusa@rg.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 76, NULL, NULL, NULL, NULL, 3, NULL),
(212, 'Ravenna', NULL, 'ravenna@ra.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 77, NULL, NULL, NULL, NULL, 3, NULL),
(213, 'Reggio Calabria', NULL, 'reggiocalabria@rc.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 78, NULL, NULL, NULL, NULL, 3, NULL),
(214, 'Reggio Emilia', NULL, 'reggioemilia@re.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 79, NULL, NULL, NULL, NULL, 3, NULL),
(215, 'Rieti', NULL, 'rieti@ri.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 80, NULL, NULL, NULL, NULL, 3, NULL),
(216, 'Rimini', NULL, 'rimini@rn.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 81, NULL, NULL, NULL, NULL, 3, NULL),
(217, 'Roma', NULL, 'roma@rm.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 82, NULL, NULL, NULL, NULL, 3, NULL),
(218, 'Rovigo', NULL, 'rovigo@ro.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 83, NULL, NULL, NULL, NULL, 3, NULL),
(219, 'Salerno', NULL, 'salerno@sa.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 84, NULL, NULL, NULL, NULL, 3, NULL),
(220, 'Sassari', NULL, 'sassari@ss.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 85, NULL, NULL, NULL, NULL, 3, NULL),
(221, 'Savona', NULL, 'savona@sv.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 86, NULL, NULL, NULL, NULL, 3, NULL),
(222, 'Siena', NULL, 'siena@si.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 87, NULL, NULL, NULL, NULL, 3, NULL),
(223, 'Siracusa', NULL, 'siracusa@sr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 88, NULL, NULL, NULL, NULL, 3, NULL),
(224, 'Sondrio', NULL, 'sondrio@so.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 89, NULL, NULL, NULL, NULL, 3, NULL),
(225, 'Sud Sardegna', NULL, 'sudsardegna@su.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 90, NULL, NULL, NULL, NULL, 3, NULL),
(226, 'Taranto', NULL, 'taranto@ta.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 91, NULL, NULL, NULL, NULL, 3, NULL),
(227, 'Teramo', NULL, 'teramo@te.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 92, NULL, NULL, NULL, NULL, 3, NULL),
(228, 'Terni', NULL, 'terni@tr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 93, NULL, NULL, NULL, NULL, 3, NULL),
(229, 'Torino', NULL, 'torino@to.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 94, NULL, NULL, NULL, NULL, 3, NULL),
(230, 'Trapani', NULL, 'trapani@tp.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 95, NULL, NULL, NULL, NULL, 3, NULL),
(231, 'Trento', NULL, 'trento@tn.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 96, NULL, NULL, NULL, NULL, 3, NULL),
(232, 'Treviso', NULL, 'treviso@tv.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 97, NULL, NULL, NULL, NULL, 3, NULL),
(233, 'Trieste', NULL, 'trieste@ts.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 98, NULL, NULL, NULL, NULL, 3, NULL),
(234, 'Udine', NULL, 'udine@ud.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 99, NULL, NULL, NULL, NULL, 3, NULL),
(235, 'Varese', NULL, 'varese@va.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 100, NULL, NULL, NULL, NULL, 3, NULL),
(236, 'Venezia', NULL, 'venezia@ve.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 101, NULL, NULL, NULL, NULL, 3, NULL),
(237, 'Verbano-Cusio-Ossola', NULL, 'verbano-cusio-ossola@vb.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 102, NULL, NULL, NULL, NULL, 3, NULL),
(238, 'Vercelli', NULL, 'vercelli@vc.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 103, NULL, NULL, NULL, NULL, 3, NULL),
(239, 'Verona', NULL, 'verona@vr.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 104, NULL, NULL, NULL, NULL, 3, NULL),
(240, 'Vibo Valentia', NULL, 'vibovalentia@vv.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 105, NULL, NULL, NULL, NULL, 3, NULL),
(241, 'Vicenza', NULL, 'vicenza@vi.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 106, NULL, NULL, NULL, NULL, 3, NULL),
(242, 'Viterbo', NULL, 'viterbo@vt.ssp.it', '$2a$11$JnkazrTODJQIxuDC7Yo5SuAcP5wOM9j1RkXIS/QbfBYGqwMWJHoT.', 107, NULL, NULL, NULL, NULL, 3, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `Visite`
--
-- Creazione: Ago 17, 2019 alle 08:19
-- Ultimo aggiornamento: Ott 06, 2019 alle 00:08
-- Ultimo controllo: Ott 06, 2019 alle 00:08
--

DROP TABLE IF EXISTS `Visite`;
CREATE TABLE `Visite` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `Visite`:
--

--
-- Svuota la tabella prima dell'inserimento `Visite`
--

TRUNCATE TABLE `Visite`;
--
-- Dump dei dati per la tabella `Visite`
--

INSERT INTO `Visite` (`id`, `nome`) VALUES
(1, 'Ambulatorio infermieristico'),
(2, 'Colloquio psicologico clinico'),
(3, 'Consigli per adattamento a perdita di vista, uso di aiuti per bassa vista'),
(4, 'Consulenza anatomopatologica per revisione diagnostica di preparati allestiti in altra sede (prescrivibile una sola volta per lo stesso episodio patologico)'),
(5, 'Consultorio'),
(6, 'Controllo e programmazione pace-maker'),
(7, 'Controllo periodico per terapia anticoagulante orale (TAO)'),
(8, 'Controllo protesico elettroacustico'),
(9, 'Controllo/programmazione di neurostimolatore encefalico'),
(10, 'Controllo/programmazione di neurostimolatore spinale'),
(11, 'Medico di guardia ostetrico h24'),
(12, 'Messa a punto e fornitura di occhiali'),
(13, 'Plicometria, valutazione dello stato nutrizionale'),
(14, 'Prescrizione, messa a punto e fornitura di lenti a contatto'),
(15, 'Relazione per organi giudiziari'),
(16, 'Visita Immunologica di controllo'),
(17, 'Visita allergologica'),
(18, 'Visita allergologica di controllo'),
(19, 'Visita anestesiologica/algologica'),
(20, 'Visita anestesiologica/algologica di controllo'),
(21, 'Visita angiologica'),
(22, 'Visita angiologica di controllo'),
(23, 'Visita audiologica, visita foniatrica'),
(24, 'Visita cardiochirurgica'),
(25, 'Visita cardiochirurgica di controllo'),
(26, 'Visita cardiochirurgica pediatrica'),
(27, 'Visita cardiologica di controllo'),
(28, 'Visita cardiologica. Incluso: ECG'),
(29, 'Visita chirurgia di controllo'),
(30, 'Visita chirurgia generale'),
(31, 'Visita chirurgia maxillo facciale'),
(32, 'Visita chirurgia maxillo facciale di controllo'),
(33, 'Visita chirurgia pediatrica'),
(34, 'Visita chirurgia pediatrica di controllo'),
(35, 'Visita chirurgia plastica'),
(36, 'Visita chirurgia plastica di controllo'),
(37, 'Visita chirurgia toracica'),
(38, 'Visita chirurgia toracica di controllo'),
(39, 'Visita chirurgia vascolare'),
(40, 'Visita chirurgia vascolare di controllo'),
(41, 'Visita dentistica - odontostomatologica'),
(42, 'Visita dentistica - odontostomatologica di controllo'),
(43, 'Visita dentistica - odontostomatologica pediatrica'),
(44, 'Visita dermatologica'),
(45, 'Visita dermatologica di controllo'),
(46, 'Visita di medicina estetica'),
(47, 'Visita di medicina estetica di controllo'),
(48, 'Visita di medicina fisica e riabilitazione'),
(49, 'Visita di medicina fisica e riabilitazione di controllo'),
(50, 'Visita di medicina nucleare'),
(51, 'Visita di medicina nucleare di controllo'),
(52, 'Visita di radiologia interventistica'),
(53, 'Visita di radiologia interventistica di controllo'),
(54, 'Visita di radioterapia'),
(55, 'Visita di riabilitazione neurologica'),
(56, 'Visita di riabilitazione neurologica di controllo'),
(57, 'Visita dietologica'),
(58, 'Visita dietologica di controllo'),
(59, 'Visita ematologica'),
(60, 'Visita ematologica di controllo'),
(61, 'Visita endocrinologica e diabetologica'),
(62, 'Visita endocrinologica e diabetologica di controllo'),
(63, 'Visita endocrinologica pediatrica'),
(64, 'Visita epatologica'),
(65, 'Visita epatologica di controllo'),
(66, 'Visita fisiatrica'),
(67, 'Visita fisiatrica di controllo'),
(68, 'Visita gastroenterologica'),
(69, 'Visita gastroenterologica di controllo'),
(70, 'Visita genetica di controllo'),
(71, 'Visita genetica medica'),
(72, 'Visita geriatrica'),
(73, 'Visita geriatrica di controllo'),
(74, 'Visita ginecologica'),
(75, 'Visita ginecologica di controllo'),
(76, 'Visita immunologica'),
(77, 'Visita infettivologica o delle malattie tropicali'),
(78, 'Visita infettivologica o delle malattie tropicali di controllo'),
(79, 'Visita medicina generale / assistenza primaria / servizio di continuità assistenziale'),
(80, 'Visita del medico di base'),
(81, 'Visita medico-sportiva'),
(82, 'Visita medico-sportiva di controllo'),
(83, 'Visita multidisciplinare'),
(84, 'Visita multidisciplinare per cure palliative'),
(85, 'Visita nefrologica'),
(86, 'Visita nefrologica di controllo'),
(87, 'Visita neurochirurgica'),
(88, 'Visita neurochirurgica di controllo'),
(89, 'Visita neurochirurgica pediatrica'),
(90, 'Visita neurologica'),
(91, 'Visita neurologica di controllo'),
(92, 'Visita neurologica pediatrica'),
(93, 'Visita neuropsichiatrica infantile'),
(94, 'Visita neuropsichiatrica infantile di controllo'),
(95, 'Visita oculistica'),
(96, 'Visita oculistica di controllo'),
(97, 'Visita oculistica pediatrica'),
(98, 'Visita oncoematologica'),
(99, 'Visita oncoematologica di controllo'),
(100, 'Visita oncoematologica pediatrica'),
(101, 'Visita oncoematologica pediatrica di controllo'),
(102, 'Visita oncologica'),
(103, 'Visita oncologica di controllo'),
(104, 'Visita ortopedica'),
(105, 'Visita ortopedica di controllo'),
(106, 'Visita ostetrica'),
(107, 'Visita ostetrica di controllo'),
(108, 'Visita otorinolaringoiatrica'),
(109, 'Visita otorinolaringoiatrica di controllo'),
(110, 'Visita pediatrica'),
(111, 'Visita per cure palliative di controllo'),
(112, 'Visita pneumologica'),
(113, 'Visita pneumologica di controllo'),
(114, 'Visita procreazione (fecondazione) assistita'),
(115, 'Visita proctologica'),
(116, 'Visita proctologica di controllo'),
(117, 'Visita psichiatrica'),
(118, 'Visita psichiatrica di controllo'),
(119, 'Visita psicodiagnostica'),
(120, 'Visita radioterapica di controllo'),
(121, 'Visita reumatologica'),
(122, 'Visita reumatologica di controllo'),
(123, 'Visita senologica'),
(124, 'Visita senologica di controllo'),
(125, 'Visita urologica pediatrica'),
(126, 'Visita urologica pediatrica di controllo'),
(127, 'Visita urologica/andrologica'),
(128, 'Visita urologica/andrologica di controllo'),
(129, 'Visita vulnologica'),
(130, 'Visita vulnologica di controllo');

-- --------------------------------------------------------

--
-- Struttura della tabella `VisitePrescritte`
--
-- Creazione: Set 28, 2019 alle 14:35
-- Ultimo aggiornamento: Ott 10, 2019 alle 19:42
--

DROP TABLE IF EXISTS `VisitePrescritte`;
CREATE TABLE `VisitePrescritte` (
  `idDottore` int(11) NOT NULL,
  `idPaziente` int(11) NOT NULL,
  `dataOraPrescrizione` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idVisita` int(11) NOT NULL DEFAULT '80',
  `costo` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '€',
  `idMedico` int(11) DEFAULT NULL,
  `anamnesi` text,
  `dataOraEvasione` timestamp(3) NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELAZIONI PER TABELLA `VisitePrescritte`:
--   `idDottore`
--       `Utenti` -> `id`
--   `idMedico`
--       `Utenti` -> `id`
--   `idPaziente`
--       `Utenti` -> `id`
--   `idSpecialista`
--       `Utenti` -> `id`
--   `idVisita`
--       `Visite` -> `id`
--

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `VisitePrescritteView`
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `VisitePrescritteView`;
CREATE TABLE `VisitePrescritteView` (
`idDottore` int(11)
,`nomeDottore` varchar(100)
,`cognomeDottore` varchar(100)
,`nomeProvinciaDottore` varchar(60)
,`siglaProvinciaDottore` varchar(5)
,`DOCFotoDottore` timestamp(3)
,`extensionFotoDottore` varchar(10)
,`idPaziente` int(11)
,`nomePaziente` varchar(100)
,`cognomePaziente` varchar(100)
,`cfPaziente` varchar(17)
,`nomeProvinciaPaziente` varchar(60)
,`siglaProvinciaPaziente` varchar(5)
,`DOCFotoPaziente` timestamp(3)
,`extensionFotoPaziente` varchar(10)
,`dataOraPrescrizione` timestamp(3)
,`idVisita` int(11)
,`nomeVisita` varchar(200)
,`costo` float(10,2)
,`idMedico` int(11)
,`nomeErogatore` varchar(100)
,`cognomeErogatore` varchar(100)
,`nomeProvinciaErogatore` varchar(60)
,`siglaProvinciaErogatore` varchar(5)
,`DOCFotoErogatore` timestamp(3)
,`extensionFotoErogatore` varchar(10)
,`ruoloErogatore` int(3)
,`anamnesi` text
,`dataOraEvasione` timestamp(3)
);

-- --------------------------------------------------------

--
-- Struttura per vista `EsamiPrescrittiView`
--
DROP TABLE IF EXISTS `EsamiPrescrittiView`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `EsamiPrescrittiView`  AS  select sql_cache `esame`.`idDottore` AS `idDottore`,`dottore`.`nome` AS `nomeDottore`,`dottore`.`cognome` AS `cognomeDottore`,`dottore`.`nomeProvincia` AS `nomeProvinciaDottore`,`dottore`.`siglaProvincia` AS `siglaProvinciaDottore`,`dottore`.`dataOraCreazioneFoto` AS `DOCFotoDottore`,`dottore`.`extensionFoto` AS `extensionFotoDottore`,`esame`.`idPaziente` AS `idPaziente`,`paziente`.`nome` AS `nomePaziente`,`paziente`.`cognome` AS `cognomePaziente`,`paziente`.`cf` AS `cfPaziente`,`paziente`.`nomeProvincia` AS `nomeProvinciaPaziente`,`paziente`.`siglaProvincia` AS `siglaProvinciaPaziente`,`paziente`.`dataOraCreazioneFoto` AS `DOCFotoPaziente`,`paziente`.`extensionFoto` AS `extensionFotoPaziente`,`esame`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`Esami`.`id` AS `idEsame`,`Esami`.`nome` AS `nomeEsame`,`esame`.`costo` AS `costo`,`esame`.`idSsp` AS `idSsp`,`erogatore`.`nome` AS `nomeErogatore`,`erogatore`.`nomeProvincia` AS `nomeProvinciaErogatore`,`erogatore`.`siglaProvincia` AS `siglaProvinciaErogatore`,`erogatore`.`dataOraCreazioneFoto` AS `DOCFotoErogatore`,`erogatore`.`extensionFoto` AS `extensionFotoErogatore`,`erogatore`.`ruolo` AS `ruoloErogatore`,`esame`.`risultati` AS `risultati`,`esame`.`dataOraEvasione` AS `dataOraEvasione` from ((((`EsamiPrescritti` `esame` join `Esami` on((`Esami`.`id` = `esame`.`idEsame`))) join `UtenteView` `dottore` on((`esame`.`idDottore` = `dottore`.`id`))) join `UtenteView` `paziente` on((`esame`.`idPaziente` = `paziente`.`id`))) left join `UtenteView` `erogatore` on((`esame`.`idSsp` = `erogatore`.`id`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `RicettePrescritteView`
--
DROP TABLE IF EXISTS `RicettePrescritteView`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `RicettePrescritteView`  AS  select sql_cache `ricetta`.`idDottore` AS `idDottore`,`dottore`.`nome` AS `nomeDottore`,`dottore`.`cognome` AS `cognomeDottore`,`dottore`.`nomeProvincia` AS `nomeProvinciaDottore`,`dottore`.`siglaProvincia` AS `siglaProvinciaDottore`,`dottore`.`dataOraCreazioneFoto` AS `DOCFotoDottore`,`dottore`.`extensionFoto` AS `extensionFotoDottore`,`ricetta`.`idPaziente` AS `idPaziente`,`paziente`.`nome` AS `nomePaziente`,`paziente`.`cognome` AS `cognomePaziente`,`paziente`.`cf` AS `cfPaziente`,`paziente`.`nomeProvincia` AS `nomeProvinciaPaziente`,`paziente`.`siglaProvincia` AS `siglaProvinciaPaziente`,`paziente`.`dataOraCreazioneFoto` AS `DOCFotoPaziente`,`paziente`.`extensionFoto` AS `extensionFotoPaziente`,`ricetta`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`ricetta`.`costo` AS `costo`,`ricetta`.`idFarmacia` AS `idFarmacia`,`erogatore`.`nome` AS `nomeErogatore`,`erogatore`.`nomeProvincia` AS `nomeProvinciaErogatore`,`erogatore`.`siglaProvincia` AS `siglaProvinciaErogatore`,`erogatore`.`dataOraCreazioneFoto` AS `DOCFotoErogatore`,`erogatore`.`extensionFoto` AS `extensionFotoErogatore`,`erogatore`.`ruolo` AS `ruoloErogatore`,`ricetta`.`descrizione` AS `descrizione`,`ricetta`.`dataOraEvasione` AS `dataOraEvasione` from (((`RicettePrescritte` `ricetta` join `UtenteView` `dottore` on((`ricetta`.`idDottore` = `dottore`.`id`))) join `UtenteView` `paziente` on((`ricetta`.`idPaziente` = `paziente`.`id`))) left join `UtenteView` `erogatore` on((`ricetta`.`idFarmacia` = `erogatore`.`id`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `Ticket`
--
DROP TABLE IF EXISTS `Ticket`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `Ticket`  AS  select sql_cache `EsamiPrescritti`.`idDottore` AS `idDottore`,`EsamiPrescritti`.`idPaziente` AS `idPaziente`,`EsamiPrescritti`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`Esami`.`nome` AS `descrizione`,`Utenti`.`ruolo` AS `ruoloErogatore`,`EsamiPrescritti`.`idSsp` AS `idErogatore`,`EsamiPrescritti`.`costo` AS `costo`,`EsamiPrescritti`.`dataOraEvasione` AS `dataOraEvasione` from ((`EsamiPrescritti` join `Esami` on((`Esami`.`id` = `EsamiPrescritti`.`idEsame`))) join `Utenti` on((`EsamiPrescritti`.`idSsp` = `Utenti`.`id`))) where ((`EsamiPrescritti`.`dataOraEvasione` is not null) and (`EsamiPrescritti`.`costo` > 0)) union select `RicettePrescritte`.`idDottore` AS `idDottore`,`RicettePrescritte`.`idPaziente` AS `idPaziente`,`RicettePrescritte`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`RicettePrescritte`.`descrizione` AS `descrizione`,`Utenti`.`ruolo` AS `ruoloErogatore`,`RicettePrescritte`.`idFarmacia` AS `idErogatore`,`RicettePrescritte`.`costo` AS `costo`,`RicettePrescritte`.`dataOraEvasione` AS `dataOraEvasione` from (`RicettePrescritte` join `Utenti` on((`RicettePrescritte`.`idFarmacia` = `Utenti`.`id`))) where ((`RicettePrescritte`.`dataOraEvasione` is not null) and (`RicettePrescritte`.`costo` > 0)) union select `VisitePrescritte`.`idDottore` AS `idDottore`,`VisitePrescritte`.`idPaziente` AS `idPaziente`,`VisitePrescritte`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`Visite`.`nome` AS `descrizione`,`Utenti`.`ruolo` AS `ruoloErogatore`,`VisitePrescritte`.`idMedico` AS `idErogatore`,`VisitePrescritte`.`costo` AS `costo`,`VisitePrescritte`.`dataOraEvasione` AS `dataOraEvasione` from ((`VisitePrescritte` join `Visite` on((`Visite`.`id` = `VisitePrescritte`.`idVisita`))) join `Utenti` on((`Utenti`.`id` = `VisitePrescritte`.`idMedico`))) where ((`VisitePrescritte`.`dataOraEvasione` is not null) and (`VisitePrescritte`.`costo` > 0)) ;

-- --------------------------------------------------------

--
-- Struttura per vista `TicketView`
--
DROP TABLE IF EXISTS `TicketView`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `TicketView`  AS  select sql_cache `EsamiPrescrittiView`.`idDottore` AS `idDottore`,`EsamiPrescrittiView`.`nomeDottore` AS `nomeDottore`,`EsamiPrescrittiView`.`cognomeDottore` AS `cognomeDottore`,`EsamiPrescrittiView`.`nomeProvinciaDottore` AS `nomeProvinciaDottore`,`EsamiPrescrittiView`.`siglaProvinciaDottore` AS `siglaProvinciaDottore`,`EsamiPrescrittiView`.`DOCFotoDottore` AS `DOCFotoDottore`,`EsamiPrescrittiView`.`extensionFotoDottore` AS `extensionFotoDottore`,`EsamiPrescrittiView`.`idPaziente` AS `idPaziente`,`EsamiPrescrittiView`.`nomePaziente` AS `nomePaziente`,`EsamiPrescrittiView`.`cognomePaziente` AS `cognomePaziente`,`EsamiPrescrittiView`.`cfPaziente` AS `cfPaziente`,`EsamiPrescrittiView`.`nomeProvinciaPaziente` AS `nomeProvinciaPaziente`,`EsamiPrescrittiView`.`siglaProvinciaPaziente` AS `siglaProvinciaPaziente`,`EsamiPrescrittiView`.`DOCFotoPaziente` AS `DOCFotoPaziente`,`EsamiPrescrittiView`.`extensionFotoPaziente` AS `extensionFotoPaziente`,`EsamiPrescrittiView`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`EsamiPrescrittiView`.`costo` AS `costo`,`EsamiPrescrittiView`.`idSsp` AS `idErogatore`,`EsamiPrescrittiView`.`nomeErogatore` AS `nomeErogatore`,NULL AS `cognomeErogatore`,`EsamiPrescrittiView`.`nomeProvinciaErogatore` AS `nomeProvinciaErogatore`,`EsamiPrescrittiView`.`siglaProvinciaErogatore` AS `siglaProvinciaErogatore`,`EsamiPrescrittiView`.`DOCFotoErogatore` AS `DOCFotoErogatore`,`EsamiPrescrittiView`.`extensionFotoErogatore` AS `extensionFotoErogatore`,`EsamiPrescrittiView`.`ruoloErogatore` AS `ruoloErogatore`,`EsamiPrescrittiView`.`nomeEsame` AS `descrizione`,`EsamiPrescrittiView`.`dataOraEvasione` AS `dataOraEvasione` from `EsamiPrescrittiView` where ((`EsamiPrescrittiView`.`dataOraEvasione` is not null) and (`EsamiPrescrittiView`.`costo` > 0)) union select `RicettePrescritteView`.`idDottore` AS `idDottore`,`RicettePrescritteView`.`nomeDottore` AS `nomeDottore`,`RicettePrescritteView`.`cognomeDottore` AS `cognomeDottore`,`RicettePrescritteView`.`nomeProvinciaDottore` AS `nomeProvinciaDottore`,`RicettePrescritteView`.`siglaProvinciaDottore` AS `siglaProvinciaDottore`,`RicettePrescritteView`.`DOCFotoDottore` AS `DOCFotoDottore`,`RicettePrescritteView`.`extensionFotoDottore` AS `extensionFotoDottore`,`RicettePrescritteView`.`idPaziente` AS `idPaziente`,`RicettePrescritteView`.`nomePaziente` AS `nomePaziente`,`RicettePrescritteView`.`cognomePaziente` AS `cognomePaziente`,`RicettePrescritteView`.`cfPaziente` AS `cfPaziente`,`RicettePrescritteView`.`nomeProvinciaPaziente` AS `nomeProvinciaPaziente`,`RicettePrescritteView`.`siglaProvinciaPaziente` AS `siglaProvinciaPaziente`,`RicettePrescritteView`.`DOCFotoPaziente` AS `DOCFotoPaziente`,`RicettePrescritteView`.`extensionFotoPaziente` AS `extensionFotoPaziente`,`RicettePrescritteView`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`RicettePrescritteView`.`costo` AS `costo`,`RicettePrescritteView`.`idFarmacia` AS `idErogatore`,`RicettePrescritteView`.`nomeErogatore` AS `nomeErogatore`,NULL AS `cognomeErogatore`,`RicettePrescritteView`.`nomeProvinciaErogatore` AS `nomeProvinciaErogatore`,`RicettePrescritteView`.`siglaProvinciaErogatore` AS `siglaProvinciaErogatore`,`RicettePrescritteView`.`DOCFotoErogatore` AS `DOCFotoErogatore`,`RicettePrescritteView`.`extensionFotoErogatore` AS `extensionFotoErogatore`,`RicettePrescritteView`.`ruoloErogatore` AS `ruoloErogatore`,`RicettePrescritteView`.`descrizione` AS `descrizione`,`RicettePrescritteView`.`dataOraEvasione` AS `dataOraEvasione` from `RicettePrescritteView` where ((`RicettePrescritteView`.`dataOraEvasione` is not null) and (`RicettePrescritteView`.`costo` > 0)) union select `VisitePrescritteView`.`idDottore` AS `idDottore`,`VisitePrescritteView`.`nomeDottore` AS `nomeDottore`,`VisitePrescritteView`.`cognomeDottore` AS `cognomeDottore`,`VisitePrescritteView`.`nomeProvinciaDottore` AS `nomeProvinciaDottore`,`VisitePrescritteView`.`siglaProvinciaDottore` AS `siglaProvinciaDottore`,`VisitePrescritteView`.`DOCFotoDottore` AS `DOCFotoDottore`,`VisitePrescritteView`.`extensionFotoDottore` AS `extensionFotoDottore`,`VisitePrescritteView`.`idPaziente` AS `idPaziente`,`VisitePrescritteView`.`nomePaziente` AS `nomePaziente`,`VisitePrescritteView`.`cognomePaziente` AS `cognomePaziente`,`VisitePrescritteView`.`cfPaziente` AS `cfPaziente`,`VisitePrescritteView`.`nomeProvinciaPaziente` AS `nomeProvinciaPaziente`,`VisitePrescritteView`.`siglaProvinciaPaziente` AS `siglaProvinciaPaziente`,`VisitePrescritteView`.`DOCFotoPaziente` AS `DOCFotoPaziente`,`VisitePrescritteView`.`extensionFotoPaziente` AS `extensionFotoPaziente`,`VisitePrescritteView`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`VisitePrescritteView`.`costo` AS `costo`,`VisitePrescritteView`.`idMedico` AS `idErogatore`,`VisitePrescritteView`.`nomeErogatore` AS `nomeErogatore`,`VisitePrescritteView`.`cognomeErogatore` AS `cognomeErogatore`,`VisitePrescritteView`.`nomeProvinciaErogatore` AS `nomeProvinciaErogatore`,`VisitePrescritteView`.`siglaProvinciaErogatore` AS `siglaProvinciaErogatore`,`VisitePrescritteView`.`DOCFotoErogatore` AS `DOCFotoErogatore`,`VisitePrescritteView`.`extensionFotoErogatore` AS `extensionFotoErogatore`,`VisitePrescritteView`.`ruoloErogatore` AS `ruoloErogatore`,`VisitePrescritteView`.`nomeVisita` AS `descrizione`,`VisitePrescritteView`.`dataOraEvasione` AS `dataOraEvasione` from `VisitePrescritteView` where ((`VisitePrescritteView`.`dataOraEvasione` is not null) and (`VisitePrescritteView`.`costo` > 0)) ;

-- --------------------------------------------------------

--
-- Struttura per vista `UltimaFoto`
--
DROP TABLE IF EXISTS `UltimaFoto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `UltimaFoto`  AS  select sql_cache `Foto`.`idUtente` AS `idUtente`,max(`Foto`.`dataOraCreazione`) AS `dataOraCreazione`,`Foto`.`extension` AS `extension` from `Foto` group by `Foto`.`idUtente` ;

-- --------------------------------------------------------

--
-- Struttura per vista `UtenteView`
--
DROP TABLE IF EXISTS `UtenteView`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `UtenteView`  AS  select sql_cache `Utenti`.`id` AS `id`,`Utenti`.`nome` AS `nome`,`Utenti`.`cognome` AS `cognome`,`Utenti`.`email` AS `email`,`Utenti`.`password` AS `password`,`Utenti`.`provincia` AS `provincia`,`Province`.`nome` AS `nomeProvincia`,`Province`.`sigla` AS `siglaProvincia`,`Utenti`.`sesso` AS `sesso`,`Utenti`.`cf` AS `cf`,`Utenti`.`dataNascita` AS `dataNascita`,`Utenti`.`comuneNascita` AS `comuneNascita`,`Utenti`.`ruolo` AS `ruolo`,`UltimaFoto`.`dataOraCreazione` AS `dataOraCreazioneFoto`,`UltimaFoto`.`extension` AS `extensionFoto`,`Utenti`.`idMedicoBase` AS `idMedicoBase` from ((`Utenti` join `Province` on((`Province`.`id` = `Utenti`.`provincia`))) left join `UltimaFoto` on((`UltimaFoto`.`idUtente` = `Utenti`.`id`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `VisitePrescritteView`
--
DROP TABLE IF EXISTS `VisitePrescritteView`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `VisitePrescritteView`  AS  select sql_cache `visita`.`idDottore` AS `idDottore`,`dottore`.`nome` AS `nomeDottore`,`dottore`.`cognome` AS `cognomeDottore`,`dottore`.`nomeProvincia` AS `nomeProvinciaDottore`,`dottore`.`siglaProvincia` AS `siglaProvinciaDottore`,`dottore`.`dataOraCreazioneFoto` AS `DOCFotoDottore`,`dottore`.`extensionFoto` AS `extensionFotoDottore`,`visita`.`idPaziente` AS `idPaziente`,`paziente`.`nome` AS `nomePaziente`,`paziente`.`cognome` AS `cognomePaziente`,`paziente`.`cf` AS `cfPaziente`,`paziente`.`nomeProvincia` AS `nomeProvinciaPaziente`,`paziente`.`siglaProvincia` AS `siglaProvinciaPaziente`,`paziente`.`dataOraCreazioneFoto` AS `DOCFotoPaziente`,`paziente`.`extensionFoto` AS `extensionFotoPaziente`,`visita`.`dataOraPrescrizione` AS `dataOraPrescrizione`,`Visite`.`id` AS `idVisita`,`Visite`.`nome` AS `nomeVisita`,`visita`.`costo` AS `costo`,`visita`.`idMedico` AS `idMedico`,`erogatore`.`nome` AS `nomeErogatore`,`erogatore`.`cognome` AS `cognomeErogatore`,`erogatore`.`nomeProvincia` AS `nomeProvinciaErogatore`,`erogatore`.`siglaProvincia` AS `siglaProvinciaErogatore`,`erogatore`.`dataOraCreazioneFoto` AS `DOCFotoErogatore`,`erogatore`.`extensionFoto` AS `extensionFotoErogatore`,`erogatore`.`ruolo` AS `ruoloErogatore`,`visita`.`anamnesi` AS `anamnesi`,`visita`.`dataOraEvasione` AS `dataOraEvasione` from ((((`VisitePrescritte` `visita` join `Visite` on((`Visite`.`id` = `visita`.`idVisita`))) join `UtenteView` `dottore` on((`visita`.`idDottore` = `dottore`.`id`))) join `UtenteView` `paziente` on((`visita`.`idPaziente` = `paziente`.`id`))) left join `UtenteView` `erogatore` on((`visita`.`idMedico` = `erogatore`.`id`))) ;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `Esami`
--
ALTER TABLE `Esami`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `Esami` ADD FULLTEXT KEY `nome` (`nome`);

--
-- Indici per le tabelle `EsamiPrescritti`
--
ALTER TABLE `EsamiPrescritti`
  ADD PRIMARY KEY (`idDottore`,`idPaziente`,`dataOraPrescrizione`),
  ADD KEY `idSsp` (`idSsp`);

--
-- Indici per le tabelle `Foto`
--
ALTER TABLE `Foto`
  ADD PRIMARY KEY (`idUtente`,`dataOraCreazione`);

--
-- Indici per le tabelle `Province`
--
ALTER TABLE `Province`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Province_sigla_uindex` (`sigla`),
  ADD KEY `Nome` (`nome`) USING BTREE;

--
-- Indici per le tabelle `RecuperoPassword`
--
ALTER TABLE `RecuperoPassword`
  ADD PRIMARY KEY (`idUtente`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indici per le tabelle `RicettePrescritte`
--
ALTER TABLE `RicettePrescritte`
  ADD PRIMARY KEY (`idDottore`,`idPaziente`,`dataOraPrescrizione`);

--
-- Indici per le tabelle `Ruoli`
--
ALTER TABLE `Ruoli`
  ADD PRIMARY KEY (`idRuolo`);

--
-- Indici per le tabelle `Utenti`
--
ALTER TABLE `Utenti`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Email` (`email`),
  ADD UNIQUE KEY `CF` (`cf`),
  ADD KEY `provincia` (`provincia`);
ALTER TABLE `Utenti` ADD FULLTEXT KEY `Nome` (`nome`);
ALTER TABLE `Utenti` ADD FULLTEXT KEY `Cognome` (`cognome`);

--
-- Indici per le tabelle `Visite`
--
ALTER TABLE `Visite`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `Visite` ADD FULLTEXT KEY `nome` (`nome`);

--
-- Indici per le tabelle `VisitePrescritte`
--
ALTER TABLE `VisitePrescritte`
  ADD PRIMARY KEY (`idDottore`,`idPaziente`,`dataOraPrescrizione`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `Esami`
--
ALTER TABLE `Esami`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2038;

--
-- AUTO_INCREMENT per la tabella `Province`
--
ALTER TABLE `Province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT per la tabella `Utenti`
--
ALTER TABLE `Utenti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `Visite`
--
ALTER TABLE `Visite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
